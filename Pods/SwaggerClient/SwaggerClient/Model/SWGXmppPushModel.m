#import "SWGXmppPushModel.h"

@implementation SWGXmppPushModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"to": @"to", @"body": @"body", @"group": @"group", @"messageId": @"messageId", @"messageTitle": @"messageTitle", @"ownerId": @"ownerId", @"xmlMessage": @"xmlMessage" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"group", @"messageId", @"messageTitle", @"ownerId", @"xmlMessage"];
  return [optionalProperties containsObject:propertyName];
}

@end
