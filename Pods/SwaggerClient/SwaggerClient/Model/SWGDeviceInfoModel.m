#import "SWGDeviceInfoModel.h"

@implementation SWGDeviceInfoModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"longitude": @"longitude", @"latitude": @"latitude", @"osType": @"osType", @"deviceToken": @"deviceToken", @"deviceId": @"deviceId" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"longitude", @"latitude", @"osType", @"deviceToken", @"deviceId"];
  return [optionalProperties containsObject:propertyName];
}

@end
