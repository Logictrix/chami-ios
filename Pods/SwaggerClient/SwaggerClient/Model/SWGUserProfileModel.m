#import "SWGUserProfileModel.h"

@implementation SWGUserProfileModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"chamiType": @"chamiType", @"isTutor": @"isTutor", @"_id": @"id", @"firstName": @"firstName", @"lastName": @"lastName", @"email": @"email", @"imageUrl": @"imageUrl", @"phoneNumber": @"phoneNumber" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"chamiType", @"isTutor", @"_id", @"firstName", @"lastName", @"email", @"imageUrl", @"phoneNumber"];
  return [optionalProperties containsObject:propertyName];
}

@end
