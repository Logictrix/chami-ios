#import "SWGSessionResultModel.h"

@implementation SWGSessionResultModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"partnerId": @"partnerId", @"partnerFullName": @"partnerFullName", @"startTime": @"startTime", @"endTime": @"endTime", @"cost": @"cost", @"transactionId": @"transactionId" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"partnerId", @"partnerFullName", @"startTime", @"endTime", @"cost", @"transactionId"];
  return [optionalProperties containsObject:propertyName];
}

@end
