#import "SWGProfileModel.h"

@implementation SWGProfileModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"firstName": @"firstName", @"lastName": @"lastName", @"email": @"email", @"imageUrl": @"imageUrl", @"nationality": @"nationality", @"gender": @"gender", @"phoneNumber": @"phoneNumber", @"birthDate": @"birthDate", @"introVideoUrl": @"introVideoUrl", @"isOnline": @"isOnline", @"nativeLanguageId": @"nativeLanguageId", @"about": @"about", @"studentStripeKey": @"studentStripeKey", @"tutorStripeKey": @"tutorStripeKey" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"firstName", @"lastName", @"email", @"imageUrl", @"nationality", @"gender", @"phoneNumber", @"birthDate", @"introVideoUrl", @"isOnline", @"nativeLanguageId", @"about", @"studentStripeKey", @"tutorStripeKey"];
  return [optionalProperties containsObject:propertyName];
}

@end
