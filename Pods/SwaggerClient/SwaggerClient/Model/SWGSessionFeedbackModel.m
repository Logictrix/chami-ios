#import "SWGSessionFeedbackModel.h"

@implementation SWGSessionFeedbackModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"speaking": @"speaking", @"listening": @"listening", @"reading": @"reading", @"writing": @"writing", @"pronunciation": @"pronunciation", @"attitudeToLearning": @"attitudeToLearning", @"ieltsScore": @"ieltsScore", @"rating": @"rating", @"feedback": @"feedback" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"speaking", @"listening", @"reading", @"writing", @"pronunciation", @"attitudeToLearning", @"ieltsScore", @"rating", @"feedback"];
  return [optionalProperties containsObject:propertyName];
}

@end
