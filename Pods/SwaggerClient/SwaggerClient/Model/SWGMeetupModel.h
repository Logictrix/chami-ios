#import <Foundation/Foundation.h>
#import "SWGObject.h"

/**
* chami
* No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
*
* OpenAPI spec version: v1
* 
*
* NOTE: This class is auto generated by the swagger code generator program.
* https://github.com/swagger-api/swagger-codegen.git
* Do not edit the class manually.
*/




@protocol SWGMeetupModel
@end

@interface SWGMeetupModel : SWGObject


@property(nonatomic) NSNumber* _id;

@property(nonatomic) NSString* fullName;

@property(nonatomic) NSString* nativeLanguage;

@property(nonatomic) NSString* imageUrl;

@end
