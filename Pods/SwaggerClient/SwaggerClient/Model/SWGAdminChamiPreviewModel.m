#import "SWGAdminChamiPreviewModel.h"

@implementation SWGAdminChamiPreviewModel

- (instancetype)init {
  self = [super init];
  if (self) {
    // initialize property's default value, if any
    
  }
  return self;
}


/**
 * Maps json key to property name.
 * This method is used by `JSONModel`.
 */
+ (JSONKeyMapper *)keyMapper {
  return [[JSONKeyMapper alloc] initWithModelToJSONDictionary:@{ @"_id": @"id", @"firstName": @"firstName", @"lastName": @"lastName", @"fullName": @"fullName", @"updateDate": @"updateDate", @"certificateStatus": @"certificateStatus", @"certificateUrl": @"certificateUrl" }];
}

/**
 * Indicates whether the property with the given name is optional.
 * If `propertyName` is optional, then return `YES`, otherwise return `NO`.
 * This method is used by `JSONModel`.
 */
+ (BOOL)propertyIsOptional:(NSString *)propertyName {

  NSArray *optionalProperties = @[@"_id", @"firstName", @"lastName", @"fullName", @"updateDate", @"certificateStatus", @"certificateUrl"];
  return [optionalProperties containsObject:propertyName];
}

@end
