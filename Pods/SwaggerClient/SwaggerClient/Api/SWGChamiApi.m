#import "SWGChamiApi.h"
#import "SWGQueryParamCollection.h"
#import "SWGApiClient.h"
#import "SWGResponseWrapperChamiProfileModel_.h"
#import "SWGResponseWrapperListChamiModel_.h"


@interface SWGChamiApi ()

@property (nonatomic, strong, readwrite) NSMutableDictionary *mutableDefaultHeaders;

@end

@implementation SWGChamiApi

NSString* kSWGChamiApiErrorDomain = @"SWGChamiApiErrorDomain";
NSInteger kSWGChamiApiMissingParamErrorCode = 234513;

@synthesize apiClient = _apiClient;

#pragma mark - Initialize methods

- (instancetype) init {
    return [self initWithApiClient:[SWGApiClient sharedClient]];
}


-(instancetype) initWithApiClient:(SWGApiClient *)apiClient {
    self = [super init];
    if (self) {
        _apiClient = apiClient;
        _mutableDefaultHeaders = [NSMutableDictionary dictionary];
    }
    return self;
}

#pragma mark -

-(NSString*) defaultHeaderForKey:(NSString*)key {
    return self.mutableDefaultHeaders[key];
}

-(void) setDefaultHeaderValue:(NSString*) value forKey:(NSString*)key {
    [self.mutableDefaultHeaders setValue:value forKey:key];
}

-(NSDictionary *)defaultHeaders {
    return self.mutableDefaultHeaders;
}

#pragma mark - Api Methods

///
/// get users fo map
/// 
///  @param modelChamiType  
///
///  @param modelLongitude  
///
///  @param modelLatitude  
///
///  @param authorization bearer access token 
///
///  @param modelLanguageId  (optional)
///
///  @returns SWGResponseWrapperListChamiModel_*
///
-(NSURLSessionTask*) chamiGetWithModelChamiType: (NSString*) modelChamiType
    modelLongitude: (NSNumber*) modelLongitude
    modelLatitude: (NSNumber*) modelLatitude
    authorization: (NSString*) authorization
    modelLanguageId: (NSNumber*) modelLanguageId
    completionHandler: (void (^)(SWGResponseWrapperListChamiModel_* output, NSError* error)) handler {
    // verify the required parameter 'modelChamiType' is set
    if (modelChamiType == nil) {
        NSParameterAssert(modelChamiType);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"modelChamiType"] };
            NSError* error = [NSError errorWithDomain:kSWGChamiApiErrorDomain code:kSWGChamiApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'modelLongitude' is set
    if (modelLongitude == nil) {
        NSParameterAssert(modelLongitude);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"modelLongitude"] };
            NSError* error = [NSError errorWithDomain:kSWGChamiApiErrorDomain code:kSWGChamiApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'modelLatitude' is set
    if (modelLatitude == nil) {
        NSParameterAssert(modelLatitude);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"modelLatitude"] };
            NSError* error = [NSError errorWithDomain:kSWGChamiApiErrorDomain code:kSWGChamiApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'authorization' is set
    if (authorization == nil) {
        NSParameterAssert(authorization);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"authorization"] };
            NSError* error = [NSError errorWithDomain:kSWGChamiApiErrorDomain code:kSWGChamiApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/api/chami"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    if (modelLanguageId != nil) {
        queryParams[@"model.languageId"] = modelLanguageId;
    }
    if (modelChamiType != nil) {
        queryParams[@"model.chamiType"] = modelChamiType;
    }
    if (modelLongitude != nil) {
        queryParams[@"model.longitude"] = modelLongitude;
    }
    if (modelLatitude != nil) {
        queryParams[@"model.latitude"] = modelLatitude;
    }
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (authorization != nil) {
        headerParams[@"Authorization"] = authorization;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"text/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGResponseWrapperListChamiModel_*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGResponseWrapperListChamiModel_*)data, error);
                                }
                            }];
}

///
/// Get chami(tutor) profile you want to meetup
/// 
///  @param _id  
///
///  @param authorization bearer access token 
///
///  @returns SWGResponseWrapperChamiProfileModel_*
///
-(NSURLSessionTask*) chamiGet_1WithId: (NSNumber*) _id
    authorization: (NSString*) authorization
    completionHandler: (void (^)(SWGResponseWrapperChamiProfileModel_* output, NSError* error)) handler {
    // verify the required parameter '_id' is set
    if (_id == nil) {
        NSParameterAssert(_id);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"_id"] };
            NSError* error = [NSError errorWithDomain:kSWGChamiApiErrorDomain code:kSWGChamiApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    // verify the required parameter 'authorization' is set
    if (authorization == nil) {
        NSParameterAssert(authorization);
        if(handler) {
            NSDictionary * userInfo = @{NSLocalizedDescriptionKey : [NSString stringWithFormat:NSLocalizedString(@"Missing required parameter '%@'", nil),@"authorization"] };
            NSError* error = [NSError errorWithDomain:kSWGChamiApiErrorDomain code:kSWGChamiApiMissingParamErrorCode userInfo:userInfo];
            handler(nil, error);
        }
        return nil;
    }

    NSMutableString* resourcePath = [NSMutableString stringWithFormat:@"/api/chami/{id}"];

    // remove format in URL if needed
    [resourcePath replaceOccurrencesOfString:@".{format}" withString:@".json" options:0 range:NSMakeRange(0,resourcePath.length)];

    NSMutableDictionary *pathParams = [[NSMutableDictionary alloc] init];
    if (_id != nil) {
        pathParams[@"id"] = _id;
    }

    NSMutableDictionary* queryParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary* headerParams = [NSMutableDictionary dictionaryWithDictionary:self.apiClient.configuration.defaultHeaders];
    [headerParams addEntriesFromDictionary:self.defaultHeaders];
    if (authorization != nil) {
        headerParams[@"Authorization"] = authorization;
    }
    // HTTP header `Accept`
    NSString *acceptHeader = [self.apiClient.sanitizer selectHeaderAccept:@[@"application/json", @"text/json"]];
    if(acceptHeader.length > 0) {
        headerParams[@"Accept"] = acceptHeader;
    }

    // response content type
    NSString *responseContentType = [[acceptHeader componentsSeparatedByString:@", "] firstObject] ?: @"";

    // request content type
    NSString *requestContentType = [self.apiClient.sanitizer selectHeaderContentType:@[]];

    // Authentication setting
    NSArray *authSettings = @[];

    id bodyParam = nil;
    NSMutableDictionary *formParams = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *localVarFiles = [[NSMutableDictionary alloc] init];

    return [self.apiClient requestWithPath: resourcePath
                                    method: @"GET"
                                pathParams: pathParams
                               queryParams: queryParams
                                formParams: formParams
                                     files: localVarFiles
                                      body: bodyParam
                              headerParams: headerParams
                              authSettings: authSettings
                        requestContentType: requestContentType
                       responseContentType: responseContentType
                              responseType: @"SWGResponseWrapperChamiProfileModel_*"
                           completionBlock: ^(id data, NSError *error) {
                                if(handler) {
                                    handler((SWGResponseWrapperChamiProfileModel_*)data, error);
                                }
                            }];
}



@end
