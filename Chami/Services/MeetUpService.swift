//
//  MeetUpService.swift
//  Chami
//
//  Created by Pavel Korinenko on 3/7/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient


typealias MeetUpServiceSuccess = (MeetUp) -> Void
typealias MeetUpServiceFailure = (Error) -> Void

protocol MeetUpServiceProtocol: NSObjectProtocol {
    
    func createMeetUp(withTutor tutorId: IdType, latitude: Double, longitude: Double, languageId: IdType, stripeChargeTokenString: String, success: @escaping MeetUpServiceSuccess, failure: @escaping MeetUpServiceFailure)
    
    func changeMeetUpStatus(meetupId: IdType, toStatus: String, success: @escaping MeetUpServiceSuccess, failure: @escaping MeetUpServiceFailure)

}
class MeetUpService: NSObject, MeetUpServiceProtocol {
    
    var meetupApi: SWGMeetupApi {
        get {
            return SWGMeetupApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    func createMeetUp(withTutor tutorId: IdType, latitude: Double, longitude: Double, languageId: IdType, stripeChargeTokenString: String, success: @escaping MeetUpServiceSuccess, failure: @escaping MeetUpServiceFailure) {
    
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        
        let model = SWGMeetupRequestModel()
        model.requestedUserId = (Int(tutorId) as NSNumber!)
        model.latitude = latitude as NSNumber
        model.longitude = longitude as NSNumber
        model.languageId = NSNumber(value:Int(languageId)!)
//        model.stripeChargeToken = stripeChargeTokenString

        self.meetupApi.meetupPost(with: model, authorization: authorization) { (responseWrapper, error) in
            
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let dataWr = responseWrapper.data {
                let meetup = MeetUp()
                _ = meetup.updateFromSWGObject(swgObject: dataWr)
                success(meetup)
            } else {
                let error1 = ErrorHelper.compileError(withTextMessage: (responseWrapper?.errorMessage)!)
                failure(error1)

            }

        }
    }
    
    func changeMeetUpStatus(meetupId: IdType, toStatus: String, success: @escaping MeetUpServiceSuccess, failure: @escaping MeetUpServiceFailure) {
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        let meetUpIdNum = NSNumber(value:Int(meetupId)!)
        let model = SWGMeetupStatusModel()
        model.status = toStatus
//        model.status = "Accepted"

        self.meetupApi.meetupStatusPut(withId: meetUpIdNum, model: model, authorization: authorization) { (responseWrapper, error) in
            
            //SWGResponseWrapperObject_
        }
    }

    
}
