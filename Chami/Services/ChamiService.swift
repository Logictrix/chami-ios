//
//  ChamiService.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/17/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient


typealias ChamiServiceSuccess = ([User]) -> Void
typealias ChamiServiceFailure = (Error) -> Void

protocol ChamiServiceProtocol: NSObjectProtocol {
    func getTutorsNear(latitude: Double, longitude: Double, chamiTypeString: String, languageId: IdType?, success: @escaping ChamiServiceSuccess, failure: @escaping ChamiServiceFailure)

    func getTutorDetail(withId tutorId: IdType, success: @escaping (User) -> Void, failure: @escaping ChamiServiceFailure)
    func updateTutorDetail(tutor: User, completion: @escaping (_ error:Error?) -> Void)
}

class ChamiService: NSObject, ChamiServiceProtocol {
    
    var chamiApi: SWGChamiApi {
        get {
            return SWGChamiApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    func getTutorsNear(latitude: Double, longitude: Double, chamiTypeString: String, languageId: IdType?, success: @escaping ChamiServiceSuccess, failure: @escaping ChamiServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        var langId: NSNumber?
        if languageId != nil {
            langId = NSNumber(value:Int(languageId!)!)
        }
        self.chamiApi.chamiGet(withModelChamiType: chamiTypeString, modelLongitude: longitude as NSNumber!, modelLatitude: latitude as NSNumber!, authorization: authorization, modelLanguageId: langId) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let responseWrapper = responseWrapper, let responseModels = responseWrapper.data as? [SWGChamiModel] {
                var resultData: [User] = []
                
                for responseModel in responseModels {
                    let object: User = User(withId: String(describing: responseModel.userId.intValue))
                    
                    if object.updateFromSWGObject(objSWGChamiModel: responseModel) {
                        resultData.append(object)
                    }
                }
                success(resultData)
            }
        }

    }
    
    func getTutorDetail(withId tutorId: IdType, success: @escaping (User) -> Void, failure: @escaping ChamiServiceFailure) {

        let tutor = User(withId: tutorId)
        self.updateTutorDetail(tutor: tutor) { (error) in
            if let err = error {
                failure(err)
            } else {
                success(tutor)
            }
        }
//        guard let user = AccountManager.shared.currentUser else {
//            failure(ErrorHelper.compileError(withTextMessage: "Not logged in user"))
//            return
//        }
//        guard let authorization = user.authorization else {
//            failure(ErrorHelper.compileError(withTextMessage: "Not logged in user"))
//            return
//        }
//        
//        
//        SWGChamiApi.shared().chamiGet_1(withId: NSNumber(value:Int(tutorId)!), authorization: authorization) { (responseWrapper, error) in
//            if let error = error {
//                print("\(#function), error: \(error)")
//                failure(error)
//                return
//            }
//            
//            if let responseWrapper = responseWrapper, let responseModel = responseWrapper.data {
//                
//                let resultData = User(withId: tutorId)
//                
//                if resultData.updateFromSWGObject(objSWGChamiProfileModel: responseModel) == false {
//                    print("\(#function), error: cant parse data properly")
//                }
//                
//                success(resultData)
//            }
//        }
    }
    
    //
    func updateTutorDetail(tutor: User, completion: @escaping (_ error:Error?) -> Void) {
        
        guard let user = AccountManager.shared.currentUser else {
            completion(ErrorHelper.compileError(withTextMessage: "Not logged in user"))
            return
        }
        guard let authorization = user.authorization else {
            completion(ErrorHelper.compileError(withTextMessage: "Not logged in user"))
            return
        }
        
        
        self.chamiApi.chamiGet_1(withId: NSNumber(value:Int(tutor.id)!), authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                completion(error)
                return
            }
            
            if let responseWrapper = responseWrapper, let responseModel = responseWrapper.data {
                
                
                if tutor.updateFromSWGObject(objSWGChamiProfileModel: responseModel) == false {
                    print("\(#function), error: cant parse data properly")
                }
                
                completion(nil)
            }
        }
    }
}
