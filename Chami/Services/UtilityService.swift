//
//  UtilityService.swift
//  Chami
//
//  Created by Serg Smyk on 02/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import SwaggerClient

open class UtilityService : NSObject {
//    "bucketName": "string",
//    "accessKeyId": "string",
//    "secretAccessKey": "string",
//    "bucketRegion": "string"
    
    //static let shared = AccountManager()
    var settingsApi: SWGSettingApi {
        get {
            return SWGSettingApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    func getS3credentials(authorization:String, success: @escaping (_ bucket: String, _ key: String, _ secret: String, _ region: String) -> Void, failure: @escaping (_ error:Error) -> Void) {
        self.settingsApi.settingGetSettings(withAuthorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let wrapper = responseWrapper, let response = wrapper.data {
                success(response.bucketName, response.accessKeyId, response.secretAccessKey, response.bucketRegion)
            } else {
                let error = ErrorHelper.compileError(withTextMessage: "Error server response") as NSError
                failure(error)
            }
        }
    }
    
    func getWScredentials(authorization:String, success: @escaping (_ socketHost: String, _ socketPort: String) -> Void, failure: @escaping (_ error:Error) -> Void) {
        self.settingsApi.settingGetSocketSettings(withAuthorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let wrapper = responseWrapper, let response = wrapper.data {
                success(response.socketHost, response.socketPort)
            } else {
                let error = ErrorHelper.compileError(withTextMessage: "Error server response") as NSError
                failure(error)
            }
        }
    }
    
    func getStripeCredentials(authorization:String, success: @escaping (_ stripeKey: String, _ stripeOAuthUrl: String) -> Void, failure: @escaping (_ error:Error) -> Void) {
        self.settingsApi.settingGetStripeKeySetting(withAuthorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let wrapper = responseWrapper, let response = wrapper.data {
                success(response.stripeKey, response.stripeOAuthUrl)
            } else {
                let error = ErrorHelper.compileError(withTextMessage: "Error server response") as NSError
                failure(error)
            }
        }
    }
}
