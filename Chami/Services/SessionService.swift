//
//  SessionService.swift
//  Chami
//
//  Created by Pavel Korinenko on 3/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient


typealias SessionServiceSuccess = (Session) -> Void
typealias SessionServiceFailure = (Error) -> Void

protocol SessionServiceProtocol: NSObjectProtocol {
    
    func createSession(withMeetUpId meetUpId: IdType, latitude: Double, longitude: Double, startSessionDate: Date, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)

    func getSession(withId sessionId: IdType, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)

    func startSession(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)

    func finishSession(session: Session, duration: Double, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)
    
    
    func getSessionHistoryDetail(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)
    
    func getSessionHistory(success: @escaping ([Session]) -> Void, failure: @escaping SessionServiceFailure)
    
    func sendSessionTutorFeedback(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)
    
    func sendSessionStudentFeedback(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)
    
    func getSessionResult(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure)

}


class SessionService: NSObject, SessionServiceProtocol {
    
    var sessionApi: SWGSessionApi {
        get {
            return SWGSessionApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    func getSessionResult(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        //let sessionApi = SWGSessionApi.init(apiClient: SWGApiClient.shared())
        
        self.sessionApi.sessionResult(withId: NSNumber(value:Int(session.sessionId)!), authorization: authorization, completionHandler: { (responseWrapper, error) in
//            SWGResponseWrapperSessionResultModel_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
                
                //                SWGSessionResultModel
                _ = session.updateFromSWGObject(objSWGSessionResultModel: responseData)
                success(session)
            } else {
                let err = ErrorHelper.compileError(withTextMessage: "Session not found")
                failure(err)
            }

        })
        
    }

    func getSessionHistoryDetail(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        
        self.sessionApi.sessionGetByUserFeedback(withId: NSNumber(value:Int(session.sessionId)!), authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperSessionFeedbackModel_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
                
                //                SWGSessionFeedbackModel
                _ = session.updateFromSWGObject(objSWGSessionFeedbackModel: responseData)
                success(session)
            } else {
               let err = ErrorHelper.compileError(withTextMessage: "Session was not rated")
                failure(err)
            }

        }
    }

    
    func getSessionHistory(success: @escaping ([Session]) -> Void, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        
        self.sessionApi.sessionGetByUser(withAuthorization: authorization) { (responseWrapper, error) in
            
//            SWGResponseWrapperListSessionHistoryModel_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
                
                //                SWGSessionHistoryModel
                var array: [Session] = []
                for serverModel in responseData {
                    let session = Session()
                    _ = session.updateFromSWGObject(objSWGSessionHistoryModel: serverModel as! SWGSessionHistoryModel)
                    array.append(session)

                }
                success(array)
            }
            
        }
    }

    
    func sendSessionTutorFeedback(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        let model = SWGTutorFeedbackModel()
        model.feedback = session.feedback
        model.reportedUserId = NSNumber(value:Int(session.tutorsId)!)

        for skill1 in session.studentsRates {
            if skill1.isEditable == false {
                continue
            }
            let skillType = skill1.skillName

            switch skillType {
            case NSLocalizedString("Speaking", comment:"Speaking - Session Skills to rate"):
                model.speaking = NSNumber(value: skill1.skillValue)
                break
            case NSLocalizedString("Listening", comment:"Listening - Session Skills to rate"):
                model.listening = NSNumber(value: skill1.skillValue)
                break
            case NSLocalizedString("Reading", comment:"Reading - Session Skills to rate"):
                model.reading = NSNumber(value: skill1.skillValue)
                break
            case NSLocalizedString("Writing", comment:"Writing - Session Skills to rate"):
                model.writing = NSNumber(value: skill1.skillValue)
                break
            case NSLocalizedString("Pronunciation", comment:"Pronunciation - Session Skills to rate"):
                model.pronunciation = NSNumber(value: skill1.skillValue)
                break
            case NSLocalizedString("Attitude to Learning", comment:"Attitude to Learning - Session Skills to rate"):
                model.attitudeToLearning = NSNumber(value: skill1.skillValue)
                break
                //
            default: continue
                //
            }
            
        }

        if let sessionCEFR = session.ieltsScore {
            model.ieltsScore = sessionCEFR
        }
        
        self.sessionApi.sessionTutorFeedback(withId: NSNumber(value:Int(session.sessionId)!), model: model, authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperObject_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            //            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
            success(session)

        }
    }

    
    func sendSessionStudentFeedback(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        let model = SWGStudentFeedbackModel()
        model.feedback = session.feedback
        model.reportedUserId = NSNumber(value:Int(session.tutorsId)!)
        model.rating = NSNumber(value: session.rating)
        
        self.sessionApi.sessionStudentFeedback(withId: NSNumber(value:Int(session.sessionId)!), model: model, authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperObject_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            //            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
            success(session)
            //            }

        }
    }

    
    func finishSession(session: Session, duration: Double, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        let model = SWGSessionDurationModel()
        model.duration = NSNumber(value:Int(duration/60))
        
        self.sessionApi.sessionFinish(withId: NSNumber(value:Int(session.sessionId)!), model: model, authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperObject_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            //            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
            success(session)
            //            }

        }
    }

    
    
    func startSession(session: Session, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        self.sessionApi.sessionStart(withId: NSNumber(value:Int(session.sessionId)!), code: NSNumber(value:Int(session.code!)), authorization: authorization, completionHandler: { (responseWrapper, error) in
//            SWGResponseWrapperObject_
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
//            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
                success(session)
//            }

        })
    }

    
    func createSession(withMeetUpId meetUpId: IdType, latitude: Double, longitude: Double, startSessionDate: Date, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        let model = SWGSessionCreateModel()
        model.meetupId = NSNumber(value:Int(meetUpId)!)
//        model.longitude = NSNumber(value: longitude)
//        model.latitude = NSNumber(value: latitude)
//        model.date = startSessionDate

        self.sessionApi.sessionPost(with: model, authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperSessionResponseModel_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
                
//                SWGSessionResponseModel
                let session = Session()
                _ = session.updateFromSWGObject(objSWGSessionModel: responseData)
                success(session)
            }
        }
        
    }
    
    func getSession(withId sessionId: IdType, success: @escaping SessionServiceSuccess, failure: @escaping SessionServiceFailure) {
        
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
       
        let sessionIdNum = NSNumber(value:Int(sessionId)!)

        self.sessionApi.sessionGet(withId: sessionIdNum, authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperSessionModel_?
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let responseData = responseWrapper.data  {
                
                //                SWGSessionModel
                let session = Session()
                _ = session.updateFromSWGObject(objSWGSessionModel: responseData)
                success(session)
            }
        }
    }

}

