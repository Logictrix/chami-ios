//
//  AccountService.swift
//  Chami
//
//  Created by Igor Markov on 2/1/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient
import MapKit

enum SocialNetworkLoginType: Int {
    case facebook = 1
}

typealias AccountServiceSuccess = (Account) -> Void
typealias AccountServiceFailure = (Error) -> Void

protocol AccountServiceProtocol: NSObjectProtocol {

    func isEmailUnique(email: String, completion: @escaping (_ unique:Bool) -> Void)
    func resetPassword(email: String, completion: @escaping (_ error:Error?) -> Void)
    
    func loginUser(withEmail email: String, password: String, coordinate: CLLocationCoordinate2D, success: @escaping AccountServiceSuccess, failure: @escaping AccountServiceFailure)
    func loginUser(withFacebook token: String, coordinate: CLLocationCoordinate2D, success: @escaping AccountServiceSuccess, failure: @escaping AccountServiceFailure)
    func signUpUser(withFirstName firstName: String, lastName: String, email: String, password: String, coordinate: CLLocationCoordinate2D, success: @escaping AccountServiceSuccess, failure: @escaping AccountServiceFailure)
    
    func changeUserStatus(isOnline: Bool, completion: @escaping (_ error:Error?) -> Void)

    func changePassword(oldPassword: String, newPassword: String, completion: @escaping (_ error:Error?) -> Void)
    
    func logoutUser(completion: @escaping (_ error:Error?) -> Void)
}

class AccountService: NSObject, AccountServiceProtocol {

    var accountApi: SWGAccountApi {
        get {
            return SWGAccountApi.init(apiClient: SWGApiClient.shared())
        }
    }
    var accountStatusApi: SWGAccountStatusApi {
        get {
            return SWGAccountStatusApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    lazy var stubAccount: Account = {
        let account = Account(withId: Temp.stubAccountId)
        account.token = Temp.stubAccountToken
        return account
    }()

    func isEmailUnique(email: String, completion: @escaping (Bool) -> Void) {
        self.accountApi.accountIsEmailUnique(withEmail: email) { (responseWrapper, error) in
            var isUnique = true
            if let responseWrapper = responseWrapper, let response = responseWrapper.data as? Dictionary<String, Any> {
                if let number = response["isUnique"] {
                    isUnique = Bool(number as! NSNumber)
                }
            }
            completion(isUnique)
        }
    }
    func resetPassword(email: String, completion: @escaping (Error?) -> Void) {
        let model = SWGPasswordForgotModel()
        model.email = email
        self.accountApi.accountResetPassword(with: model) { (responseWrapper, error) in
            completion(error)
        }
    }
    
    func logoutUser(completion: @escaping (_ error:Error?) -> Void) {
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }
        self.accountApi.accountLogout(withAuthorization: authorization) { (_, error) in
            completion(error)
        }
    }
    
    func changeUserStatus(isOnline: Bool, completion: @escaping (_ error:Error?) -> Void) {
        let model = SWGChamiStatusModel()
        model.isOnline = NSNumber.init(value: isOnline)
        guard let user = AccountManager.shared.currentUser else {
            return
        }
        guard let authorization = user.authorization else {
            return
        }

        self.accountStatusApi.accountStatusOnlineStatusStatus(with: model, authorization: authorization) { (responseWrapper, error) in
//            SWGResponseWrapperObject_?
            
            if let error = error {
                print("\(#function), error: \(error)")
                completion(error)
                return
            }
            completion(nil)

        }
    }
    

    func loginUser(withEmail email: String, password: String, coordinate: CLLocationCoordinate2D, success: @escaping AccountServiceSuccess, failure: @escaping AccountServiceFailure) {
        
        let login = SWGLoginModel()
        login.email = email
        login.password = password
        login.osType = Define.osType
        login.deviceId = AccountManager.shared.deviceId
//        login.longitude = NSNumber.init(value: coordinate.longitude)
//        login.latitude = NSNumber.init(value: coordinate.latitude)
        print("login.email",login.email)
        print("login.password",login.password)
        print("login.email",login.osType)
        print("deviceId",login.deviceId)
        
        self.accountApi.accountLogin(with: login) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
        
            if let responseWrapper = responseWrapper, let response = responseWrapper.data {
                let account = Account(withId: Temp.stubAccountId)
                if let userId = response.userId {
                    account.id = String(describing: userId)
                }
                account.isOnline = response.isOnline.boolValue
                account.token = response.token
                if (response.chamiType != nil)
                {
                    
                    let type = ChamiLevelType(rawValue: response.chamiType)!
                    switch type {
                    case .chamiX:
                        account.role = .student
                    case .chamiPlus:
                        account.role = .tutor
                    case .chamiMax:
                        account.role = .tutor
                    }
                    
                    success(account)
                }
                
            }
            else
            {
                var userInfo = UserInfoType()
                userInfo[NSLocalizedDescriptionKey] = NSLocalizedString((responseWrapper!.errorMessage as NSString) as String, comment: "")
                let error = NSError(domain: AppError.errorModelDomain,
                                    code: AppError.unsuccessResultErrorCode,
                                    userInfo: userInfo as! [String : Any])
                failure(error)
            }
        }
    }
    
    func loginUser(withFacebook token: String, coordinate: CLLocationCoordinate2D, success: @escaping AccountServiceSuccess, failure: @escaping AccountServiceFailure) {
        
        let login = SWGExternalLoginModel()

        login.code = token
//        login.longitude = NSNumber.init(value: coordinate.longitude)
//        login.latitude = NSNumber.init(value: coordinate.latitude)
        login.deviceId = AccountManager.shared.deviceId
        login.deviceToken = ""
        login.osType = Define.osType
    
        
        self.accountApi.accountLoginFacebook(with: login) { (responseWrapper, error) in
                   
        
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let responseWrapper = responseWrapper, let response = responseWrapper.data {
                let account = Account(withId: Temp.stubAccountId)
                if let userId = response.userId {
                    account.id = String(describing: userId)
                }
                
                account.token = response.token
                //account.chamiType = response.chamiType
                
                if (response.chamiType != nil)
                {
                    let type = ChamiLevelType(rawValue: response.chamiType)!
                    switch type {
                    case .chamiX:
                        account.role = .student
                    case .chamiPlus:
                        account.role = .tutor
                    case .chamiMax:
                        account.role = .tutor
                    }
                }
                
                success(account)
            }
        }
    }

    func signUpUser(withFirstName firstName: String, lastName: String, email: String, password: String, coordinate: CLLocationCoordinate2D, success: @escaping AccountServiceSuccess, failure: @escaping AccountServiceFailure) {

        let registrationModel = SWGRegistrationModel()
        registrationModel.firstName = firstName
        registrationModel.lastName = lastName
        registrationModel.email = email
        registrationModel.password = password
        registrationModel.osType = Define.osType
        registrationModel.deviceId = AccountManager.shared.deviceId
//        registrationModel.longitude = NSNumber.init(value: coordinate.longitude)
//        registrationModel.latitude = NSNumber.init(value: coordinate.latitude)

        self.accountApi.accountRegistration(with: registrationModel) { (responseWrapper, error) in
            if let error = error {
                
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let responseWrapper = responseWrapper, let response = responseWrapper.data {
                let account = Account(withId: Temp.stubAccountId)
                if let userId = response.userId {
                    account.id = String(describing: userId)
                }
                account.token = response.token
                account.firstName = firstName
                account.lastName = lastName
                
                success(account)
            }
        }
    }
    
    func sendDeviceInfo(longitude: String?, latitude: String?, apnsToken: String?, completion: @escaping (_ error:Error?) -> Void) {
        
        let model = SWGDeviceInfoModel()
        if let lon = longitude {
            model.longitude = NSNumber(value:Double(lon)!)
        }
        
        if let lat = latitude {
            model.latitude = NSNumber(value:Double(lat)!)
        }
        
        if let apns = apnsToken {
            model.deviceToken = apns
        }
        
        model.deviceId = AccountManager.shared.deviceId
        model.osType = Define.osType
        

        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            return
        }
        self.accountApi.accountUpdateDeviceInfo(with: model, authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                
                print("\(#function), error: \(error)")
                completion(error)
                return
            }
            completion(nil)
        }
    }
    func changePassword(oldPassword: String, newPassword: String, completion: @escaping (_ error:Error?) -> Void) {
        let model = SWGChangePasswordModel()
        model.currentPassword = oldPassword
        model.password = newPassword
        

        
        
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            return
        }

        self.accountApi.accountChangePassword(with: model, authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                
                print("\(#function), error: \(error)")
                completion(error)
                return
            }
            completion(nil)
        }
    }
}

