//
//  ProfileService.swift
//  Chami
//
//  Created by Serg Smyk on 22/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

//typealias ProfileServiceSuccess = (SWGProfileModel) -> Void
//typealias ProfileServiceFailure = (Error) -> Void


protocol ProfileServiceProtocol: NSObjectProtocol {
    //static var shared:AccountManager {get set}
    
    func getProfileInfo(success: @escaping (_ model:SWGProfileModel?) -> Void, failure: @escaping (_ error:Error) -> Void)
    func setProfileInfo(account:Account, success: @escaping () -> Void, failure: @escaping (_ error:Error) -> Void)

    //func update(firstName: String?, lastName: String?, email: String?, about: String?, success: @escaping () -> Void, failure: @escaping (Error) -> Void)

    func setProfileLanguages(account: Account, success: @escaping () -> Void, failure: @escaping (Error) -> Void)
    func getProfileLanguages(account: Account, success: @escaping () -> Void, failure: @escaping (Error) -> Void)
    
    func updateProfileStripeStudentKey(stripeKey: String, success: @escaping () -> Void, failure: @escaping (Error) -> Void)
    func updateProfileStripeTutorKey(stripeKey: String, success: @escaping () -> Void, failure: @escaping (Error) -> Void)

}

class ProfileService: NSObject, ProfileServiceProtocol {

    var profileApi: SWGProfileApi {
        get {
            return SWGProfileApi.init(apiClient: SWGApiClient.shared())
        }
    }
    var accountStatusApi: SWGAccountStatusApi {
        get {
            return SWGAccountStatusApi.init(apiClient: SWGApiClient.shared())
        }
    }
    func updateProfileStripeStudentKey(stripeKey: String, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        
        guard let token = AccountManager.shared.currentUser?.token else {
            let er2 = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!")
            failure(er2)
            return
        }
        let tokenBearer = "bearer \(token)"

        let model = SWGStripeAccountModel()
        
        model.key = stripeKey
        
        self.profileApi.profileStripeStudent(with: model, authorization: tokenBearer) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
            } else {
                if let user = AccountManager.shared.currentUser {
                    user.stripeKeyStudent = stripeKey
                }
                success()
            }

        }
    }

    func updateProfileStripeTutorKey(stripeKey: String, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        
        guard let token = AccountManager.shared.currentUser?.token else {
            let er2 = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!")
            failure(er2)
            return
        }
        let tokenBearer = "bearer \(token)"
        
        let model = SWGStripeAccountModel()
        
        model.key = stripeKey
        
        self.profileApi.profileStripeTutor(with: model, authorization: tokenBearer) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
            } else {
                if let user = AccountManager.shared.currentUser {
                    user.stripeKeyTutor = stripeKey
                }
                
                success()
            }
            
        }
    }

    func updateProfileStripeConnectAccountKey(stripeKey: String, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
    
        guard let token = AccountManager.shared.currentUser?.token else {
            let er2 = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!")
            failure(er2)
            return
        }
        let tokenBearer = "bearer \(token)"
        
        let model = SWGStripeAccountModel()
        
        model.key = stripeKey
        
        self.profileApi.profileStripeConnect(with: model, authorization: tokenBearer) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
            } else {
                if let user = AccountManager.shared.currentUser {
                    user.stripeKeyTutor = stripeKey
                }
                
                success()
            }
            
        }
    }
    
    //    internal static var shared: AccountManager = AccountManager()
//    init() {
//        shared = AccountManager()
//        super.init()
//    }
    //fileprivate static var shared = AccountManager()
    func getProfileInfo(success: @escaping (_ model:SWGProfileModel?) -> Void, failure: @escaping (_ error:Error) -> Void) {
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            let error = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!") as NSError
            failure(error)
            return
        }
        
        self.profileApi.profileGet(withAuthorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if responseWrapper?.success.boolValue == false {
                failure(ErrorHelper.compileError(withTextMessage: "Unexpected error"))
                return
            }
            
            if let wrapper = responseWrapper, let response = wrapper.data {
                success(response)
            } else {
                success(nil)
            }
        }
    }
   /*
    func update(firstName: String?, lastName: String?, email: String?, about: String?, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            let error = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!") as NSError
            failure(error)
            return
        }
        
        let model = SWGProfileModel()
        model.firstName = firstName
        model.lastName = lastName
        model.email = email
        model.about = about
        
        SWGProfileApi.shared().profilePut(with: model, authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                // TODO: wrap error
                failure(error)
                return
            }
            
            guard let response = responseWrapper else {
                let error = ErrorHelper.compileError(withTextMessage: "Incorrect response") as NSError
                failure(error)
                return
            }
            
            if !response.success.boolValue {
                let message = response.errorMessage ?? "Unsuccess"
                let error = ErrorHelper.compileError(withTextMessage: message) as NSError
                failure(error)
                return
            }

            success()
        }
    }
   */
    
    func getProfileType(success: @escaping (_ model:SWGSwitchTypeModel?) -> Void, failure: @escaping (_ error:Error) -> Void) {
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            let error = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!") as NSError
            failure(error)
            return
        }
        
        self.accountStatusApi.accountStatusGetAccountType(withAuthorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if responseWrapper?.success.boolValue == false {
                failure(ErrorHelper.compileError(withTextMessage: "Unexpected error"))
                return
            }
            
            if let wrapper = responseWrapper, let response = wrapper.data {
                success(response)
            } else {
                success(nil)
            }
        }
    }

    
     internal func setProfileInfo(account: Account, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        let model = SWGProfileModel()
        
        model._id = NSNumber.init(value: Int32(account.id)!)
        model.firstName = account.firstName
        model.lastName = account.lastName
        model.email = account.email
        model.imageUrl = account.imageUrl
        model.nationality = account.nationality
        model.gender = account.gender
        model.phoneNumber = account.phoneNumber
        model.birthDate = account.birthDate
        model.introVideoUrl = account.introVideoUrl
        model.about = account.about
        model.nativeLanguageId = account.nativeLanguage?.languageIdNumber
        

        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            return
        }
        
        self.profileApi.profilePut(with: model, authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            else
            {
                success()
            }
            
        }
    }
    
    internal func setProfileLanguages(account: Account, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {
        
        let model = SWGProfileLanguageModel()
        model.chamiType = account.chamiType.rawValue
        //model.isCertificated = NSNumber.init(value: account.isCertificated)
        model.certificates = account.certificates
        
        var langs:[SWGUserLanguageModel] = []
        for language in account.teachingLanguages {
            let langModel = SWGUserLanguageModel()
            langModel.languageId = language.languageIdNumber
            langModel.imageUrl = language.languageImageUrl
            langModel.level = NSNumber.init(value: language.level)
            if account.chamiType == .chamiMax {
                langModel.price = NSNumber.init(value: language.priceMax)
            } else {
                langModel.price = NSNumber.init(value: language.pricePlus)
            }
            langs.append(langModel)
        }
        model.languages = langs
        
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            let error = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!") as NSError
            failure(error)
            return
        }
        
        self.profileApi.profileSetLanguagesByUser(with: model, authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            success()
        }
    }
    
    
    internal func getProfileLanguages(account: Account, success: @escaping () -> Void, failure: @escaping (Error) -> Void) {

        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            let error = ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!") as NSError
            failure(error)
            return
        }
        
        self.profileApi.profileGetLanguagesByUser(withAuthorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            
            if let responseWrapper = responseWrapper, let response = responseWrapper.data {
                account.certificates = response.certificates!
                account.isCertificated = response.isCertificated.boolValue
                account.chamiType = ChamiLevelType(rawValue:response.chamiType)!
                var langs:[Language] = []
                
                SettingsService().getAvailableLanguages(success: {(languages) in
                    for langObject in response.languages {
                        let lang = Language()
                        _ = lang.updateFromSWGObject(objeSWGUserLanguageModel: langObject as! SWGUserLanguageModel)
                        
                        if account.chamiType == .chamiMax {
                            lang.priceMax = lang.languageCost
                        } else {
                            lang.pricePlus = lang.languageCost
                        }
                        langs.append(lang)
                    }
                    account.teachingLanguages = langs
                    
                    success()
                    
                    
                }) { (error) in
                    print(error.localizedDescription)
                    failure(error)
                }
            } else {
                failure(ErrorHelper.compileError(withTextMessage: "Can't collect user languages"))
            }
        }
    }
}
