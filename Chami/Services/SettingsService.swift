//
//  SettingsService.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/13/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

typealias SettingsServiceSuccess = ([Language]) -> Void
typealias SettingsServiceFailure = (Error) -> Void

protocol SettingsServiceProtocol: NSObjectProtocol {
    
    func getAvailableLanguages(success: @escaping SettingsServiceSuccess, failure: @escaping SettingsServiceFailure)
}

class SettingsService: NSObject {

    var settingApi: SWGSettingApi {
        get {
            return SWGSettingApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    func getAvailableLanguages(success: @escaping SettingsServiceSuccess, failure: @escaping SettingsServiceFailure) {
        
        self.settingApi.settingGetLanguages { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                failure(error)
                return
            }
            if let responseWrapper = responseWrapper, let languagesModels = responseWrapper.data as? [SWGLanguageModel] {
                var resultLanguages: [Language] = []
                
                for languageModel in languagesModels {
                    let language = Language()
                    if language.updateFromSWGObject(swgObject: languageModel) {
                        resultLanguages.append(language)
                    }
                }
                success(resultLanguages)
            }
        }
    }


}
