//
//  AppDelegate.swift
//  Chami
//
//  Created by Igor Markov on 1/27/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
// com.chami.chami


import UIKit
import CoreData
//import FacebookCore
import FBSDKCoreKit
import FBSDKLoginKit
import Fabric
import Crashlytics
import GoogleMaps
import PKHUD
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    static let modelName = "Chami"

   func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
      
      SessionManager.shared.launchCounter += 1
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        self.window?.makeKeyAndVisible()

        NotificationCenter.default.addObserver(self, selector: #selector(handle(userDidChangeNotification:)), name: NSNotification.Name(rawValue: Notification.userDidChange), object: nil)

        
        AccountManager.shared.updateTheme()
        self.selectRootViewControllerOnStart()
        
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        
        Fabric.with([Crashlytics.self])
        GMSServices.provideAPIKey(AppApiServices.googleApiKey)
        
        STPPaymentConfiguration.shared().publishableKey = AppApiServices.stripePublishableKey
      
      _ = SessionManager.shared
        return true
   }
//   func openURL(_ url: URL) -> Bool {
//      return false
//   }
//   func application(_ application: UIApplication, handleOpen url: URL) -> Bool {
//      return false
//   }
//   func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
//      return false
//   }
   
   func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
      let appId = Settings.appID
      if url.scheme != nil && url.scheme!.hasPrefix("fb\(String(describing: appId))") && url.host ==  "authorize" { // facebook
            return ApplicationDelegate.shared.application(app, open: url, options: options)
        }
        return false
    }

    fileprivate func selectRootViewController() {
        
        self.selectRootViewControllerOnStart()
//        switch AccountManager.shared.hasAuthenticationStatus() {
//        case .unauthenticated:
//            NavigationManager.shared.showAuthentication(animated: false)
//        case .authenticated:
//            let role = AccountManager.shared.currentRole
//            if role == .notDefined {
//                NavigationManager.shared.showSelectRole(animated: true)
//            } else {
//                guard let currentUser = AccountManager.shared.currentUser else {
//                    return
//                }
//                if currentUser.hasCompletedProfile(withRole: role) {
//                    NavigationManager.shared.showMainScreen(animated: true)
//                } else {
//                    NavigationManager.shared.showSetupProfileScreen(animated: true)
//                }
//            }
//        }
    }
    
    fileprivate func selectRootViewControllerOnStart() {
      let storyboard = UIStoryboard.init(name: "LaunchScreen", bundle: Bundle.main)
      let launchScreen = storyboard.instantiateInitialViewController()
      launchScreen?.view.frame = (self.window?.bounds)!
      self.window?.addSubview((launchScreen?.view)!)
      
        switch AccountManager.shared.hasAuthenticationStatus() {
        case .unauthenticated:
          NavigationManager.shared.showLoginScreen(animated: false)
         // OLD CODE
        /*    NavigationManager.shared.showAuthentication(animated: false)
            self.window?.addSubview((launchScreen?.view)!)
            //if SessionManager.shared.launchCounter <= 1 {
               let vc = IntroBuilder.viewController()
               self.window?.rootViewController?.present(vc, animated: false, completion: {
                  launchScreen?.view.removeFromSuperview()
               })*/
         //}
         
        case .authenticated:
            
            NavigationManager.shared.showAuthentication(animated: false)
            self.window?.addSubview((launchScreen?.view)!)
            
            //HUD.show(.progress)
            
            
            AccountManager.shared.currentUser?.updateProfileWithFinishBlock(completion: { (error:Error?) in
               launchScreen?.view.removeFromSuperview()
               //HUD.hide(animated: true)
               if error != nil {
                  return
               }
                
                AccountManager.shared.updateTheme(to: AccountManager.shared.currentUser?.role)
                AccountManager.shared.currentRole = (AccountManager.shared.currentUser?.role)!
                
                //let acc2 = AccountManager.shared.currentUser
                
                let role = AccountManager.shared.currentUser?.role
                
                if role == .notDefined {
                    NavigationManager.shared.showSelectRole(animated: true)
                } else if (role == .student) || (role == .tutor){
                  guard let currentUser = AccountManager.shared.currentUser else {
                        return
                  }
                  if currentUser.hasCompletedProfile(withRole: role!) {
                     if role == .tutor && AccountManager.shared.currentUser?.teachingLanguages.count == 0 {
                        NavigationManager.shared.showEditLanguagesScreen(animated: true)
                     } else {
                        NavigationManager.shared.showMainScreen(animated: true)
                        
                        if currentUser.isOnline == true {
                           NotificationsManager.shared.setNotificationEnabled(enabled: true)
                        }
                     }
                  } else {
                        NavigationManager.shared.showSetupProfileScreen(animated: true)
                  }
                }
                else {
                    NavigationManager.shared.showSelectRole(animated: true)
                }
                
                _ = WebSocketsManager.shared
            })
            
            
        }
    }

    @objc fileprivate func handle(userDidChangeNotification notification: Foundation.Notification) {
        
        DispatchQueue.main.async {
            self.selectRootViewController()
            
//            if AccountManager.sharedInstance.hasAuthenticationStatus() == .authenticated {
//                RemoteNotificationManager.sharedInstance.registerForPushNotification()
//            } else {
//                RemoteNotificationManager.sharedInstance.unregisterPushNotifications()
//            }
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
//        self.saveContext()
      guard let currentUser = AccountManager.shared.currentUser else {
         return
      }
      if currentUser.role == .student {
         AccountService().changeUserStatus(isOnline: false) { (error) in
            if let error = error {
               print(error.localizedDescription)
            }
         }

      }
    }
   
//    // MARK: - Core Data stack (generic)
//
//    func saveContext () {
//        do {
//            if databaseContext.hasChanges {
//                try databaseContext.save()
//            }
//        } catch {
//            let nserror = error as NSError
//            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
//        }
//    }
//   
//    lazy var databaseContext : NSManagedObjectContext = {
//        if #available(iOS 10.0, *) {
//            return self.persistentContainer.viewContext
//        } else {
//            return self.managedObjectContext
//        }
//    }()
//    
//    lazy var managedObjectModel: NSManagedObjectModel = {
//        let modelURL = Bundle.main.url(forResource: modelName, withExtension: "momd")!
//        return NSManagedObjectModel(contentsOf: modelURL)!
//    }()
//   
//    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
//        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
//        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        let url = self.applicationDocumentsDirectory.appendingPathComponent(modelName).appendingPathExtension("sqlite")
//        
//        do {
//            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
//        } catch {
//            let dict : [String : Any] = [NSLocalizedDescriptionKey        : "Failed to initialize the application's saved data" as NSString,
//                                         NSLocalizedFailureReasonErrorKey : "There was an error creating or loading the application's saved data." as NSString,
//                                         NSUnderlyingErrorKey             : error as NSError]
//            
//            let wrappedError = NSError(domain: "com.chami.error", code: 9999, userInfo: dict)
//            fatalError("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
//        }
//        
//        return coordinator
//    }()
//   
//    // MARK: - Core Data stack (iOS 9)
//
//    @available(iOS 9.0, *)
//    lazy var managedObjectContext: NSManagedObjectContext = {
//        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
//        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
//        return managedObjectContext
//    }()
//    
//    // MARK: - Core Data stack (iOS 10)
//
//    @available(iOS 10.0, *)
//    lazy var persistentContainer: NSPersistentContainer = {
//        let container = NSPersistentContainer(name: modelName)
//        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
//            if let error = error as NSError? {
//                fatalError("Unresolved error \(error), \(error.userInfo)")
//            }
//        })
//        
//        return container
//    }()
//    
//    // MARK: - utility routines
//    
//    lazy var applicationDocumentsDirectory: URL = {
//        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
//        return urls[urls.count - 1]
//    }()

   // Notifications
   func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
      if let remoteNotif = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? NSDictionary {
         NotificationsManager.shared.handleNotification(userInfo: remoteNotif)
      }
      
      return true
   }
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
      let remoteNotif = userInfo as NSDictionary
      
      if (application.applicationState == .inactive) {
         NotificationsManager.shared.handleNotification(userInfo: remoteNotif)
         
      } else if (application.applicationState == .background) {
         NotificationsManager.shared.handleNotification(userInfo: remoteNotif)
         
      } else if (application.applicationState == .active) {
         // save
      }
   }
   func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
      let remoteNotif = userInfo as NSDictionary
      
      NotificationsManager.shared.handleNotification(userInfo: remoteNotif)
      
      completionHandler(.newData);
   }
   
   func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
      NotificationsManager.shared.handleDeviceTokenRegistration(data: deviceToken)
   }
   func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
      #if TARGET_IPHONE_SIMULATOR
      #else
         print("didFailToRegisterForRemoteNotificationsWithError \(error)")
      #endif
   }
   
   func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
      NotificationsManager.shared.handleNotificationRegistrationSettings(notificationSettings: notificationSettings)
   }
   
   func applicationDidBecomeActive(_ application: UIApplication) {
      //AppEventsLogger.activate(application)
      AppEvents.activateApp()
      NotificationsManager.shared.handleAllDeliveredNotifications()
      NotificationsManager.shared.setupNewNotificationsCounter()
   }
   
   
}
