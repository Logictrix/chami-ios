//
//  FormatterHelper.swift
//  Stadio
//
//  Created by Igor Markov on 7/12/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class FormatterHelper: NSObject {
    
    static let locale: Locale = {
        let locale = NSLocale.current
//        let locale = Locale(identifier: "US")
        return locale
    }()

    static let generalDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateStyle = .medium
        return dateFormatter
    }()
    
    static let serverDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZZZZZ"
        return dateFormatter
    }()
    
    static let exampleDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = "YYYY-MM-dd' at 'HH:mm"
        return dateFormatter
    }()
    
    
    static let hhmm12DateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = "hh:mma"
        return dateFormatter
    }()

    static let ddmmyyyDateFormatter: DateFormatter = {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale
        dateFormatter.dateFormat = "dd/MM/YYYY"
        return dateFormatter
    }()

    
    
    private static var regexpHTMLRemover: NSRegularExpression = {
        return try! NSRegularExpression(pattern: "<[^>]+>", options: [])
    }()
    
    class func removeHTML(_ string: String) -> String {
        let result = regexpHTMLRemover.stringByReplacingMatches(in: string, options: [], range: NSRange(location: 0, length: string.characters.count), withTemplate: "")
        return result
    }
    
    class func encodePassword(_ password: String) -> String {
        return password.SHA256()
    }
    
}
