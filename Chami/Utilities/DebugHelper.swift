//
//  DebugHelper.swift
//  Chami
//
//  Created by Pavel Korinenko on 3/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class DebugHelper: NSObject {
    
    static func jsonString(fromDictionary dictionary: NSDictionary) -> String? {
        if let theJSONData = try? JSONSerialization.data(
            withJSONObject: dictionary,
            options: []) {
            let theJSONText = String(data: theJSONData,
                                     encoding: .ascii)
            print("JSON string = \(theJSONText!)")
            return theJSONText!
        }
        return nil
    }

}
