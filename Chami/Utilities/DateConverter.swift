//
//  DateConverter.swift
//  Stadio
//
//  Created by Igor Markov on 5/26/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation

class DateConverter {
    
    class func string(fromDate date: Date) -> String {
        let string: String = FormatterHelper.generalDateFormatter.string(from: date)
        return string
    }
    
    class func date(fromString string: String) -> Date {
        if let date = FormatterHelper.generalDateFormatter.date(from: string) {
            return date
        } else {
            assert(false, "incorrect date in string \(string)")
            return Date()
        }
    }

    class func stringForServer(fromDate date: Date) -> String {
        let string: String = FormatterHelper.serverDateFormatter.string(from: date)
        return string
    }

    
    class func stringTimeFormat(fromDate date: Date) -> String {
        let string: String = FormatterHelper.hhmm12DateFormatter.string(from: date)
        return string
    }

    class func stringDateWithoutTimeFormat(fromDate date: Date) -> String {
        let string: String = FormatterHelper.ddmmyyyDateFormatter.string(from: date)
        return string
    }
    
    class func stringFullYears(fromDate date: Date) -> String {
        let years = Calendar.current.dateComponents([.year], from: date, to: Date()).year ?? 0

        let string: String = "\(years)"
        return string
    }

    class func stringHhMmSsFromSeconds(seconds : Int) -> String {
        let hours = seconds / 3600
        var hoursStr = "\(hours)"
        if hours < 10 {
            hoursStr = "0\(hours)"
        }
        
        let minutes = (seconds % 3600) / 60
        var minutesStr = "\(minutes)"
        if minutes < 10 {
            minutesStr = "0\(minutes)"
        }

        let seconds = (seconds % 3600) % 60
        var secondsStr = "\(seconds)"
        if seconds < 10 {
            secondsStr = "0\(seconds)"
        }
        
        let strTime = "\(hoursStr):\(minutesStr):\(secondsStr)"
        return strTime
    }
    
    class func stringHhMmFromSeconds(seconds : Int) -> String {
        var hoursStr = "hours"
        var minutesStr = "minutes"
        
        let hours = seconds / 3600
        if hours == 1 {
            hoursStr = "hour"
        }
        
        let minutes = (seconds % 3600) / 60
        if minutes == 1 {
            minutesStr = "minute"
        }

        let strTime = "\(hours) \(hoursStr), \(minutes) \(minutesStr)"
        return strTime
    }

}
