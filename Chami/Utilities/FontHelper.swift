//
//  FontHelper.swift
//  Stadio
//
//  Created by Igor Markov on 7/22/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum CustomFontName: String {
    case sfui = "SFUIDisplay-Regular"
    case sfuiSemi = "SFUIDisplay-SemiBold"
    case sfuiMedium = "SFUIDisplay-Medium"
    case sfuiBold = "SFUIDisplay-Bold"
    case awesome = "FontAwesome"
}

class FontHelper: NSObject {
    
    class func font(name: String, size: CGFloat) -> UIFont {
        return UIFont(name: name, size: size)!
    }
    
    class func defaultFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: CustomFontName.sfui.rawValue, size: size)!
    }

    class func defaultMediumFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: CustomFontName.sfuiSemi.rawValue, size: size)!
    }

    class func defaultBoldFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: CustomFontName.sfuiBold.rawValue, size: size)!
    }
    
    class func awesomeFont(withSize size: CGFloat) -> UIFont {
        return UIFont(name: CustomFontName.awesome.rawValue, size: size)!
    }
}

enum FontImage {
    static let checkbox: String = "\u{f00c}"
    static let chevronRight: String = "\u{f054}"
}
