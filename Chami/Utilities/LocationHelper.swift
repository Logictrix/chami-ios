//
//  LocationHelper.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit

class LocationHelper: NSObject {
    class func stringDistanceInMiles(fromLocation location1: CLLocation, toLocation location2: CLLocation) -> String {
        let distance = String(format: "%.2f", location1.distance(from: location2)/1000)
            return distance
    }

    class func stringLocationFromCoordinates(latitude: Double, longitude: Double, completion: @escaping (String) -> Void) {
        let geocoder = CLGeocoder()
        let centerLocation: CLLocation! = CLLocation.init(latitude: latitude, longitude: longitude)
        
        var locationString = ""
        geocoder.reverseGeocodeLocation(centerLocation) { (placemarks, error) in
            if let placemarks = placemarks, let placemark = placemarks.first {
                let arr = placemark.addressDictionary?["FormattedAddressLines"] as! NSArray
                locationString = arr.componentsJoined(by: ", ")
            }
            completion(locationString)
        }
    }

}
