//
//  ErrorHelper.swift
//  Stadio
//
//  Created by Igor Markov on 6/15/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

typealias UserInfoType = [AnyHashable: Any]

enum AppErrorCode: Int {
    case UserNotAuthorized = 2020,
    TextReasonAuthorized = 2025
//    TextReasonAuthorized22 = 2025
    
}

class ErrorHelper {
    
    fileprivate static let errorAreaComment = "Error description string"
    
    
    class func compileError(withTextMessage message: String ) -> Error {
//        let infoDict = [NSLocalizedDescriptionKey: message]
        
//        let error = NSError(domain: "", code: AppErrorCode.TextReasonAuthorized.rawValue, userInfo: infoDict)
        let error = ErrorHelper.compileError(withTextMessage: message, errorCode: .TextReasonAuthorized)
        return (error as Error)
    }

    class func compileError(withTextMessage message: String, errorCode: AppErrorCode ) -> Error {
        let infoDict = [NSLocalizedDescriptionKey: message]
        
        let error = NSError(domain: "", code: errorCode.rawValue, userInfo: infoDict)
        return (error as Error)
    }

    class func compileUserInfo(fromApiError error: NSError) -> UserInfoType {
        
        var userInfo = UserInfoType()
        
        userInfo[NSUnderlyingErrorKey] = error
        
//        if let descriptionDictionary = error.userInfo[SWGResponseObjectErrorKey] as? [String: String], let errorDescription = descriptionDictionary["error"] {
//            
//            if errorDescription.characters.count > 0 {
//                userInfo[NSLocalizedDescriptionKey] = FormatterHelper.removeHTML(errorDescription)
//            } else {
//                userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Some server response error", comment: errorAreaComment)
//            }
//        }
        
        // Extract response body as NSData (debug purpose)
//        if let responseErrorData = error.userInfo[AFNetworkingOperationFailingURLResponseDataErrorKey] as? Data {
//            if let responseErrorString = String(data: responseErrorData, encoding: String.Encoding.utf8) {
//                userInfo["extracted response data"] = responseErrorString
//            }
//        }
        
        return userInfo
    }
    
    
    class func errorMessageFor(Error error: Error) -> NSString {
        
        let err = (error as NSError)
        let responseObject = err.userInfo[SWGResponseObjectErrorKey]
        if (responseObject != nil && responseObject is NSDictionary)
        {
            let errorMessage = (responseObject as! NSDictionary) ["errorMessage"] as? NSString
            return errorMessage! as NSString
        }
        else
        {
            return error.localizedDescription as NSString
        }
        
    }
    
    class func compileUserInfo(fromServiceError error: NSError) -> UserInfoType {
        
        var userInfo = UserInfoType()
        
        userInfo[NSUnderlyingErrorKey] = error
        if let errorDescription = error.userInfo[NSLocalizedDescriptionKey] as? String {
            userInfo[NSLocalizedDescriptionKey] = FormatterHelper.removeHTML(errorDescription)
        } else {
            userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Some error in network service", comment: errorAreaComment)
        }
        
        return userInfo
    }
    
    
    
    class func compileUserInfo(forModelErrorCode errorCode: Int) -> UserInfoType {
        
        var userInfo = UserInfoType()
        
        if errorCode == AppError.invalidParametersErrorCode {
            userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Please check parameters", comment: errorAreaComment)
        } else if errorCode == AppError.unsuccessResultErrorCode {
            userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Failed operation", comment: errorAreaComment)
        }
        
        return userInfo
    }
    
    class func compileUserInfo(forModelErrorString errorString: NSString) -> UserInfoType {
        
        var userInfo = UserInfoType()
        userInfo[NSLocalizedDescriptionKey] = NSLocalizedString(errorString as String, comment: errorAreaComment)
//        if errorCode == AppError.invalidParametersErrorCode {
//            userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Please check parameters", comment: errorAreaComment)
//        } else if errorCode == AppError.unsuccessResultErrorCode {
//            userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Failed operation", comment: errorAreaComment)
//        }
        
        return userInfo
    }
    
}
