//
// Created by Igor Markov on 5/26/16.
// Copyright (c) 2016 DB Best Technologies LLC. All rights reserved.
//

import Foundation

class ValidationUtils {

    class func validateEmail(_ candidate: String) -> Bool {

        if candidate.characters.count > 100 {
            return false
        }
        
        let emailRegex: String = "[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"
        let emailTest = NSPredicate(format: "SELF MATCHES[c] %@", emailRegex)
        return emailTest.evaluate(with: candidate)
    }

    class func validatePassword(_ candidate: String) -> Bool {
        if candidate.characters.count < 6 || candidate.characters.count > 30 {
            return false
        }
        if self.checkIfStringContainsOnlyWhiteSpaces(candidate) {
            return false
        }
        return true
    }

    class func validateComment(_ candidate: String) -> Bool {
        if candidate.characters.count < 2 || candidate.characters.count > 500 {
            return false
        }
        if self.checkIfStringContainsOnlyWhiteSpaces(candidate) {
            return false
        }
        return true
    }

    class func validateWebsite(_ candidate: String) -> Bool {
        if candidate.characters.count < 4 || candidate.characters.count > 200 {
            return false
        }
        if self.checkIfStringContainsOnlyWhiteSpaces(candidate) {
            return false
        }
        return true
    }

    class func validateUserInfo(_ candidate: String) -> Bool {
        if candidate.characters.count < 2 || candidate.characters.count > 150 {
            return false
        }
        if self.checkIfStringContainsOnlyWhiteSpaces(candidate) {
            return false
        }
        return true
    }

    class func validateAccountname(_ candidate: String) -> Bool {
        if candidate.characters.count < 2 || candidate.characters.count > 50 {
            return false
        }
        if self.checkIfStringContainsOnlyWhiteSpaces(candidate) {
            return false
        }
        let regex: String = "^[a-zA-Z0-9\\._\\@]{0,255}"
        let test: NSPredicate = NSPredicate(format: "SELF MATCHES[c] %@", regex)
        return test.evaluate(with: candidate)
    }

    class func validateUsername(_ candidate: String) -> Bool {
        // can use ' '
        if candidate.characters.count < 2 || candidate.characters.count > 50 {
            return false
        }
        if self.checkIfStringContainsOnlyWhiteSpaces(candidate) {
            return false
        }
        let regex: String = "^[a-zA-Z0-9 \\._\\@]{0,255}"
        let test: NSPredicate = NSPredicate(format: "SELF MATCHES[c] %@", regex)
        return test.evaluate(with: candidate)
    }

    class func validatePhoneNumber(_ candidate: String) -> Bool {
        if candidate.characters.count < 5 || candidate.characters.count > 50 {
            return false
        }
        let regex: String = "^\\+?(?:\\([0-9]{2,4}\\))?[0-9]{4,44}$"
        let test: NSPredicate = NSPredicate(format: "SELF MATCHES[c] %@", regex)
        return test.evaluate(with: candidate)
    }

    class func validateIdNumber(_ candidate: String) -> Bool {
        let regex: String = "[0-9]{4,14}"
        let test: NSPredicate = NSPredicate(format: "SELF MATCHES[c] %@", regex)
        let result: Bool = test.evaluate(with: candidate)
        return result
    }

    class func validationWarningMessageForStringWithKey(_ key: String, value val: String) -> String {

        if val.characters.count > 0 {
            return String(format: NSLocalizedString("Correct the %@", comment: "Correct the %@"), key)
        } else {
            return String(format: NSLocalizedString("Please enter your %@ ", comment: "Please enter your %@"), key)
        }
    }

    class func validationWarningMessageForPasswordWithKey(_ key: String, value val: String) -> String {

        var message: String = self.validationWarningMessageForStringWithKey(key, value: val)
        if val.characters.count > 0 {
            // Add ValidationInfo ToMessage
            message += ". " + NSLocalizedString("It should contain at least 6 symbols", comment: "Password validation warning message")
        }

        return message
    }

    
    class func checkIfStringContainsOnlyWhiteSpaces(_ string: String) -> Bool {

        let whitespaces: CharacterSet = CharacterSet.whitespaces
        if string.trimmingCharacters(in: whitespaces).characters.count == 0 {
            // String contains only whitespace.
            return true
        }
        // Not only whiteSpaces
        return false
    }
}
