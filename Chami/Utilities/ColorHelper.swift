//
//  ColorHelper.swift
//  Chami
//
//  Created by Igor Markov on 1/27/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

func RGBA(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat, _ alpha: CGFloat) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
}

func RGB(_ red: CGFloat, _ green: CGFloat, _ blue: CGFloat) -> UIColor {
    return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: 1)
}


class ColorHelper {

    class func regularColor() -> UIColor {
        return Theme.current.regularColor()
    }

    class func lightTextColor() -> UIColor {
        return UIColor.white
    }
    
    class func lightGrayColor() -> UIColor {
        return RGB(230, 230, 230)
    }

    // MARK: - Form field colors

    class func defaultFormFieldTextColor() -> UIColor {
        //return UIColor.black
        return RGB(40, 55, 60)
    }
    
    class func correctFormFieldTextColor() -> UIColor {
        return RGB(80, 184, 73)
    }
    
    class func linkTextColor() -> UIColor {
        return RGB(15, 161, 243)
    }
    
    class func incorrectFormFieldTextColor() -> UIColor {
        return RGB(242, 31, 84)
    }
    
    class func backgroundFormFieldColor() -> UIColor {
        return RGB(245, 241, 221)
    }
    
    class func yellowAppColor() -> UIColor {
        return RGB(239, 210, 54)
    }

}

