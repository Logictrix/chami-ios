//
//  PagingServiceProtocol.swift
//  Stadio
//
//  Created by Igor Markov on 5/25/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol PagingServiceProtocol: NSObjectProtocol {
    
    associatedtype PagingServiceObjectType
    
    func getObjects(_ page: Int, count: UInt, success: @escaping  (([PagingServiceObjectType]) -> ()), failure: @escaping (NSError) -> ())
}
