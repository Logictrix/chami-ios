//
//  DiscreteLabelsScaleView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class DiscreteLabelsScaleView: UIView {

    var labelsArray: [UIView] = []
    var namesArray: [String] = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]
//    var ticksDistance: CGFloat = 44.0 {
//        didSet {
//            self.layoutTrack()
//        }
//    }
    var labelsFont = UIFont.systemFont(ofSize: 14)
    var labelsColor = UIColor.black

    
    override init(frame: CGRect) {
        super.init(frame: frame)
        nibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        nibSetup()
    }
    
    private func nibSetup() {
        
        let view = loadViewFromNib()
//        view.backgroundColor = UIColor.red
        view.frame = bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
//        view.translatesAutoresizingMaskIntoConstraints = true
        
        addSubview(view)
        self.layoutTrack()

    }
    
    private func loadViewFromNib() -> UIView {
        
        let viewNibName = String(describing: DiscreteLabelsScaleView.self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: self, options: nil)
        guard let nibView = nibContent?.first as? UIView else {
            fatalError("Nib \(viewNibName) does not contain Home View as first object")
        }
        return nibView
    }
    
    override var frame: CGRect{
        didSet{
            self.layoutTrack()
//            self.setNeedsDisplay()
//            self.layoutIfNeeded()
        }
    }
    
    func layoutTrack() {
        for (_, value) in self.labelsArray.enumerated() {
            value.removeFromSuperview()
        }
        self.labelsArray.removeAll()
        let count = self.namesArray.count
        if count > 0 {
            let ticksDistance: CGFloat = self.bounds.size.width / 10.0
            
            var centerX: CGFloat = (self.bounds.size.width - ((CGFloat(count) - 1.0) * ticksDistance))/2.0
            
            let centerY: CGFloat = self.bounds.size.height / 2.0
            
            for (name) in self.namesArray {
                let label = UILabel(frame: CGRect.zero)
                self.labelsArray.append(label)
                label.text = name
                label.font = self.labelsFont
                label.textColor = self.labelsColor
                label.sizeToFit()
                label.center = CGPoint(x: centerX, y: centerY)
                var frame1 = label.frame
                frame1.origin.y = self.bounds.size.height - frame1.size.height
                label.frame = frame1
                
                self.addSubview(label)
                centerX += ticksDistance
            }
        }
    }


}
