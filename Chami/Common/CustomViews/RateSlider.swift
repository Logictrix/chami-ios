//
//  RateSlider.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/10/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RateSlider: UISlider {

    override func trackRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: 0, y: 0, width: bounds.size.width, height: 6)
    }

}
