//
//  GradientView.swift
//  Chami
//

import UIKit

class GradientView: UIView {
    override open class var layerClass: Swift.AnyClass {
        get {
            return CAGradientLayer.self
        }
    }
    @IBInspectable open var colors: Array<UIColor>! {
        didSet {
            self.setupGradientLayerWithColors(colors: self.colors)
//            if self.colors.count == 2{
//                self.startColor = self.colors[0]
//                self.endColor = self.colors[1]
//                self.setNeedsDisplay()
//            }
        }
    }
    
    func setupGradientLayerWithColors(colors: [UIColor]) {
        var cgColors: [CGColor] = []
        for color in colors {
            cgColors.append(color.cgColor)
        }
        let gradientLayer:CAGradientLayer = self.layer as! CAGradientLayer
        gradientLayer.colors = cgColors
    
        self.backgroundColor = UIColor.clear
    }

    //    @IBInspectable var startColor: UIColor = UIColor.red
//        {
//        didSet {
//            if self.colors.count == 2{
//                self.colors.removeFirst()
//            }
//            self.colors.insert(self.startColor, at: 0)
//        }
//    }
    //    @IBInspectable var endColor: UIColor = UIColor.green
//        {
//        didSet {
//            if self.colors.count == 2{
//                self.colors.removeLast()
//            }
//            self.colors.append(self.endColor)
//        }
//    }
//
//    override func draw(_ rect: CGRect) {
//        let context = UIGraphicsGetCurrentContext()
//        let colors = [startColor.cgColor, endColor.cgColor]
//        
//        let colorSpace = CGColorSpaceCreateDeviceRGB()
//        let colorLocations:[CGFloat] = [0.0, 1.0]
//        let gradient = CGGradient(colorsSpace: colorSpace,
//                                  colors: colors as CFArray,
//                                  locations: colorLocations)
//        let startPoint = CGPoint.zero
//        let endPoint = CGPoint(x:0, y:self.bounds.height)
//        if let context = context, let gradient = gradient {
//            context.drawLinearGradient(gradient, start: startPoint, end: endPoint, options: CGGradientDrawingOptions())
//        }
//    }
}
