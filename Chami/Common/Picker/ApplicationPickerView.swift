//
//  ApplicationPickerView.swift
//  Chami
//
//  Created by Serg Smyk on 17/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum ApplicationPickerType {
    case defaultType
    case dateType
}

class ApplicationPickerView: UIView {
    @IBOutlet weak var pickerConteinerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var toolbarView: UIToolbar!
    @IBOutlet weak var cancelButton: UIBarButtonItem!
    @IBOutlet weak var doneButton: UIBarButtonItem!
    @IBOutlet weak var pickerView: UIPickerView!
    @IBOutlet weak var datePickerView: UIDatePicker!

    static func createWithType(type:ApplicationPickerType) -> ApplicationPickerView {
        let pickerView:ApplicationPickerView = ApplicationPickerView.loadFromXib(owner: nil) as! ApplicationPickerView
        if type ==  ApplicationPickerType.dateType {
//            pickerView.pickerView.removeFromSuperview()
//            pickerView.pickerView = nil
            pickerView.pickerView.isHidden = true
        } else {
//            pickerView.datePickerView.removeFromSuperview()
//            pickerView.datePickerView = nil
            pickerView.datePickerView.isHidden = true
        }
//        pickerView.toolbarView.backgroundColor = Theme.current.regularColor()
        pickerView.toolbarView.barTintColor = Theme.current.regularColor()
        //        pickerView.toolbarView.tintColor = Theme.current.regularColor()
        return pickerView
    }
    
    func showInView(view:UIView, animated:Bool) {
        self.frame = view.bounds
        self.alpha = 1
        view.addSubview(self)
        if animated {
            self.pickerConteinerView.alpha = 0
            UIView.animate(withDuration: 0.2, animations: { 
                self.pickerConteinerView.alpha = 1
            })
        } else {
            self.pickerConteinerView.alpha = 1
        }
    }
    func dismiss(animated:Bool) {
        if animated {
            UIView.animate(withDuration: 0.2, animations: { 
                self.pickerConteinerView.alpha = 0
            }, completion: { (Bool) in
                self.removeFromSuperview()
            })
        } else {
            self.removeFromSuperview()
        }
    }
    
    
    @IBAction func onCancelClick(_ sender: Any) {
        self.dismiss(animated: true)
        if (self.cancelActionBlock != nil) {
            self.cancelActionBlock!()
        }
    }
    @IBAction func onDoneClick(_ sender: Any) {
        self.dismiss(animated: true)
        if (self.doneActionBlock != nil) {
            self.doneActionBlock!()
        }
    }
    
    var cancelActionBlock : (() -> Void)?
    var doneActionBlock : (() -> Void)?
    
    var pickerTitles:[String]? {
        didSet {
            if let count = self.pickerTitles?.count, count > 0 {
                self.pickerView.dataSource = self
                self.pickerView.delegate = self
                self.pickerView.reloadComponent(0)
            }
        }
    }
    
    var selectedIndex:Int {
        set {
            self.pickerView.selectRow(newValue, inComponent: 0, animated: false)
        }
        get {
            return self.pickerView.selectedRow(inComponent: 0)
        }
//        didSet {
//            if let count = self.pickerTitles?.count, count > self.selectedIndex {
//                self.pickerView.selectRow(self.selectedIndex, inComponent: 0, animated: false)
//            }
//        }
    }
}

extension ApplicationPickerView: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.pickerTitles?.count ?? 0
    }

}

extension ApplicationPickerView: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return self.pickerTitles?[row] ?? ""
    }

    
}
