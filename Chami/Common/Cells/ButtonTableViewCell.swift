//
//  ButtonTableViewCell.swift
//  Chami
//
//  Created by Serg Smyk on 21/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum ButtonTableViewCellType: Int {
    case defaultStyle
    case fillStyle
}

class ButtonTableViewCell: UITableViewCell {

    @IBOutlet weak var button: UIButton!
    
    var buttonClickedBlock : ((_ sender: ButtonTableViewCell) -> Void)?
    
    class func cellHeight() -> CGFloat {
        return 64
    }
    
    @IBAction func onButtonClicked(_ sender: UISlider) {
        if let buttonClickedBlock = self.buttonClickedBlock {
            buttonClickedBlock(self)
        }
    }
    
    var cellStyle:ButtonTableViewCellType = .defaultStyle {
        didSet {
            switch self.cellStyle {
            case .defaultStyle:
                Theme.current.apply(borderedButton: self.button)
            case .fillStyle:
                Theme.current.apply(filledButton: self.button)
            }
        }
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.cellStyle = .defaultStyle
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellStyle = .defaultStyle
        self.selectedBackgroundView = UIView()
    }
    
    class func dequeueCell(tableView:UITableView) -> ButtonTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "ButtonTableViewCell") as? ButtonTableViewCell else {
            let separatorCellNib = UINib(nibName: String(describing: ButtonTableViewCell.self), bundle: nil)
            tableView.register(separatorCellNib, forCellReuseIdentifier: "ButtonTableViewCell")
            
            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: "ButtonTableViewCell") as? ButtonTableViewCell else {
                fatalError()
            }
            return cell2
        }
        return cell
    }
}
