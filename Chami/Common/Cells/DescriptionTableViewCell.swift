//
//  DescriptionTableViewCell.swift
//  Chami
//
//  Created by Serg Smyk on 21/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
let kDescriptionCellId:String = "DescriptionTableViewCell"

class DescriptionTableViewCell: UITableViewCell {


    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedBackgroundView = UIView()
        self.titleLabel.font = DescriptionTableViewCell.descriptionCellFont()
    }
    
    class func dequeueCell(tableView:UITableView) -> DescriptionTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: kDescriptionCellId) as? DescriptionTableViewCell else {
            let separatorCellNib = UINib(nibName: String(describing: DescriptionTableViewCell.self), bundle: nil)
            tableView.register(separatorCellNib, forCellReuseIdentifier: kDescriptionCellId)
            
            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: kDescriptionCellId) as? DescriptionTableViewCell else {
                fatalError()
            }
            return cell2
        }
        return cell
    }
    
    class func descriptionCellFont() -> UIFont {
        return FontHelper.defaultFont(withSize: 16)
    }
    
    class func cellHeight(tableView:UITableView, text:String) -> CGFloat {
        let borderX = 25.0 as CGFloat
        let borderY = 2.0 as CGFloat
        let width = tableView.frame.size.width - (borderX * 2)
        let font = DescriptionTableViewCell.descriptionCellFont()
        return text.heightWithConstrainedWidth(width: width, font: font) + (borderY * 2)
    }
}
