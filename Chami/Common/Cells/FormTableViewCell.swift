//
//  FormTableViewCell.swift
//  Stadio
//
//  Created by Igor Markov on 5/27/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol FormCellProtocol: NSObjectProtocol {
    
    weak var delegate: FormCellDelegate? { get set }
    
    var field: FormField? { get set }
    
    func setValidationState(_ state: ValidationState)
    func forceEndEditing()
    func activate()
    
}

protocol FormCellDelegate: NSObjectProtocol {
    
    
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didEnterString string: String?) where CellType: UITableViewCell
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didChangeString string: String?) where CellType: UITableViewCell
}

class FormTableViewCell: UITableViewCell {
    
    fileprivate weak var _delegate: FormCellDelegate?
    fileprivate var _field: FormField?
    
    @IBOutlet weak var valueTextField: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.valueTextField.delegate = self
        self.valueTextField.autocorrectionType = .no
        self.valueTextField.textColor = ColorHelper.defaultFormFieldTextColor()
        self.valueTextField.font = FontHelper.defaultFont(withSize: UI.formCellFontSize)
        
        self.selectionStyle = .none
        self.backgroundColor = UIColor.clear
    }
}

// MARK: - FormCellProtocol
extension FormTableViewCell: FormCellProtocol {
    
    weak var delegate: FormCellDelegate? {
        get { return _delegate }
        set (newDelegate) { _delegate = newDelegate }
    }
    
    var field: FormField? {
        get { return _field }
        set (newField) {
            
            _field = newField
            
            if let field = _field {
                self.valueTextField.text = field.stringFromValue()
                
                switch field.keyboardType {
                case .string: self.valueTextField.keyboardType = .default
                case .email: self.valueTextField.keyboardType = .emailAddress
                case .number: self.valueTextField.keyboardType = .numberPad
                }
                
                switch field.returnButtonType {
                case .next: self.valueTextField.returnKeyType = .next
                case .done: self.valueTextField.returnKeyType = .done
                case .notDefined: self.valueTextField.returnKeyType = .default
                }
                
                self.valueTextField.isSecureTextEntry = field.isSecure
                self.setValidationState(field.validationState)
            } else {
                self.valueTextField.text = nil
                self.valueTextField.keyboardType = .default
                self.valueTextField.returnKeyType = .default
                self.valueTextField.isSecureTextEntry = false
                self.setValidationState(.undefined)
            }
            
            self.updateConstraints()
        }
    }
    
    func setValidationState(_ state: ValidationState) {
        switch state {
        case .undefined: self.valueTextField.textColor = ColorHelper.defaultFormFieldTextColor()
        case .correct: self.valueTextField.textColor = ColorHelper.defaultFormFieldTextColor()
        case .incorrect: self.valueTextField.textColor = ColorHelper.incorrectFormFieldTextColor()
        }
    }
    
    func forceEndEditing() {
        self.valueTextField.resignFirstResponder()
    }
    
    func activate() {
        self.valueTextField.becomeFirstResponder()
    }
    
}

// MARK: - UITextFieldDelegate
extension FormTableViewCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        if let delegate = self.delegate {
            delegate.cell(self, didChangeString: txtAfterUpdate as String)
        }
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let delegate = self.delegate {
            delegate.cell(self, didEnterString: textField.text)
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.setValidationState(.undefined)
    }
    
}
