//
//  TitleValueCell.swift
//  Chami
//
//  Created by Igor Markov on 3/16/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class TitleValueCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 17)
        self.valueLabel.font = FontHelper.defaultFont(withSize: 17)
    }
    
    class func getDefaultSeparatorInset() -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 10)
    }
}
