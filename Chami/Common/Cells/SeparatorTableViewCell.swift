//
//  SeparatorTableViewCell.swift
//  Chami
//
//  Created by Serg Smyk on 21/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

let kSeparatorCellId:String = "SeparatorTableViewCell"

class SeparatorTableViewCell: UITableViewCell {

    class func separatorHeight() -> CGFloat {
        return 26
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectedBackgroundView = UIView()
    }

    class func dequeueCell(tableView:UITableView) -> SeparatorTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: kSeparatorCellId) as? SeparatorTableViewCell else {
            let separatorCellNib = UINib(nibName: String(describing: SeparatorTableViewCell.self), bundle: nil)
            tableView.register(separatorCellNib, forCellReuseIdentifier: kSeparatorCellId)

            guard let cell2 = tableView.dequeueReusableCell(withIdentifier: kSeparatorCellId) as? SeparatorTableViewCell else {
                fatalError()
            }
            return cell2
        }
        return cell
    }
    
    
}
