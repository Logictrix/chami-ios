//
//  SoundPlayback.swift
//  Chami
//
//  Created by Serg Smyk on 07/06/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import AudioToolbox

class SoundPlayback: NSObject {
    var soundEffect: SystemSoundID = 0
    
    init(name: String, type: String) {
        let path  = Bundle.main.path(forResource: name, ofType: type)!
        let pathURL = NSURL.fileURL(withPath: path)
        AudioServicesCreateSystemSoundID(pathURL as CFURL, &soundEffect)
    }
    
    func play() {
        AudioServicesPlaySystemSound(soundEffect)
    }
    
    func stopPlayback() {
        AudioServicesDisposeSystemSoundID(soundEffect)
    }
    
    static func playerWithDefaultSound() -> SoundPlayback{
        let soundPlayback = SoundPlayback.init(name: "sound", type: "caf")
        return soundPlayback
    }

    static func playDefaultSound() {
        let soundPlayback = self.playerWithDefaultSound()
        soundPlayback.play()
    }
    
}
