//
//  PagingModel.swift
//  Stadio
//
//  Created by Igor Markov on 5/25/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol PagingModelProtocol: NSObjectProtocol {

    associatedtype PageObjectType
    func items() -> [PageObjectType]
    func replaceItem(withId itemId: ObjectIdType, byItem newItem: PageObjectType)
    func deleteItem(withId itemId: ObjectIdType)
    func clearData()

    associatedtype SortClosureType = ((PageObjectType, PageObjectType) -> Bool)
    var sortClosure: SortClosureType? { get set }

    func loadData(_ complition: ((NSError?) -> ())?)
    func reloadData(_ complition: ((NSError?) -> ())?)
    func reloadAllData(_ complition: ((NSError?) -> ())?)
}


// TODO: - simplify generics
class PagingModel<PagingModelObjectType: PagingObjectProtocol, PagingServiceType: PagingServiceProtocol>: NSObject where PagingModelObjectType == PagingServiceType.PagingServiceObjectType {

    typealias PageObjectType = PagingModelObjectType
    typealias SortClosureType = ((PageObjectType, PageObjectType) -> Bool)

    let pageSize: UInt
    var numberOfLoadedPages: Int = 0

    fileprivate var pageObjects: [PageObjectType] = []
    var _sortClosure: SortClosureType?

    let pagingService: PagingServiceType

    init(pageSize: UInt, pagingService: PagingServiceType) {

        self.pageSize = pageSize
        self.pagingService = pagingService
    }

    // MARK: - implementation

    fileprivate func getMoreObjects(_ complition: @escaping ([PageObjectType], NSError?) -> ()) {

        let pageToLoad = numberOfLoadedPages

        self.getObjects(pageToLoad, complition: complition)
    }

    fileprivate func reloadPage(_ complition: @escaping ([PageObjectType], NSError?) -> ()) {

        numberOfLoadedPages = 0

        self.getMoreObjects(complition)
    }

    fileprivate func processLoadedData(_ objectArray: [PageObjectType]) {

        let newObjects = Array.init(objectArray)

        defer {
            if let sorter = sortClosure {
                pageObjects.sort(by: sorter)
            }
        }

        if (numberOfLoadedPages == 1) {
            pageObjects = newObjects
            return
        }

        for newObject in newObjects {
            if let foundObjectIndex = pageObjects.index(where: { return $0.identifier == newObject.identifier }) {
                pageObjects[foundObjectIndex] = newObject
            } else {
                pageObjects.append(newObject)
            }
        }
    }

    fileprivate func getObjects(_ page: Int, complition: (([PageObjectType], NSError?) -> ())?) {

        self.pagingService.getObjects(page, count: pageSize, success: { (pageObjects) in

            self.numberOfLoadedPages += 1
            if let complition = complition {
                complition(pageObjects, nil)
            }

        }) { (error) in
            if let complition = complition {
                let resultError = NSError(domain: AppError.errorDomain, code: AppError.unsuccessResultErrorCode, userInfo: [NSUnderlyingErrorKey: error])
                complition([], resultError)
            }
        }
    }

    fileprivate func getAllCurrentObjects(withCompletion complition: (([PageObjectType], NSError?) -> Void)?) {

        if numberOfLoadedPages == 0 {
            if let complition = complition {
                complition([], nil)
            }
            return
        }

        let loadedObjectNumber = UInt(numberOfLoadedPages) * pageSize
        self.pagingService.getObjects(0, count: loadedObjectNumber, success: { (pageObjects) in

            if let complition = complition {
                complition(pageObjects, nil)
            }
        }) { (error) in
            if let complition = complition {
                let resultError = NSError(domain: AppError.errorDomain, code: AppError.unsuccessResultErrorCode, userInfo: [NSUnderlyingErrorKey: error])
                complition([], resultError)
            }
        }
    }
}

// MARK: - PagingModelProtocol

extension PagingModel: PagingModelProtocol {

    func items() -> [PageObjectType] {

        return pageObjects
    }

    func replaceItem(withId itemId: ObjectIdType, byItem newItem: PageObjectType) {

        if let foundObjectIndex = pageObjects.index(where: { return $0.identifier == itemId }) {
            pageObjects[foundObjectIndex] = newItem
        }
    }

    func deleteItem(withId itemId: ObjectIdType) {

        if let foundObjectIndex = pageObjects.index(where: { return $0.identifier == itemId }) {
            pageObjects.remove(at: foundObjectIndex)
        }
    }

    func clearData() {

        numberOfLoadedPages = 0
        pageObjects = []
    }

    var sortClosure: SortClosureType? {
        get { return _sortClosure }
        set {
            _sortClosure = newValue

            if let sorter = _sortClosure {
                pageObjects.sort(by: sorter)
            }
        }
    }

    func loadData(_ complition: ((NSError?) -> ())?) {

        self.getMoreObjects { (pageObjects, error) in

            if error != nil, let complition = complition {
                complition(error)
                return
            }

            self.processLoadedData(pageObjects)

            if let complition = complition {
                complition(nil)
            }
        }
    }

    func reloadData(_ complition: ((NSError?) -> ())?) {

        self.reloadPage { (pageObjects, error) in

            if let error = error {
                if let complition = complition {
                    complition(error)
                }
                return
            }

            self.processLoadedData(pageObjects)

            if let complition = complition {
                complition(nil)
            }
        }
    }

    func reloadAllData(_ complition: ((NSError?) -> ())?) {

        self.getAllCurrentObjects { (pageObjects, error) in

            if let error = error {
                if let complition = complition {
                    complition(error)
                }
                return
            }

            self.processLoadedData(pageObjects)

            if let complition = complition {
                complition(nil)
            }
        }
    }

}
