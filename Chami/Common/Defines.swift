//
//  Defines.swift
//  Chami
//
//  Created by Igor Markov on 1/31/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Appication types
typealias IdType = String
typealias SessionSkillDataType = (skillName: String, skillValue: Int, skillLogoUrl: String, isEditable: Bool)
typealias HelpAndFaqDataType = (title: String, description: String, attributedDescription: NSAttributedString?)

// MARK: - Common defines
enum Define {
    static let notDefinedId: IdType = "-1"
    static let osType: String = "ios"
}

// MARK: - Temp development defines
enum Temp {
    static let stubAccountId: IdType = "0"
    static let stubAccountToken: IdType = "stub_token"
}

// MARK: - Third-party api constants etc
enum Service {
}

// MARK: - Notification Center constants
enum Notification {
    static let userDidChange = "userDidChangeNotification"
    static let userDidUpdate = "userDidUpdateNotification"
}

// MARK: - Application error domains and error codes
enum AppError {
    static let errorDomain = "com.chami.error.data"
    static let errorNetworkDomain = "com.chami.error.network"
    static let errorModelDomain = "com.chami.error.model"
    
    static let noCredentialsErrorCode = 5000
    static let expiredCredentialsErrorCode = 5001
    static let responseErrorCode = 5100
    static let unexpectedResponseErrorCode = 5200
    static let invalidParametersErrorCode = 5300
    static let invalidContextErrorCode = 5301
    static let unsuccessResultErrorCode = 5400
    static let cancelResultErrorCode = 5500
}

enum AppApiServices {
    static let googleApiKey: String = "AIzaSyDkvFQDKHONQGqq7AhSd_g-QYabV-EPhOg" // "AIzaSyDOnpao3rvHXZnr04xamWrWxcFFdP5_1oY"
    
    static let stripePublishableKey: String =
//    "pk_test_lkelhWhn9ZsuvkbaPH9v62di" // - test NEW // Stage
//    "pk_test_RGtSvSh9BNxECdnqcRw7VZ1Q"  // - test
    "pk_live_56vWEtYMtBL4PTCMMks9jD05" // - live
    
    static let stripeSecretKey: String =
//    "sk_test_ccRw0d7FoF71QpgKt4vwZgTr" // - test NEW // Stage
//    "sk_test_YJZj1MMn4XYOY3XZkbCnutKC" // - test
    "sk_live_LtdjwK1TkgUzyfbpY0nbNPv2" //- live

}


