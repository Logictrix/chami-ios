//
//  ConstantsUI.swift
//  Chami
//
//  Created by Igor Markov on 1/31/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

enum UI {
    
    // Sizes
    static let cornerRadius: CGFloat = 4
    static let buttonBorderWidth: CGFloat = 3
    
    // Durations
    static let animationDuration: TimeInterval = 0.3
    
    // FontSizes:
    static let headerFontSize: CGFloat = 24
    static let buttonFontSize: CGFloat = 18
    static let formCellFontSize: CGFloat = 16
    static let labelFontSize: CGFloat = 16

    static let tableCellMediumHeight: CGFloat = 64
}
