//
//  BaseTableViewDataSource.swift
//  Stadio
//
//  Created by Igor Markov on 5/30/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class BaseTableViewDataSource: NSObject {
    
    func activateNextCell(forCellAtIndexPath indexPath: IndexPath, tableView: UITableView) {
        
        var row = indexPath.row
        var section = indexPath.section
        
        var isLastCell = false
        
        row += 1
        if row >= tableView.numberOfRows(inSection: section) {
            
            row = 0
            section += 1
            
            if section >= tableView.numberOfSections {
                row = 0; section = 0
                isLastCell = true
            }
        }
        
        if isLastCell {
            return
        }
        
        let nextActiveCellIndexPath = IndexPath(row: row, section: section)
        if let nextFormCell = tableView.cellForRow(at: nextActiveCellIndexPath) as? FormCellProtocol {
            nextFormCell.activate()
        }
    }
}

extension BaseTableViewDataSource: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        assert(false, "should be implement")
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        assert(false, "should be implement")
        return UITableViewCell()
    }
}
