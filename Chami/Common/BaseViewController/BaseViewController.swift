//
//  BaseViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/27/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SideMenu
import MobileCoreServices
import PKHUD

class BaseViewController<ModelType, ViewType, RouterType>: MVCViewController<ModelType, ViewType, RouterType>, SessionManagerDelegate, UIGestureRecognizerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    deinit {
        SessionManager.shared.multicastDelegate.removeDelegate(self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
        self.navigationItem.backBarButtonItem?.tintColor = UIColor.white
        self.edgesForExtendedLayout = UIRectEdge()
        
        let gesture = UITapGestureRecognizer.init(target: self, action: #selector(hideKeyboard))
        gesture.delegate = self
        gesture.cancelsTouchesInView = false
        self.view.addGestureRecognizer(gesture)
        
        SessionManager.shared.multicastDelegate.addDelegate(self)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.view.endEditing(animated)
    }
    
    func setProgressVisible(visible:Bool) {
        if visible {
            HUD.show(.progress)
        } else {
            HUD.hide(animated: true)
        }
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let view = touch.view else {
            return true
        }
        
        if view.isKind(of: UITableViewCell.self) || view.isKind(of: UIControl.self) {
            return false
        }
        if let superview = view.superview {
            if superview.isKind(of: UITableViewCell.self) {
                return false
            }
            if let superview = superview.superview {
                if superview.isKind(of: UITableViewCell.self) {
                    return false
                }
            }
        }
        return true
    }
    
    @objc func hideKeyboard() {
        self.view.endEditing(true)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    func addSideMenuLeftButton() {
        if let sideMenuImage = UIImage(named: "top_bar_btn_burg") {
            let sideMenuButton = UIBarButtonItem(image: sideMenuImage, style: .plain, target: self, action: #selector(showSideMenu))
            self.navigationItem.leftBarButtonItem = sideMenuButton
        }
    }

    @objc func showSideMenu() {
        if let leftMenuNavigationController = SideMenuManager.defaultManager.menuLeftNavigationController {
            self.present(leftMenuNavigationController, animated: true, completion: nil)
        }
    }

    
    
    //:Video/Images selection
    var showVideoPickerFinishBlock : ((NSURL) -> Void)?
    var showImagePickerFinishBlock : ((UIImage) -> Void)?
    var imageMediaPicker: UIImagePickerController?
    var videoMediaPicker: UIImagePickerController?
    
    func showChoosePhotoActionSheet(completion: @escaping (_ image:UIImage) -> Void) {
        let alertController = UIAlertController(title: "Choose Photo Source", message: nil, preferredStyle: .actionSheet)
            
        let alertAction1 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            //
        }
        alertController.addAction(alertAction1)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let alertAction2 = UIAlertAction(title: "Camera", style: .default) { (action) in
                self.showImagePicker(type: .camera, completion: { (image) in
                    completion(image)
                })
            }
            alertController.addAction(alertAction2)

        }
        let alertAction3 = UIAlertAction(title: "Library", style: .default) { (action) in
            self.showImagePicker(type: .photoLibrary, completion: { (image) in
                completion(image)
            })
        }
        alertController.addAction(alertAction3)

        self.present(alertController, animated: true) { 
            //
        }
    }
    
    func showChooseVideoActionSheet(completion: @escaping (_ url:NSURL) -> Void) {
        let alertController = UIAlertController(title: "Choose Video Source", message: nil, preferredStyle: .actionSheet)
        
        let alertAction1 = UIAlertAction(title: "Cancel", style: .cancel) { (action) in
            //
        }
        alertController.addAction(alertAction1)
        
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            let alertAction2 = UIAlertAction(title: "Camera", style: .default) { (action) in
                self.showVideoPicker(type: .camera, completion: { (url) in
                    completion(url)
                })
            }
            alertController.addAction(alertAction2)
            
        }
        let alertAction3 = UIAlertAction(title: "Library", style: .default) { (action) in
            self.showVideoPicker(type: .photoLibrary, completion: { (url) in
                completion(url)
            })
        }
        alertController.addAction(alertAction3)
        
        self.present(alertController, animated: true) {
            //
        }
    }

    
    
    func showImagePicker(type: UIImagePickerController.SourceType, completion: @escaping (_ image:UIImage) -> Void) {
        self.imageMediaPicker = UIImagePickerController()
    //    self.imageMediaPicker?.delegate = self
        if let picker = self.imageMediaPicker {
//            picker.mediaTypes = [kUTTypePNG as String]
            picker.sourceType = type
//            picker.allowsEditing = true
            picker.delegate = self
            self.present(picker, animated: true, completion: nil)
            
            self.showImagePickerFinishBlock = completion
        }
    }
    
    func showVideoPicker(type: UIImagePickerController.SourceType, completion: @escaping (_ url:NSURL) -> Void) {
        self.videoMediaPicker = UIImagePickerController()
        if let picker = self.videoMediaPicker {
            picker.mediaTypes = [kUTTypeMovie as String]
            picker.sourceType = type
            picker.allowsEditing = true
            picker.delegate = self
            picker.videoMaximumDuration = 30.0
            self.present(picker, animated: true, completion: nil)
            
            self.showVideoPickerFinishBlock = completion
        }
    }
    
     func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true) {
            if picker == self.videoMediaPicker {
                guard let videoUrl = info[UIImagePickerController.InfoKey.mediaURL] as? NSURL else {
                    return
                }

                if let showVideoPickerFinishBlock = self.showVideoPickerFinishBlock {
                    showVideoPickerFinishBlock(videoUrl)
                }
                
            } else if picker == self.imageMediaPicker {
                guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else {
                    return
                }
                
                if let showImagePickerFinishBlock = self.showImagePickerFinishBlock {
                    showImagePickerFinishBlock(image.fixedOrientation())
                }
            }
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true) { }
    }
    
    var isVisible: Bool {
        get {
            //UIApplication
            return (self.view.window != nil)
//            if let sideNav = NavigationManager.shared.sideMenuNavigationController {
//                //SideMenuViewController
//                //UIPresentationController
//                //SideMenuManager.menu
//                print("sideNav = \(String(describing: sideNav.topViewController?.presentationController?.presentingViewController))")
//                if let nav = sideNav.presentingViewController as? UINavigationController {
//                    if nav.topViewController == self {
//                        return true
//                    }
//                    if nav.presentingViewController == self || nav.topViewController?.presentingViewController == self {
//                        return true
//                    }
//                }
//            }
//            return false
        }
    }

    // MARK: - SessionManagerDelegate
     @objc var canHandleMeetupRequests = true
    /*
    fileprivate var sessionRequestsControllerPopups = NSMutableArray()
    func didReceivedMeetUpRequest(session: MeetUpSession) {
//        return
        guard self.canHandleMeetupRequests == true else {
            return
        }
        guard self.isVisible == true else {
            return
        }
//        if self.isKind(of: HomeViewController.self) == false {
//            return
//        }
        guard let parentVC = self.view.window?.rootViewController else {
            return
        }
        
        let controller = MeetUpRequestBuilder.viewControllerForSessionRequest(sessionRequest: session)
        parentVC.view.addSubview(controller.view)
        parentVC.addChildViewController(controller)
        self.sessionRequestsControllerPopups.add(controller)
        controller.answerSelectBlock = {[weak self](controller: MeetUpRequestViewController, answer: Bool) in
            guard let strongSelf = self else { return }
            strongSelf.hideMeetUpRequestPopup(controller: controller)
            
            if answer == false {
                controller.model.cancelCurrentSession(success: { 
                    
                }, failure: { (error) in
                    
                })
                
            } else {
                strongSelf.hideAllMeetUpRequests()
                strongSelf.setProgressVisible(visible: true)
                controller.model.startCurrentSession(success: { [weak self] in
                    guard let strongSelf = self else { return }
                    strongSelf.setProgressVisible(visible: false)
                    
//                    if strongSelf.isKind(of: HomeViewController.self) == false {
//                        NavigationManager.shared.showHomeScreen(animated: false)
//                    }
                    
                    let nextVC = ChamiConfirmedTutorBuilder.viewController()
                    nextVC.setMeetUpSession(meetUpSession: session)
                    //let topVC = NavigationManager.shared.sideMenuNavigationController?.topViewController
                    strongSelf.navigationController?.pushViewController(nextVC, animated: false)
                    
                }) { [weak self] (error) in
                    guard let strongSelf = self else { return }
                    strongSelf.setProgressVisible(visible: false)
                    AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: nil)
                    
                }
            }
        }
        self.layoutSessionRequests()
    }
    
    func layoutSessionRequests() {
        for i in 0...self.sessionRequestsControllerPopups.count - 1
        {
            let controller = self.sessionRequestsControllerPopups[i] as! UIViewController
            guard let superview = controller.view.superview else {
                continue
            }
            //controller.view.frame = CGRect(x: 0, y: superview.frame.size.height - controller.view.frame.size.height, width: superview.frame.size.width, height: controller.view.frame.size.height)
            controller.view.frame = superview.bounds
            
        }
    }
    func hideAllMeetUpRequests() {
        var i = self.sessionRequestsControllerPopups.count - 1
        while i >= 0 {
            let controller = self.sessionRequestsControllerPopups[i] as! MeetUpRequestViewController
            self.hideMeetUpRequestPopup(controller: controller)
            i -= 1
        }
    }
    
    func hideMeetUpRequestPopup(controller: UIViewController) {
        self.sessionRequestsControllerPopups.remove(controller);
        controller.view.removeFromSuperview()
        controller.removeFromParentViewController()
    }
    
    func didReceivedMeetUpRequestCancel(session: MeetUpSession) {
        
        // hide popup
        var i = self.sessionRequestsControllerPopups.count - 1
        while i >= 0 {
            
            let controller = self.sessionRequestsControllerPopups[i] as! MeetUpRequestViewController
            if controller.model.currentMeetUpSession?.session_id == session.session_id {
                self.hideMeetUpRequestPopup(controller: controller)
            }
            i -= 1
        }
        
    }
    */
}

extension BaseViewController {
    func showAlert(error:String) {
        self.showAlert(title: "Error", message: error)
    }
    func showAlert(text:String) {
        self.showAlert(title: text, message: "")
    }
    
    func showAlert(title:String, message:String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: ""), style: UIAlertAction.Style.cancel, handler: { action in }))
        self.present(alert, animated: true, completion: nil)
    }
}

