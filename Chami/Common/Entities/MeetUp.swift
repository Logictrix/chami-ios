//
//  MeetUp.swift
//  Chami
//
//  Created by Pavel Korinenko on 3/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

class MeetUp: NSObject {
    
    var studentFullName: String = ""
    var meetUpId: IdType = ""
    var studentImageUrl: String = ""
    var languageId: IdType = ""
//    var latitude: NSNumber = NSNumber(value: 0)
//    var longitude: NSNumber = NSNumber(value: 0)

   
    func updateFromSWGObject(swgObject: SWGMeetupModel) -> Bool {
        self.studentFullName = swgObject.fullName
        if let meetUpIdUnwrap = swgObject._id {
            self.meetUpId = String("\(meetUpIdUnwrap)")
        }
        if let languageIdUnwrap = swgObject.nativeLanguage {
            self.languageId = String("\(languageIdUnwrap)")
        }
        if let url = swgObject.imageUrl {
            self.studentImageUrl = url
        }
        self.studentFullName = swgObject.fullName

        return true
    }
    
//    fullName = "stu stu";
//    id = 6;
//    imageUrl = "https://s3.eu-west-2.amazonaws.com/chami-media/7F79CEE3-CC3D-4DC6-904F-AD7596350E9C";
//    nativeLanguage = English;

}
