//
// Created by Igor Markov on 5/26/16.
// Copyright (c) 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class FormField: NSObject {
    
    var key: String?
    var value: AnyObject?
    var validationState: ValidationState = .undefined
    var keyboardType: KeyboardType = .string
    var fieldType: FormFieldType = .none
    var returnButtonType: ReturnButtonType = .notDefined
    var isSecure: Bool = false

    func stringFromValue() -> String {
        if let valueString = value as? String {
            return valueString
        } else if let valueDate = value as? Date {
            return DateConverter.string(fromDate: valueDate)
        } else {
            return ""
        }
    }
}
