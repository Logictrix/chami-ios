//
// Created by Igor Markov on 5/26/16.
// Copyright (c) 2016 DB Best Technologies LLC. All rights reserved.
//

import Foundation

enum FormFieldType {
    case none
    case email
    case password
    case confirmPassword
    case oldPassword
    case firstName
    case lastName
}
