//
//  ReturnButtonType.swift
//  Stadio
//
//  Created by Igor Markov on 5/26/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation

enum ReturnButtonType {
    case notDefined
    case next
    case done
}
