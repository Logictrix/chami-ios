//
//  Language.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/13/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

class Language: NSObject {
    
    
    var languageId: IdType = ""
    var languageIdNumber:NSNumber {
        get {
            if self.languageId.characters.count > 0 {
                let langIdInt = Int(self.languageId)
                return NSNumber.init(value: langIdInt!)
            }
            return NSNumber.init(value: 0)

        }
    }
    
    var languageName: String = ""
    var languageImageUrl: String?
    
    var level: Float! = 0.0
    
    var pricePlus: Float! = 1
    var priceMax: Float! = 1
    var languageCost: Float {
        set {
            if AccountManager.shared.currentUser!.chamiType == .chamiPlus {
                self.pricePlus = newValue
            } else {
                self.priceMax = newValue
            }
        }
        get {
            return (AccountManager.shared.currentUser!.chamiType == .chamiPlus) ? self.pricePlus : self.priceMax
        }
    }
    func priceString(price:Float) -> String {
        return String.init(format: "￡%.1f, p/h", price)
    }
    
    
//    var languageCostChamiPlus: Double = 1.0
//    var languageCostChamiMax: Double = 1.0
    
    func updateFromSWGObject(swgObject: SWGLanguageModel) -> Bool {
        self.languageId = String("\(swgObject._id!)")
        self.languageName = swgObject.name
        self.languageImageUrl = swgObject.imageUrl
        return true
    }
    
    func updateFromSWGObject(objeSWGUserLanguageModel swgObject: SWGUserLanguageModel) -> Bool {
        if let languageId = swgObject.languageId {
            self.languageId = String("\(languageId)")
        }
        if let imageUrl = swgObject.imageUrl {
            self.languageImageUrl = imageUrl
        }
        if let level = swgObject.level {
            self.level = level.floatValue
        }
        if let price = swgObject.price {
            self.languageCost = price.floatValue
        }
        if let languageName = swgObject.languageName {
            self.languageName = languageName
        }
        
        return true
    }

//    "id": 1,
//    "name": "English",
//    "imageUrl": "http://img.freeflagicons.com/thumb/round_icon/united_kingdom/united_kingdom_640.png"

}
