//
//  User.swift
//  Chami
//
//  Created by Igor Markov on 2/1/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient
import MapKit

class User: NSObject, NSCoding {
    
    var id: IdType {
        didSet {
            
        }
    }
    var locationLatitude: Double = 0
    var locationLongitude: Double = 0

    var firstName: String? = nil
    var lastName: String? = nil
    var rating = 0.0
    
    private var _fullName: String? = nil
    var fullName: String? {
        set {
            self._fullName = newValue
        }
        get {
            if self._fullName == nil {
                var res = ""
                if let firstName = self.firstName {
                    res.append(firstName)
                }
                if let lastName = self.lastName {
                    if res.characters.count > 0 {
                        res.append(" ")
                    }
                    res.append(lastName)
                }
                return res
            } else {
               return self._fullName
            }

        }
    }
    var gender: String!
    var birthDate: Date!
    var introVideoUrl: String! // TODO: use NSUrl
    var imageUrl: String! // TODO: use NSUrl
    var about: String! = ""
    var teachingLanguages: [Language]! = []
    var nativeLanguage: Language!
    var nationality: String? = nil
    var phoneNumber: String? = nil
    var email: String? = nil
    
    var distanceToCurrentUser: String {
        get {
            if (AccountManager.shared.currentUser != nil) {
                let userLocation = CLLocation(latitude: self.locationLatitude, longitude: self.locationLongitude)
                let currentUserLocation = CLLocation(latitude: LocationManager.shared.latitude, longitude: LocationManager.shared.longitude)
                
                let strLocation = LocationHelper.stringDistanceInMiles(fromLocation: currentUserLocation, toLocation: userLocation)
                return strLocation

            } else {
                return ""
            }

        }
    }
    
    init(withId id: IdType) {
        self.id = id
    }
    
    func updateGender(gender: String?) {
        if gender != nil {
            if gender == SetupProfileGenderType.male.rawValue {
                self.gender = SetupProfileGenderType.male.rawValue
                
            } else if gender == SetupProfileGenderType.female.rawValue {
                self.gender = SetupProfileGenderType.female.rawValue
                
            } else {
                self.gender = SetupProfileGenderType.notDefined.rawValue
            }
        } else {
            self.gender = SetupProfileGenderType.notDefined.rawValue
        }
    }
    
    func updateFromSWGObject(objSWGChamiProfileModel swgObject: SWGChamiProfileModel) -> Bool {

        self.locationLatitude = (swgObject.latitude as! Double?)!
        self.locationLongitude = (swgObject.longitude as! Double?)!

        self.fullName = swgObject.fullName as String
        self.updateGender(gender: swgObject.gender)
        
        
        if let bdate = swgObject.birthDate {
            self.birthDate =  bdate
        }
        self.introVideoUrl = swgObject.introVideoUrl as String?
        self.imageUrl = swgObject.imageUrl as String?
        self.about = swgObject.about as String?
        if let ratingUnwrap = swgObject.rating {
            self.rating = ratingUnwrap.doubleValue
            
        }

        var resultLanguages: [Language] = []
        if let languagesModels = swgObject.languages as? [SWGUserLanguageModel] {
            
            for languageModel in languagesModels {
                let language = Language()
                if language.updateFromSWGObject(objeSWGUserLanguageModel: languageModel) {
                    resultLanguages.append(language)
                }
            }
        }

        self.teachingLanguages = resultLanguages

        return true
    }
    
    func updateFromSWGObject(objSWGChamiModel swgObject: SWGChamiModel) -> Bool {
        self.locationLatitude = (swgObject.latitude as! Double?)!
        self.locationLongitude = (swgObject.longitude as! Double?)!
        return true
    }

    // MARK: - NSCoding
    fileprivate static let userIdKey = "identifier"
    required init?(coder aDecoder: NSCoder) {
        
        self.id = aDecoder.decodeObject(forKey: User.userIdKey) as! IdType
    }
    
    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.id, forKey: User.userIdKey)
    }
    
    fileprivate let chamiService = ChamiService()
    func updateTutorProfile(completion: @escaping (_ error:Error?) -> Void) {
        guard let account = AccountManager.shared.currentUser else {
            return
        }
        self.chamiService.updateTutorDetail(tutor: account) { (error) in
            completion(error)
        }
    }
}
