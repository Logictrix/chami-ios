//
//  Account.swift
//  Chami
//
//  Created by Igor Markov on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

enum ChamiLevelType: String {
    case chamiX = "ChamiX"
    case chamiPlus = "ChamiPlus"
    case chamiMax = "ChamiMax"
    
    static func fromInt(value: Int) -> ChamiLevelType{
        switch value {
        case 0:
            return .chamiX
        case 1:
            return .chamiPlus
        case 2:
            return .chamiMax
        default:
            return .chamiX
        }
    }
}


class Account: User {
    var token: String? = nil
    var authorization:String? {
        get {
            guard let token = self.token else {
                return nil
            }
            let tokenBearer = "bearer \(token)"
            return tokenBearer
        }
    }
    var isOnline: Bool = false {
        didSet {
            if let token = self.token {
                if token.characters.count > 0 {
                    NotificationsManager.shared.setNotificationEnabled(enabled: self.isOnline)
                }
            }
        }
    }

    var chamiType: ChamiLevelType = .chamiPlus
    var isCertificated: Bool = false
    var certificates:[String] = []
    
    var role: AccountRole = .notDefined
    
    var selectedLanguage: Language?

    var stripeKeyStudent: String?
    var stripeKeyTutor: String?

    override init(withId id: IdType) {
        super.init(withId: id)
    }
    
    func isAuthorized() -> Bool {
        if let token = self.token {
            return token.characters.count > 0
        } else {
            return false
        }
    }

    func hasCompletedProfile(withRole role: AccountRole) -> Bool {
        
        switch role {
        case .student:
            if (self.nativeLanguage == nil
                || self.introVideoUrl == nil || self.introVideoUrl.characters.count == 0
                || self.about == nil || self.about.characters.count == 0
                //|| self.nationality == nil
                //|| self.birthDate == nil
                //|| self.phoneNumber == nil
                //|| self.gender == nil
                ) {
                return false
            }
        case .tutor:
            if (self.introVideoUrl == nil || self.introVideoUrl.characters.count == 0
                || self.about == nil || self.about.characters.count == 0
                //|| self.teachingLanguages == nil || self.teachingLanguages.count == 0
                //|| self.nationality == nil
                //|| self.birthDate == nil
                //|| self.phoneNumber == nil
                //|| self.gender == nil
                ) {
                return false
            }
        default:
            return false
        }        
        
        
        // check for name
        guard let firstName = self.firstName,
            let lastName = self.lastName  else {
            return false
        }

        if firstName.characters.count == 0 {
            return false
        }

        if lastName.characters.count == 0 {
            return false
        }
        
        return true
    }

    // MARK: - NSCoding
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        self.token = aDecoder.decodeObject(forKey: Account.tokenKey) as? String
    }
    
    override func encode(with aCoder: NSCoder) {
        super.encode(with: aCoder)
        aCoder.encode(self.token, forKey: Account.tokenKey)
    }
    
    fileprivate static let tokenKey = "token"
    
    
    fileprivate let profileService = ProfileService()
    
    func updateFromSWGObject(objSWGProfileModel swgObject: SWGProfileModel, completion: @escaping (_ error:Error?) -> Void) {
        if (swgObject._id != nil) {
            self.id = swgObject._id.stringValue
        }
        
        if ((swgObject.firstName) != nil)
        {
            self.firstName = swgObject.firstName as String
        }
        if ((swgObject.lastName) != nil)
        {
            self.lastName = swgObject.lastName as String
        }
        if ((swgObject.email) != nil)
        {
            self.email = swgObject.email as String
        }
        
        if swgObject.isOnline != nil {
            self.isOnline = swgObject.isOnline.boolValue
        }
        
        self.updateGender(gender: swgObject.gender)        
        
        if ((swgObject.studentStripeKey) != nil)
        {
            self.stripeKeyStudent = swgObject.studentStripeKey as String
        }
        if ((swgObject.tutorStripeKey) != nil)
        {
            self.stripeKeyTutor = swgObject.tutorStripeKey as String
        }
        
        
        
        if ((swgObject.imageUrl) != nil)
        {
            self.imageUrl = swgObject.imageUrl as String
        }
        if ((swgObject.nationality) != nil)
        {
            self.nationality = swgObject.nationality as String
        }
        if ((swgObject.phoneNumber) != nil)
        {
            self.phoneNumber = swgObject.phoneNumber as String
        }
        if ((swgObject.birthDate) != nil)
        {
            self.birthDate = (swgObject.birthDate as NSDate) as Date!
            
        }
        
        if ((swgObject.introVideoUrl) != nil && (swgObject.introVideoUrl) != "string")
        {
            self.introVideoUrl = swgObject.introVideoUrl as String
        }
        if ((swgObject.about) != nil)
        {
            self.about = swgObject.about as String
        }
        
        
        
        if ((swgObject.nativeLanguageId) != nil)
        {
            SettingsService().getAvailableLanguages(success: { (languages) in
                
                let language = languages.filter({(language) in
                    return Bool(language.languageIdNumber == swgObject.nativeLanguageId)
                })
                
                AccountManager.shared.currentUser?.nativeLanguage = language.first
                completion(nil)
                
            }) { (error) in
                print(error.localizedDescription)
                completion(nil)
                //failure(error as NSError)
            }
            
            
            
            
            //AccountManager.shared.currentUser?.nativeLanguage?.languageId = swgObject.nativeLanguageId.stringValue
        }
        else
        {
            completion(nil)
        }
    }

    
    func updateProfileWithFinishBlock(completion: @escaping (_ error:Error?) -> Void) {
        
        self.checkProfileTypeWithFinishBlock(completion: { (error:Error?) in
            if error != nil {
                completion(error)
                return
            }
            
            self.profileService.getProfileInfo(success: { account in
                if let accountInfo = account  {
                    self.updateFromSWGObject(objSWGProfileModel: accountInfo, completion: { (error:Error?) in
                        
                        if (AccountManager.shared.currentUser?.role == .tutor)
                        {
                            self.profileService.getProfileLanguages(account: self, success: {
                                completion(nil)
                            }, failure: { error in
                                completion(error)
                            })
                        }
                        else
                        {
                            completion(nil)
                        }
                    })
                }
                else
                {
                    completion(nil)
                }
                
            }, failure: { error in
                completion(error)
            })
        })
        
        
        
    }
    
    var accountStatusApi: SWGAccountStatusApi {
        get {
            return SWGAccountStatusApi.init(apiClient: SWGApiClient.shared())
        }
    }
    
    func changeProfileTypeWithFinishBlock(role: AccountRole, completion: @escaping (_ error:Error?) -> Void) {
        
        let model = SWGSwitchTypeModel()
        if role == .tutor {
            model.chamiType = ChamiLevelType.chamiPlus.rawValue
        } else {
            model.chamiType = ChamiLevelType.chamiX.rawValue
        }

        
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            return
        }
        
        
        self.accountStatusApi.accountStatusSwitchAccount(with: model, authorization: authorization) { (responseWrapper, error) in
            if let error = error {
                print("\(#function), error: \(error)")
                //failure(error)
                completion(error)
                return
            }
            completion(nil)
            
            
        }
        
        
    }
    
    func checkProfileTypeWithFinishBlock(completion: @escaping (_ error:Error?) -> Void) {
        
        self.profileService.getProfileType(success: { typeModel in
            guard let account = AccountManager.shared.currentUser else {
                completion(ErrorHelper.errorMessageFor(Error: "Somthing is wong" as! Error) as? Error)
                return;
            }
           
            account.role = .notDefined
            
            if let accRole = typeModel?.chamiType {
                account.chamiType = ChamiLevelType(rawValue:accRole)!
                if account.chamiType == .chamiX {
                    //AccountManager.shared.currentUser?.role = .student
                    account.role = .student
                } else {
                    //AccountManager.shared.currentUser?.role = .tutor
                    account.role = .tutor
                }
                //completion(nil)
            } else {
                
                //completion(ErrorHelper.compileError(withTextMessage: "Unexpected error"))
            }
            
            completion(nil)
        }, failure: { error in
            completion(error)
        })
        
        
    }
}
