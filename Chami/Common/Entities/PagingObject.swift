//
//  PagingObject.swift
//  Stadio
//
//  Created by Igor Markov on 5/25/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

typealias ObjectIdType = Int

protocol PagingObjectProtocol {

    init(withId objectId: ObjectIdType)

    var identifier: ObjectIdType { get }
}


class PagingObject: NSObject, PagingObjectProtocol, NSCoding {

    var identifier: ObjectIdType = 0

    required init(withId objectId: ObjectIdType) {
        self.identifier = objectId
    }

    // MARK: - NSCoding

    required init?(coder aDecoder: NSCoder) {
        self.identifier = aDecoder.decodeInteger(forKey: PagingObject.identifierKey)
    }

    func encode(with aCoder: NSCoder) {
        aCoder.encode(self.identifier, forKey: PagingObject.identifierKey)
    }

    fileprivate static let identifierKey = "identifierKey"
}
