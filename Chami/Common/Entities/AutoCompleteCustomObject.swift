//
//  AutoCompleteCustomObject.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MLPAutoCompleteTextField

class AutoCompleteCustomObject: NSObject, MLPAutoCompletionObject {
    private var name: String!
    var placeId: String?

    init(name: String) {
        super.init()
        self.name = name
    }
    
    func autocompleteString() -> String! {
        return self.name
    }
}
