//
//  MeetUpSession.swift
//  Chami
//
//  Created by Serg Smyk on 16/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum MeetUpSessionStatus: uint {
    case undefined
    case requestSent
    case requestReceived
    case requestCanceled
    case responseSent
    case responseReceived
    case started
    case finished
}


class MeetUpSession: NSObject {
    var internal_session_id: IdType = UUID().uuidString
    var myId: IdType = ""
    var partnerId: IdType = ""
    var my_full_name: String? = ""
    var partner_full_name: String? = ""
    var my_image_url: String? = ""
    var partner_image_url: String? = ""

    
    var session_id: IdType = ""
    
    var meetup_id: IdType = ""
    var language: IdType = ""
    var language_name: String = ""
    
    var request_native_language: IdType = ""
    var request_native_language_name: String = ""

    var isChamiXFlow: Bool {
        get {
            return (self.comment?.characters.count == 0)
        }
    }
//    var user_id: IdType = ""
//    var initiator_id: IdType = ""
    
//    var image_url: String? = ""
//    var studentId: IdType = ""
//    var studentImageUrl: String = ""
//    var tutorId: IdType = ""
//    var tutorFullName: String = ""
//    var tutorImageUrl: String = ""
    var comment: String? = ""
    var exactLocation: String? = ""
    
    var location_name: String = ""
    var latitude: NSNumber = NSNumber(value: 0)
    var longitude: NSNumber = NSNumber(value: 0)
    
    var costPerHour: NSNumber = NSNumber(value: 0)
    var session_cost: NSNumber = NSNumber(value: 0)

    var duration: Int = 0
    var maxDuration: Int = 60 * 60 * 3 //(sec * min * hour)
    
    
    var statusRevision: Int = 0
    var status:MeetUpSessionStatus = .undefined {
        didSet {
            self.statusRevision += 1
        }
    }
    
    
    var isAccepted: Bool = false
    
    
    var toDictionary: NSMutableDictionary! {
        get {
            let dic = NSMutableDictionary()
            dic["internal_session_id"] = self.internal_session_id
            dic["user_id"] = self.partnerId
            dic["initiator_id"] = self.myId
            dic["session_id"] = self.session_id
            dic["meetup_id"] = self.meetup_id
            //dic["image_url"] = self.image_url
            dic["comment"] = self.comment
            dic["exact_location"] = self.exactLocation
            dic["longitude"] = "\(self.longitude)"
            dic["latitude"] = "\(self.latitude)"
            dic["location_name"] = self.location_name
            dic["language"] = self.language
            dic["language_name"] = self.language_name
            dic["request_native_language"] = self.request_native_language
            dic["request_native_language_name"] = self.request_native_language_name
            dic["partner_full_name"] = self.partner_full_name
            dic["my_full_name"] = self.my_full_name
            dic["isAccept"] = NSNumber(value:self.isAccepted)
            dic["statusRevision"] = NSNumber(value:self.statusRevision)
            dic["my_image_url"] = self.my_image_url
            dic["partner_image_url"] = self.partner_image_url
            if self.costPerHour.floatValue > 0 {
                dic["costPerHour"] = self.costPerHour
            }
            
//            if self.session_cost.floatValue > 0 {
//                dic["session_cost"] = self.session_cost
//            }
            
            return dic
        }
    }
    func updateFromDictionary(dic:NSDictionary, changePartner: Bool) {
        let item = self
        
        if let internal_session_id = dic["internal_session_id"] {
            item.internal_session_id = internal_session_id as! IdType
        }
//        if let user_id = dic["user_id"] {
//            item.user_id = user_id as! IdType
//        }
//        if let initiator_id = dic["initiator_id"] {
//            item.initiator_id = initiator_id as! IdType
//        }
        if let session_id = dic["session_id"] {
            item.session_id = session_id as! IdType
        }
        if let meetup_id = dic["meetup_id"] {
            item.meetup_id = meetup_id as! IdType
        }
        if let language = dic["language"] {
            item.language = language as! IdType
        }
        if let language_name = dic["language_name"] {
            item.language_name = language_name as! IdType
        }
        if let request_native_language = dic["request_native_language"] {
            item.request_native_language = request_native_language as! IdType
        }
        if let request_native_language_name = dic["request_native_language_name"] {
            item.request_native_language_name = request_native_language_name as! IdType
        }
//        if let image_url = dic["image_url"] {
//            item.image_url = image_url as? String
//        }
        if let comment = dic["comment"] {
            item.comment = comment as? String
        }
        if let exactLocation = dic["exact_location"] {
            item.exactLocation = exactLocation as? String
        }

        if let my_full_name = dic["my_full_name"] {
            item.my_full_name = my_full_name as? String
        }
        if let my_image_url = dic["my_image_url"] {
            item.my_image_url = my_image_url as? String
        }
        if let partner_full_name = dic["partner_full_name"] {
            item.partner_full_name = partner_full_name as? String
        }
        if let partner_image_url = dic["partner_image_url"] {
            item.partner_image_url = partner_image_url as? String
        }
        if let longitude = dic["longitude"] as? String {
            if let longitudeInt = Double(longitude) {
                item.longitude = NSNumber(value:longitudeInt)
            }
        }
        if let latitude = dic["latitude"] as? String {
            if let latitudeInt = Double(latitude) {
                item.latitude = NSNumber(value:latitudeInt)
            }
        }
        if let location_name = dic["location_name"] {
            item.location_name = location_name as! String
        }

        if let isAccept = dic["isAccept"] as? NSNumber {
            item.isAccepted = isAccept.boolValue
        }
        if let statusRevision = dic["statusRevision"] as? NSNumber {
            item.statusRevision = statusRevision.intValue
        }
        if let duration = dic["duration"] as? NSNumber {
            item.duration = duration.intValue
        }
        if let costPerHour = dic["costPerHour"] as? NSNumber {
            item.costPerHour = costPerHour
        }
        if let session_cost = dic["session_cost"] as? NSNumber {
            item.session_cost = session_cost            
        }

        if changePartner == true {
            if let myId = AccountManager.shared.currentUser?.id {
                item.myId = myId
            }
            if let my_full_name = AccountManager.shared.currentUser?.fullName {
                item.my_full_name = my_full_name
            }
            if let my_image_url = AccountManager.shared.currentUser?.imageUrl {
                item.my_image_url = my_image_url
            }
            
            if let user_id = dic["initiator_id"] {
                item.partnerId = user_id as! IdType
            }
            if let partner_full_name = dic["my_full_name"] {
                item.partner_full_name = partner_full_name as? String
            }
            if let partner_image_url = dic["my_image_url"] {
                item.partner_image_url = partner_image_url  as? String
            }
        }
    }
    
}
