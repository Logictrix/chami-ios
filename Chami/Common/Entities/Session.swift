//
//  Session.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/10/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

class Session: NSObject {
    
    var sessionId: IdType = ""
    var tutorsFullName: String  = ""
    var latitude: Double = 0
    var longitude: Double = 0
    var etaDate: Date = Date(timeInterval: 0, since: Date())
    var code: NSNumber?
    var tutorsImageUrl: String  = ""

    var isChamiX: Bool = false
    var isChamiMax: Bool = false

    
    var feedback: String = ""
    var rating: Int = 0
    
    var ieltsScore: NSNumber?
    var durationMinutes: Int = 0

    
    var tutorsId: IdType  = "0"
    var studentId: IdType  = "0"

    var startDateTime: Date = Date()
    var endDateTime: Date = Date()
    var length: String = ""
    var transactionRef: String? = nil
    var cost: Int = 0
    
    var costPerHour: NSNumber = NSNumber.init(value: 0.0)

    var tutorsFeedback: String? = nil
    var studentsFeedback: String? = nil
    var tutorsRates: [SessionSkillDataType] = []
    var studentsRates: [SessionSkillDataType] = []
    
    func createSkillDataTypeWithName(name: String, skillValue: Int) -> SessionSkillDataType {
        let skillName = NSLocalizedString(name, comment:"Speaking - Session Skills to rate")
        var skillLogoUrl = ""
        switch skillName {
        case NSLocalizedString("Speaking", comment:"Speaking - Session Skills to rate"):
            skillLogoUrl = "session_scr_icon_speaking"
            break
        case NSLocalizedString("Listening", comment:"Listening - Session Skills to rate"):
            skillLogoUrl = "session_scr_icon_listening"
            break
        case NSLocalizedString("Reading", comment:"Reading - Session Skills to rate"):
            skillLogoUrl = "session_scr_icon_reading"
            break
        case NSLocalizedString("Writing", comment:"Writing - Session Skills to rate"):
            skillLogoUrl = "session_scr_icon_writing"
            break
        case NSLocalizedString("Pronunciation", comment:"Pronunciation - Session Skills to rate"):
            skillLogoUrl = "session_scr_icon_pron"
            break
        case NSLocalizedString("Attitude to Learning", comment:"Attitude to Learning - Session Skills to rate"):
            skillLogoUrl = "session_scr_icon_attit"
            break
        //
        default: break
            //
        }

        return (skillName: skillName, skillValue: skillValue, skillLogoUrl: skillLogoUrl, isEditable: false)
    }
    
    func updateFromSWGObject(objSWGSessionResultModel swgObject: SWGSessionResultModel) -> Bool {

        if let partnerId = swgObject.partnerId {
            self.tutorsId = String("\(partnerId)")
        }
        if let partnerFullName = swgObject.partnerFullName {
            self.tutorsFullName = partnerFullName
        }
        
        if let sessionIdUnwrap = swgObject._id {
            self.sessionId = String("\(sessionIdUnwrap)")
        }
        
        if let startTimeUnwrap = swgObject.startTime {
            self.startDateTime = startTimeUnwrap
        }
        if let endTimeUnwrap = swgObject.endTime {
            self.endDateTime = endTimeUnwrap
        }
        if let costUnwrap = swgObject.cost {
            self.cost = costUnwrap.intValue
        }
        
        return true
    }

    
    func updateFromSWGObject(objSWGSessionFeedbackModel swgObject: SWGSessionFeedbackModel) -> Bool {
        
        var array :[SessionSkillDataType] = []
        if let speakingUnwrap = swgObject.speaking, speakingUnwrap.intValue > 0 {
            let skill: SessionSkillDataType = self.createSkillDataTypeWithName(name: "Speaking", skillValue: speakingUnwrap.intValue)
            array.append(skill)
        }
        if let listeningUnwrap = swgObject.listening, listeningUnwrap.intValue > 0 {
            let skill: SessionSkillDataType = self.createSkillDataTypeWithName(name: "Listening", skillValue: listeningUnwrap.intValue)
            array.append(skill)
        }
        if let readingUnwrap = swgObject.reading, readingUnwrap.intValue > 0 {
            let skill: SessionSkillDataType = self.createSkillDataTypeWithName(name: "Reading", skillValue: readingUnwrap.intValue)
            array.append(skill)
        }
        if let writingUnwrap = swgObject.writing, writingUnwrap.intValue > 0 {
            let skill: SessionSkillDataType = self.createSkillDataTypeWithName(name: "Writing", skillValue: writingUnwrap.intValue)
            array.append(skill)
        }
        if let pronunciationUnwrap = swgObject.pronunciation, pronunciationUnwrap.intValue > 0 {
            let skill: SessionSkillDataType = self.createSkillDataTypeWithName(name: "Pronunciation", skillValue: pronunciationUnwrap.intValue)
            array.append(skill)
        }
        if let attitudeToLearningUnwrap = swgObject.attitudeToLearning, attitudeToLearningUnwrap.intValue > 0 {
            let skill: SessionSkillDataType = self.createSkillDataTypeWithName(name: "Attitude to Learning", skillValue: attitudeToLearningUnwrap.intValue)
            array.append(skill)
        }
    
        self.studentsRates = array
        
        if let feedbackUnwrap = swgObject.feedback {
            self.feedback = feedbackUnwrap
        }
        if let ieltsScoreUnwrap = swgObject.ieltsScore {
            self.ieltsScore = ieltsScoreUnwrap
        }
        
        
        return true
    }

    
    func updateFromSWGObject(objSWGSessionModel swgObject: SWGSessionModel) -> Bool {

        if let partnerId = swgObject.partnerId {
            self.tutorsId = String("\(partnerId)")
        }
        if let partnerFullName = swgObject.partnerFullName {
            self.tutorsFullName = partnerFullName
        }
        if let imageUrlUnwrap = swgObject.imageUrl {
            self.tutorsImageUrl = imageUrlUnwrap
        }
        
        if let costPerHourUnwrap = swgObject.costPerHour {
            self.costPerHour = costPerHourUnwrap
        }

        
        if let sessionIdUnwrap = swgObject._id {
            self.sessionId = String("\(sessionIdUnwrap)")
        }
        if let latitudeUnwrap = swgObject.latitude {
            self.latitude = Double(latitudeUnwrap)
        }

        if let longitudeUnwrap = swgObject.longitude {
            self.longitude = Double(longitudeUnwrap)
        }
        if let etaDateUnwrap = swgObject.eta {
            self.etaDate = etaDateUnwrap
        }
        
        if let codeUnwrap = swgObject.code {
            self.code = codeUnwrap
        }

        return true
    }
    
    func updateFromSWGObject(objSWGSessionHistoryModel swgObject: SWGSessionHistoryModel) -> Bool {
        
//        open var type: String!
//        open var firstName: String!
//        open var lastName: String!
        if let startDateUnwrap = swgObject.startDate {
            self.startDateTime = startDateUnwrap
        }
        
        if let durationUnwrap = swgObject.duration {
            self.durationMinutes = durationUnwrap.intValue
        }
        
        if let ratingUnwrap = swgObject.rating {
            self.rating = ratingUnwrap.intValue
        }
        
        if let costUnwrap = swgObject.cost {
            self.cost = Int(costUnwrap)
        }

        if let fullNameUnwrap = swgObject.fullName {
            self.tutorsFullName = fullNameUnwrap
        }

        if let sessionIdUnwrap = swgObject._id {
            self.sessionId = String("\(sessionIdUnwrap)")
        }
        
        if let imageUrlUnwrap = swgObject.imageUrl {
            self.tutorsImageUrl = imageUrlUnwrap
        }
        
        if let transactionIdUnwrap = swgObject.transactionId {
            self.transactionRef = transactionIdUnwrap
        }
        
        return true
    }


    
//    func updateFromSWGObject(swgObject: SWGSessionResponseModel) -> Bool {
//        self.studentFullName = swgObject.fullName
//        if let meetUpIdUnwrap = swgObject._id {
//            self.meetUpId = String("\(meetUpIdUnwrap)")
//        }
//        self.studentImageUrl = swgObject.imageUrl
//        self.languageStringName = swgObject.nativeLanguage
//        
//        return true
//    }

}
