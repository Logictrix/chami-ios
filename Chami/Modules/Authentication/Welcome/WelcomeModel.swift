//
//  WelcomeModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit

protocol WelcomeModelDelegate: NSObjectProtocol {
    
    func authenticatedByFacebook()
    func facebookAuthenticationFailed(error: NSError)
}

protocol WelcomeModelProtocol: NSObjectProtocol {
    
    weak var delegate: WelcomeModelDelegate? { get set }
    
    func authenticateWithFacebook()
    
    func loginUser(_ token: String?, _ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ()))
}

class WelcomeModel: NSObject, WelcomeModelProtocol {
    
    fileprivate let service: AccountServiceProtocol
    
    // MARK: - WelcomeModel methods

    weak var delegate: WelcomeModelDelegate?
    
    init(withService service: AccountServiceProtocol) {
        
        self.service = service
        
    }
    
    func authenticateWithFacebook() {
        
        self.delegate?.authenticatedByFacebook()
    }
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    func loginUser(_ token: String?, _ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        
        
        self.service.loginUser(withFacebook: token!, coordinate: LocationManager.shared.coordinate, success: { account in print("account: \(account)")
           
            AccountManager.shared.currentUser = account
            LocationManager.shared.sendMyLocationToServer()
            success(account)
        }, failure: { error in
            let error = NSError(domain: AppError.errorModelDomain,
                                code: AppError.unsuccessResultErrorCode,
                                userInfo: nil)
            failure(error)
        })
    }
}
