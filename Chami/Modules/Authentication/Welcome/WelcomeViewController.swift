//
//  WelcomeViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin

typealias WelcomeViewControllerType = BaseViewController<WelcomeModelProtocol, WelcomeViewProtocol, WelcomeRouter>

class WelcomeViewController: WelcomeViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: WelcomeViewProtocol!, model: WelcomeModelProtocol!, router: WelcomeRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.tintColor = ColorHelper.lightTextColor()
        self.canHandleMeetupRequests = false
        
        customView.delegate = self
        model.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = true
    }
    
    func showRegistration() {
        self.router?.navigateToRegistration(from: self)
    }
    
    func showLogin() {
        self.router?.navigateToLogin(from: self)
    }
}

// MARK: - WelcomeViewDelegate

extension WelcomeViewController: WelcomeViewDelegate {

    func viewDidTapFacebookButton(view: WelcomeViewProtocol) {
        self.model.authenticateWithFacebook()
    }

    func viewDidTapEmailButton(view: WelcomeViewProtocol) {
        self.showLogin()
    }
}

// MARK: - WelcomeModelDelegate

extension WelcomeViewController: WelcomeModelDelegate {

    func authenticatedByFacebook() {
        //self.showRegistration()
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: [ Permission.publicProfile, Permission.email ], viewController: self) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
                self.model.loginUser(accessToken.tokenString, { (account) in
                    AccountManager.shared.loggedUserIn()
                }, failure: { (error) in
                    print("error: \(error)")
                })
                print("Logged in!")
            }
        }
        
        
    }

    func facebookAuthenticationFailed(error: NSError) {
        print("error: \(error)")
    }
}


