//
//  WelcomeRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class WelcomeRouter: NSObject {
    
    func navigateToRegistration(from vc: UIViewController) {
        let registrationVC = RegistrationBuilder.viewController()
        
        vc.navigationController?.pushViewController(registrationVC, animated: true)
    }
    
    func navigateToLogin(from vc: UIViewController) {
        let loginVC = LoginBuilder.viewController()
        
        vc.navigationController?.pushViewController(loginVC, animated: true)
    }
}
