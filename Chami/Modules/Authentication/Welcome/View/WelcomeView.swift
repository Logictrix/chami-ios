//
//  WelcomeView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol WelcomeViewDelegate: NSObjectProtocol {
    
    func viewDidTapFacebookButton(view: WelcomeViewProtocol)
    func viewDidTapEmailButton(view: WelcomeViewProtocol)
}

protocol WelcomeViewProtocol: NSObjectProtocol {
    
    weak var delegate: WelcomeViewDelegate? { get set }
}

class WelcomeView: UIView, WelcomeViewProtocol{

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    class func create() -> WelcomeView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? WelcomeView else {
            fatalError("Nib \(viewNibName) does not contain Welcome View as first object")
        }
        
        return view
    }
    
    // MARK: - WelcomeView interface methods

    weak var delegate: WelcomeViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.headerView.backgroundColor = ColorHelper.regularColor()
        self.titleLabel.textColor = ColorHelper.yellowAppColor()
        Theme.current.apply(borderedButton: self.emailButton)
        Theme.current.apply(onlyBorderedButton: self.emailButton, isOpposit: true)
        Theme.current.apply(filledButton: self.facebookButton)
        self.facebookButton.backgroundColor = /*RGB(47, 121, 168)*/ ColorHelper.regularColor()
    }
    
    // MARK: - IBActions
    
    @IBAction func facebookButtonAction() {
        self.delegate?.viewDidTapFacebookButton(view: self)
    }

    @IBAction func emailButtonAction() {
        self.delegate?.viewDidTapEmailButton(view: self)
    }
}
