//
//  WelcomeBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class WelcomeBuilder: NSObject {

    class func viewController() -> WelcomeViewController {
        
        let authService = AccountService()
        let view: WelcomeViewProtocol = WelcomeView.create()
        let model: WelcomeModelProtocol = WelcomeModel(withService: authService)
        let router = WelcomeRouter()
        
        let viewController = WelcomeViewController(withView: view, model: model, router: router)
        return viewController
    }
}
