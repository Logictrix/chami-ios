//
//  IntroModel.swift
//  Chami
//
//  Created by Serg Smyk on 09/08/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol IntroModelDelegate: NSObjectProtocol {
    
}

protocol IntroModelProtocol: NSObjectProtocol {
    
    weak var delegate: IntroModelDelegate? { get set }
}

class IntroModel: NSObject, IntroModelProtocol {
    
    // MARK: - IntroModel methods

    weak public var delegate: IntroModelDelegate?
    
    /** Implement IntroModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
