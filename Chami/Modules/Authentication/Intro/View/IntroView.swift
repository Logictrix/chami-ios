//
//  IntroView.swift
//  Chami
//
//  Created by Serg Smyk on 09/08/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol IntroViewDelegate: NSObjectProtocol {
    
    func goToNextIntro(view: IntroViewProtocol)
    func closeIntro(view: IntroViewProtocol)
}

protocol IntroViewProtocol: NSObjectProtocol {
    
    weak var delegate: IntroViewDelegate? { get set }
    var introIndex: Int { get set }
    var introPagesCollection: [UIView]! { get }
    
    func setupLayout()
}

class IntroView: UIView, IntroViewProtocol{
    
    @IBOutlet var scrollView: UIScrollView!
    @IBOutlet var introPagesCollection: [UIView]!
    // MARK: - IntroView interface methods

    weak public var delegate: IntroViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.setupLayout()
    }
    var introIndex: Int = 0 {
        didSet {
            self.scrollView.contentOffset = CGPoint(x: CGFloat(self.scrollView.frame.size.width) * CGFloat(self.introIndex), y: 0)
        }
    }
    func setupLayout() {
        var introRect = CGRect(x: 0, y: 0, width: self.scrollView.frame.size.width, height: self.scrollView.frame.size.height)
        for introPage in self.introPagesCollection {
            self.scrollView.addSubview(introPage)
            introPage.frame = introRect
            introPage.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            introRect.origin.x += introRect.size.width
        }
        self.scrollView.contentSize = CGSize(width: introRect.origin.x, height: 0)
    }
    // MARK: - Overrided methods

    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        self.introIndex = Int(scrollView.contentOffset.x / scrollView.frame.size.width)
    }
    
    // MARK: - IBActions
    
    @IBAction func goToNextIntro(_ sender: UIButton) {
        self.delegate?.goToNextIntro(view: self)
    }
    @IBAction func skipIntroAction(_ sender: UIButton) {
        self.delegate?.closeIntro(view: self)
    }
}
