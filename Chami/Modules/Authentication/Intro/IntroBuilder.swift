//
//  IntroBuilder.swift
//  Chami
//
//  Created by Serg Smyk on 09/08/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class IntroBuilder: NSObject {

    class func viewController() -> IntroViewController {

        let view: IntroViewProtocol = IntroView.loadFromXib() as! IntroViewProtocol
        let model: IntroModelProtocol = IntroModel()
        let router = IntroRouter()
        
        let viewController = IntroViewController(withView: view, model: model, router: router)
        return viewController
    }
}
