//
//  IntroViewController.swift
//  Chami
//
//  Created by Serg Smyk on 09/08/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias IntroViewControllerType = MVCViewController<IntroModelProtocol, IntroViewProtocol, IntroRouter>

class IntroViewController: IntroViewControllerType {
    
    // MARK: Initializers
    
    required public init(withView view: IntroViewProtocol!, model: IntroModelProtocol!, router: IntroRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        customView.delegate = self
        model.delegate = self
        
        self.customView.setupLayout()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.customView.setupLayout()
    }
}

// MARK: - IntroViewDelegate

extension IntroViewController: IntroViewDelegate {

    func goToNextIntro(view: IntroViewProtocol) {
        if self.customView.introIndex < self.customView.introPagesCollection.count - 1 {
            self.customView.introIndex += 1
        }
    }
    func closeIntro(view: IntroViewProtocol) {
        self.dismiss(animated: false) { 
        }
    }

}

// MARK: - IntroModelDelegate

extension IntroViewController: IntroModelDelegate {
}
