//
//  SelectRoleModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SelectRoleModelDelegate: NSObjectProtocol {
    
}

protocol SelectRoleModelProtocol: NSObjectProtocol {
    
    weak var delegate: SelectRoleModelDelegate? { get set }
}

class SelectRoleModel: NSObject, SelectRoleModelProtocol {
    
    // MARK: - SelectRoleModel methods

    weak var delegate: SelectRoleModelDelegate?
    
    /** Implement SelectRoleModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
