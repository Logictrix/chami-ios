//
//  SelectRoleRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SelectRoleRouter: NSObject {

    func navigateToSetupStudent(from vc: UIViewController) {
        
        let setupProfileVC = SetupProfileBuilder.viewController()
        vc.navigationController?.pushViewController(setupProfileVC, animated: true)
    }

    func navigateToSetupTutor(from vc: UIViewController) {
        let setupProfileVC = SetupProfileBuilder.viewController()
        vc.navigationController?.pushViewController(setupProfileVC, animated: true)
    }
}
