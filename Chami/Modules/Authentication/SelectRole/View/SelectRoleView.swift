//
//  SelectRoleView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SelectRoleViewDelegate: NSObjectProtocol {

    func viewDidTapStudentControl()
    func viewDidTapTutorControl()
}

protocol SelectRoleViewProtocol: NSObjectProtocol {
    
    weak var delegate: SelectRoleViewDelegate? { get set }
}

class SelectRoleView: UIView, SelectRoleViewProtocol{

    @IBOutlet weak var studentPlaceholder: UIView!
    @IBOutlet weak var tutorPlaceholder: UIView!
    
    fileprivate var studentView: SelectRoleSingleView = SelectRoleSingleView()
    fileprivate var tutorView: SelectRoleSingleView = SelectRoleSingleView()

    class func create() -> SelectRoleView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? SelectRoleView else {
            fatalError("Nib \(viewNibName) does not contain SelectRole View as first object")
        }
        
        return view
    }
    
    // MARK: - SelectRoleView interface methods

    weak var delegate: SelectRoleViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        self.setupStudentView()
        self.setupTutorView()
    }
    
    // MARK: - Setup methods
    
    fileprivate func setupStudentView() {
        self.studentView = self.createSingleSelectRoleView()
        self.studentView.translatesAutoresizingMaskIntoConstraints = true
        self.studentView.frame = self.studentPlaceholder.bounds
        self.studentPlaceholder.addSubview(self.studentView)

        self.studentView.titleLabel.textColor = Theme.student.regularColor()
        self.studentView.titleLabel.attributedText = self.compileTitleText(withRoleString: NSLocalizedString("Learn", comment: "Select Role"))
        self.studentView.descriptionLabel.font = FontHelper.defaultFont(withSize: 17)
        self.studentView.descriptionLabel.text = "Learn or exchange languages, find a language partner or a professional language tutor near you."
        //self.studentView.descriptionLabel.text = "Get professional Spanish tuition at your favorite café, tour around London with another like-minded learner or have a casual chat about English football over a cold pint of beer, the power is in your hands!"
        self.studentView.imageView.image = UIImage(named: "who_are_you_scr_btn_student")
        self.studentView.imageView.isUserInteractionEnabled = true
        
        let tapStudentGesture = UITapGestureRecognizer(target: self, action: #selector(tapStudentControlAction))
        self.studentView.gestureRecognizers = [tapStudentGesture]
    }
    
    fileprivate func setupTutorView() {
        self.tutorView = self.createSingleSelectRoleView()
        self.tutorView.frame = self.tutorPlaceholder.bounds
        self.tutorPlaceholder.addSubview(self.tutorView)
        
        self.tutorView.titleLabel.textColor = Theme.tutor.regularColor()
        self.tutorView.titleLabel.attributedText = self.compileTitleText(withRoleString: NSLocalizedString("Teach", comment: "Select Role"))
        self.tutorView.descriptionLabel.font = FontHelper.defaultFont(withSize: 17)
        self.tutorView.descriptionLabel.text = "Be free to work to your own availability. Register as a tutor and you can receive tuition requests from learners near you."
        self.tutorView.imageView.image = UIImage(named: "who_are_you_scr_btn_tutor")
        self.tutorView.imageView.isUserInteractionEnabled = true
        
        let tapTutorGesture = UITapGestureRecognizer(target: self, action: #selector(tapTutorControlAction))
        self.tutorView.gestureRecognizers = [tapTutorGesture]
    }
    
    fileprivate func createSingleSelectRoleView() -> SelectRoleSingleView {
        let viewNibName = String(describing: SelectRoleSingleView.self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let singleSelectRoleView = nibContent?.first as? SelectRoleSingleView else {
            fatalError("Nib \(viewNibName) does not contain SelectRoleSingleView View as first object")
        }

        singleSelectRoleView.layer.borderColor = RGB(226, 226, 226).cgColor
        singleSelectRoleView.layer.borderWidth = 1.5
        singleSelectRoleView.layer.cornerRadius = 2
        
        return singleSelectRoleView
    }
    
    fileprivate func compileTitleText(withRoleString roleString: String) -> NSAttributedString {
        let resultTitle = NSMutableAttributedString()

        var attributes = [NSAttributedString.Key.font: FontHelper.defaultFont(withSize: 25)]
        let startString = NSLocalizedString("I want to ", comment: "Select Role")
        resultTitle.append(NSAttributedString(string: startString, attributes: attributes))
        
        attributes = [NSAttributedString.Key.font: FontHelper.defaultMediumFont(withSize: 25)]
        resultTitle.append(NSAttributedString(string: roleString, attributes: attributes))
        
        return resultTitle
    }
    
    // MARK: - IBActions

    @objc fileprivate func tapStudentControlAction() {
        self.delegate?.viewDidTapStudentControl()
    }
    
    @objc fileprivate func tapTutorControlAction() {
        self.delegate?.viewDidTapTutorControl()
    }
}
