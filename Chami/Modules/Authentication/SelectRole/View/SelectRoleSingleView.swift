//
//  SelectRoleSingleView.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SelectRoleSingleView: UIView {
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
}
