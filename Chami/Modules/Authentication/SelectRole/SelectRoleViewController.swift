//
//  SelectRoleViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias SelectRoleViewControllerType = BaseViewController<SelectRoleModelProtocol, SelectRoleViewProtocol, SelectRoleRouter>

class SelectRoleViewController: SelectRoleViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: SelectRoleViewProtocol!, model: SelectRoleModelProtocol!, router: SelectRoleRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("What do you want to do today?", comment: "Select Role")
        self.canHandleMeetupRequests = false
        
        customView.delegate = self
        model.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        AccountManager.shared.settings?.currentRole = .notDefined
//        AccountManager.shared.updateTheme()
        
        //self.setupNavigationBar()
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle {
//        return .default
//    }
    
    // MARK: Private methods
    
//    func setupNavigationBar() {
////        let navigationBarFont = FontHelper.defaultMediumFont(withSize: 19)
////        let navigationBarAttributes: [String: Any] = [NSFontAttributeName: navigationBarFont,
////                                                      NSForegroundColorAttributeName: UIColor.white]
////        self.navigationController?.navigationBar.titleTextAttributes = navigationBarAttributes
////        self.navigationController?.navigationBar.barTintColor = Theme.student.regularColor()
//        
//        self.title = NSLocalizedString("What do you want to do today?", comment: "Select Role")
//    }
}

// MARK: - SelectRoleViewDelegate

extension SelectRoleViewController: SelectRoleViewDelegate {

    func viewDidTapStudentControl() {
        self.selectRoleAndContinue(role: AccountRole.student)
    }

    func viewDidTapTutorControl() {
        self.selectRoleAndContinue(role: AccountRole.tutor)
    }
    
    func selectRoleAndContinue(role: AccountRole) {
        guard let currentUser = AccountManager.shared.currentUser else {
            return
        }
        self.setProgressVisible(visible: true)
        
        currentUser.updateProfileWithFinishBlock {[weak self] (error:Error?) in
            AccountManager.shared.settings?.currentRole = role
            AccountManager.shared.currentUser?.role = role
            AccountManager.shared.updateTheme()
                        
            currentUser.changeProfileTypeWithFinishBlock(role: role, completion: { (error:Error?) in
                
                guard let strongSelf = self else { return }
                strongSelf.setProgressVisible(visible: false)
                
                if currentUser.hasCompletedProfile(withRole: role) {
                    NavigationManager.shared.showMainScreen(animated: true)
                } else {
                    if role == .student {
                        strongSelf.router?.navigateToSetupStudent(from: strongSelf)
                    } else {
                        strongSelf.router?.navigateToSetupTutor(from: strongSelf)
                    }
                    
                }
                
            })
            
            
        }
    }
}

// MARK: - SelectRoleModelDelegate

extension SelectRoleViewController: SelectRoleModelDelegate {
}
