//
//  SelectRoleBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SelectRoleBuilder: NSObject {

    class func viewController() -> SelectRoleViewController {

        let view: SelectRoleViewProtocol = SelectRoleView.create()
        let model: SelectRoleModelProtocol = SelectRoleModel()
        let router = SelectRoleRouter()
        
        let viewController = SelectRoleViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
