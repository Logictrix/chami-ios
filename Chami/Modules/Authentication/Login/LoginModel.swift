//
//  LoginModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import FacebookCore
import FacebookLogin
import MapKit
import PKHUD

// MARK: - LoginFieldType

enum LoginFieldType: Int {
    case email = 0
    case password = 1
}

// MARK: - LoginFieldGroup

enum LoginFieldGroup: Int {
    case general = 0
}

// MARK: - LoginModelDelegate

protocol LoginModelDelegate: NSObjectProtocol {

    func modelDidUpdateData(_ model: LoginModelProtocol)
}

// MARK: - LoginModelProtocol

protocol LoginModelProtocol: NSObjectProtocol {

    weak var delegate: LoginModelDelegate? { get set }

    // MARK: - Data
    func clearAllData()

    // MARK: - Fields
    func numberOfFields() -> Int
    func setValue(_ value: String, fieldType: LoginFieldType)
    func getField(withType registerFiledType: LoginFieldType) -> FormField?

    // MARK: - Field index pathes
    func getField(withIndexPath indexPath: IndexPath) -> FormField?
    func getLoginFieldType(atIndexPath indexPath: IndexPath) -> LoginFieldType?
    func getFieldPosition(atIndexPath indexPath: IndexPath) -> GroupPosition

    // MARK: - Field groups
    func numberOfGroup() -> Int
    func numberOfFields(inGroup group: LoginFieldGroup) -> Int
    func getFieldTypes(inGroup group: LoginFieldGroup) -> [LoginFieldType]

    // MARK: - Validation
    func setValidationState(_ state: ValidationState, fieldType: LoginFieldType)
    func validateValue(_ value: String?, fieldType: LoginFieldType) -> ValidationState
    func warningMessageIfInputedDataIsInCorrect() -> String
    func checkInputedData() -> (result: Bool, warning: String?)

    // MARK: Log In
    func authenticateWithFacebook(viewController: UIViewController!)
    func loginUser(_ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ()))
    
    func resetPassword(_ login: String, completion: @escaping ((_ error: Error?) -> ()))
}

// MARK: - LoginModel

class LoginModel: NSObject, LoginModelProtocol {



    typealias FieldTypeAndField = (type: LoginFieldType, field: FormField)

    weak var delegate: LoginModelDelegate?

    fileprivate let service: AccountServiceProtocol
    fileprivate let fields: [LoginFieldType : FormField]
    fileprivate let fieldGroups: [LoginFieldGroup: [LoginFieldType]]

    init(withService service: AccountServiceProtocol) {

        self.service = service
        self.fields = LoginModel.setupFields()
        self.fieldGroups = [.general:     [.email, .password]]
    }

    fileprivate class func setupFields() -> [LoginFieldType : FormField] {

        let emailField = FormField()
        emailField.key = NSLocalizedString("Email", comment: "Login field Email")
        emailField.fieldType = .email
        emailField.keyboardType = .email
        emailField.returnButtonType = .next
        emailField.validationState = .incorrect

        let passwordField = FormField()
        passwordField.isSecure = true
        passwordField.key = NSLocalizedString("Password", comment: "Login field Password")
        passwordField.fieldType = .password
        passwordField.keyboardType = .string
        passwordField.returnButtonType = .done
        passwordField.validationState = .incorrect

        // Debug
        //#if IOS_SIMULATOR//TARGET_IPHONE_SIMULATOR
//        if TARGET_OS_SIMULATOR == true {
//            
////            emailField.value = "tt@gg.com" as AnyObject?
////            passwordField.value = "qwerty" as AnyObject?
//
//            emailField.value = "student6@gmail.com" as AnyObject?
//            passwordField.value = "qwerty" as AnyObject?
//            emailField.validationState = .correct
//            passwordField.validationState = .correct
//        }
        //#endif

        return [.email: emailField,
                .password: passwordField]
    }

    // MARK: - Data

    func clearAllData() {
        for field in self.fields.values {
            field.value = nil
        }

        if let delegate = delegate {
            delegate.modelDidUpdateData(self)
        }
    }

    // MARK: - Fields

    func numberOfFields() -> Int {
        return fields.count
    }

    func setValue(_ value: String, fieldType: LoginFieldType) {
        if let field = fields[fieldType] {
            field.value = value as AnyObject?
        }
    }

    func getField(withType registerFiledType: LoginFieldType) -> FormField? {
        return fields[registerFiledType]
    }

    // MARK: - Field index pathes

    func getField(withIndexPath indexPath: IndexPath) -> FormField? {
        guard let group = LoginFieldGroup(rawValue: indexPath.section),
              let fieldGroup = self.fieldGroups[group] else {
            return nil
        }

        guard indexPath.row < fieldGroup.count else {
            return nil
        }

        let fieldType = fieldGroup[indexPath.row]
        let loginField =  self.fields[fieldType]
        return loginField
    }

    func getLoginFieldType(atIndexPath indexPath: IndexPath) -> LoginFieldType? {
        guard let group = LoginFieldGroup(rawValue: indexPath.section),
              let fieldGroup = self.fieldGroups[group] else {
            return nil
        }

        return indexPath.row < fieldGroup.count ? fieldGroup[indexPath.row] : nil
    }

    func getFieldPosition(atIndexPath indexPath: IndexPath) -> GroupPosition {
        guard let group = LoginFieldGroup(rawValue: indexPath.section),
              let fieldGroup = self.fieldGroups[group] else {
            return []
        }

        var groupPosition: GroupPosition = []
        if indexPath.row == 0 {
            groupPosition.insert(.start)
        }
        if indexPath.row == fieldGroup.count - 1 {
            groupPosition.insert(.end)
        }

        return groupPosition
    }

    // MARK: - Field groups

    func numberOfGroup() -> Int {
        return self.fieldGroups.count
    }

    func numberOfFields(inGroup group: LoginFieldGroup) -> Int {
        return self.getFieldTypes(inGroup: group).count
    }

    func getFieldTypes(inGroup group: LoginFieldGroup) -> [LoginFieldType] {
        if let fieldGroup = self.fieldGroups[group] {
            return fieldGroup
        } else {
            return []
        }
    }

    // MARK: - Validation

    func setValidationState(_ state: ValidationState, fieldType: LoginFieldType) {
        if let field = fields[fieldType] {
            field.validationState = state
        }
    }

    func validateValue(_ value: String?, fieldType: LoginFieldType) -> ValidationState {

        guard let value = value else {
            return .incorrect
        }

        switch fieldType {
        case .email:
            return ValidationUtils.validateEmail(value) ? .correct : .incorrect
        case .password:
            return ValidationUtils.validatePassword(value) ? .correct : .incorrect
        }
    }

    func checkInputedData() -> (result: Bool, warning: String?) {

        for (_, field) in fields {
            if (field.validationState != .correct) {
                let fieldValue = field.stringFromValue()
                guard let fieldKey = field.key else {
                    fatalError()
                }

                var warning = ""
                if (field.fieldType == .password) {
                    warning = ValidationUtils.validationWarningMessageForPasswordWithKey(fieldKey, value: fieldValue)
                } else {
                    warning = ValidationUtils.validationWarningMessageForStringWithKey(fieldKey, value: fieldValue)
                }

                if warning.characters.count > 0 {
                    return (false, warning)
                }
            }
        }

        return (true, nil)
    }

    func warningMessageIfInputedDataIsInCorrect() -> String {

        var message = ""
        for (_, field) in fields {
            if (field.validationState != .correct) {
                let fieldValue = field.stringFromValue()
                guard let fieldKey = field.key else {
                    fatalError()
                }

                if (field.fieldType == .password) {
                    message = ValidationUtils.validationWarningMessageForPasswordWithKey(fieldKey, value: fieldValue)
                } else {
                    message = ValidationUtils.validationWarningMessageForStringWithKey(fieldKey, value: fieldValue)
                }

                break
            }
        }

        return message
    }
    
    func authenticateWithFacebook(viewController: UIViewController!) {
        //self.showRegistration()
        let loginManager = LoginManager()
        loginManager.logOut()
        loginManager.logIn(permissions: [ Permission.publicProfile, Permission.email ], viewController: viewController) { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success( _, _, let accessToken):
//                self.loginUser(accessToken.authenticationToken, { (account) in
//                    AccountManager.shared.loggedUserIn()
//                }, failure: { (error) in
//                    print("error: \(error)")
//                })
                
                self.loginUser(accessToken.tokenString, { (account) in
                    guard let currentUser = AccountManager.shared.currentUser else {
                        return
                    }
                    currentUser.updateProfileWithFinishBlock(completion: { (error:Error?) in
                        AccountManager.shared.loggedUserIn()
                        guard let strongSelf = viewController else { return }
                        print("obj=\(strongSelf) error: \(String(describing: error))")
                        //strongSelf.setProgressVisible(visible: false)
                        HUD.show(.progress)
                    })
                    
                    
                }, failure: { (error) in
                    guard let strongSelf = viewController else { return }
                    //strongSelf.setProgressVisible(visible: false)
                    HUD.show(.progress)
                    //AlertManager.showWarning(withMessage: error.localizedDescription, onController: viewController, closeHandler: nil)
                    AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: viewController, closeHandler: nil)
                    print("obj=\(strongSelf) error: \(error)")
                })

                print("Logged in!")
            }
        }
    }

    // MARK: - Login

    func loginUser(_ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ())) {

        guard let email = fields[.email]?.value as? String,
              let password = fields[.password]?.value as? String else {

            let error = NSError(domain: AppError.errorModelDomain,
                    code: AppError.invalidParametersErrorCode,
                    userInfo: ErrorHelper.compileUserInfo(forModelErrorCode: AppError.invalidParametersErrorCode) as! [String : Any])
            failure(error)
            return
        }
        
        self.service.loginUser(withEmail: email, password: password, coordinate:LocationManager.shared.coordinate, success: { account in
            print("account: \(account)")

            AccountManager.shared.currentUser = account
            AccountManager.shared.currentRole = account.role
            
            LocationManager.shared.sendMyLocationToServer()
            
            //var rolle = AccountManager.shared.currentRole
            success(account)
        }, failure: { error in
//            let error = NSError(domain: AppError.errorModelDomain,
//                    code: AppError.unsuccessResultErrorCode,
//                    userInfo: ErrorHelper.compileUserInfo(forModelErrorCode: AppError.invalidParametersErrorCode))
            failure(error as NSError)
        })
    }
    
    func loginUser(_ token: String?, _ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        
        
        self.service.loginUser(withFacebook: token!, coordinate: LocationManager.shared.coordinate, success: { account in print("account: \(account)")
            
            AccountManager.shared.currentUser = account
            AccountManager.shared.currentRole = account.role
            LocationManager.shared.sendMyLocationToServer()
            success(account)
        }, failure: { error in
            let error = NSError(domain: AppError.errorModelDomain,
                                code: AppError.unsuccessResultErrorCode,
                                userInfo: nil)
            failure(error)
        })
    }
    
    func resetPassword(_ login: String, completion: @escaping ((Error?) -> ())) {
        self.service.resetPassword(email: login) { (error) in
            completion(error)
        }
    }
}
