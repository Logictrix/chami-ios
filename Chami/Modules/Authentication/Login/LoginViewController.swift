//
//  LoginViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import PKHUD

typealias LoginViewControllerType = BaseViewController<LoginModelProtocol, LoginViewProtocol, LoginRouter>

class LoginViewController: LoginViewControllerType, UITableViewDelegate {

    fileprivate var dataSource: LoginDataSource!

    // MARK: - Initializers

    convenience init(withView view: LoginViewProtocol, model: LoginModelProtocol, router: LoginRouter, dataSource: LoginDataSource) {

        self.init(withView: view, model: model, router: router)

        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
    }

    internal required init(withView view: LoginViewProtocol!, model: LoginModelProtocol!, router: LoginRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = NSLocalizedString("Sign In", comment: "Login")
        self.canHandleMeetupRequests = false
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.navigationController?.isNavigationBarHidden = false
    }

    fileprivate func connectTableViewDependencies() {
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }

    // MARK: - Private methods

    fileprivate func isLoginDataCorrect() -> Bool {

        let modelCheckResult = self.model.checkInputedData()
        if !modelCheckResult.result {
            if let warning = modelCheckResult.warning {
                AlertManager.showWarning(withMessage: warning, onController: self, closeHandler: nil)
            }
        }

        return modelCheckResult.result
    }

    // MARK: - Actions

    fileprivate func loginUser() {
        self.setProgressVisible(visible: true)
        self.model.loginUser({[weak self] (account) in
            guard let currentUser = AccountManager.shared.currentUser else {
                return
            }
            currentUser.updateProfileWithFinishBlock(completion: { (error:Error?) in
                AccountManager.shared.loggedUserIn()
                guard let strongSelf = self else { return }
                strongSelf.setProgressVisible(visible: false)
            })

            
        }) {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            //AlertManager.showWarning(withMessage: error.localizedDescription, onController: self!, closeHandler: nil)
            // new code
             AlertManager.showWarning(withMessage: "Incorrect email or password", onController: strongSelf, closeHandler: nil)
          //  old code
          //  AlertManager.showWarning(withMessage: "Something was wrong. Please try again.", onController: strongSelf, closeHandler: nil)
            //AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: self!, closeHandler: nil)
            
            print("error: \(error)")
        }
    }

    // MARK: - Table view delegate

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return nil
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        let fieldGroupPosition = self.model.getFieldPosition(atIndexPath: indexPath)

        var roundCorners: UIRectCorner = []
        if fieldGroupPosition.contains(.start) {
            roundCorners = roundCorners.union([.topLeft, .topRight])
        }
        if fieldGroupPosition.contains(.end) {
            roundCorners = roundCorners.union([.bottomLeft, .bottomRight])
        }
        cell.contentView.roundCorners(corners: roundCorners, radius: 6, separatorHeight: 2)
    }
}

// MARK: - LoginViewDelegate

extension LoginViewController: LoginViewDelegate {
    func viewDidTapResetPassword(_ view: LoginViewProtocol) {
        let alert = UIAlertController(title: "Reset Password", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addTextField { (textField : UITextField!) in
            textField.placeholder = "Enter your email"
            textField.keyboardType = UIKeyboardType.emailAddress
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in }))
        //weak let weakAlert = alert
        alert.addAction(UIAlertAction(title: "Reset", style: UIAlertAction.Style.default, handler: { [weak alert] action in
            guard let textField = alert?.textFields?.first, let login = textField.text else {
                return
            }
            let validationState = self.model.validateValue(login, fieldType: .email)
            guard validationState == .correct else {
                self.showAlert(text: "Email is not correct")
                return
            }
            self.setProgressVisible(visible: true)
            self.model.resetPassword(login, completion: {[weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.setProgressVisible(visible: false)
                
                if error != nil {
                       AlertManager.showWarning(withMessage: "Incorrect email or password", onController: strongSelf, closeHandler: nil)
//                    AlertManager.showWarning(withMessage: "Something was wrong. Please try again.", onController: strongSelf, closeHandler: nil)
                } else {
                    strongSelf.showAlert(text: "New password was sent succesfully. Please check your email.")
                }
            })
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func viewDidTapConfirmButton(_ view: LoginViewProtocol) {
        self.customView.forceEndEditing()
        
        if self.isLoginDataCorrect() {
            self.loginUser()
        }
        
    }
    
    func viewDidTapRegisterButton(_ view: LoginViewProtocol) {
        self.customView.forceEndEditing()
        
        self.router?.navigateToRegistration(from: self)
    }
    
    func viewDidTapFacebookButton(_ view: LoginViewProtocol) {
        self.customView.forceEndEditing()
        
        self.model.authenticateWithFacebook(viewController:self)
               
    }

}

// MARK: - LoginModelDelegate

extension LoginViewController: LoginModelDelegate {

    func modelDidUpdateData(_ model: LoginModelProtocol) {
        self.customView.tableView.reloadData()
    }
}

// MARK: - LoginCellDelegate

extension LoginViewController: FormCellDelegate {

    func cell<CellType: FormCellProtocol>(_ cell: CellType, didChangeString string: String?) where CellType: UITableViewCell {}
    
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didEnterString string: String?) where CellType: UITableViewCell {

        guard let indexPath = self.customView.tableView.indexPath(for: cell),
              let registrationFieldType = self.model.getLoginFieldType(atIndexPath: indexPath),
              let field = self.model.getField(withIndexPath: indexPath) else {
            fatalError()
        }

        field.value = string as AnyObject?

        let validationState = self.model.validateValue(string, fieldType: registrationFieldType)
        self.model.setValidationState(validationState, fieldType: registrationFieldType)

        self.customView.tableView.reloadRows(at: [indexPath], with: .automatic)
        self.dataSource.activateNextCell(forCellAtIndexPath: indexPath, tableView: self.customView.tableView)
    }
}
