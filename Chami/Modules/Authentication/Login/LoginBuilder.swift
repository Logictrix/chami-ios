//
//  LoginBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LoginBuilder: NSObject {

    class func viewController() -> LoginViewController {
        let authService = AccountService()
        let view: LoginViewProtocol = LoginView.create()
        let model: LoginModelProtocol = LoginModel(withService: authService)
        let dataSource = LoginDataSource(withModel: model)
        let router = LoginRouter()

        let viewController = LoginViewController(withView: view, model: model, router: router, dataSource: dataSource)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
