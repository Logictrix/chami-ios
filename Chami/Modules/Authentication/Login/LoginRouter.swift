//
//  LoginRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LoginRouter: NSObject {

    func navigateToSelectRole(from vc: UIViewController) {
        let selectRoleVC = SelectRoleBuilder.viewController()
        vc.navigationController?.pushViewController(selectRoleVC, animated: true)
    }
    
    func navigateToRegistration(from vc: UIViewController) {
        let registrationVC = RegistrationBuilder.viewController()
        
        vc.navigationController?.pushViewController(registrationVC, animated: true)
    }
}
