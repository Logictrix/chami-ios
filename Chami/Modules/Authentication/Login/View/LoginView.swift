//
//  LoginView.swift
//  Stadio
//
//  Created by Igor Markov on 6/7/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

protocol LoginViewProtocol: NSObjectProtocol {

    weak var delegate: LoginViewDelegate? { get set }
    weak var tableView: UITableView! { get set }

    func forceEndEditing()
}

protocol LoginViewDelegate: NSObjectProtocol {

    func viewDidTapResetPassword(_ view: LoginViewProtocol)
    func viewDidTapConfirmButton(_ view: LoginViewProtocol)
    func viewDidTapRegisterButton(_ view: LoginViewProtocol)
    func viewDidTapFacebookButton(_ view: LoginViewProtocol)
}

class LoginView: UIView, LoginViewProtocol {

    weak var delegate: LoginViewDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var resetPasswordButton: UIButton!

    class func create() -> LoginView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? LoginView else {
            fatalError("Nib \(viewNibName) does not contain Login View as first object")
        }

        return view
    }

    override func awakeFromNib() {

        super.awakeFromNib()

        self.titleLabel.attributedText = self.compileTitleLabelText()
        
        self.resetPasswordButton.setTitleColor(Theme.current.regularColor(), for: .normal)
        
        Theme.current.apply(borderedButton: self.confirmButton, isOpposit: true)
        let createButtonTitle = self.compileCreateButtonTitle()
        self.confirmButton.setAttributedTitle(createButtonTitle, for: .normal)
        
        Theme.current.apply(borderedButton: self.registrationButton, isOpposit: true)
        let registrationButtonTitle = self.compileRegisterButtonTitle()
        self.registrationButton.setAttributedTitle(registrationButtonTitle, for: .normal)
        
        Theme.current.apply(filledButton: self.facebookButton)
        self.facebookButton.backgroundColor = RGB(47, 121, 168)
        
    }

    func compileTitleLabelText() -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()

        let labelFont = FontHelper.defaultMediumFont(withSize: UI.headerFontSize)
        let textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString("Get started with ", comment: "Login"),
                attributes: textPartAttributes)
        titleLabelText.append(textPart)

        let logoImage = UIImage(named: "logo_text")
        let logoAttachment = NSTextAttachment()
        logoAttachment.image = logoImage
        logoAttachment.bounds = CGRect(origin: CGPoint(x: 0, y: -labelFont.lineHeight / 2), size: logoImage!.size)
        let logoAttachmentString = NSAttributedString(attachment: logoAttachment)
        titleLabelText.append(logoAttachmentString)

        return titleLabelText
    }

    func compileCreateButtonTitle() -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString("Sign In", comment: "Sign In"),
                                          attributes: textPartAttributes)
        
        titleLabelText.append(textPart)
        
        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        //titleLabelText.append(NSAttributedString(string: FontImage.checkbox, attributes: textPartAttributes))
        return titleLabelText
    }
    func compileRegisterButtonTitle() -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString("Create a new account", comment: "Create a new account"),
                                          attributes: textPartAttributes)
        
        titleLabelText.append(textPart)
        
        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        //titleLabelText.append(NSAttributedString(string: FontImage.checkbox, attributes: textPartAttributes))
        return titleLabelText
    }

    // MARK: - LoginViewProtocol

    func forceEndEditing() {
        self.endEditing(true)
    }

    // MARK: - Button actions
    
    @IBAction func resetPasswordAction(_ sender: UIButton) {
        delegate?.viewDidTapResetPassword(self)
    }
    
    @IBAction func registrationButtonAction(_ sender: Any) {
        delegate?.viewDidTapRegisterButton(self)
    }

    @IBAction func confirmButtonAction() {
        delegate?.viewDidTapConfirmButton(self)
    }
    
    @IBAction func facebookButtonAction() {
        delegate?.viewDidTapFacebookButton(self)
    }
}
