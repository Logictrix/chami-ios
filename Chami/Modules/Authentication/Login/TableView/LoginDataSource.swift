//
//  LoginDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LoginDataSource: BaseTableViewDataSource {
    
     var cellDelegate: FormCellDelegate?
    private let model: LoginModelProtocol
    private let cellReuseId = "LoginTableViewCell"
    
    init(withModel model: LoginModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: LoginTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.model.numberOfGroup()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let group = LoginFieldGroup(rawValue: section) {
            let numberOfRows = self.model.numberOfFields(inGroup: group)
            return numberOfRows
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? LoginTableViewCell,
            let registrationField = self.model.getField(withIndexPath: indexPath) else {
                fatalError()
        }
        
        cell.delegate = cellDelegate
        cell.field = registrationField
        if let field = cell.field {
            switch field.fieldType {
            case .email:
                cell.valueTextField.placeholder = NSLocalizedString("Email", comment: "Email placeholder")
            case .password:
                cell.valueTextField.placeholder = NSLocalizedString("Password", comment: "Password placeholder")
            default:
                cell.valueTextField.placeholder = nil
            }
            
        }
        return cell
    }
    
}
