//
//  LoginTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LoginTableViewCell: FormTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = ColorHelper.backgroundFormFieldColor()
    }
}


extension LoginTableViewCell {
   // OLD CODE
//    override var field: FormField? {
//        didSet {
//            if let field = self.field {
//                switch field.fieldType {
//                case .email:
//                    self.valueTextField.placeholder = NSLocalizedString("Email", comment: "Email placeholder")
//                case .password:
//                    self.valueTextField.placeholder = NSLocalizedString("Password", comment: "Password placeholder")
//                default:
//                    self.valueTextField.placeholder = nil
//                }
//            }
//        }
//    }
}
