//
//  RegistrationViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import PKHUD

typealias RegistrationViewControllerType = BaseViewController<RegistrationModelProtocol, RegistrationViewProtocol, RegistrationRouter>

class RegistrationViewController: RegistrationViewControllerType, UITableViewDelegate {
    
    fileprivate var dataSource: RegistrationDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: RegistrationViewProtocol, model: RegistrationModelProtocol, router: RegistrationRouter, dataSource: RegistrationDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
    }
    
    internal required init(withView view: RegistrationViewProtocol!, model: RegistrationModelProtocol!, router: RegistrationRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Create Account", comment: "Registration")
        
        self.canHandleMeetupRequests = false
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
//        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Login", style: .plain, target: self, action: #selector(login))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.isNavigationBarHidden = false
    }
    
    fileprivate func connectTableViewDependencies() {
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Private methods
    
    fileprivate func isRegistrationDataCorrect() -> Bool {
        
        let modelCheckResult = self.model.checkInputedData()
        if !modelCheckResult.result {
            if let warning = modelCheckResult.warning {
                AlertManager.showWarning(withMessage: warning, onController: self, closeHandler: nil)
            }
        }
        
        return modelCheckResult.result
    }

    
    // MARK: - Actions
    // WARNING: 
//    @objc fileprivate func testLogin() {
//        let controller:SetupProfileViewController = SetupProfileBuilder.viewController()
//        self.navigationController?.pushViewController(controller, animated: true);
//NavigationManager.shared.showMainScreen(animated: true)

//    @objc fileprivate func login() {
//        self.router?.navigateToLogin(from: self)
//    }
    
    fileprivate func registerUser() {
        
        self.setProgressVisible(visible: true)
        self.model.signUpUser({[weak self] (account) in
            AccountManager.shared.loggedUserIn()
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
        }) {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if (error as NSError).code == 5400 {
                AlertManager.showWarning(withMessage: "Email already exists.", onController: self!, closeHandler: nil)
            } else {
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: self!, closeHandler: nil)
            }
            
            print("error: \(error)")
        }
    }

    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let fieldGroup = RegistrationFieldGroup(rawValue: section) else {
            return 0
        }
        
        let headerHeight: CGFloat = 8
        switch fieldGroup {
        case .email,
             .passwords:
            return headerHeight
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let fieldGroup = RegistrationFieldGroup(rawValue: section) else {
            return nil
        }
        
        switch fieldGroup {
        case .email,
             .passwords:
            let header = UIView(frame: .zero)
            header.backgroundColor = self.view.backgroundColor
            return header
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let fieldGroupPosition = self.model.getFieldPosition(atIndexPath: indexPath)

        var roundCorners: UIRectCorner = []
        if fieldGroupPosition.contains(.start) {
            roundCorners = roundCorners.union([.topLeft, .topRight])
        }
        if fieldGroupPosition.contains(.end) {
            roundCorners = roundCorners.union([.bottomLeft, .bottomRight])
        }
        cell.contentView.roundCorners(corners: roundCorners, radius: 6, separatorHeight: 2)
    }

}

// MARK: - RegistrationViewDelegate

extension RegistrationViewController: RegistrationViewDelegate {
    func viewTapAgreeTermsControl(_ view: RegistrationViewProtocol) {
        self.model.changeAgreeWithTerms()
    }

    func viewDidTapConfirmButton(_ view: RegistrationViewProtocol) {
        self.customView.forceEndEditing()

        if self.isRegistrationDataCorrect() {
            self.registerUser()
        }
    }
    
    func viewDidTapPrivacyPolicy(_ view: RegistrationViewProtocol) {
        let attributedText = NSAttributedString.loadRTF(from: "PrivacyPolicy")
        let pp_item: HelpAndFaqDataType = ("Privacy Policy", "", attributedText)
        
        self.router?.navigateToHelpDetailScreen(from: self, withHelpItem: pp_item)
    }
    func viewDidTapTermsOfService(_ view: RegistrationViewProtocol) {
        let attributedText = NSAttributedString.loadRTF(from: "TermsOfService")
        let ts_item: HelpAndFaqDataType = ("Terms of Service", "", attributedText)
        
        self.router?.navigateToHelpDetailScreen(from: self, withHelpItem: ts_item)
    }
}

// MARK: - RegistrationModelDelegate

extension RegistrationViewController: RegistrationModelDelegate {
    
    func modelDidUpdateData(_ model: RegistrationModelProtocol) {
        self.customView.setAgreeTermsControl(model.agreeWithTerms)
        self.customView.tableView.reloadData()
    }
}

// MARK: - RegistrationCellDelegate

extension RegistrationViewController: FormCellDelegate {
    
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didChangeString string: String?) where CellType: UITableViewCell {
        guard let indexPath = self.customView.tableView.indexPath(for: cell),
            let registrationFieldType = self.model.getRegisterFieldType(atIndexPath: indexPath) else {
                fatalError()
        }
        
        if registrationFieldType == .email {
            if let cell = cell as? RegistrationTableViewCell {
                let validationState = self.model.validateValue(string, fieldType: registrationFieldType)
                if validationState == .correct {
                    self.model.isLoginUnique(string!, completion: { (unique) in
                        cell.valueTextField.textColor = unique ? .darkText : .red
                    })
                } else {
                    cell.valueTextField.textColor = .darkText
                }
            }
            
        }
    }
    
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didEnterString string: String?) where CellType: UITableViewCell {
        
        guard let indexPath = self.customView.tableView.indexPath(for: cell),
            let registrationFieldType = self.model.getRegisterFieldType(atIndexPath: indexPath),
            let field = self.model.getField(withIndexPath: indexPath) else {
                fatalError()
        }
        
        field.value = string as AnyObject?
        
        let validationState = self.model.validateValue(string, fieldType: registrationFieldType)
        self.model.setValidationState(validationState, fieldType: registrationFieldType)

        self.customView.tableView.reloadRows(at: [indexPath], with: .automatic)
        self.dataSource.activateNextCell(forCellAtIndexPath: indexPath, tableView: self.customView.tableView)
    }
}
