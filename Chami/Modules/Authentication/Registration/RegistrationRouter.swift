//
//  RegistrationRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RegistrationRouter: NSObject {
    
    func navigateToLogin(from vc: UIViewController) {
        let loginVC = LoginBuilder.viewController()

        vc.navigationController?.pushViewController(loginVC, animated: true)
    }
    
    func navigateToHelpDetailScreen(from vc: UIViewController, withHelpItem helpItem: HelpAndFaqDataType) {
        let nextVC = HelpDetailBuilder.viewController()
        nextVC.setHelpItem(helpItem: helpItem)
        vc.navigationController?.pushViewController(nextVC, animated: true)
        nextVC.isFullScreen = true
    }
}
