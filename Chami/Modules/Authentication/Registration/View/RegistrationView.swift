//
//  RegistrationView.swift
//  Stadio
//
//  Created by Igor Markov on 6/7/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

protocol RegistrationViewProtocol: NSObjectProtocol {
    
    weak var delegate: RegistrationViewDelegate? { get set }
    weak var tableView: UITableView! { get set }

    func setAgreeTermsControl(_ selected: Bool)
    func forceEndEditing()
}

protocol RegistrationViewDelegate: NSObjectProtocol {

    func viewTapAgreeTermsControl(_ view: RegistrationViewProtocol)
    func viewDidTapConfirmButton(_ view: RegistrationViewProtocol)
    func viewDidTapPrivacyPolicy(_ view: RegistrationViewProtocol)
    func viewDidTapTermsOfService(_ view: RegistrationViewProtocol)
    
}

class RegistrationView: UIView, RegistrationViewProtocol {

    weak var delegate: RegistrationViewDelegate?
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var agreeTermsButton: UIButton!
    @IBOutlet weak var termsTextView: UITextView!
    
    @IBOutlet weak var confirmButton: UIButton!

    class func create() -> RegistrationView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? RegistrationView else {
            fatalError("Nib \(viewNibName) does not contain Registration View as first object")
        }
        
        return view
    }

    override func awakeFromNib() {
        
        super.awakeFromNib()

        self.titleLabel.attributedText = self.compileTitleLabelText()
        self.termsTextView.attributedText = self.compileTermsLabelText()
        
        Theme.current.apply(borderedButton: self.confirmButton)
        Theme.current.apply(onlyBorderedButton: self.confirmButton, isOpposit: true)
        let createButtonTitle = self.compileCreateButtonTitle()
        self.confirmButton.setAttributedTitle(createButtonTitle, for: .normal)
    }
    
    func compileTitleLabelText() -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()

        let labelFont = FontHelper.defaultMediumFont(withSize: UI.headerFontSize)
        let textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: labelFont,
                                                                 NSAttributedString.Key.foregroundColor: ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString("Get started with ", comment: "Register"),
                                          attributes: textPartAttributes)
        titleLabelText.append(textPart)
        
        let logoImage = UIImage(named: "logo_text")
        let logoAttachment = NSTextAttachment()
        logoAttachment.image = logoImage
        logoAttachment.bounds = CGRect(origin: CGPoint(x: 0, y: -labelFont.lineHeight / 2), size: logoImage!.size)
        let logoAttachmentString = NSAttributedString(attachment: logoAttachment)
        titleLabelText.append(logoAttachmentString)
        
        return titleLabelText
    }
    
    func compileTermsLabelText() -> NSAttributedString {
        
        
        let labelFont = FontHelper.defaultFont(withSize: UI.labelFontSize)
        let textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.defaultFormFieldTextColor()]
        var linkPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.linkTextColor(),
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.underlineStyle.rawValue): NSNumber(value: NSUnderlineStyle.single.rawValue)]
        
        
        let termsLabelText = NSMutableAttributedString()
        let textPart = NSAttributedString(string: NSLocalizedString("To continue, you must agree to the chami ", comment: "Register"), attributes: textPartAttributes)
        termsLabelText.append(textPart)
        
        /// OLD CODE
        linkPartAttributes[NSAttributedString.Key.link] = "termsofservice"
        let termsLink1 = NSAttributedString(string: NSLocalizedString("Terms of Service", comment: "Register"), attributes: linkPartAttributes)
        termsLabelText.append(termsLink1)

        let termsAnd = NSAttributedString(string: NSLocalizedString(" and ", comment: "Register"), attributes: textPartAttributes)
        termsLabelText.append(termsAnd)

        linkPartAttributes[NSAttributedString.Key.link] = "privacypolicy"
        let termsLink2 = NSAttributedString(string: NSLocalizedString("Privacy Policy", comment: "Register"), attributes: linkPartAttributes)
        termsLabelText.append(termsLink2)
        
        
        return termsLabelText
    }
    
    func compileCreateButtonTitle() -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                 NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString("Create your chami account ", comment: "Register"),
                                          attributes: textPartAttributes)

        titleLabelText.append(textPart)

        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        titleLabelText.append(NSAttributedString(string: FontImage.checkbox, attributes: textPartAttributes))
        return titleLabelText
    }
    
    // MARK: - RegistrationViewProtocol
    
    func setAgreeTermsControl(_ selected: Bool) {
        self.agreeTermsButton.isSelected = selected
    }

    func forceEndEditing() {
        self.endEditing(true)
    }
    
    // MARK: - Button actions
    
    @IBAction func tapAgreeButton() {
        self.delegate?.viewTapAgreeTermsControl(self)
    }
    
    @IBAction func confirmButtonAction() {
        self.delegate?.viewDidTapConfirmButton(self)
    }
}
extension RegistrationView: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange) -> Bool {
        let str = URL.absoluteString
        if str == "termsofservice" {
            self.delegate?.viewDidTapTermsOfService(self)
        } else if str == "privacypolicy" {
            self.delegate?.viewDidTapPrivacyPolicy(self)
        }
        return false
    }
}
