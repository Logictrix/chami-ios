//
//  RegistrationModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SwaggerClient

// MARK: - GroupPosition

struct GroupPosition: OptionSet {
    let rawValue: Int
    
    static let start    = GroupPosition(rawValue: 1 << 1)
    static let end      = GroupPosition(rawValue: 1 << 2)
}

// MARK: - RegistrationFieldType

enum RegistrationFieldType: Int {
    case firstName = 0
    case lastName = 1
    case email = 2
    case password = 3
    case confirm = 4
}

// MARK: - RegistrationFieldGroup

enum RegistrationFieldGroup: Int {
    case names = 0
    case email = 1
    case passwords = 2
}

// MARK: - RegistrationModelDelegate

protocol RegistrationModelDelegate: NSObjectProtocol {
    
    func modelDidUpdateData(_ model: RegistrationModelProtocol)
}

// MARK: - RegistrationModelProtocol

protocol RegistrationModelProtocol: NSObjectProtocol {
    
    weak var delegate: RegistrationModelDelegate? { get set }

    // MARK: - Agree with terms
    var agreeWithTerms: Bool { get }
    func changeAgreeWithTerms()

    // MARK: - Data
    func clearAllData()
    
    // MARK: - Fields
    func numberOfFields() -> Int
    func setValue(_ value: String, fieldType: RegistrationFieldType)
    func getField(withType registerFiledType: RegistrationFieldType) -> FormField?

    // MARK: - Field index pathes
    func getField(withIndexPath indexPath: IndexPath) -> FormField?
    func getRegisterFieldType(atIndexPath indexPath: IndexPath) -> RegistrationFieldType?
    func getFieldPosition(atIndexPath indexPath: IndexPath) -> GroupPosition
    
    // MARK: - Field groups
    func numberOfGroup() -> Int
    func numberOfFields(inGroup group: RegistrationFieldGroup) -> Int
    func getFieldTypes(inGroup group: RegistrationFieldGroup) -> [RegistrationFieldType]

    // MARK: - Validation
    func setValidationState(_ state: ValidationState, fieldType: RegistrationFieldType)
    func validateValue(_ value: String?, fieldType: RegistrationFieldType) -> ValidationState
    func warningMessageIfInputedDataIsInCorrect() -> String
    func checkInputedData() -> (result: Bool, warning: String?)
    func isLoginUnique(_ login: String, completion: @escaping ((_ unique: Bool) -> ()))
    
    // MARK: Log In
    func signUpUser(_ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ()))
}

// MARK: - RegistrationModel

class RegistrationModel: NSObject, RegistrationModelProtocol {



    typealias FieldTypeAndField = (type: RegistrationFieldType, field: FormField)

    var agreeWithTerms: Bool {
        didSet {
            if oldValue != agreeWithTerms {
                self.delegate?.modelDidUpdateData(self)
            }
        }
    }
    
    weak var delegate: RegistrationModelDelegate?
    
    fileprivate let service: AccountServiceProtocol
    fileprivate let fields: [RegistrationFieldType : FormField]
    fileprivate let fieldGroups: [RegistrationFieldGroup: [RegistrationFieldType]]
    
    init(withService service: AccountServiceProtocol) {
        
        self.service = service
        self.agreeWithTerms = false
        self.fields = RegistrationModel.setupFields()
        self.fieldGroups = [.names:     [.firstName, .lastName],
                            .email:     [.email],
                            .passwords: [.password, .confirm]]
    }
    
    fileprivate class func setupFields() -> [RegistrationFieldType : FormField] {
        
        let firstNameField = FormField()
        firstNameField.key = NSLocalizedString("First Name", comment: "Registeration field First Name")
        firstNameField.fieldType = .firstName
        firstNameField.keyboardType = .string
        firstNameField.returnButtonType = .next
        firstNameField.validationState = .incorrect
        
        let lastNameField = FormField()
        lastNameField.key = NSLocalizedString("Last Name", comment: "Registeration field Last Name")
        lastNameField.fieldType = .lastName
        lastNameField.keyboardType = .string
        lastNameField.returnButtonType = .next
        lastNameField.validationState = .incorrect
        
        let emailField = FormField()
        emailField.key = NSLocalizedString("Email", comment: "Registration field Email")
        emailField.fieldType = .email
        emailField.keyboardType = .email
        emailField.returnButtonType = .next
        emailField.validationState = .incorrect
        
        let passwordField = FormField()
        passwordField.isSecure = true
        passwordField.key = NSLocalizedString("Password", comment: "Registration field Password")
        passwordField.fieldType = .password
        passwordField.keyboardType = .string
        passwordField.returnButtonType = .next
        passwordField.validationState = .incorrect
        
        let confirmPasswordField = FormField()
        confirmPasswordField.isSecure = true
        confirmPasswordField.key = NSLocalizedString("Confirm Password", comment: "Registration field Confirm Password")
        confirmPasswordField.fieldType = .confirmPassword
        confirmPasswordField.keyboardType = .string
        confirmPasswordField.returnButtonType = .done
        confirmPasswordField.validationState = .incorrect

        // Debug
//        firstNameField.value = "va_first" as AnyObject?
//        lastNameField.value = "va_last" as AnyObject?
//        emailField.value = "testus1@gmail.com" as AnyObject?
//        passwordField.value = "12341234" as AnyObject?
//        confirmPasswordField.value = "qwerty" as AnyObject?
//        firstNameField.validationState = .correct
//        lastNameField.validationState = .correct
//        emailField.validationState = .correct
//        passwordField.validationState = .correct
//        confirmPasswordField.validationState = .correct
        
        return [.firstName: firstNameField,
                .lastName: lastNameField,
                .email: emailField,
                .password: passwordField,
                .confirm: confirmPasswordField]
    }
    
    func changeAgreeWithTerms() {
        self.agreeWithTerms = !self.agreeWithTerms
    }
    
    // MARK: - Data

    func clearAllData() {
        self.agreeWithTerms = false
        for field in self.fields.values {
            field.value = nil
        }
        
        if let delegate = delegate {
            delegate.modelDidUpdateData(self)
        }
    }
    
    // MARK: - Fields

    func numberOfFields() -> Int {
        return fields.count
    }
    
    func setValue(_ value: String, fieldType: RegistrationFieldType) {
        if let field = fields[fieldType] {
            field.value = value as AnyObject?
        }
    }
    
    func getField(withType registerFiledType: RegistrationFieldType) -> FormField? {
        return fields[registerFiledType]
    }
    
    // MARK: - Field index pathes

    func getField(withIndexPath indexPath: IndexPath) -> FormField? {
        guard let group = RegistrationFieldGroup(rawValue: indexPath.section),
            let fieldGroup = self.fieldGroups[group] else {
                return nil
        }
        
        guard indexPath.row < fieldGroup.count else {
            return nil
        }
        
        let fieldType = fieldGroup[indexPath.row]
        let registrationField =  self.fields[fieldType]
        return registrationField
    }

    func getRegisterFieldType(atIndexPath indexPath: IndexPath) -> RegistrationFieldType? {
        guard let group = RegistrationFieldGroup(rawValue: indexPath.section),
            let fieldGroup = self.fieldGroups[group] else {
                return nil
        }
        
        return indexPath.row < fieldGroup.count ? fieldGroup[indexPath.row] : nil
    }
    
    func getFieldPosition(atIndexPath indexPath: IndexPath) -> GroupPosition {
        guard let group = RegistrationFieldGroup(rawValue: indexPath.section),
            let fieldGroup = self.fieldGroups[group] else {
                return []
        }
        
        var groupPosition: GroupPosition = []
        if indexPath.row == 0 {
            groupPosition.insert(.start)
        }
        if indexPath.row == fieldGroup.count - 1 {
            groupPosition.insert(.end)
        }
        
        return groupPosition
    }
    
    // MARK: - Field groups

    func numberOfGroup() -> Int {
        return self.fieldGroups.count
    }
    
    func numberOfFields(inGroup group: RegistrationFieldGroup) -> Int {
        return self.getFieldTypes(inGroup: group).count
    }
    
    func getFieldTypes(inGroup group: RegistrationFieldGroup) -> [RegistrationFieldType] {
        if let fieldGroup = self.fieldGroups[group] {
            return fieldGroup
        } else {
            return []
        }
    }
    
    // MARK: - Validation

    func setValidationState(_ state: ValidationState, fieldType: RegistrationFieldType) {
        if let field = fields[fieldType] {
            field.validationState = state
        }
    }
    
    func validateValue(_ value: String?, fieldType: RegistrationFieldType) -> ValidationState {
        
        
        guard let value = value else {
            return .incorrect
        }
        
        switch fieldType {
        case .firstName:
            return ValidationUtils.validateAccountname(value) ? .correct : .incorrect
        case .lastName:
            return ValidationUtils.validateAccountname(value) ? .correct : .incorrect
        case .email:
            return ValidationUtils.validateEmail(value) ? .correct : .incorrect
        case .password:
            return ValidationUtils.validatePassword(value) ? .correct : .incorrect
        case .confirm:
//            if !ValidationUtils.validatePassword(value) {
//                return .incorrect
//            }
            
            if let passwordValue = fields[.password]?.value as? String {
//                'Password must contain minimum 6 symbols'
                return value == passwordValue ? .correct : .incorrect
            } else {
                return .incorrect
            }
        }
    }
    
    func checkInputedData() -> (result: Bool, warning: String?) {
        let orderedFieldTypes = fields.keys.sorted(by: { $0.rawValue < $1.rawValue })
        for fieldType in orderedFieldTypes {
            guard let field = self.getField(withType: fieldType) else {
                continue
            }
            
            if (field.validationState != .correct) {
                let fieldValue = field.stringFromValue()
                guard let fieldKey = field.key else {
                    fatalError()
                }
                
                var warning = ""
                if (field.fieldType == .password) {
                    warning = ValidationUtils.validationWarningMessageForPasswordWithKey(fieldKey, value: fieldValue)
                } else if (field.fieldType == .confirmPassword) {
                    warning = NSLocalizedString("Please confirm your Password", comment: "Please confirm your Password")
                } else {
                    warning = ValidationUtils.validationWarningMessageForStringWithKey(fieldKey, value: fieldValue)
                }
                
                if warning.characters.count > 0 {
                    return (false, warning)
                }
            }
        }
        
        if !self.agreeWithTerms {
            return (false, NSLocalizedString("Please check Terms & Conditions", comment: "Registration"))
        }
        
        return (true, nil)
    }
    
    func isLoginUnique(_ login: String, completion: @escaping ((Bool) -> ())) {
        self.service.isEmailUnique(email: login) { (unique) in
            completion(unique)
        }
    }
    
    func warningMessageIfInputedDataIsInCorrect() -> String {
        
        var message = ""
        for (_, field) in fields {
            if (field.validationState != .correct) {
                let fieldValue = field.stringFromValue()
                guard let fieldKey = field.key else {
                    fatalError()
                }
                
                if (field.fieldType == .password) {
                    message = ValidationUtils.validationWarningMessageForPasswordWithKey(fieldKey, value: fieldValue)
                } else {
                    message = ValidationUtils.validationWarningMessageForStringWithKey(fieldKey, value: fieldValue)
                }
                
                break
            }
        }
        
        if message.isEmpty && !self.agreeWithTerms {
            return NSLocalizedString("Please check Terms & Conditions", comment: "Registration")
        }
        
        return message
    }
    
    // MARK: - Registration
    
    func signUpUser(_ success: @escaping ((_ account: Account) -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        
        guard let email = fields[.email]?.value as? String,
            let password = fields[.password]?.value as? String,
            let firstName = fields[.firstName]?.value as? String,
            let lastName = fields[.lastName]?.value as? String else {
                
                let error = NSError(domain: AppError.errorModelDomain,
                                    code: AppError.invalidParametersErrorCode,
                                    userInfo: ErrorHelper.compileUserInfo(forModelErrorCode: AppError.invalidParametersErrorCode) as! [String : Any])
                failure(error)
                return
        }
        
        self.service.signUpUser(withFirstName: firstName, lastName: lastName, email: email, password: password, coordinate: LocationManager.shared.coordinate, success: { (account) in
            print("account: \(account)")
            
            AccountManager.shared.currentUser = account
            LocationManager.shared.sendMyLocationToServer()
            success(account)
        }) { (error) in
            
            var userInfo = UserInfoType()
            //userInfo[NSLocalizedDescriptionKey] = NSLocalizedString(error.localizedDescription, comment: "")
            userInfo[NSLocalizedDescriptionKey] = NSLocalizedString(ErrorHelper.errorMessageFor(Error: error) as String, comment: "")
            
            //            let descriptionDictionary = (error as NSError).userInfo[SWGResponseObjectErrorKey] as? [String: String]
            //
            //let resp = (error as NSError).userInfo["SWGResponseObject"] as SWGResponseObject
            //            if let descriptionDictionary = (error as NSError).userInfo[SWGResponseObjectErrorKey] as? [String: String], let errorDescription = descriptionDictionary["error"] {
            //
            //                            if errorDescription.characters.count > 0 {
            //                                userInfo[NSLocalizedDescriptionKey] = FormatterHelper.removeHTML(errorDescription)
            //                            } else {
            //                               // userInfo[NSLocalizedDescriptionKey] = NSLocalizedString("Some server response error", comment: errorAreaComment)
            //                            }
            //                        }
            
            
            let error = NSError(domain: AppError.errorModelDomain,
                                code: AppError.unsuccessResultErrorCode,
                                userInfo: userInfo as! [String : Any])
            failure(error)
        }
    }
}
