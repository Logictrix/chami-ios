//
//  RegistrationBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RegistrationBuilder: NSObject {

    class func viewController() -> RegistrationViewController {
        let authService = AccountService()
        let view: RegistrationViewProtocol = RegistrationView.create()
        let model: RegistrationModelProtocol = RegistrationModel(withService: authService)
        let dataSource = RegistrationDataSource(withModel: model)
        let router = RegistrationRouter()
        
        let viewController = RegistrationViewController(withView: view, model: model, router: router, dataSource: dataSource)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
