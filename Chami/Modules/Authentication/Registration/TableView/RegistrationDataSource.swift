//
//  RegistrationDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RegistrationDataSource: BaseTableViewDataSource {
    
    weak var cellDelegate: FormCellDelegate?
    private let model: RegistrationModelProtocol
    private let cellReuseId = "RegistrationTableViewCell"
    
    init(withModel model: RegistrationModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: RegistrationTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.model.numberOfGroup()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let group = RegistrationFieldGroup(rawValue: section) {
            let numberOfRows = self.model.numberOfFields(inGroup: group)
            return numberOfRows
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? RegistrationTableViewCell,
            let registrationField = self.model.getField(withIndexPath: indexPath) else {
                fatalError()
        }
        
        cell.delegate = cellDelegate
        cell.field = registrationField
        
        
        if let field = cell.field {
            switch field.fieldType {
            case .firstName:
                cell.valueTextField.placeholder = NSLocalizedString("First Name", comment: "First name placeholder")
            case .lastName:
                cell.valueTextField.placeholder = NSLocalizedString("Last Name", comment: "Last name placeholder")
            case .email:
                cell.valueTextField.placeholder = NSLocalizedString("Email address", comment: "Email placeholder")
            case .password:
                cell.valueTextField.placeholder = NSLocalizedString("Create Password", comment: "Password placeholder")
            case .confirmPassword:
                cell.valueTextField.placeholder = NSLocalizedString("Confirm Password", comment: "Confirm password placeholder")
            default:
                cell.valueTextField.placeholder = nil
            }
        }
        
        
        return cell
    }
}
