//
//  RegistrationTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RegistrationTableViewCell: FormTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.contentView.backgroundColor = ColorHelper.backgroundFormFieldColor()
    }
}


extension RegistrationTableViewCell {
   // OLD CODE
//    override var field: FormField? {
//        didSet {
//            if let field = self.field {
//                switch field.fieldType {
//                case .firstName:
//                    self.valueTextField.placeholder = NSLocalizedString("First Name", comment: "First name placeholder")
//                case .lastName:
//                    self.valueTextField.placeholder = NSLocalizedString("Last Name", comment: "Last name placeholder")
//                case .email:
//                    self.valueTextField.placeholder = NSLocalizedString("Email address", comment: "Email placeholder")
//                case .password:
//                    self.valueTextField.placeholder = NSLocalizedString("Create Password", comment: "Password placeholder")
//                case .confirmPassword:
//                    self.valueTextField.placeholder = NSLocalizedString("Confirm Password", comment: "Confirm password placeholder")
//                default:
//                    self.valueTextField.placeholder = nil
//                }
//            }
//        }
//    }
}
