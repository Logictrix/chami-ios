//
//  HelpDetailView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol HelpDetailViewDelegate: NSObjectProtocol {
    
}

protocol HelpDetailViewProtocol: NSObjectProtocol {
    
    weak var delegate: HelpDetailViewDelegate? { get set }
    var topView: UIView! { get }
    
    func createGradientLayer(isStudent: Bool)
    weak var titleLabel: UILabel! { get }
    weak var descriptionTextView: UITextView! { get }

}

class HelpDetailView: UIView, HelpDetailViewProtocol{

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionTextView: UITextView!
    
    var gradientLayer: CAGradientLayer!
    
    func createGradientLayer(isStudent: Bool) {
        if gradientLayer != nil {
            gradientLayer.removeFromSuperlayer()
        }
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.topView.bounds
        
        if isStudent {
            let color1 = RGB(27, 229, 243)
            let color2 = RGB(13, 173, 247)

            gradientLayer.colors = [color1.cgColor, color2.cgColor]
            
            self.titleLabel.textColor = UIColor.white
        } else {
//            let color1 = RGB(247, 223, 89)
//            let color2 = RGB(241, 191, 56)
            let color1 = RGB(246, 239, 221)
            let color2 = RGB(246, 239, 221)

            gradientLayer.colors = [color1.cgColor, color2.cgColor]
            
            self.titleLabel.textColor = UIColor.darkGray
        }
        self.topView.layer.addSublayer(gradientLayer)
    }
    
    
    class func create() -> HelpDetailView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? HelpDetailView else {
            fatalError("Nib \(viewNibName) does not contain HelpDetail View as first object")
        }
        
        return view
    }
    
    // MARK: - HelpDetailView interface methods

    weak var delegate: HelpDetailViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - IBActions
    
}
