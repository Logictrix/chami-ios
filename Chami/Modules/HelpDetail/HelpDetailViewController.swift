//
//  HelpDetailViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias HelpDetailViewControllerType = BaseViewController<HelpDetailModelProtocol, HelpDetailViewProtocol, HelpDetailRouter>

class HelpDetailViewController: HelpDetailViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: HelpDetailViewProtocol!, model: HelpDetailModelProtocol!, router: HelpDetailRouter?) {
        super.init(withView: view, model: model, router: router)
        self.title = NSLocalizedString("Help & FAQs", comment: "Help")
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        //self.isFullScreen = true
        

        customView.delegate = self
        model.delegate = self
    }
    var isFullScreen: Bool = false {
        didSet {
            var topViewRect = self.customView.topView.frame
            var textViewRect = self.view.bounds
            if self.isFullScreen == false {
                self.title = NSLocalizedString("Help & FAQs", comment: "Help")
                topViewRect.size.height = 100
                textViewRect.origin.y = topViewRect.size.height
                textViewRect.size.height -= topViewRect.size.height
            } else {
                self.title = self.model.helpItem.title
                topViewRect.size.height = 0
            }
            self.customView.topView.frame = topViewRect
            self.customView.descriptionTextView.frame = textViewRect
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
        
    }
    //    override func viewDidAppear(_ animated: Bool) {
    //        super.viewDidAppear(animated)
    //        self.customView.descriptionTextView.setContentOffset(.zero, animated: true)
    //    }

    // MARK: - Public methods
    func setHelpItem(helpItem: HelpAndFaqDataType) {
        self.model.helpItem = helpItem
    }
    
    // MARK: - Private methods
    private func updateView() {
        DispatchQueue.main.async {
            self.customView.titleLabel.text = self.model.helpItem.title
            if self.model.helpItem.attributedDescription != nil {
                self.customView.descriptionTextView.attributedText = self.model.helpItem.attributedDescription
            } else {
                self.customView.descriptionTextView.text = self.model.helpItem.description
            }
            if self.model.accountRole == .tutor {
                self.customView.createGradientLayer(isStudent: false)
            } else {
                self.customView.createGradientLayer(isStudent: true)
            }
        }
    }

}

// MARK: - HelpDetailViewDelegate

extension HelpDetailViewController: HelpDetailViewDelegate {

}

// MARK: - HelpDetailModelDelegate

extension HelpDetailViewController: HelpDetailModelDelegate {
}
