//
//  HelpDetailModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol HelpDetailModelDelegate: NSObjectProtocol {

}

protocol HelpDetailModelProtocol: NSObjectProtocol {
    
    weak var delegate: HelpDetailModelDelegate? { get set }
    var accountRole: AccountRole { get set }
    var helpItem: HelpAndFaqDataType { get set }


}

class HelpDetailModel: NSObject, HelpDetailModelProtocol {
    
    // MARK: - HelpDetailModel methods

    weak var delegate: HelpDetailModelDelegate?
    
    var accountRole: AccountRole = AccountManager.shared.currentRole
    var helpItem: HelpAndFaqDataType = ("", "", nil)

    /** Implement HelpDetailModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
