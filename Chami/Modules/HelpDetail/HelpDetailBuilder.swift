//
//  HelpDetailBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HelpDetailBuilder: NSObject {

    class func viewController() -> HelpDetailViewController {

        let view: HelpDetailViewProtocol = HelpDetailView.create()
        let model: HelpDetailModelProtocol = HelpDetailModel()
        let router = HelpDetailRouter()
        
        let viewController = HelpDetailViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
