//
//  UserSettingsModel.swift
//  Chami
//
//  Created by Serg Smyk on 07/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum UserSettingsMode {
    case accountDetails
    case profileInfo
}

protocol UserSettingsModelDelegate: NSObjectProtocol {
    func userSettingsModeDidChange(mode: UserSettingsMode)
}

protocol UserSettingsModelProtocol: NSObjectProtocol {
    
    weak var delegate: UserSettingsModelDelegate? { get set }
    var account: Account { get }
    var mode: UserSettingsMode { get }
    
    func updateTo(mode: UserSettingsMode)
    
    func updateChamiUserInfo(completion: @escaping (Error?) -> Void)
}

class UserSettingsModel: NSObject, UserSettingsModelProtocol {
    
    fileprivate let chamiService: ChamiServiceProtocol
    
    init(withAccount account: Account) {
        self.account = account
        self.mode = .accountDetails

        self.chamiService = ChamiService()
        super.init()
    }
    
    // MARK: - UserSettingsModel methods

    weak var delegate: UserSettingsModelDelegate?
    var account: Account
    
    var mode: UserSettingsMode = .accountDetails {
        didSet {
            if oldValue != self.mode {
                self.delegate?.userSettingsModeDidChange(mode: self.mode)
            }
        }
    }
    
    func updateTo(mode: UserSettingsMode) {
        self.mode = mode
    }
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
    
    func updateChamiUserInfo(completion: @escaping (Error?) -> Void) {
        self.chamiService.getTutorDetail(withId: self.account.id, success: { [weak self] (user) in
            guard let strongSelf = self else { return }
            strongSelf.account.rating = user.rating
            completion(nil)
        }) { (error) in
            completion(error as NSError)
            
        }
    }
}
