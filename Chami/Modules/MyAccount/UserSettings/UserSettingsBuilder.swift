//
//  UserSettingsBuilder.swift
//  Chami
//
//  Created by Serg Smyk on 07/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class UserSettingsBuilder: NSObject {

    class func viewController() -> UserSettingsViewController {

        let view: UserSettingsViewProtocol = UserSettingsView.create()
        let model: UserSettingsModelProtocol = UserSettingsModel(withAccount: AccountManager.shared.currentUser!) // TODO: remove force unwrap
        let router = UserSettingsRouter()
        
        let viewController = UserSettingsViewController(withView: view, model: model, router: router)
        return viewController
    }
}
