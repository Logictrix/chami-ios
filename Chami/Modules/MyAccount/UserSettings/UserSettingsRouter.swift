//
//  UserSettingsRouter.swift
//  Chami
//
//  Created by Serg Smyk on 07/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class UserSettingsRouter: NSObject {
    
    func navigateToEditAccountDetails(withAccount account: Account, from vc: UIViewController) {
        let editAccountDetails = AccountDetailsEditBuilder.viewController(withAccount: account)
        let navigationController = UINavigationController(rootViewController: editAccountDetails)
        vc.present(navigationController, animated: true, completion: nil)
    }

    func navigateToEditProfileInfo(withAccount account: Account, from vc: UIViewController) {
        let controller = SetupProfileBuilder.viewController()
        controller.model.isLoginMode = false
        vc.navigationController?.pushViewController(controller, animated: true)
    }
}
