//
//  UserSettingsView.swift
//  Chami
//
//  Created by Serg Smyk on 07/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol UserSettingsViewDelegate: NSObjectProtocol {
    
    func viewDidSelectMode(view: UserSettingsViewProtocol, mode: UserSettingsMode)
}

protocol UserSettingsViewProtocol: NSObjectProtocol {
    
    weak var delegate: UserSettingsViewDelegate? { get set }
    var containerView: UIView! { get  }

    var selectedMode: UserSettingsMode { get set }
}

class UserSettingsView: UIView, UserSettingsViewProtocol{

    weak var delegate: UserSettingsViewDelegate?
    @IBOutlet var containerView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!

    var selectedMode: UserSettingsMode = .accountDetails {
        didSet {
            if oldValue == selectedMode {
                return
            }
            
            switch selectedMode {
            case .accountDetails:
                self.segmentedControl.selectedSegmentIndex = 0
            case .profileInfo:
                self.segmentedControl.selectedSegmentIndex = 1
            }
        }
    }

    // MARK: - UserSettingsView interface methods

    class func create() -> UserSettingsView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? UserSettingsView else {
            fatalError("Nib \(viewNibName) does not contain UserSettings View as first object")
        }
        
        return view
    }

    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.segmentedControl.tintColor = Theme.current.regularColor()
    }
    
    // MARK: - IBActions
    
    @IBAction func onSegmentedControlClick(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex {
        case 0:
            self.delegate?.viewDidSelectMode(view: self, mode: .accountDetails)
        case 1:
            self.delegate?.viewDidSelectMode(view: self, mode: .profileInfo)
        default:
            break
        }
    }
}
