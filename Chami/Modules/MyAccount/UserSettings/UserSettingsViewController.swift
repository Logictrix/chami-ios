//
//  UserSettingsViewController.swift
//  Chami
//
//  Created by Serg Smyk on 07/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias UserSettingsViewControllerType = BaseViewController <UserSettingsModelProtocol, UserSettingsViewProtocol, UserSettingsRouter>

class UserSettingsViewController: UserSettingsViewControllerType {
    
    
    private var accountDetailsVC: AccountDetailsViewController!
    private var profileInformationVC: ProfileInfoViewController!
    
    // MARK: Initializers
    
    required init(withView view: UserSettingsViewProtocol!, model: UserSettingsModelProtocol!, router: UserSettingsRouter?) {
        super.init(withView: view, model: model, router: router)
        
        accountDetailsVC = AccountDetailsBuilder.viewController(withAccount: self.model.account)
        profileInformationVC = ProfileInfoBuilder.viewController(withAccount: self.model.account)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("My Account", comment: "My Account")
        
        customView.delegate = self
        model.delegate = self
        
        self.setupNavigationBar()
        
        self.setProgressVisible(visible: true)
        self.model.updateChamiUserInfo {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            strongSelf.setupContentViews()
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateViews()
    }
    
    fileprivate func setupNavigationBar() {
        let editButton = UIBarButtonItem(title: NSLocalizedString("Edit", comment: "My Account"), style: .plain, target: self, action: #selector(onEditClick))
        self.navigationItem.rightBarButtonItem = editButton
    }
    
    fileprivate func setupContentViews() {
        self.customView.containerView.addSubview(self.accountDetailsVC.view)
        self.addChild(self.accountDetailsVC)
        self.accountDetailsVC.view.frame = self.customView.containerView.bounds
        self.accountDetailsVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        
        self.customView.containerView.addSubview(self.profileInformationVC.view)
        self.addChild(self.profileInformationVC)
        self.profileInformationVC.view.frame = self.customView.containerView.bounds
        self.profileInformationVC.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    }
    
    fileprivate func updateViews() {
        self.customView.selectedMode = self.model.mode
        self.updateContentViews()
    }
    
    fileprivate func updateContentViews() {
        self.accountDetailsVC.view.isHidden = (self.model.mode != .accountDetails)
        self.profileInformationVC.view.isHidden = (self.model.mode != .profileInfo)
    }

    // MARK: - Actions
    
    @objc func onEditClick() {
        switch self.model.mode {
        case .accountDetails:
            self.router?.navigateToEditAccountDetails(withAccount: self.model.account, from: self)
        case .profileInfo:
            self.router?.navigateToEditProfileInfo(withAccount: self.model.account, from: self)
        }
    }
}

// MARK: - UserSettingsViewDelegate

extension UserSettingsViewController: UserSettingsViewDelegate {

    func viewDidSelectMode(view: UserSettingsViewProtocol, mode: UserSettingsMode) {
        self.model.updateTo(mode: mode)
    }
}

// MARK: - UserSettingsModelDelegate

extension UserSettingsViewController: UserSettingsModelDelegate {

    func userSettingsModeDidChange(mode: UserSettingsMode) {
        self.updateViews()
    }
}
