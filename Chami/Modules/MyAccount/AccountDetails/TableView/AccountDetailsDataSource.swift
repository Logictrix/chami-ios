//
//  AccountDetailsDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class AccountDetailsDataSource: NSObject, UITableViewDataSource {
    
    private let model: AccountDetailsModelProtocol
    private let cellReuseId = "AccountDetailsTableViewCell"
    
    init(withModel model: AccountDetailsModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: AccountDetailsTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: AccountDetailsTableViewCell, formField: FormField) {
        if let cellTitle = formField.key {
            cell.titleLabel.text = String("\(cellTitle):")
        } else {
            cell.titleLabel.text = nil
        }

        cell.valueLabel.text = formField.stringFromValue()
    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.fields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? AccountDetailsTableViewCell else {
            fatalError()
        }
        
        let accountDetailsField = self.model.fields[indexPath.row];
        self.configure(cell: cell, formField: accountDetailsField)
        
        return cell
    }
}
