//
//  AccountDetailsTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 17)
        self.valueLabel.font = FontHelper.defaultFont(withSize: 17)
        
    }
}
