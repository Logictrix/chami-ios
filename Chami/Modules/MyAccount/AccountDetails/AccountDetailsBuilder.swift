//
//  AccountDetailsBuilder.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsBuilder: NSObject {

    class func viewController(withAccount account: Account) -> AccountDetailsViewController {
        let view: AccountDetailsViewProtocol = AccountDetailsView.create()
        let model: AccountDetailsModelProtocol = AccountDetailsModel(withAccount: account)
        let dataSource = AccountDetailsDataSource(withModel: model)
        let router = AccountDetailsRouter()
        
        let viewController = AccountDetailsViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
