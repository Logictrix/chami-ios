//
//  AccountDetailsRouter.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsRouter: NSObject {
    
    func navigateToPaymentDetails(from vc: UIViewController) {
        StripeApiManager.shared.showPaymentViewController(fromViewController: vc, completion: {[weak vc] (saved, error) in
            guard let strongSelf = vc else { return }
            if let error = error {
                print("================\(error.localizedDescription)")
                print("strongSelf \(strongSelf)")
                print("================")
                //                        strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)
            }
            if saved == true {
                
            }

            
        })
        //        let alert = UIAlertController(title: nil, message: "not implemented", preferredStyle: .alert)
        //        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        //        alert.addAction(cancelAction)
        
        //        vc.present(alert, animated: true, completion: nil)
    }
}
