//
//  AccountDetailsViewController.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias AccountDetailsViewControllerType = BaseViewController<AccountDetailsModelProtocol, AccountDetailsViewProtocol, AccountDetailsRouter>

class AccountDetailsViewController: AccountDetailsViewControllerType, UITableViewDelegate {
    
    private var dataSource: AccountDetailsDataSource!
    private var headerView: ProfileInfoHeader?
    //private var accountDetailsHeader: AccountDetailsHeader
    private var accountDetailsFooter: AccountDetailsFooter
    
    
    // MARK: - Initializers

    convenience init(withView view: AccountDetailsViewProtocol, model: AccountDetailsModelProtocol, router: AccountDetailsRouter, dataSource: AccountDetailsDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
    }
    
    internal required init(withView view: AccountDetailsViewProtocol!, model: AccountDetailsModelProtocol!, router: AccountDetailsRouter?) {
        self.accountDetailsFooter = AccountDetailsFooter.create()

        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        self.setupTableViewHeaderFooter()
        
        self.updateHeaderView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)        
        self.updateView()
    }
    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    private func setupTableViewHeaderFooter() {
        
        if let headerView = ProfileInfoHeader.loadFromXib() as? ProfileInfoHeader {
            headerView.headerTitleLabel.textColor = ColorHelper.regularColor()
            //headerView.profileDescriptionLabel.textColor = ColorHelper.lightTextColor()
            //headerView.gradientView.colors = Theme.current.profileBackgroundGradientColor()
            self.headerView = headerView
            self.customView.tableView.tableHeaderView = self.headerView
        }
        
        //self.customView.tableView.tableHeaderView = self.accountDetailsHeader
        self.customView.tableView.tableFooterView = self.accountDetailsFooter
        self.accountDetailsFooter.didTapViewPaymentDetailsControl = { [weak self] in
            guard let strongSelf = self else { return }
            let text = "If you wish to check your bank account details please visit stripe.com and login with your details"
            AlertManager.showAlert(withTitle: "", message: text, onController: strongSelf, closeHandler: {
            })

//            guard let user = AccountManager.shared.currentUser else {
//                return;
//            }
//
//            if user.role == .tutor {
//            }
//
//            strongSelf.router?.navigateToPaymentDetails(from: strongSelf)
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        // Dynamic sizing for the header view
        if let headerView = self.customView.tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            // If we don't have this check, viewDidLayoutSubviews() will get
            // repeatedly, causing the app to hang.
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                self.customView.tableView.tableHeaderView = headerView
            }
        }
        
        // Dynamic sizing for the footer view
        if let footerView = self.customView.tableView.tableFooterView {
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var footerFrame = footerView.frame
            
            // If we don't have this check, viewDidLayoutSubviews() will get
            // repeatedly, causing the app to hang.
            if height != footerFrame.size.height {
                footerFrame.size.height = height
                footerView.frame = footerFrame
                self.customView.tableView.tableFooterView = footerView
            }
        }
    }
    
    // MARK: - Private methods
    
    func updateView() {
        self.model.reloadFields(withAccount: AccountManager.shared.currentUser!)
        
        self.customView.tableView.reloadData()
        self.updateHeaderView()
    }
    
    func updateHeaderView() {
        guard let headerView = self.headerView else {
            return
        }
        
        if let avatarUrl = self.model.profileAvatarUrl {
            headerView.avatarImageView.kf.setImage(with: avatarUrl)
        }
        headerView.profileDescriptionLabel.text = self.model.accountDescription
        
        headerView.ratingView.rating = self.model.account.rating
    }
}

// MARK: - AccountDetailsViewDelegate

extension AccountDetailsViewController: AccountDetailsViewDelegate {

}

// MARK: - AccountDetailsModelDelegate

extension AccountDetailsViewController: AccountDetailsModelDelegate {
    
    func modelDidChanged(model: AccountDetailsModelProtocol) {
        self.updateView()
    }
}
