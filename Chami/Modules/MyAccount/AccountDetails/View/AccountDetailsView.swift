//
//  AccountDetailsView.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol AccountDetailsViewDelegate: NSObjectProtocol {
}

protocol AccountDetailsViewProtocol: NSObjectProtocol {
    
    weak var delegate: AccountDetailsViewDelegate? { get set }
    var tableView: UITableView! { get }
}

class AccountDetailsView: UIView, AccountDetailsViewProtocol{

    class func create() -> AccountDetailsView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? AccountDetailsView else {
            fatalError("Nib \(viewNibName) does not contain AccountDetails View as first object")
        }
        
        return view
    }
    
    // MARK: - AccountDetailsView interface methods

    weak var delegate: AccountDetailsViewDelegate?
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        self.tableView.rowHeight = UI.tableCellMediumHeight
        
    }
}
