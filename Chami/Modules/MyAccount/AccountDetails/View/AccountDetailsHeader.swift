//
//  AccountDetailsHeader.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsHeader: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accountDescriptionLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 19)
        self.titleLabel.textColor = ColorHelper.regularColor()
        
        self.accountDescriptionLabel.font = FontHelper.defaultFont(withSize: 16)
    }

    class func create() -> AccountDetailsHeader {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? AccountDetailsHeader else {
            fatalError("Nib \(viewNibName) does not contain AccountDetails Header View as first object")
        }
        
        return view
    }
}
