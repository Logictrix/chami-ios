//
//  AccountDetailsFooter.swift
//  Chami
//
//  Created by Igor Markov on 3/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsFooter: UIView {

    @IBOutlet weak var viewPaymentDetailsButton: UIButton!
    var didTapViewPaymentDetailsControl: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let buttonTitle = self.compileViewPaymentDetailsButtonTitle()
        self.viewPaymentDetailsButton.setAttributedTitle(buttonTitle, for: .normal)
    }
    
    fileprivate func compileViewPaymentDetailsButtonTitle() -> NSAttributedString {
        let titleFont = FontHelper.font(name: CustomFontName.sfuiMedium.rawValue, size: 19)
        let titleAttributes = [NSAttributedString.Key.font: titleFont,
                               NSAttributedString.Key.foregroundColor: ColorHelper.regularColor(),
                               NSAttributedString.Key.underlineStyle: NSNumber(value: true)]
        let title = NSLocalizedString("View Payment Details", comment: "Account Details")
        return NSAttributedString(string: title, attributes: titleAttributes)
    }

    class func create() -> AccountDetailsFooter {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? AccountDetailsFooter else {
            fatalError("Nib \(viewNibName) does not contain AccountDetails Footer View as first object")
        }
        
        return view
    }
    
    @IBAction func viewPaymentDetailsAction() {
        if let didTapViewPaymentDetailsControl = self.didTapViewPaymentDetailsControl {
            didTapViewPaymentDetailsControl()
        }
    }
}
