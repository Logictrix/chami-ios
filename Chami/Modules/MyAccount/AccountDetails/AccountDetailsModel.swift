//
//  AccountDetailsModel.swift
//  Chami
//
//  Created by Igor Markov on 3/14/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

// MARK: - AccountDetailsFieldType

enum AccountDetailsFieldType: Int {
    case firstName = 0
    case lastName = 1
    case email = 2
    case password = 3
}

// MARK: - AccountDetailsModelDelegate

protocol AccountDetailsModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: AccountDetailsModelProtocol)
}

// MARK: - AccountDetailsModelProtocol

protocol AccountDetailsModelProtocol: NSObjectProtocol {
    
    weak var delegate: AccountDetailsModelDelegate? { get set }
    var profileAvatarUrl: URL? { get }
    var accountDescription: String { get }
    var account: Account { get }
    var fields: [FormField] { get }
    func reloadFields(withAccount account: Account)
}

// MARK: - AccountDetailsModel

class AccountDetailsModel: NSObject, AccountDetailsModelProtocol {
    
    private(set) var profileAvatarUrl: URL?
    private(set) var account: Account
    
    init(withAccount account: Account) {
        self.fields = []
        self.accountDescription = ""
        self.account = account

        super.init()
        
        self.reloadFields(withAccount: account)
    }
    
    func reloadFields(withAccount account: Account) {
        if let avatarUrlString = account.imageUrl {
            self.profileAvatarUrl = URL(string: avatarUrlString)
        }
        self.fields = AccountDetailsModel.createFields(withAccount: account)
        self.accountDescription = account.about ?? ""
        self.account = account
    }
    
    class private func createFields(withAccount account: Account) -> [FormField] {

        let firstNameField = FormField()
        firstNameField.key = NSLocalizedString("First name", comment: "Account Details field First Name")
        firstNameField.value = account.firstName as AnyObject

        let lastNameField = FormField()
        lastNameField.key = NSLocalizedString("Last name", comment: "Account Details field Last Name")
        lastNameField.value = account.lastName as AnyObject
        
        let emailField = FormField()
        emailField.key = NSLocalizedString("Email", comment: "Account Details field Email")
        emailField.value = account.email as AnyObject?
        
        return [firstNameField, lastNameField, emailField]
    }
    
    // MARK: - AccountDetailsModel methods

    weak var delegate: AccountDetailsModelDelegate?
    private(set) var accountDescription: String
    private(set) var fields: [FormField]
}
