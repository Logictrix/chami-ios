//
//  AccountDetailsEditModel.swift
//  Chami
//
//  Created by Igor Markov on 3/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

// MARK: - AccountDetailsEditFieldType

enum AccountDetailsEditFieldType: Int {
    case firstName = 0
    case lastName = 1
    case email = 2
    case oldPassword = 3
    case newPassword = 4
    case confirmPassword = 5
}

// MARK: - AccountDetailsEditFieldGroup

enum AccountDetailsEditFieldGroup: Int {
    case names = 0
    case email = 1
    case passwords = 2
}

// MARK: - AccountDetailsEditModelDelegate

protocol AccountDetailsEditModelDelegate: NSObjectProtocol {
    
    func modelDidUpdateData(model: AccountDetailsEditModelProtocol)
}

// MARK: - AccountDetailsEditModelProtocol

protocol AccountDetailsEditModelProtocol: NSObjectProtocol {
    
    weak var delegate: AccountDetailsEditModelDelegate? { get set }

    // MARK: - Data
    func getAccountRole() -> AccountRole
    func clearAllData()
    
    
    // MARK: - Fields
    var aboutInfo: String? { get set }
    
    func numberOfFields() -> Int
    func setValue(_ value: String, fieldType: AccountDetailsEditFieldType)
    func getField(withType actionDetailsFieldType: AccountDetailsEditFieldType) -> FormField?
    
    // MARK: - Field index pathes
    func getField(withIndexPath indexPath: IndexPath) -> FormField?
    func getAccountDetailsEditFieldType(atIndexPath indexPath: IndexPath) -> AccountDetailsEditFieldType?
    func getFieldPosition(atIndexPath indexPath: IndexPath) -> GroupPosition
    
    // MARK: - Field groups
    func numberOfGroup() -> Int
    func numberOfFields(inGroup group: AccountDetailsEditFieldGroup) -> Int
    func getFieldTypes(inGroup group: AccountDetailsEditFieldGroup) -> [AccountDetailsEditFieldType]
    
    // MARK: Validation
    //func validateFields() -> String? // - return error string

    // MARK: - Actions
    func saveChanges(withCompletion completion: @escaping (Error?) -> Void)
}

// MARK: - AccountDetailsEditModel

class AccountDetailsEditModel: NSObject, AccountDetailsEditModelProtocol {
    
    typealias FieldTypeAndField = (type: AccountDetailsEditFieldType, field: FormField)
    
    weak var delegate: AccountDetailsEditModelDelegate?

    internal var aboutInfo: String?
    internal let fields: [AccountDetailsEditFieldType : FormField]
    private let fieldGroups: [AccountDetailsEditFieldGroup: [AccountDetailsEditFieldType]]

    
    private var account: Account
    private let profileService: ProfileServiceProtocol
    private let accountService: AccountServiceProtocol
    
    init(withAccount account: Account, service: ProfileServiceProtocol) {

        
        self.account = account
        self.profileService = service
        self.accountService = AccountService()
        
        self.aboutInfo = account.about
        self.fields = AccountDetailsEditModel.createFields(withAccount: account)
        self.fieldGroups = [.names:     [.firstName, .lastName],
                            .email:     [.email],
                            .passwords: [.oldPassword, .newPassword, .confirmPassword]]
        super.init()
    }
    
    private class func createFields(withAccount account: Account) -> [AccountDetailsEditFieldType : FormField] {
        
        var resultFields = [AccountDetailsEditFieldType : FormField]()

        let firstNameField = FormField()
        firstNameField.key = NSLocalizedString("First Name", comment: "Account Details Edit")
        firstNameField.value = account.firstName as AnyObject?
        firstNameField.fieldType = .firstName
        firstNameField.keyboardType = .string
        firstNameField.returnButtonType = .next
        firstNameField.validationState = .correct
        resultFields[.firstName] = firstNameField
        
        let lastNameField = FormField()
        lastNameField.key = NSLocalizedString("Last Name", comment: "Account Details Edit")
        lastNameField.value = account.lastName as AnyObject?
        lastNameField.fieldType = .lastName
        lastNameField.keyboardType = .string
        lastNameField.returnButtonType = .next
        lastNameField.validationState = .correct
        resultFields[.lastName] = lastNameField
        
        let emailField = FormField()
        emailField.key = NSLocalizedString("Email address", comment: "Account Details Edit")
        emailField.value = account.email as AnyObject?
        emailField.fieldType = .email
        emailField.keyboardType = .email
        emailField.returnButtonType = .done
        emailField.validationState = .correct
        resultFields[.email] = emailField
        
        let oldPasswordField = FormField()
        oldPasswordField.isSecure = true
        oldPasswordField.key = NSLocalizedString("Enter your old password", comment: "Account Details Edit")
        oldPasswordField.fieldType = .oldPassword
        oldPasswordField.keyboardType = .string
        oldPasswordField.returnButtonType = .next
        oldPasswordField.validationState = .correct
        resultFields[.oldPassword] = oldPasswordField
        
        let newPasswordField = FormField()
        newPasswordField.isSecure = true
        newPasswordField.key = NSLocalizedString("Create a new password", comment: "Account Details Edit")
        newPasswordField.fieldType = .password
        newPasswordField.keyboardType = .string
        newPasswordField.returnButtonType = .next
        newPasswordField.validationState = .correct
        resultFields[.newPassword] = newPasswordField
        
        let confirmPasswordField = FormField()
        confirmPasswordField.isSecure = true
        confirmPasswordField.key = NSLocalizedString("Confirm new password", comment: "Account Details Edit")
        confirmPasswordField.fieldType = .confirmPassword
        confirmPasswordField.keyboardType = .string
        confirmPasswordField.returnButtonType = .done
        confirmPasswordField.validationState = .correct
        resultFields[.confirmPassword] = confirmPasswordField
        
        return resultFields
    }
    
    // MARK: - AccountDetailsEditModel methods

    // MARK: - Data
    
    func getAccountRole() -> AccountRole {
        return self.account.role
    }

    func clearAllData() {
        for field in self.fields.values {
            field.value = nil
        }
        
        if let delegate = delegate {
            delegate.modelDidUpdateData(model: self)
        }
    }
    
    // MARK: - Fields
    
    func numberOfFields() -> Int {
        return fields.count
    }
    
    func setValue(_ value: String, fieldType: AccountDetailsEditFieldType) {
        if let field = fields[fieldType] {
            field.value = value as AnyObject?
        }
    }
    
    func getField(withType actionDetailsFieldType: AccountDetailsEditFieldType) -> FormField? {
        return fields[actionDetailsFieldType]
    }
    
    // MARK: - Field index pathes
    
    func getField(withIndexPath indexPath: IndexPath) -> FormField? {
        guard let group = AccountDetailsEditFieldGroup(rawValue: indexPath.section),
            let fieldGroup = self.fieldGroups[group] else {
                return nil
        }
        
        guard indexPath.row < fieldGroup.count else {
            return nil
        }
        
        let fieldType = fieldGroup[indexPath.row]
        let registrationField =  self.fields[fieldType]
        return registrationField
    }
    
    func getAccountDetailsEditFieldType(atIndexPath indexPath: IndexPath) -> AccountDetailsEditFieldType? {
        guard let group = AccountDetailsEditFieldGroup(rawValue: indexPath.section),
            let fieldGroup = self.fieldGroups[group] else {
                return nil
        }
        
        return indexPath.row < fieldGroup.count ? fieldGroup[indexPath.row] : nil
    }
    
    func getFieldPosition(atIndexPath indexPath: IndexPath) -> GroupPosition {
        guard let group = AccountDetailsEditFieldGroup(rawValue: indexPath.section),
            let fieldGroup = self.fieldGroups[group] else {
                return []
        }
        
        var groupPosition: GroupPosition = []
        if indexPath.row == 0 {
            groupPosition.insert(.start)
        }
        if indexPath.row == fieldGroup.count - 1 {
            groupPosition.insert(.end)
        }
        
        return groupPosition
    }
    
    // MARK: - Field groups
    
    func numberOfGroup() -> Int {
        return self.fieldGroups.count
    }
    
    func numberOfFields(inGroup group: AccountDetailsEditFieldGroup) -> Int {
        return self.getFieldTypes(inGroup: group).count
    }
    
    func getFieldTypes(inGroup group: AccountDetailsEditFieldGroup) -> [AccountDetailsEditFieldType] {
        if let fieldGroup = self.fieldGroups[group] {
            return fieldGroup
        } else {
            return []
        }
    }
    
    // MARK: Validation
    func validateMainFields() -> String? { // - return error string
        let about = self.aboutInfo
        if about?.characters.count == 0 && self.getAccountRole() == .tutor {
            return NSLocalizedString("Please enter some information about you", comment: "")
        }
        let firstName = self.getField(withType: .firstName)?.stringFromValue()
        if firstName?.characters.count == 0 {
            return NSLocalizedString("Please enter your First name", comment: "")
        }
        let lastName = self.getField(withType: .lastName)?.stringFromValue()
        if lastName?.characters.count == 0 {
            return NSLocalizedString("Please enter your Last name", comment: "")
        }
        
        let email = self.getField(withType: .email)?.stringFromValue()
        if email?.characters.count == 0 {
            return NSLocalizedString("Please enter your Email address", comment: "")
        } else if email?.isEmail() == false {
            return NSLocalizedString("Please enter your correct Email address", comment: "")
        }
        return nil
    }
    
    func validatePasswordFields() -> String? { // - return error string
        let oldPass = (self.getField(withType: .oldPassword))?.stringFromValue()
        let newPass = self.getField(withType: .newPassword)?.stringFromValue()
        let newPassConfirm = self.getField(withType: .confirmPassword)?.stringFromValue()
        
        if (oldPass?.characters.count)! > 0 || (newPass?.characters.count)! > 0 || (newPassConfirm?.characters.count)! > 0 {
            
            
            if oldPass?.characters.count == 0 {
                return NSLocalizedString("Please enter your current password", comment: "")
            
            } else if newPass?.characters.count == 0 {
                return NSLocalizedString("Please enter your new password", comment: "")
                
            } else if (newPass?.characters.count)! < 6 {
                return NSLocalizedString("Password should contain 6 or more symbols", comment: "")
                
            } else if newPassConfirm?.characters.count == 0 || newPass != newPassConfirm {
                return NSLocalizedString("Please enter correct confirmation password", comment: "")
            
            } else if oldPass == newPass {
                return NSLocalizedString("Your old and new passwords the same", comment: "")
                
            }
        }
        return nil
    }
    // MARK: - Actions
    
    // TODO: implement requests logics
    func saveChanges(withCompletion completion: @escaping (Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: Error?
            let group = DispatchGroup()
            if self.shouldChangeRegularData() {
                group.enter()
                self.updateRegularDataChanges(withSuccess: { [weak self] updatedAccount in
                    guard let strongSelf = self else { return }

                    updatedAccount.email = strongSelf.account.email
                    strongSelf.account = updatedAccount
                    print(updatedAccount.about)
                    AccountManager.shared.currentUser = updatedAccount
                    
                    group.leave()
                }, failure: { error in
                    resultError = error
                    group.leave()
                })
            }
            
            group.wait()
            if resultError == nil && self.shouldChangePassword() {
                group.enter()
                self.updatePassword(completion: { (error) in
                    resultError = error
                    group.leave()
                })
                
            }
            group.wait()
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
    }

    // TODO: implement success/failure callbacks
    private func updateRegularDataChanges(withSuccess success: @escaping (_ updatedAccount: Account) -> Void, failure: @escaping (Error) -> Void) {
        //var account = self.account.copy()
        
        let errorText = self.validateMainFields()
        if let text = errorText, text.characters.count > 0 {
            let error = ErrorHelper.compileError(withTextMessage: text)
            failure(error as Error)
            return
        }
        
        //var about: String? = nil
        if let aboutValue = self.aboutInfo {
            //about = (aboutValue == self.account.about) ? nil : aboutValue
            self.account.about = aboutValue
        }
        
        //var firstName: String? = nil
        if let firstNameField = self.getField(withType: .firstName),
            let firstNameValue = firstNameField.value as? String {
            //firstName = (firstNameValue == self.account.firstName) ? nil : firstNameValue
            self.account.firstName = firstNameValue
        }
        
        //var lastName: String? = nil
        if let lastNameField = self.getField(withType: .lastName),
            let lastNameValue = lastNameField.value as? String {
            //lastName = (lastNameValue == self.account.lastName) ? nil : lastNameValue
            self.account.lastName = lastNameValue
        }
        
        //var email: String? = nil
        if let emailField = self.getField(withType: .email),
            let emailValue = emailField.value as? String {
            //email = (emailValue == self.account.email) ? nil : emailValue
            self.account.email = emailValue
        }
        
        self.profileService.setProfileInfo(account: self.account, success: {[weak self] in 
            //print("\(#function)")
            guard let strongSelf = self else { return }

            success(strongSelf.account)
        }) { error in
            print("\(#function), error: \(error)")
            failure(error)
        }
    }
    
    // TODO: implement success/failure callbacks
    private func updatePassword(completion: @escaping (Error?) -> Void) {
        let errorText = self.validatePasswordFields()
        if let text = errorText, text.characters.count > 0 {
            let error = ErrorHelper.compileError(withTextMessage: text)
            completion(error as Error)
            return
        }
        
        guard let oldPasswordField = self.getField(withType: .oldPassword),
            let oldPassword = oldPasswordField.value as? String,
            let newPasswordField = self.getField(withType: .newPassword),
            let newPassword = newPasswordField.value as? String,
            oldPassword.characters.count > 0,
            newPassword.characters.count > 0 else {
            return
        }
        
        self.accountService.changePassword(oldPassword: oldPassword, newPassword: newPassword) { (error) in
            if let error = error {
                print("error: \(error)")
            }
            completion(error)
        }
    }
    
    // MARK: - Private methods
    
    private func shouldChangeRegularData() -> Bool {
        if let about = self.aboutInfo {
            if about != self.account.about {
                return true
            }
        }
        if let firstNameField = self.getField(withType: .firstName) {
            if firstNameField.value as? String != self.account.firstName {
                return true
            }
        }
        
        if let lastNameField = self.getField(withType: .lastName) {
            if lastNameField.value as? String != self.account.lastName {
                return true
            }
        }
        
        
        if let emailNameField = self.getField(withType: .email) {
            if emailNameField.value as? String != self.account.email {
                return true
            }
        }

        return false
    }
    
    private func shouldChangePassword() -> Bool {
        let oldPass = (self.getField(withType: .oldPassword))?.stringFromValue()
        let newPass = self.getField(withType: .newPassword)?.stringFromValue()
        let newPassConfirm = self.getField(withType: .confirmPassword)?.stringFromValue()
        
        if (oldPass?.characters.count)! > 0 || (newPass?.characters.count)! > 0 || (newPassConfirm?.characters.count)! > 0 {
            return true
        }
        return false
    }
}
