//
//  AccountDetailsEditViewController.swift
//  Chami
//
//  Created by Igor Markov on 3/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias AccountDetailsEditViewControllerType = BaseViewController<AccountDetailsEditModelProtocol, AccountDetailsEditViewProtocol, AccountDetailsEditRouter>

class AccountDetailsEditViewController: AccountDetailsEditViewControllerType, UITableViewDelegate {
    
    fileprivate var dataSource: AccountDetailsEditDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: AccountDetailsEditViewProtocol, model: AccountDetailsEditModelProtocol, router: AccountDetailsEditRouter, dataSource: AccountDetailsEditDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self
        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
    }
    
    internal required init(withView view: AccountDetailsEditViewProtocol!, model: AccountDetailsEditModelProtocol!, router: AccountDetailsEditRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
        self.setupNavigationBar()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.updateViews()
    }
    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.regicterNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    private func setupNavigationBar() {
        self.title = NSLocalizedString("Edit Details", comment: "Account Details Edit")
        
        let cloaseButtonImage = UIImage(named: "top_bar_btn_close")
        let closeButton = UIBarButtonItem(image: cloaseButtonImage, style: .plain, target: self, action: #selector(closeAction))
        self.navigationItem.rightBarButtonItem = closeButton
    }
    
    fileprivate func updateViews() {
        
        self.customView.headerDescriptionTextView.text = self.model.aboutInfo
        
        self.customView.tableView.reloadData()
        self.customView.apply(accountRole: self.model.getAccountRole())
    }
    
    // MARK: - Actions
    
    @objc private func closeAction() {

        self.view.endEditing(true)
        
        // TODO: check if there are unsaved data
        
        self.router?.close(from: self)
    }
    
    fileprivate func saveChanges() {
        
        self.view.endEditing(true)
        
        self.model.aboutInfo = self.customView.headerDescriptionTextView.text
        // TODO: validate input data
        self.model.saveChanges {[weak self] (error) in
            guard let strongSelf = self else { return }
            if let nserr = error as NSError? {
                if let responseObject = nserr.userInfo["SWGResponseObject"] as? NSDictionary {
                    if let msg = responseObject["errorMessage"] as? NSString {
                        strongSelf.showAlert(error: msg as String)
                    }
                } else if let err = error {
                    if (err as NSError).code == -1011 {
                        let errorMSG = NSLocalizedString("Please, enter correct password", comment: "Change pass - incorrect old pass")
                        strongSelf.showAlert(error: errorMSG)
                    } else {
                        strongSelf.showAlert(error: err.localizedDescription)
                    }
                }
            } else {
                strongSelf.router?.close(from: strongSelf)
            }
        }
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        guard let fieldGroup = AccountDetailsEditFieldGroup(rawValue: section) else {
            return 0
        }
        
        switch fieldGroup {
        case .email:
            return 8
        case .passwords:
            return 44
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let fieldGroup = AccountDetailsEditFieldGroup(rawValue: section) else {
            return nil
        }
        
        switch fieldGroup {
        case .email:
            let header = UIView(frame: .zero)
            header.backgroundColor = self.view.backgroundColor
            return header
        case .passwords:
            let passwordHeaderLabel = UILabel()
            passwordHeaderLabel.text = NSLocalizedString("Reset your password", comment: "")
            passwordHeaderLabel.textColor = UIColor.darkGray
            passwordHeaderLabel.font = FontHelper.defaultFont(withSize: 16)
            return passwordHeaderLabel
        default:
            return nil
        }
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        return nil
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        let fieldGroupPosition = self.model.getFieldPosition(atIndexPath: indexPath)
        
        var roundCorners: UIRectCorner = []
        if fieldGroupPosition.contains(.start) {
            roundCorners = roundCorners.union([.topLeft, .topRight])
        }
        if fieldGroupPosition.contains(.end) {
            roundCorners = roundCorners.union([.bottomLeft, .bottomRight])
        }
        cell.contentView.roundCorners(corners: roundCorners, radius: 6, separatorHeight: 2)
    }
}

// MARK: - AccountDetailsEditViewDelegate

extension AccountDetailsEditViewController: AccountDetailsEditViewDelegate {

    internal func viewSaveChangesDidTap(view: AccountDetailsEditViewProtocol) {
        self.saveChanges()
    }
}

// MARK: - AccountDetailsEditModelDelegate

extension AccountDetailsEditViewController: AccountDetailsEditModelDelegate {

    internal func modelDidUpdateData(model: AccountDetailsEditModelProtocol) {
        self.updateViews()
    }
}

// MARK: - RegistrationCellDelegate

extension AccountDetailsEditViewController: FormCellDelegate {
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didChangeString string: String?) where CellType: UITableViewCell {}
    func cell<CellType: FormCellProtocol>(_ cell: CellType, didEnterString string: String?) where CellType: UITableViewCell {
        
        guard let indexPath = self.customView.tableView.indexPath(for: cell),
//            let accountDetailsFieldType = self.model.getAccountDetailsEditFieldType(atIndexPath: indexPath),
            let field = self.model.getField(withIndexPath: indexPath) else {
                fatalError()
        }
        
        field.value = string as AnyObject?
        
//        let validationState = self.model.validateValue(string, fieldType: registrationFieldType)
//        self.model.setValidationState(validationState, fieldType: registrationFieldType)
//        
        self.customView.tableView.reloadRows(at: [indexPath], with: .automatic)
        if field.returnButtonType == .next {
            self.dataSource.activateNextCell(forCellAtIndexPath: indexPath, tableView: self.customView.tableView)
        }
    }
}
