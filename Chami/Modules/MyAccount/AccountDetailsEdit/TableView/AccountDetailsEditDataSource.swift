//
//  AccountDetailsEditDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class AccountDetailsEditDataSource: BaseTableViewDataSource {
    
    var cellDelegate: FormCellDelegate?
    private let model: AccountDetailsEditModelProtocol
    private let cellReuseId = "AccountDetailsEditTableViewCell"
    
    init(withModel model: AccountDetailsEditModelProtocol) {
        self.model = model
    }

    func regicterNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: AccountDetailsEditTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - UITableViewDataSource

    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.model.numberOfGroup()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let group = AccountDetailsEditFieldGroup(rawValue: section) {
            let numberOfRows = self.model.numberOfFields(inGroup: group)
            return numberOfRows
        } else {
            return 0
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? AccountDetailsEditTableViewCell,
            let accountDetailsEditField = self.model.getField(withIndexPath: indexPath) else {
            fatalError()
        }

     //   cell.field = accountDetailsEditField
        cell.delegate = cellDelegate
        
        return cell
    }
}
