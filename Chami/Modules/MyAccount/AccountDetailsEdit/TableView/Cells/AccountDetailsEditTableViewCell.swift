//
//  AccountDetailsEditTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 3/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsEditTableViewCell: FormTableViewCell {
    
    override func awakeFromNib() {
        super.awakeFromNib()

        self.contentView.backgroundColor = ColorHelper.backgroundFormFieldColor()
    }

}

extension AccountDetailsEditTableViewCell {
    // OLD CODE

//    override var field: FormField? {
//        didSet {
//            if let field = self.field {
//                switch field.fieldType {
//                case .firstName:
//                    self.valueTextField.placeholder = NSLocalizedString("First Name", comment: "First name placeholder")
//                case .lastName:
//                    self.valueTextField.placeholder = NSLocalizedString("Last Name", comment: "Last name placeholder")
//                case .email:
//                    self.valueTextField.placeholder = NSLocalizedString("Email address", comment: "Email placeholder")
//                case .oldPassword:
//                    self.valueTextField.placeholder = NSLocalizedString("Enter your old password", comment: "Password placeholder")
//                case .password:
//                    self.valueTextField.placeholder = NSLocalizedString("Create a new password", comment: "Password placeholder")
//                case .confirmPassword:
//                    self.valueTextField.placeholder = NSLocalizedString("Confirm new password", comment: "Confirm password placeholder")
//                default:
//                    self.valueTextField.placeholder = nil
//                }
//            }
//        }
//    }
}
