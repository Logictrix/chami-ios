//
//  AccountDetailsEditView.swift
//  Chami
//
//  Created by Igor Markov on 3/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import TPKeyboardAvoiding

protocol AccountDetailsEditViewDelegate: NSObjectProtocol {
    
    func viewSaveChangesDidTap(view: AccountDetailsEditViewProtocol)
}

protocol AccountDetailsEditViewProtocol: NSObjectProtocol {
    
    weak var delegate: AccountDetailsEditViewDelegate? { get set }
    var tableView: UITableView! { get }
    var headerDescriptionTextView: UITextView! { get }
    
    func apply(accountRole: AccountRole)
}

class AccountDetailsEditView: UIView, AccountDetailsEditViewProtocol{

    weak var delegate: AccountDetailsEditViewDelegate?
    
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerDescriptionTextView: UITextView!
    
    @IBOutlet weak var scrollView: TPKeyboardAvoidingScrollView!
    @IBOutlet weak var scrollContentView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var saveChangesButton: UIButton!
    
    class func create() -> AccountDetailsEditView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? AccountDetailsEditView else {
            fatalError("Nib \(viewNibName) does not contain AccountDetailsEdit View as first object")
        }
        
        return view
    }
    
    // MARK: - AccountDetailsEditView interface methods

    func apply(accountRole: AccountRole) {
        switch accountRole {
        case .student:
            self.applyStudentRole()
        case .tutor:
            self.applyTutorRole()
        default:
            break
        }

        Theme.current.apply(borderedButton: self.saveChangesButton)
    }
    
    // MARK: Private methods

    private func applyStudentRole() {
        // TODO: add colors, style and texts to header title, header description, save button
    }
    
    private func applyTutorRole() {
        // TODO: add colors, style and texts to header title, header description, save button
    }
    
    func compileSaveButtonTitle() -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: labelFont,
                                                                 NSAttributedString.Key.foregroundColor: ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString("Save changes ", comment: "Account Details Edit"),
                                          attributes: textPartAttributes)
        
        titleLabelText.append(textPart)
        
        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        titleLabelText.append(NSAttributedString(string: FontImage.checkbox, attributes: textPartAttributes))
        return titleLabelText
    }
    
    // MARK: - IBActions
    
    @IBAction func saveChangesButtonAction() {
        delegate?.viewSaveChangesDidTap(view: self)
    }

    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        let saveChangesButtonTitle = self.compileSaveButtonTitle()
        self.saveChangesButton.setAttributedTitle(saveChangesButtonTitle, for: .normal)
        
        self.headerTitleLabel.font = FontHelper.defaultMediumFont(withSize: 19)
        self.headerTitleLabel.textColor = ColorHelper.regularColor()
        self.headerDescriptionTextView.font = FontHelper.defaultFont(withSize: 16)
        //self.headerDescriptionTextView.backgroundColor = ColorHelper.backgroundFormFieldColor()
        //self.headerDescriptionTextView.layer.cornerRadius = 4
        
        
        
    }
}
