//
//  AccountDetailsEditRouter.swift
//  Chami
//
//  Created by Igor Markov on 3/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsEditRouter: NSObject {
    
    func close(from vc: UIViewController) {
        vc.dismiss(animated: true, completion: nil)
    }
}
