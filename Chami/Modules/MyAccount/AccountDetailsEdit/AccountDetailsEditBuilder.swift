//
//  AccountDetailsEditBuilder.swift
//  Chami
//
//  Created by Igor Markov on 3/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AccountDetailsEditBuilder: NSObject {

    class func viewController(withAccount account: Account) -> AccountDetailsEditViewController {
        let service = ProfileService()
        let view: AccountDetailsEditViewProtocol = AccountDetailsEditView.create()
        let model: AccountDetailsEditModelProtocol = AccountDetailsEditModel(withAccount: account, service: service)
        let dataSource = AccountDetailsEditDataSource(withModel: model)
        let router = AccountDetailsEditRouter()
        
        let viewController = AccountDetailsEditViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
