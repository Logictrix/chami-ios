//
//  ProfileInfoView.swift
//  Chami
//
//  Created by Igor Markov on 3/16/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ProfileInfoViewDelegate: NSObjectProtocol {
    
}

protocol ProfileInfoViewProtocol: NSObjectProtocol {
    
    weak var delegate: ProfileInfoViewDelegate? { get set }
    var tableView: UITableView! { get }
}

class ProfileInfoView: UIView, ProfileInfoViewProtocol{

    class func create() -> ProfileInfoView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? ProfileInfoView else {
            fatalError("Nib \(viewNibName) does not contain ProfileInfo View as first object")
        }
        
        return view
    }
    
    // MARK: - ProfileInfoView interface methods

    weak var delegate: ProfileInfoViewDelegate?
    @IBOutlet weak var tableView: UITableView!
}
