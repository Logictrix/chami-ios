//
//  ProfileInfoFooter.swift
//  Chami
//
//  Created by Igor Markov on 3/17/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ProfileInfoFooter: UIView {

    @IBOutlet weak var playButton: UIButton!
    @IBOutlet weak var footerTitleLabel: UILabel!
    
    var didTapPlayControl: (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    @IBAction func playButtonAction() {
        if let didTapPlayControl = self.didTapPlayControl {
            didTapPlayControl()
        }
    }
}
