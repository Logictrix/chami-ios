//
//  ProfileInfoHeader.swift
//  Chami
//
//  Created by Igor Markov on 3/17/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Cosmos

class ProfileInfoHeader: UIView {

    @IBOutlet weak var gradientView: GradientView!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var profileDescriptionLabel: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var ratingLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()

        let avatarImageRadius = min(self.avatarImageView.bounds.width, self.avatarImageView.bounds.height)
        self.avatarImageView.roundCorners(corners: .allCorners, radius: avatarImageRadius, separatorHeight: 5)
        self.headerTitleLabel.text = NSLocalizedString("Your account details", comment: "Profile Info")
        
        self.ratingView.setupPresentation()
        self.ratingView.settings.fillMode = .precise
    }
}
