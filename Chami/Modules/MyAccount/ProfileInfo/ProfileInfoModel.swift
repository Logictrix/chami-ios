//
//  ProfileInfoModel.swift
//  Chami
//
//  Created by Igor Markov on 3/16/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

// MARK: - ProfileInfoFieldType

enum ProfileInfoFieldType: Int {
    case birthdate = 0
    case nationality = 1
    case phoneNumber = 2
}

// MARK: - ProfileInfoModelDelegate

protocol ProfileInfoModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: ProfileInfoModelProtocol)
}

// MARK: - ProfileInfoModelProtocol

protocol ProfileInfoModelProtocol: NSObjectProtocol {
    
    weak var delegate: ProfileInfoModelDelegate? { get set }
    var profileAvatarUrl: URL? { get }
    var profileDescription: String { get }
    var fields: [FormField] { get }
    var account: Account { get }
    var profileVideoUrl: URL? { get }
    
    func reloadFields(withAccount account: Account)
}

// MARK: - ProfileInfoModel

class ProfileInfoModel: NSObject, ProfileInfoModelProtocol {
     
    init(withAccount account: Account) {
        self.account = account
        
        self.fields = []
        self.profileDescription = ""
        
        super.init()
        
        self.reloadFields(withAccount: account)
    }
    
    func reloadFields(withAccount account: Account) {
        self.account = account
        
        self.fields = ProfileInfoModel.createFields(withAccount: account)
        if let about = account.about {
            self.profileDescription = about
        }
        
        if let avatarUrlString = account.imageUrl {
            self.profileAvatarUrl = URL(string: avatarUrlString)
        }
        
//        if self.account.role == .tutor {
//        }
     if let introVideoUrlString = account.introVideoUrl {
          self.profileVideoUrl = URL(string: introVideoUrlString)
     }

     }
    // MARK: - ProfileInfoModel methods

    weak var delegate: ProfileInfoModelDelegate?
    private(set) var account: Account
    private(set) var profileAvatarUrl: URL?
    private(set) var profileDescription: String
    private(set) var fields: [FormField]
    private(set) var profileVideoUrl: URL?
    
    /** Implement ProfileInfoModel methods here */
    
    
    // MARK: - Private methods
    
    class private func createFields(withAccount account: Account) -> [FormField] {
        
        var formFields = [FormField]()
        
        let birthdayField = FormField()
        birthdayField.key = NSLocalizedString("Date of birth", comment: "Profile Info")
        var birthdayString = ""
        if account.birthDate != nil {
            birthdayString = DateConverter.stringDateWithoutTimeFormat(fromDate: account.birthDate)
        }
        birthdayField.value = birthdayString as AnyObject
        formFields.append(birthdayField)
        
        let nationalityField = FormField()
        nationalityField.key = NSLocalizedString("Nationality", comment: "Profile Info")
        nationalityField.value = account.nationality as AnyObject
        formFields.append(nationalityField)
        
        let phoneNumberField = FormField()
        phoneNumberField.key = NSLocalizedString("Phone number", comment: "Profile Info")
        phoneNumberField.value = account.phoneNumber as AnyObject?
        formFields.append(phoneNumberField)
        
        if account.role == .tutor {
            let genderField = FormField()
            genderField.key = NSLocalizedString("Gender", comment: "Profile Info")
            genderField.value = account.gender as AnyObject?
            formFields.append(genderField)
        }
        
        return formFields
    }
    
}
