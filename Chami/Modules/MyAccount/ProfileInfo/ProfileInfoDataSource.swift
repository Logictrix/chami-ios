//
//  ProfileInfoDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class ProfileInfoDataSource: NSObject, UITableViewDataSource {
    
    private let model: ProfileInfoModelProtocol
    private let cellReuseId = "TitleValueCell"
    
    init(withModel model: ProfileInfoModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: TitleValueCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: TitleValueCell, forField field: FormField) {
        cell.titleLabel.text = field.key
        cell.valueLabel.text = field.stringFromValue()
        cell.valueLabel.textColor = UIColor.darkGray
        cell.titleLabel.textColor = RGB(42,42,42)
    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.fields.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? TitleValueCell else {
            fatalError()
        }
        
        let field = self.model.fields[indexPath.row];
        self.configure(cell: cell, forField: field)
        
        return cell
    }
}
