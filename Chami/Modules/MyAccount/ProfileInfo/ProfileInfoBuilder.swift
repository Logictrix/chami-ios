//
//  ProfileInfoBuilder.swift
//  Chami
//
//  Created by Igor Markov on 3/16/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ProfileInfoBuilder: NSObject {

    class func viewController(withAccount account: Account) -> ProfileInfoViewController {
        let view: ProfileInfoViewProtocol = ProfileInfoView.create()
        let model: ProfileInfoModelProtocol = ProfileInfoModel(withAccount: account)
        let dataSource = ProfileInfoDataSource(withModel: model)
        let router = ProfileInfoRouter()
        
        let viewController = ProfileInfoViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
