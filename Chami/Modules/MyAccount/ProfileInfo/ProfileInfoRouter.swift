//
//  ProfileInfoRouter.swift
//  Chami
//
//  Created by Igor Markov on 3/16/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation

class ProfileInfoRouter: NSObject {
    
    func openPlayer(withVideoUrl videoUrl: URL, from vc: UIViewController) {

        let player = AVPlayer(url: videoUrl)
        let playerController = AVPlayerViewController()
        playerController.player = player
        
        vc.present(playerController, animated: true, completion: nil)
    }
}
