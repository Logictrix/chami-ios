//
//  ProfileInfoViewController.swift
//  Chami
//
//  Created by Igor Markov on 3/16/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Kingfisher

typealias ProfileInfoViewControllerType = MVCViewController<ProfileInfoModelProtocol, ProfileInfoViewProtocol, ProfileInfoRouter>

class ProfileInfoViewController: ProfileInfoViewControllerType, UITableViewDelegate {
    
    private var dataSource: ProfileInfoDataSource!

    private var headerView: ProfileInfoHeader?
    private var footerView: ProfileInfoFooter?
    
    // MARK: - Initializers

    convenience init(withView view: ProfileInfoViewProtocol, model: ProfileInfoModelProtocol, router: ProfileInfoRouter, dataSource: ProfileInfoDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self
        self.dataSource = dataSource
    }
    
    internal required init(withView view: ProfileInfoViewProtocol!, model: ProfileInfoModelProtocol!, router: ProfileInfoRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        self.setupTableView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        // Autolayout table view header
        if let headerView = self.customView.tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                self.customView.tableView.tableHeaderView = headerView
            }
        }
        
        // Autolayout table view footer
        if let footerView = self.customView.tableView.tableFooterView {
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var footerFrame = footerView.frame
            
            if height != footerFrame.size.height {
                footerFrame.size.height = height
                footerView.frame = footerFrame
                self.customView.tableView.tableFooterView = footerView
            }
        }
    }
    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    private func setupTableView() {
        self.customView.tableView.rowHeight = UI.tableCellMediumHeight
        self.customView.tableView.separatorInset = TitleValueCell.getDefaultSeparatorInset()
        
        self.setupTableViewHeaderFooter()
    }
    
    private func setupTableViewHeaderFooter() {
        
        if let headerView = ProfileInfoHeader.loadFromXib() as? ProfileInfoHeader {
            headerView.headerTitleLabel.textColor = ColorHelper.regularColor()
            headerView.profileDescriptionLabel.textColor = UIColor.darkGray
            headerView.headerTitleLabel.text = NSLocalizedString("Your profile information", comment: "Profile Info")
            //headerView.gradientView.colors = Theme.current.profileBackgroundGradientColor()
            self.headerView = headerView
            self.customView.tableView.tableHeaderView = self.headerView
        }
        
        if let profileVideoUrl = self.model.profileVideoUrl,
            let footerView = ProfileInfoFooter.loadFromXib() as? ProfileInfoFooter {

            footerView.didTapPlayControl = { [weak self] in
                guard let strongSelf = self else { return }
                strongSelf.router?.openPlayer(withVideoUrl: profileVideoUrl, from: strongSelf)
            }
            
            footerView.backgroundColor = RGB(246, 239, 221)
            footerView.footerTitleLabel.textColor = RGB(238, 210, 52)
            self.footerView = footerView
            self.customView.tableView.tableFooterView = self.footerView
        } else {
            self.footerView = nil
            self.customView.tableView.tableFooterView = self.footerView
        }
    }
    
    // MARK: - Private methods
    
    func updateView() {
        self.model.reloadFields(withAccount: AccountManager.shared.currentUser!)
        
        self.customView.tableView.reloadData()
        self.updateHeaderView()
    }
    
    func updateHeaderView() {
        guard let headerView = self.headerView else {
            return
        }
        if let avatarUrl = self.model.profileAvatarUrl {
            headerView.avatarImageView.kf.setImage(with: avatarUrl)
        }
        headerView.profileDescriptionLabel.text = self.model.profileDescription
        
        headerView.ratingView.rating = self.model.account.rating
    }
}

// MARK: - ProfileInfoViewDelegate

extension ProfileInfoViewController: ProfileInfoViewDelegate {

}

// MARK: - ProfileInfoModelDelegate

extension ProfileInfoViewController: ProfileInfoModelDelegate {
    
    func modelDidChanged(model: ProfileInfoModelProtocol) {
        self.updateView()
    }
}
