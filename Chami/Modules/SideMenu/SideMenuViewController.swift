//
//  SideMenuViewController.swift
//  Chami
//
//  Created by Igor Markov on 2/7/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import SideMenu

typealias SideMenuViewControllerType = BaseViewController<SideMenuModelProtocol, SideMenuViewProtocol, SideMenuRouter>

class SideMenuViewController: SideMenuViewControllerType, UITableViewDelegate {

    private var topDataSource: SideMenuDataSource!
    private var bottomDataSource: SideMenuDataSource!

    // MARK: - Initializers

    convenience init(withView view: SideMenuViewProtocol, model: SideMenuModelProtocol, router: SideMenuRouter, topDataSource: SideMenuDataSource, bottomDataSource: SideMenuDataSource) {

        self.init(withView: view, model: model, router: router)

        self.model.delegate = self

        self.topDataSource = topDataSource
        self.bottomDataSource = bottomDataSource
    }

    internal required init(withView view: SideMenuViewProtocol!, model: SideMenuModelProtocol!, router: SideMenuRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.isNavigationBarHidden = true

        self.customView.delegate = self
        self.connectTableViewDependencies()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateView()
    }

    private func connectTableViewDependencies() {

        self.customView.topTableView.delegate = self
        self.topDataSource.registerNibsForTableView(tableView: self.customView.topTableView)
        self.customView.topTableView.dataSource = self.topDataSource

//        self.customView.bottomTableView.delegate = self
//        self.bottomDataSource.registerNibsForTableView(tableView: self.customView.bottomTableView)
//        self.customView.bottomTableView.dataSource = self.bottomDataSource
    }

    fileprivate func updateView() {
        self.customView.topTableView.reloadData()
        //self.customView.bottomTableView.reloadData()
        self.customView.accountRole = AccountManager.shared.currentRole
    }

    // MARK: - Table view delegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)

        guard let cell = tableView.cellForRow(at: indexPath) as? SideMenuTableViewCell else {
            return
        }

        let selectedSideMenuItemType = cell.sideMenuItemType
        switch selectedSideMenuItemType {
        case .home:
            NavigationManager.shared.showHomeScreen(animated: true)
        case .sessionHistory:
            NavigationManager.shared.showSessionHistoryScreen(animated: true)
        case .messages:
            NavigationManager.shared.showMessagesScreen(animated: true)
        case .friends:
            NavigationManager.shared.showFriendsScreen(animated: true)
        case .languages:
            NavigationManager.shared.showLanguagesScreen(animated: true)
        case .share:
            NavigationManager.shared.showShareScreen(animated: true)
        case .freeCredits:
            NavigationManager.shared.showFreeCreditsScreen(animated: true)
        case .help:
            NavigationManager.shared.showHelpScreen(animated: true)
        case .logout:
            AlertManager.showQuestion(withTitle: NSLocalizedString("Are you sure you want to logout?", comment: "Are you sure you want to logout?"), message: "", onController: self, okHandler: { 
                //
                if let currentVisibleController = NavigationManager.shared.currentVisibleController(), SideMenuManager.defaultManager.menuLeftNavigationController != nil {
                    currentVisibleController.dismiss(animated: true, completion: {
                        AccountManager.shared.loggedUserOut()
                    })
                }
            })
        default:
            break
        }
    }
    
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? SideMenuTableViewCell else {
            return
        }

        cell.titleLabel.textColor = ColorHelper.lightTextColor()
        cell.titleLabel.font = FontHelper.defaultMediumFont(withSize: 19)
        
        var separatorColor = UIColor()
        switch AccountManager.shared.currentRole {
        case .student:
            separatorColor = UIColor.white
        case .tutor:
            separatorColor = RGB(218, 162, 73)
        default:
            break
        }

        cell.topSeparator.backgroundColor = separatorColor
        cell.bottomSeparator.backgroundColor = separatorColor
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 1 {
            return 30
        }
        return 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        //view.backgroundColor = UIColor.white
        return view
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 0
    }
}

// MARK: - SideMenuViewDelegate

extension SideMenuViewController: SideMenuViewDelegate {

    func didTapChangeRoleButton() {
        NavigationManager.shared.showSwitchRole(animated: true)
    }
}

// MARK: - SideMenuModelDelegate

extension SideMenuViewController: SideMenuModelDelegate {

    func modelDidChanged(model: SideMenuModelProtocol) {
        self.updateView()
    }
}
