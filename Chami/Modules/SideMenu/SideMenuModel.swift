//
//  SideMenuModel.swift
//  Chami
//
//  Created by Igor Markov on 2/7/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum SideMenuType {
    case notDefined
    case home
    case sessionHistory
    case messages
    case friends
    case languages
    case share
    case freeCredits
    case help
    case logout
}


typealias SideMenuItem = (type: SideMenuType, title: String)
typealias SideMenuItemArray = [SideMenuItem]

protocol SideMenuModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: SideMenuModelProtocol)
}

protocol SideMenuModelProtocol: NSObjectProtocol {
    
    weak var delegate: SideMenuModelDelegate? { get set }

    var topMenuItems: SideMenuItemArray { get }
    var bottomMenuItems: SideMenuItemArray { get }
}

class SideMenuModel: NSObject, SideMenuModelProtocol {

    var accountRole: AccountRole

    init(withAccountRole accountRole: AccountRole) {
        self.accountRole = accountRole
        super.init()

        self.topMenuItems = self.createTopMenuItems(accountRole: accountRole)
        self.bottomMenuItems = self.createBottomMenuItems(accountRole: accountRole)
    }

    // MARK: - SideMenuModel methods

    weak var delegate: SideMenuModelDelegate?
    var topMenuItems: SideMenuItemArray = []
    var bottomMenuItems: SideMenuItemArray = []
    
    // MARK: - Private methods
    
    func createTopMenuItems(accountRole: AccountRole) -> SideMenuItemArray {
        switch accountRole {
        case .tutor:
            // OLD CODE
//            return [(type: .home, title: NSLocalizedString("My Chami", comment: "Side Menu")),
//                    (type: .sessionHistory, title: NSLocalizedString("Session History", comment: "Side Menu")),
//                    (type: .messages, title: NSLocalizedString("Messages", comment: "Side Menu")),
//                    (type: .friends, title: NSLocalizedString("Friends", comment: "Side Menu")),
//                    (type: .languages, title: NSLocalizedString("My Languages", comment: "Side Menu"))]
            return [(type: .home, title: NSLocalizedString("My Chami", comment: "Side Menu")),
                    (type: .sessionHistory, title: NSLocalizedString("Session History", comment: "Side Menu")),
//                    (type: .messages, title: NSLocalizedString("Messages", comment: "Side Menu")),
//                    (type: .friends, title: NSLocalizedString("Friends", comment: "Side Menu")),
                    (type: .languages, title: NSLocalizedString("My Languages", comment: "Side Menu"))]
        case .student:
            // OLD CODE
//            return [(type: .home, title: NSLocalizedString("Find a Tutor", comment: "Side Menu")),
//                    (type: .sessionHistory, title: NSLocalizedString("Session History", comment: "Side Menu")),
//                    (type: .messages, title: NSLocalizedString("Messages", comment: "Side Menu")),
//                    (type: .friends, title: NSLocalizedString("Friends", comment: "Side Menu"))
//                    ]
            return [(type: .home, title: NSLocalizedString("Find a Tutor", comment: "Side Menu")),
                    (type: .sessionHistory, title: NSLocalizedString("Session History", comment: "Side Menu")),
//                    (type: .messages, title: NSLocalizedString("Messages", comment: "Side Menu")),
//                    (type: .friends, title: NSLocalizedString("Friends", comment: "Side Menu"))
            ]
        default:
            return []
        }
    }
    
    func createBottomMenuItems(accountRole: AccountRole) -> SideMenuItemArray {
        switch accountRole {
        case .tutor,
             .student:
            // OLD CODE
//            return [(type: .share, title: NSLocalizedString("Share Chami", comment: "Side Menu")),
//                    //(type: .freeCredits, title: NSLocalizedString("Get Free Credits", comment: "Side Menu")),
//                    (type: .help, title: NSLocalizedString("Help and FAQs", comment: "Side Menu")),
//                    (type: .logout, title: NSLocalizedString("Logout", comment: "Side Menu"))]
            return [
//                (type: .share, title: NSLocalizedString("Share Chami", comment: "Side Menu")),
                    //(type: .freeCredits, title: NSLocalizedString("Get Free Credits", comment: "Side Menu")),
                (type: .help, title: NSLocalizedString("Help and FAQs", comment: "Side Menu")),
                (type: .logout, title: NSLocalizedString("Logout", comment: "Side Menu"))]
        default:
            return []
        }
    }
}
