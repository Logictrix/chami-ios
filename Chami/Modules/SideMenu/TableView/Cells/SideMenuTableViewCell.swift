//
//  SideMenuTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 2/7/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {
    
    var sideMenuItemType: SideMenuType = .notDefined

    @IBOutlet weak var topSeparator: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bottomSeparator: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        self.backgroundColor = UIColor.clear
        
        self.topSeparator.isHidden = true
        self.bottomSeparator.isHidden = true

    }
}
