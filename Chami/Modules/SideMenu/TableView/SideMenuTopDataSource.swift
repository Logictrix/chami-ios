//
//  SideMenuDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class SideMenuDataSource: NSObject, UITableViewDataSource {
    
    private let model: SideMenuModelProtocol
    private let cellReuseId = "SideMenuTableViewCell"
    //private let isTop: Bool
    
    init(withModel model: SideMenuModelProtocol, isTop: Bool) {
        self.model = model
        //self.isTop = isTop
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: SideMenuTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: SideMenuTableViewCell, forItem item: SideMenuItem, indexPath: IndexPath) {
        cell.sideMenuItemType = item.type
        cell.titleLabel.text = item.title
        
//        let shouldShowTopSeparator = !self.isTop && indexPath.row == 0
//        let shouldHideBottomSeparator = (item.type == .help)
//        cell.topSeparator.isHidden = !shouldShowTopSeparator
//        cell.bottomSeparator.isHidden = shouldHideBottomSeparator
    }
    
    // MARK: - UITableViewDataSource

    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 1 {
            return " text "
        }
        return nil
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        return UIView()
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (section == 0) ? self.model.topMenuItems.count : self.model.bottomMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? SideMenuTableViewCell else {
            fatalError()
        }
        
        let menuItem = (indexPath.section == 0) ? self.model.topMenuItems[indexPath.row] : self.model.bottomMenuItems[indexPath.row];
        self.configure(cell: cell, forItem: menuItem, indexPath: indexPath)
        
        return cell
    }
}
