//
//  SideMenuBuilder.swift
//  Chami
//
//  Created by Igor Markov on 2/7/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SideMenuBuilder: NSObject {

    class func viewController(withAccountRole accountRole: AccountRole) -> SideMenuViewController {
        let view: SideMenuViewProtocol = SideMenuView.create()
        let model: SideMenuModelProtocol = SideMenuModel(withAccountRole: accountRole)
        let topDataSource = SideMenuDataSource(withModel: model, isTop: true)
        let bottomDataSource = SideMenuDataSource(withModel: model, isTop: false)
        let router = SideMenuRouter()
        
        let viewController = SideMenuViewController(withView: view, model: model, router: router, topDataSource: topDataSource, bottomDataSource: bottomDataSource)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
