//
//  SideMenuView.swift
//  Chami
//
//  Created by Igor Markov on 2/7/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SideMenuViewDelegate: NSObjectProtocol {

    func didTapChangeRoleButton()
}

protocol SideMenuViewProtocol: NSObjectProtocol {
    
    weak var delegate: SideMenuViewDelegate? { get set }
    var accountRole: AccountRole { get set }
    var topTableView: UITableView! { get }
    //var bottomTableView: UITableView! { get }
}

class SideMenuView: GradientView, SideMenuViewProtocol{

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var changeRoleButton: UIButton!
    class func create() -> SideMenuView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? SideMenuView else {
            fatalError("Nib \(viewNibName) does not contain SideMenu View as first object")
        }
        
        return view
    }
    
    let tableRowHeight: CGFloat = 50
    
    weak var delegate: SideMenuViewDelegate?
    @IBOutlet weak var topTableView: UITableView!
    //@IBOutlet weak var bottomTableView: UITableView!
    
    
    var accountRole: AccountRole = .notDefined {
        didSet {
            if accountRole != oldValue {
                self.update(withRole: accountRole)
            }
        }
    }


    func update(withRole accountRole: AccountRole) {
        switch accountRole {
        case .tutor:
            self.changeRoleButton.setTitle(NSLocalizedString("I want to be a Student", comment: "Side Menu"), for: .normal)
            self.changeRoleButton.backgroundColor = RGB(242, 176, 73)
            self.colors = [RGB(244, 220, 87), RGB(244, 188, 54)]
        case .student:
            self.changeRoleButton.setTitle(NSLocalizedString("I want to be a Tutor", comment: "Side Menu"), for: .normal)
            self.changeRoleButton.backgroundColor = RGB(27, 227, 243)
            self.colors = [RGB(27, 229, 243), RGB(13, 173, 247)]
        default:
            self.changeRoleButton.setTitle(nil, for: .normal)
            self.changeRoleButton.backgroundColor = UIColor.clear
            self.colors = [UIColor.white, UIColor.white]
        }
    }

    // MARK: - IBActions

    @IBAction func changeLogiButtonAction() {
        self.delegate?.didTapChangeRoleButton()
    }
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.changeRoleButton.setTitleColor(ColorHelper.lightTextColor(), for: .normal)
        self.changeRoleButton.titleLabel?.font = FontHelper.defaultMediumFont(withSize: 19)

        self.topTableView.backgroundColor = UIColor.clear
        self.topTableView.rowHeight = self.tableRowHeight
        
        self.topTableView.tableHeaderView = UIView()
        self.topTableView.tableFooterView = UIView()
        //self.bottomTableView.backgroundColor = UIColor.clear
        //self.bottomTableView.rowHeight = self.tableRowHeight
    }
}
