//
//  LanguagesView.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol LanguagesViewDelegate: NSObjectProtocol {
    
}

protocol LanguagesViewProtocol: NSObjectProtocol {
    
    weak var delegate: LanguagesViewDelegate? { get set }
    var tableView: UITableView! { get }
}

class LanguagesView: UIView, LanguagesViewProtocol{

    class func create() -> LanguagesView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? LanguagesView else {
            fatalError("Nib \(viewNibName) does not contain Languages View as first object")
        }
        
        return view
    }
    
    // MARK: - LanguagesView interface methods

    weak var delegate: LanguagesViewDelegate?
    @IBOutlet weak var tableView: UITableView!

    // add view private properties/outlets/methods here
    
    // MARK: - IBActions
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        // setup view and table view programmatically here
    }
}
