//
//  LanguagesBuilder.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LanguagesBuilder: NSObject {

    class func viewController() -> LanguagesViewController {
        let view: LanguagesViewProtocol = LanguagesView.create()
        let model: LanguagesModelProtocol = LanguagesModel()
        let dataSource = LanguagesDataSource(withModel: model)
        let router = LanguagesRouter()
        
        let viewController = LanguagesViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
