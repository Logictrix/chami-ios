//
//  LanguagesModel.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol LanguagesModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: LanguagesModelProtocol)
}

protocol LanguagesModelProtocol: NSObjectProtocol {
    
    weak var delegate: LanguagesModelDelegate? { get set }
    var teachingLanguages: [Language] { get }
    //var items: [String] { get }
    
    func updateProfileLangs(completion: @escaping (_ error:Error?) -> Void)
}

class LanguagesModel: NSObject, LanguagesModelProtocol {
    
    var teachingLanguages: [Language] {
        get {
            return (AccountManager.shared.currentUser?.teachingLanguages)!
        }
    }
//    override init() {
//        super.init()
//
//    }
    

    fileprivate let profileService = ProfileService()
    func updateProfileLangs(completion: @escaping (_ error:Error?) -> Void) {
        guard let account = AccountManager.shared.currentUser else {
            return
        }
        
        self.profileService.getProfileLanguages(account: account, success: {
            completion(nil)
        }, failure: { error in
            completion(error)
        })
    }

    
    // MARK: - LanguagesModel methods

    weak var delegate: LanguagesModelDelegate?
    
    /** Implement LanguagesModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
