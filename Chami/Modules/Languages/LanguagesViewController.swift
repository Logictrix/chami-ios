//
//  LanguagesViewController.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias LanguagesViewControllerType = BaseViewController<LanguagesModelProtocol, LanguagesViewProtocol, LanguagesRouter>

class LanguagesViewController: LanguagesViewControllerType {
    
    private var dataSource: LanguagesDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: LanguagesViewProtocol, model: LanguagesModelProtocol, router: LanguagesRouter, dataSource: LanguagesDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        self.addSideMenuLeftButton()
    }
    
    internal required init(withView view: LanguagesViewProtocol!, model: LanguagesModelProtocol!, router: LanguagesRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
        self.navigationItem.title = NSLocalizedString("Languages", comment: "Languages")
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Edit", style: .plain, target: self, action: #selector(editButtonTapped))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setProgressVisible(visible: true)
        self.model.updateProfileLangs {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let err = error {
                AlertManager.showError(withMessage: err.localizedDescription, onController: strongSelf)
            } else {
                strongSelf.customView.tableView.reloadData()
            }
        }
    }
    
    @objc private func editButtonTapped() {
        let controller = EditLanguagesBuilder.viewController()
        self.navigationController?.pushViewController(controller, animated: true)
    }

    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self.dataSource
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
    }
}

// MARK: - LanguagesViewDelegate

extension LanguagesViewController: LanguagesViewDelegate {

}

// MARK: - LanguagesModelDelegate

extension LanguagesViewController: LanguagesModelDelegate {
    
    func modelDidChanged(model: LanguagesModelProtocol) {
    }
}

// MARK: - LanguagesCellDelegate

extension LanguagesViewController: LanguagesCellDelegate {
    
    func cellDidTapSomeButton(cell: LanguagesTableViewCell) {
    }
}
