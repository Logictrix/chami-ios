//
//  LanguagesTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LanguagesTableViewCell: UITableViewCell {
    
    weak var delegate: LanguagesCellDelegate?
    
    @IBOutlet weak var langImageView: UIImageView!
    @IBOutlet weak var langTitleLabel: UILabel!
    @IBOutlet weak var langDescriptionLabel: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.langTitleLabel.font = FontHelper.defaultFont(withSize: 15)
        self.langDescriptionLabel.font = FontHelper.defaultMediumFont(withSize: 15)
    }

    // MARK: - IBAction
    
//    @IBAction func someButtonAction() {
//        self.delegate?.cellDidTapSomeButton(cell: self)
//    }
}


protocol LanguagesCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    //func cellDidTapSomeButton(cell: LanguagesTableViewCell)
}
