//
//  LanguagesDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class LanguagesDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    weak var cellDelegate: LanguagesCellDelegate?
    private let model: LanguagesModelProtocol
    private let cellReuseId = "LanguagesTableViewCell"
    private let titleCellReuseId = "EditLanguagesTableTitleCell"
    
    
    init(withModel model: LanguagesModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: LanguagesTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
        
        let titleCellNib = UINib(nibName: String(describing: EditLanguagesTableTitleCell.self), bundle: nil)
        tableView.register(titleCellNib, forCellReuseIdentifier: titleCellReuseId)

    }
    
    
    // MARK: - UITableViewDataSource
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return 3
        }
        return self.model.teachingLanguages.count
    }
    
    private let standardLangDesc = NSLocalizedString("Letting us know about your standard of teaching will help us match you to relevant students.", comment: "")
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: titleCellReuseId) as? EditLanguagesTableTitleCell
                cell?.titleLabel.text = NSLocalizedString("Your teaching standard", comment: "")
                return cell!
            } else if indexPath.row == 1 {
                let cell = DescriptionTableViewCell.dequeueCell(tableView: tableView)
                cell.titleLabel.text = standardLangDesc
                return cell
            } else {
                let separator = SeparatorTableViewCell.dequeueCell(tableView: tableView)
                return separator
            }
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? LanguagesTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let language:Language = self.model.teachingLanguages[indexPath.row]
        
        cell.langTitleLabel.text = language.languageName
        
        cell.langImageView?.image = nil
        cell.langImageView?.kf.setImage(with: nil)
        if (language.languageImageUrl != nil) {
            let url = URL(string: language.languageImageUrl!)
            cell.langImageView?.kf.setImage(with: url)
        }
        
        cell.langDescriptionLabel.text = language.priceString(price: language.languageCost)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                return EditLanguagesTableTitleCell.cellHeight()
            } else if indexPath.row == 1 {
                return DescriptionTableViewCell.cellHeight(tableView: tableView, text: standardLangDesc)
            } else {
                return SeparatorTableViewCell.separatorHeight()
            }
        }
     
        return tableView.rowHeight
    }
}
