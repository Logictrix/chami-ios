//
//  CurrentRequestViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias CurrentRequestViewControllerType = BaseViewController<CurrentRequestModelProtocol, CurrentRequestViewProtocol, CurrentRequestRouter>

class CurrentRequestViewController: CurrentRequestViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: CurrentRequestViewProtocol!, model: CurrentRequestModelProtocol!, router: CurrentRequestRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:false)

        self.title = NSLocalizedString("Current Request", comment: "Current Request - Screen title")

        self.canHandleMeetupRequests = false
        
        customView.delegate = self
        model.delegate = self
        self.customView.createProgress()
        self.updateView()
        self.model.startTimer()
        
        SessionManager.shared.multicastDelegate.addDelegate(self)
    }
    
    deinit {
        SessionManager.shared.multicastDelegate.removeDelegate(self)
    }
    
    
    
    fileprivate func updateView() {
        self.customView.updateView(withTutorName: self.model.currentTutor.fullName!, withTutorImage: self.model.currentTutor.imageUrl, isChamiX: self.model.isChamiX)
    }

    fileprivate func requestWasApproved() {
        SoundPlayback.playDefaultSound()
        self.model.stopTimer()
        if self.isVisible == false {
            return
        }
        if self.model.accountRole == .student {
            self.router?.navigateToChamiConfirmedStudentScreen(from: self, withMeetUp: self.model.currentMeetUp, withMeetUpSession: self.model.currentMeetUpSession!, withTutor: self.model.currentTutor, isChamiX: self.model.isChamiX)
            
        } else if self.model.accountRole == .tutor {
            self.router?.navigateToChamiConfirmedTutorScreen(from: self, withRequestId: "1")
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
    }
    
    
    // MARK: - SessionManagerDelegate
    func didReceivedMeetUpResponse(session: MeetUpSession) {
        // from tutor to student
        if session.isAccepted == false {
            self.router?.navigateToRequestFailedScreen(from: self, isChami: self.model.isChamiX)
//            AlertManager.showAlert(withTitle: "Sorry, we couldn't get you a chami this time", message: "We were unable to get a response from the tutor, or they rejected the request", onController: self, closeHandler: {
//                //
//                _ = self.navigationController?.popToRootViewController(animated: true)
//            })
        } else {
            self.model.currentMeetUpSession = session;
            self.requestWasApproved()
        }

    }

    // MARK: - Public methods
    func setTutor(tutor: User) {
        self.model.currentTutor = tutor
    }
    func setMeetUp(meetUp: MeetUp) {
        self.model.currentMeetUp = meetUp
    }
    func setMeetUpSession(meetUpSession: MeetUpSession) {
        self.model.currentMeetUpSession = meetUpSession
    }
    func setIsChamiX(isChamiX: Bool) {
        self.model.isChamiX = isChamiX
    }

}

// MARK: - CurrentRequestViewDelegate

extension CurrentRequestViewController: CurrentRequestViewDelegate {
    
    func viewSendActionCancelRequest(view: CurrentRequestViewProtocol) {
        
        SessionManager.shared.executeRequestCancel(session: self.model.currentMeetUpSession!) { (error) in
            //
            if let error = error {
                print(error.localizedDescription)
            }
            _ = self.navigationController?.popToRootViewController(animated: true)
        }

    }

}

// MARK: - CurrentRequestModelDelegate

extension CurrentRequestViewController: CurrentRequestModelDelegate {
    func modelDidUpdateTimer(model: CurrentRequestModelProtocol, timeLeft: Double, fullTime: Double) {
        self.customView.updateViewProgress(timeLeft: timeLeft, fullTime: fullTime)
    }
    
    func modelFinishedCountdown(model: CurrentRequestModelProtocol) {
//        self.customView.updateViewProgress(timeLeft: timeLeft, fullTime: fullTime)
        
        
        SessionManager.shared.executeRequestCancel(session: self.model.currentMeetUpSession!) { (error) in
            //
            if let error = error {
                print(error.localizedDescription)
            }
        }
        
        self.router?.navigateToRequestFailedScreen(from: self, isChami: self.model.isChamiX)

//        AlertManager.showAlert(withTitle: "Sorry, we couldn't get you a chami this time", message: "We were unable to get a response from the tutor, or they rejected the request", onController: self, closeHandler: {
//            //
//            _ = self.navigationController?.popToRootViewController(animated: true)
//        })
    }

}
