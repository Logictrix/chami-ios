//
//  CurrentRequestView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import KDCircularProgress

protocol CurrentRequestViewDelegate: NSObjectProtocol {
    
    func viewSendActionCancelRequest(view: CurrentRequestViewProtocol)

}

protocol CurrentRequestViewProtocol: NSObjectProtocol {
    
    weak var delegate: CurrentRequestViewDelegate? { get set }
    
    func createProgress()
    func updateView(withTutorName tutorName: String, withTutorImage imageUrlString: String?, isChamiX: Bool)
    func updateViewProgress(timeLeft: Double, fullTime: Double)


}

class CurrentRequestView: UIView, CurrentRequestViewProtocol{

    class func create() -> CurrentRequestView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? CurrentRequestView else {
            fatalError("Nib \(viewNibName) does not contain CurrentRequest View as first object")
        }
        
        return view
    }
    
    // MARK: - CurrentRequestView interface methods

    @IBOutlet weak var tutorNameLabel: UILabel!
    @IBOutlet weak var tutorLogoImageView: UIImageView!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var timerStaticLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    @IBOutlet weak var waitingForResponseLabel: UILabel!
    @IBAction func actionCancelRequest(_ sender: Any) {
        self.delegate?.viewSendActionCancelRequest(view: self)
    }
    
    var progress: KDCircularProgress!

    weak var delegate: CurrentRequestViewDelegate?
    func updateView(withTutorName tutorName: String, withTutorImage imageUrlString: String?, isChamiX: Bool) {
        self.tutorNameLabel.text = tutorName
        if isChamiX {
            self.waitingForResponseLabel.text = "Waiting for a response from your chosen partner, to confirm their availability for this session."
        } else {
            self.waitingForResponseLabel.text = "Waiting for a response from your chosen tutor, to confirm their availability for this session."

        }
        if (imageUrlString != nil) {
            let url = URL(string: imageUrlString!)
            self.tutorLogoImageView.kf.setImage(with: url)
        }

    }

    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        Theme.current.apply(borderedButton: self.cancelButton)
        self.layoutIfNeeded()
        self.tutorLogoImageView.layer.cornerRadius = self.tutorLogoImageView.bounds.size.height / 2.0
        self.tutorLogoImageView.layer.masksToBounds = true
        self.tutorLogoImageView.contentMode = .scaleAspectFill


    }
    func createProgress() {
        progress = KDCircularProgress(frame: CGRect(x: 0, y: 0, width: self.timerView.bounds.size.width, height: self.timerView.bounds.size.height))
        progress.startAngle = -90
        progress.progressThickness = 0.2
        progress.trackThickness = 0.2
        progress.clockwise = false
        progress.gradientRotateSpeed = 2
        progress.roundedCorners = false
        progress.glowMode = .forward
        progress.glowAmount = 0.1
        progress.set(colors: UIColor.cyan , UIColor.blue)
        progress.trackColor = UIColor.lightGray
        
        progress.progress = 1.0
//        progress.center = CGPoint(x: self.timerView.center.x, y: self.timerView.center.y)
        self.timerView.addSubview(progress)
        self.timerView.bringSubviewToFront(self.timerLabel)
        self.timerView.bringSubviewToFront(self.timerStaticLabel)

    }

    func updateViewProgress(timeLeft: Double, fullTime: Double) {
        progress.progress = 1 - (fullTime - timeLeft) / fullTime
        self.timerLabel.text = "\(Int(timeLeft))"
        
    }

    // MARK: - IBActions
}
