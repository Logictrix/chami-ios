//
//  CurrentRequestRouter.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class CurrentRequestRouter: NSObject {
    
    func navigateToChamiConfirmedStudentScreen(from vc: UIViewController, withMeetUp meetUp: MeetUp, withMeetUpSession session: MeetUpSession, withTutor tutor: User, isChamiX: Bool) {
        
        let nextVC = ChamiConfirmedStudentBuilder.viewController()
        nextVC.setMeetUp(meetUp: meetUp)
        nextVC.setTutor(tutor: tutor)
        nextVC.setMeetUpSession(meetUpSession: session)
        nextVC.setIsChamiX(isChamiX: isChamiX)

        vc.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    func navigateToChamiConfirmedTutorScreen(from vc: UIViewController, withRequestId requestId: IdType) {
        
        let nextVC = ChamiConfirmedTutorBuilder.viewController()
        vc.navigationController?.pushViewController(nextVC, animated: false)
    }
    
    func navigateToRequestFailedScreen(from vc: UIViewController, isChami: Bool) {
        
        let nextVC = RequestFailedBuilder.viewController()
        nextVC.setIsChamiX(isChamiX: isChami)
        vc.navigationController?.pushViewController(nextVC, animated: false)
    }


}
