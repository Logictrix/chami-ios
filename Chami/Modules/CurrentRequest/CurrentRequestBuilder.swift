//
//  CurrentRequestBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class CurrentRequestBuilder: NSObject {

    class func viewController() -> CurrentRequestViewController {

        let view: CurrentRequestViewProtocol = CurrentRequestView.create()
        let model: CurrentRequestModelProtocol = CurrentRequestModel()
        let router = CurrentRequestRouter()
        
        let viewController = CurrentRequestViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
