//
//  CurrentRequestModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol CurrentRequestModelDelegate: NSObjectProtocol {
    func modelDidUpdateTimer(model: CurrentRequestModelProtocol, timeLeft: Double, fullTime: Double)
    func modelFinishedCountdown(model: CurrentRequestModelProtocol)


}

protocol CurrentRequestModelProtocol: NSObjectProtocol {
    
    weak var delegate: CurrentRequestModelDelegate? { get set }
    var accountRole: AccountRole { get set }
    var currentTutor: User! { get set }
    var currentMeetUp: MeetUp! { get set }
    
    var currentMeetUpSession: MeetUpSession? { get set }

    func startTimer()
    func stopTimer()
    var isChamiX: Bool { get set }

}

class CurrentRequestModel: NSObject, CurrentRequestModelProtocol {
    
    // MARK: - CurrentRequestModel methods
    var currentTutor: User!
    var currentMeetUp: MeetUp! = MeetUp()
    var currentMeetUpSession: MeetUpSession?
    var isChamiX = false
    
    weak var delegate: CurrentRequestModelDelegate?
    var accountRole: AccountRole = AccountManager.shared.currentRole
    

    // MARK: - CurrentRequestModel methods
    private var tickTimer: Timer!
    private var timeLeft = 0.0
    private let timeoutFullTime = 60.0
    private let tickUpdate = 0.1

    func startTimer() {
        self.timeLeft = self.timeoutFullTime
        self.tickTimer = Timer.scheduledTimer(timeInterval: tickUpdate,
                                              target: self,
                                              selector: #selector(self.updateTime),
                                              userInfo: nil,
                                              repeats: true)
        self.updateTime()
    }

    @objc func updateTime() {
        self.timeLeft -= tickUpdate
        if self.timeLeft <= 0.0 {
            self.stopTimer()
            self.delegate?.modelFinishedCountdown(model: self)

        } else {
            self.delegate?.modelDidUpdateTimer(model: self, timeLeft: self.timeLeft, fullTime:self.timeoutFullTime)
        }
        
    }
    
    func stopTimer() {
        self.timeLeft = 0.0
        if self.tickTimer.isValid  {
            self.tickTimer.invalidate()
        }
    }


    /** Implement CurrentRequestModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
