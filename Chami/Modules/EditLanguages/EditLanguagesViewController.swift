//
//  EditLanguagesViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias EditLanguagesViewControllerType = BaseViewController<EditLanguagesModelProtocol, EditLanguagesViewProtocol, EditLanguagesRouter>

class EditLanguagesViewController: EditLanguagesViewControllerType, UITableViewDelegate {
    
    private var dataSource: EditLanguagesDataSource!
    
    var isLoginMode: Bool = false
    // MARK: - Initializers

    convenience init(withView view: EditLanguagesViewProtocol, model: EditLanguagesModelProtocol, router: EditLanguagesRouter, dataSource: EditLanguagesDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        
        //self.dataSource.cellDelegate = self
    }
    
    internal required init(withView view: EditLanguagesViewProtocol!, model: EditLanguagesModelProtocol!, router: EditLanguagesRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //self.customView.delegate = self
        self.connectTableViewDependencies()

        self.navigationItem.title = NSLocalizedString("Edit Languages", comment: "Edit Languages Screen")
        
        self.setProgressVisible(visible: true)
        self.model.updateProfileLangs {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let err = error {
                AlertManager.showError(withMessage: err.localizedDescription, onController: strongSelf)
            } else {
                strongSelf.customView.tableView.reloadData()
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.customView.setupTableView()
    }
    
    
    private func connectTableViewDependencies() {
        
        
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
        self.customView.tableView.delegate = self.dataSource
        self.dataSource.viewDelegate = self
    }
    
    // MARK: - Table view delegate

}

// MARK: - delegates
extension EditLanguagesViewController: SelectLanguageViewControllerDelegate {
    func viewControllerDidFinishWithSelectedLanguage(viewController: UIViewController , language: Language?) {
        _ = self.navigationController?.popToViewController(self, animated: true)
        guard let lang = language else { return }
        //self.model.teachingLanguages.insert(lang, at: self.model.teachingLanguages.count)   
        
        var alreadyExists = false
        for storedLang in self.model.teachingLanguages {
            if (lang.languageId == storedLang.languageId) {
                alreadyExists = true
            }
        }
        if (!alreadyExists) {
            self.model.teachingLanguages.append(lang)
        }
        
    }
}

extension EditLanguagesViewController: EditLanguagesViewDelegate {

//    func addSpeakingLanguageAction() {}
//    func removeSpeakingLanguageAction(index:Int) {}
    
    func addTeachingLanguageAction() {
        SelectLanguageRouter.push(from:self, delegate:self)
        
    }
    func removeTeachingLanguageAction(index:Int) {
        self.model.teachingLanguages.remove(at: index)
    }
    
    func teachingLanguageValueChanged(value:Float, index:Int, section:TeachingLanguageType) {
        let lang = self.model.teachingLanguages[index]
        if section == .standard {
            lang.level = value
        } else if section == .chamiPlus {
            lang.pricePlus = value
        } else if section == .chamiMax {
            lang.priceMax = value
        }
    }
    
    func teachingLanguageSwitchedValue(enabled:Bool, section:TeachingLanguageType) {
        self.model.chamiMaxEnabled = ((section == .chamiMax && enabled) || (section == .chamiPlus && !enabled))
    }
    
    func uploadDocumentAction() {
        self.showChoosePhotoActionSheet {[weak self] (image) in
            guard let strongSelf = self else { return }
            strongSelf.model.documentImages.append(image)

        }
    }
    
    func saveAction() {
        if let warning = self.model.validateFields() {
            self.showAlert(text: warning)
            return
        }
        self.setProgressVisible(visible: true)
        self.model.setup {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let msg = error?.localizedDescription  {
                strongSelf.showAlert(error: msg)
                return
            }
            
            if strongSelf.isLoginMode == true {
                strongSelf.router?.navigateToLoginSetupCompleteScreen(from: strongSelf)
            } else {
                NavigationManager.shared.showMainScreen(animated: true)
            }
        }
        
    }
}

// MARK: - EditLanguagesModelDelegate

extension EditLanguagesViewController: EditLanguagesModelDelegate {
    
    func modelDidChanged(model: EditLanguagesModelProtocol) {
        self.customView.tableView.reloadData()
    }
}

// MARK: - EditLanguagesCellDelegate

//extension EditLanguagesViewController: EditLanguagesCellDelegate {
//    
//    func cellDidTapSomeButton(cell: EditLanguagesTableViewCell) {
//    }
//}
