//
//  EditLanguagesTableHeaderView.swift
//  Chami
//
//  Created by Serg Smyk on 17/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class EditLanguagesTableHeaderView: UIView {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    override func awakeFromNib() {
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 20)
        self.titleLabel.textColor = Theme.current.regularColor()
        
        self.descriptionLabel.font = FontHelper.defaultFont(withSize: 16)
    }

}
