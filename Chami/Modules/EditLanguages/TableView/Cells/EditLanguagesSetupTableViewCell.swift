//
//  EditLanguagesSetupTableViewCell.swift
//  Chami
//
//  Created by Serg Smyk on 21/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class EditLanguagesSetupTableViewCell: UITableViewCell {

    @IBOutlet weak var titleImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sliderView: UISlider!
    @IBOutlet weak var valueLeftLabel: UILabel!
    @IBOutlet weak var valueCenterLabel: UILabel!
    @IBOutlet weak var valueRightLabel: UILabel!
    @IBOutlet weak var deleteButton: UIButton!
    
    
    var valueChangedBlock : ((_ setupValue:Float) -> Void)?
    var deleteClickedBlockBlock : (() -> Void)?
    
    class func cellHeight() -> CGFloat {
        return 115
    }
    
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.setupCell()
    }
    override func awakeFromNib() {
        super.awakeFromNib()

        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 16)
        
        let valueColor = ColorHelper.linkTextColor()
        let valueFont = FontHelper.defaultMediumFont(withSize: 14)
        
        self.sliderView.thumbTintColor = valueColor
        
        self.valueLeftLabel.textColor = valueColor
        self.valueCenterLabel.textColor = valueColor
        self.valueRightLabel.textColor = valueColor
        
        self.valueLeftLabel.font = valueFont
        self.valueCenterLabel.font = valueFont
        self.valueRightLabel.font = valueFont
        
        self.setupCell()
    }

    private func setupCell() {
        self.sliderView.value = 0
        self.valueLeftLabel.text = nil
        self.valueCenterLabel.text = nil
        self.valueRightLabel.text = nil
        self.deleteButton.isHidden = true
        self.sliderView.minimumValue = 0
        self.sliderView.maximumValue = 1

        self.selectedBackgroundView = UIView()
    }
    

    
    let step: Float = 0.1
    @IBAction func sliderValueChanged(_ sender: UISlider) {
        if let valueChangedBlock = self.valueChangedBlock {
            let roundedValue = round(sender.value / step) * step
            sender.value = roundedValue
            valueChangedBlock(sender.value)
        }
    }
    @IBAction func onDeleteClicked(_ sender: UIButton) {
        if let deleteClickedBlockBlock = self.deleteClickedBlockBlock {
            deleteClickedBlockBlock()
        }
    }
}
