//
//  EditLanguagesTableTitleCell.swift
//  Chami
//
//  Created by Serg Smyk on 17/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class EditLanguagesTableTitleCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchControl: UISwitch!
    
    var showSwitchControl:Bool { // FALSE is default
        get {
            return !self.switchControl.isHidden
        }
        set {
            self.switchControl.isHidden = !newValue
        }
    }
    
    var switchValueChangedBlock : ((_ switchValue:Bool) -> Void)?
    
    class func cellHeight() -> CGFloat {
        return 40
    }
    
    
    private func setupCell() {
        self.titleLabel.text = nil
        self.titleLabel.attributedText = nil
        self.showSwitchControl = false
        self.switchControl.isEnabled = true
        
        self.selectedBackgroundView = UIView()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 18)
        self.titleLabel.textColor = Theme.current.regularColor()

        self.setupCell()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.setupCell()
    }

    @IBAction func switchControlClick(_ sender: UISwitch) {
        if let switchValueChangedBlock = self.switchValueChangedBlock {
            switchValueChangedBlock(sender.isOn)
        }
    }

    
}
