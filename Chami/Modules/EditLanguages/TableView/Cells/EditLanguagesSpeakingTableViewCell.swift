//
//  EditLanguagesSpeakingTableViewCell.swift
//  Chami
//
//  Created by Serg Smyk on 21/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class EditLanguagesSpeakingTableViewCell: UITableViewCell {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    
    var deleteClickedBlockBlock : (() -> Void)?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.containerView.backgroundColor = ColorHelper.backgroundFormFieldColor()
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 16)
        self.selectedBackgroundView = UIView()
    }

    @IBAction func deleteButtonAction(_ sender: UIButton) {
        if let deleteClickedBlockBlock = self.deleteClickedBlockBlock {
            deleteClickedBlockBlock()
        }
    }
    
}
