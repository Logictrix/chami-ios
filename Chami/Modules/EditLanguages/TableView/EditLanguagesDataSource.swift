//
//  EditLanguagesDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum TeachingLanguageType : Int {
    case standard
    case chamiPlus
    case chamiMax
    case chamiMaxContineuPlus
}


protocol EditLanguagesViewDelegate: NSObjectProtocol {
    
//    func addSpeakingLanguageAction()
//    func removeSpeakingLanguageAction(index:Int)
    
    func addTeachingLanguageAction()
    func removeTeachingLanguageAction(index:Int)

    func teachingLanguageValueChanged(value:Float, index:Int, section:TeachingLanguageType)
    
    func teachingLanguageSwitchedValue(enabled:Bool, section:TeachingLanguageType)
    
    func uploadDocumentAction()
    func saveAction()
}


enum EditLanguagesSectionType : Int {
    case speak
    case speakFooter
    case teachingHeader
    case teaching
    case teachingFooter
    case chamiPlusHeader
    case chamiPlus
    case chamiPlusFooter
    case chamiMaxHeader
    case chamiMax
    case chamiMaxFooter
    case finalyContinueSection
    
    case count
}


class EditLanguagesDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    weak var viewDelegate: EditLanguagesViewDelegate?
    private let model: EditLanguagesModelProtocol

    private let titleCellReuseId = "EditLanguagesTableTitleCell"
    private let speakingLangCellReuseId = "EditLanguagesSpeakingTableViewCell"
    private let setupLangCellReuseId = "EditLanguagesSetupTableViewCell"
    
    
    
    init(withModel model: EditLanguagesModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let titleCellNib = UINib(nibName: String(describing: EditLanguagesTableTitleCell.self), bundle: nil)
        tableView.register(titleCellNib, forCellReuseIdentifier: titleCellReuseId)

        let speakingCellNib = UINib(nibName: String(describing: EditLanguagesSpeakingTableViewCell.self), bundle: nil)
        tableView.register(speakingCellNib, forCellReuseIdentifier: speakingLangCellReuseId)

        let setupCellNib = UINib(nibName: String(describing: EditLanguagesSetupTableViewCell.self), bundle: nil)
        tableView.register(setupCellNib, forCellReuseIdentifier: setupLangCellReuseId)

    }
    
    // MARK: - UITableViewDataSource
//    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
//        return 2
//    }
    
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return EditLanguagesSectionType.count.rawValue
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sectionType = EditLanguagesSectionType(rawValue: section) else {
            return 0
        }
        switch sectionType {
        case .speak:
            return self.model.teachingLanguages.count
        case .speakFooter:
            return 2
        case .teachingHeader:
            return 2
        case .teaching:
            return self.model.teachingLanguages.count
        case .teachingFooter:
            return 1
        case .chamiPlusHeader:
            return 3
        case .chamiPlus:
            return self.model.teachingLanguages.count
        case .chamiPlusFooter:
            return 1
        case .chamiMaxHeader:
            return 2
        case .chamiMax:
            if self.model.chamiMaxApproved {
                return self.model.teachingLanguages.count
            }
            return 0
        case .chamiMaxFooter:
            return 2
        case .finalyContinueSection:
            return 1
        default: break
        }
        
        return 0//self.model.items.count
    }
    
    // dequeue cells
    func titleCell(tableView:UITableView) -> EditLanguagesTableTitleCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: titleCellReuseId) as? EditLanguagesTableTitleCell else {
            fatalError()
        }
        return cell
    }
    func speakingCell(tableView:UITableView) -> EditLanguagesSpeakingTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: speakingLangCellReuseId) as? EditLanguagesSpeakingTableViewCell else {
            fatalError()
        }
        return cell
    }
    func setupCell(tableView:UITableView, language:Language) -> EditLanguagesSetupTableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: setupLangCellReuseId) as? EditLanguagesSetupTableViewCell else {
            fatalError()
        }
        cell.titleLabel.text = language.languageName
        if (language.languageImageUrl != nil) {
            let url = URL(string: language.languageImageUrl!)
            cell.titleImage?.kf.setImage(with: url)
        } else {
            cell.titleImage?.image = nil
        }
        return cell
    }
    
    private let standardLangDesc = NSLocalizedString("Letting us know about your standard of teaching will help us match you to relevant students.", comment: "")
    private let chamiPlusLangDesc = NSLocalizedString("Please use the slider to choose how much you are going to charge per hour.", comment: "")
    //private let chamiPlusLangDesc = NSLocalizedString("The maximum tuition you can charge on chami+ is £20.\nIf you want to go higher please select chami max.", comment: "")
    private let chamiMaxLangDesc = NSLocalizedString("You are an experienced tutor with a recognized teaching qualification (Subject to approval)", comment: "")
    private let chamiMaxFooterLangDesc = NSLocalizedString("While your account is being approved why don’t you brush up on your skills with chami+ ?", comment: "")
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let sectionType = EditLanguagesSectionType(rawValue: indexPath.section) else {
            return UITableViewCell()
        }
        switch sectionType {
        case .speak:
            let lang:Language = self.model.teachingLanguages[indexPath.row]
            let cell = self.speakingCell(tableView: tableView)
            cell.titleLabel.text = lang.languageName
            cell.deleteClickedBlockBlock = {[weak self] () in
                guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                delegate.removeTeachingLanguageAction(index: indexPath.row)
            }
            return cell
        case .speakFooter:
            if indexPath.row == 0 {
                let cell = self.titleCell(tableView: tableView)
                let string = NSLocalizedString("+ Add a language", comment: "")
                cell.titleLabel.attributedText = string.underline(font: cell.titleLabel.font, color: cell.titleLabel.textColor)
                return cell
            }
        case .teachingHeader:
            if indexPath.row == 0 {
                let cell = self.titleCell(tableView: tableView)
                cell.titleLabel.text = NSLocalizedString("Your teaching standard", comment: "")
                return cell
            } else  {
                let cell = DescriptionTableViewCell.dequeueCell(tableView: tableView)
                cell.titleLabel.text = standardLangDesc
                cell.titleLabel.textColor = UIColor.darkGray
                return cell
            }
        case .teaching:
            let lang:Language = self.model.teachingLanguages[indexPath.row]
            let cell = self.setupCell(tableView: tableView, language: lang)
            cell.valueLeftLabel.text = "Beginner"
            cell.valueCenterLabel.text = "Intermiediate"
            cell.valueRightLabel.text = "Expert"
            cell.sliderView.maximumValue = self.model.chamiLevelMaximumValue
            cell.sliderView.value = lang.level
            cell.valueChangedBlock = {[weak self](setupValue:Float) in
                guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                delegate.teachingLanguageValueChanged(value: setupValue, index: indexPath.row, section: .standard)
            }
//            cell.deleteButton.isHidden = false
//            cell.deleteClickedBlockBlock = {() in
//            }
            return cell
//        case .teachingFooter:
//            if indexPath.row == 0 {
//                let cell = self.titleCell(tableView: tableView)
//                let string = NSLocalizedString("+ Add another language", comment: "")
//                cell.titleLabel.attributedText = string.underline(font: cell.titleLabel.font, color: cell.titleLabel.textColor)
//                return cell
//            }
        case .chamiPlusHeader:
            if indexPath.row == 0 {
                let cell = self.titleCell(tableView: tableView)
                cell.titleLabel.text = NSLocalizedString("Your tuition rate", comment: "")
                return cell
            } else if indexPath.row == 1 {
                let cell = DescriptionTableViewCell.dequeueCell(tableView: tableView)
                cell.titleLabel.text = chamiPlusLangDesc
                cell.titleLabel.textColor = UIColor.darkGray
                return cell
            } else if indexPath.row == 2 {
                let cell = self.titleCell(tableView: tableView)
                cell.titleLabel.text = NSLocalizedString("CHAMI +", comment: "")
                cell.showSwitchControl = true
                cell.switchControl.isOn = !self.model.chamiMaxEnabled
                cell.switchControl.isHidden = !self.model.chamiMaxApproved
                cell.switchValueChangedBlock = {[weak self] (switchValue:Bool) in
                    guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                    delegate.teachingLanguageSwitchedValue(enabled: switchValue, section: .chamiPlus)

                }
                return cell
            }
        case .chamiPlus:
            let lang:Language = self.model.teachingLanguages[indexPath.row]
            let cell = self.setupCell(tableView: tableView, language: lang)
            cell.valueRightLabel.text = lang.priceString(price: lang.pricePlus)
            cell.sliderView.minimumValue = self.model.chamiPlusMinimumPrice
            cell.sliderView.maximumValue = self.model.chamiPlusMaximumPrice
            cell.sliderView.value = lang.pricePlus
            cell.valueChangedBlock = {[weak self, weak cell] (setupValue: Float) in
                guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                delegate.teachingLanguageValueChanged(value: setupValue, index: indexPath.row, section: .chamiPlus)

                cell?.valueRightLabel.text = lang.priceString(price: lang.pricePlus)
            }
            return cell
        case .chamiMaxHeader:
            if indexPath.row == 0 {
                let cell = self.titleCell(tableView: tableView)
                cell.titleLabel.text = NSLocalizedString("CHAMI MAX", comment: "")
                cell.showSwitchControl = self.model.chamiMaxApproved
                cell.switchControl.isOn = self.model.chamiMaxEnabled
                cell.switchValueChangedBlock = {[weak self] (switchValue:Bool) in
                    guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                    delegate.teachingLanguageSwitchedValue(enabled: switchValue, section: .chamiMax)
                }
                return cell
            } else if indexPath.row == 1 {
                let cell = DescriptionTableViewCell.dequeueCell(tableView: tableView)
                cell.titleLabel.text = chamiMaxLangDesc
                cell.titleLabel.textColor = UIColor.darkGray
                return cell
            }
        case .chamiMax:
            let lang:Language = self.model.teachingLanguages[indexPath.row]
            let cell = self.setupCell(tableView: tableView, language: lang)
            cell.valueRightLabel.text = lang.priceString(price: lang.priceMax)
            cell.sliderView.minimumValue = self.model.chamiMaxMinimumPrice
            cell.sliderView.maximumValue = self.model.chamiMaxMaximumPrice
            cell.sliderView.value = lang.priceMax
            cell.valueChangedBlock = {[weak self, weak cell] (setupValue:Float) in
                guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                delegate.teachingLanguageValueChanged(value: setupValue, index: indexPath.row, section: .chamiMax)

                cell?.valueRightLabel.text = lang.priceString(price: lang.priceMax)
            }
            return cell
        case .chamiMaxFooter:
            if indexPath.row == 0 {
                let cell = ButtonTableViewCell.dequeueCell(tableView: tableView)
                cell.cellStyle = .fillStyle
                cell.button.setTitle("Upload document (jpeg)", for: .normal)
                cell.buttonClickedBlock = {[weak self] (cell:ButtonTableViewCell) in
                    guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                    delegate.uploadDocumentAction()
                }
                return cell
            } else if indexPath.row == 1 {
                let cell = DescriptionTableViewCell.dequeueCell(tableView: tableView)
                cell.titleLabel.text = chamiMaxFooterLangDesc
                cell.titleLabel.textColor = UIColor.darkGray
                return cell
            }
//            else if indexPath.row == 2 {
//                let cell = self.titleCell(tableView: tableView)
//                cell.titleLabel.text = NSLocalizedString("Continue with CHAMI +", comment: "")
//                cell.showSwitchControl = true
//                cell.switchControl.isOn = self.model.chamiMaxEnabledCanContinueWithPlus
//                cell.switchControl.isEnabled = self.model.chamiMaxEnabled
//                cell.switchValueChangedBlock = {[weak self] (switchValue:Bool) in
//                    guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
//                    delegate.teachingLanguageSwitchedValue(enabled: switchValue, section: .chamiMaxContineuPlus)
//                }
//                return cell
//            }
        case .finalyContinueSection:
            let cell = ButtonTableViewCell.dequeueCell(tableView: tableView)
            cell.button.setTitle("Save", for: .normal)
            cell.buttonClickedBlock = {[weak self] (cell:ButtonTableViewCell) in
                guard let strongSelf = self, let delegate = strongSelf.viewDelegate else { return }
                delegate.saveAction()
            }
            return cell
        default: break
        }
        let separator = SeparatorTableViewCell.dequeueCell(tableView: tableView)
        return separator
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        guard let sectionType = EditLanguagesSectionType(rawValue: indexPath.section) else {
            return 0
        }
        switch sectionType {
        case .speak:
            return 42
        case .speakFooter:
            if indexPath.row == 0 {
                return EditLanguagesTableTitleCell.cellHeight()
            }
        case .teachingHeader:
            if indexPath.row == 0 {
                return EditLanguagesTableTitleCell.cellHeight()
            } else if indexPath.row == 1 {
                return DescriptionTableViewCell.cellHeight(tableView: tableView, text: standardLangDesc)
            }
        case .teaching:
            return EditLanguagesSetupTableViewCell.cellHeight()
        case .teachingFooter:
            if indexPath.row == 0 {
                return EditLanguagesTableTitleCell.cellHeight()
            }
        case .chamiPlusHeader:
            if indexPath.row == 0 {
                return EditLanguagesTableTitleCell.cellHeight()
            } else if indexPath.row == 1 {
                return DescriptionTableViewCell.cellHeight(tableView: tableView, text: chamiPlusLangDesc)
            } else if indexPath.row == 2 {
                return EditLanguagesTableTitleCell.cellHeight()
            }
        case .chamiPlus:
            return EditLanguagesSetupTableViewCell.cellHeight()
        case .chamiMaxHeader:
            if indexPath.row == 0 {
                return EditLanguagesTableTitleCell.cellHeight()
            } else if indexPath.row == 1 {
                return DescriptionTableViewCell.cellHeight(tableView: tableView, text: chamiMaxLangDesc)
            }
        case .chamiMax:
            return EditLanguagesSetupTableViewCell.cellHeight()
        case .chamiMaxFooter:
            if indexPath.row == 0 {
                return ButtonTableViewCell.cellHeight()
            } else if indexPath.row == 1 {
                return DescriptionTableViewCell.cellHeight(tableView: tableView, text: chamiMaxFooterLangDesc)
            } else if indexPath.row == 2 {
                return EditLanguagesTableTitleCell.cellHeight()
            }
        case .finalyContinueSection:
            return ButtonTableViewCell.cellHeight()
        default: break
        }
        return SeparatorTableViewCell.separatorHeight()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        tableView.deselectRow(at: indexPath, animated: true)
        guard let sectionType = EditLanguagesSectionType(rawValue: indexPath.section), let delegate = self.viewDelegate else {
            return
        }
        
        switch sectionType {
        case .speakFooter:
            if indexPath.row == 0 {
                delegate.addTeachingLanguageAction()
            }
//        case .teachingFooter:
//            delegate.addTeachingLanguageAction()
        default: break
        }
//
//        self.router?.navigateToSomeScreen(from: self, withBackgroundColor: UIColor.gray)
    }
}
