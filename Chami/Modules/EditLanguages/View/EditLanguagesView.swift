//
//  EditLanguagesView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

//protocol EditLanguagesViewDelegate: NSObjectProtocol {
//    
//    func viewSomeAction(view: EditLanguagesViewProtocol)
//}

protocol EditLanguagesViewProtocol: NSObjectProtocol {
    
    //weak var delegate: EditLanguagesViewDelegate? { get set }
    var tableView: UITableView! { get }
    func setupTableView()
}

class EditLanguagesView: UIView, EditLanguagesViewProtocol{

    class func create() -> EditLanguagesView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? EditLanguagesView else {
            fatalError("Nib \(viewNibName) does not contain EditLanguages View as first object")
        }
        
        return view
    }
    
    // MARK: - EditLanguagesView interface methods

    //weak var delegate: EditLanguagesViewDelegate?
    @IBOutlet weak var tableView: UITableView!

    // add view private properties/outlets/methods here
    
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

//        let headerView = EditLanguagesTableHeaderView.loadFromXib(owner: nil) as! EditLanguagesTableHeaderView
//        tableView.tableHeaderView = headerView
        
//        let footerView = SetupProfileFooterView.loadFromXib(owner: nil) as! SetupProfileFooterView
//        tableView.tableFooterView = footerView
//        footerView.delegate = self    
    }
    func setupTableView() {
        let headerView = EditLanguagesTableHeaderView.loadFromXib(owner: nil) as! EditLanguagesTableHeaderView
        tableView.tableHeaderView = headerView
    }
}
