//
//  EditLanguagesBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class EditLanguagesBuilder: NSObject {

    class func viewController() -> EditLanguagesViewController {
        let view: EditLanguagesViewProtocol = EditLanguagesView.create()
        let model: EditLanguagesModelProtocol = EditLanguagesModel()
        let dataSource = EditLanguagesDataSource(withModel: model)
        let router = EditLanguagesRouter()
        
        let viewController = EditLanguagesViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
