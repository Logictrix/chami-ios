//
//  EditLanguagesRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class EditLanguagesRouter: NSObject {
    
//    // Navigation example method
//    func navigateToSomeScreen(from vc: UIViewController, withBackgroundColor backgroundColor: UIColor) {
//        // Create new screen. Here you should use another Builder to create it.
//        let someScreenVC = UIViewController()
//        // Set passed parameters
//        someScreenVC.view.backgroundColor = backgroundColor
//        
//        if UI_USER_INTERFACE_IDIOM() == .pad {
//            someScreenVC.modalPresentationStyle = .pageSheet
//            someScreenVC.modalTransitionStyle = .crossDissolve
//            
//            vc.navigationController?.present(someScreenVC, animated: true, completion: nil)
//        } else {
//            vc.navigationController?.pushViewController(someScreenVC, animated: true)
//        }
//    }
    
    func navigateToLoginSetupCompleteScreen(from vc: UIViewController) {
        let nextVC = SetupCompleteBuilder.viewController()
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
}
