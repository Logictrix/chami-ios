//
//  EditLanguagesModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol EditLanguagesModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: EditLanguagesModelProtocol)
}

protocol EditLanguagesModelProtocol: NSObjectProtocol {
    
    weak var delegate: EditLanguagesModelDelegate? { get set }
    var teachingLanguages: [Language] { get set }
    
    var chamiMaxApproved:Bool { get }
    var chamiMaxEnabled:Bool {set get}
    
    var documentImages: [UIImage] { get set }
    
    
    var chamiLevelMaximumValue: Float { get }
    var chamiPlusMinimumPrice: Float { get }
    var chamiPlusMaximumPrice: Float { get }
    var chamiMaxMinimumPrice: Float { get }
    var chamiMaxMaximumPrice: Float { get }
    
    //
    func validateFields() -> String?
    func setup(completion: @escaping (Error?) -> Void)
    
    func updateProfileLangs(completion: @escaping (_ error:Error?) -> Void)
}

class EditLanguagesModel: NSObject, EditLanguagesModelProtocol {
    fileprivate var account = AccountManager.shared.currentUser!
    
    internal var chamiLevelMaximumValue: Float {
        get {
            return 1.0
        }
    }
    internal var chamiPlusMinimumPrice: Float {
        get {
            return 1.0
        }
    }
    internal var chamiPlusMaximumPrice: Float {
        get {
            return 25.0
        }
    }
    internal var chamiMaxMinimumPrice: Float {
        get {
            return 1.0
        }
    }
    internal var chamiMaxMaximumPrice: Float {
        get {
            return 50.0
        }
    }

    internal var documentImages: [UIImage]

    var chamiMaxApproved: Bool {
        get {
            return self.account.isCertificated
        }
    }
    var chamiMaxEnabled: Bool {
        didSet {
            self.delegate?.modelDidChanged(model: self)
        }
    }

    internal var teachingLanguages: [Language] {
        didSet {
            self.delegate?.modelDidChanged(model: self)
        }
    }
    weak var delegate: EditLanguagesModelDelegate?

    func updateProfileLangs(completion: @escaping (_ error:Error?) -> Void) {
        guard let account = AccountManager.shared.currentUser else {
            return
        }
        self.profileService.getProfileLanguages(account: account, success: {
            completion(nil)
        }, failure: { error in
            completion(error)
        })
        
    }
    
    override init() {
        self.teachingLanguages = (AccountManager.shared.currentUser?.teachingLanguages)!
        self.chamiMaxEnabled = (self.account.chamiType == .chamiMax)
        self.documentImages = []
        super.init()
    }
    

    

    func validateFields() -> String? {
        if self.teachingLanguages.count == 0 {
            return NSLocalizedString("Please add a language at first", comment: "")
        }
        return nil
    }
    
    
    
    fileprivate let profileService = ProfileService()
    internal func setup(completion: @escaping (Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: Error?
            let downloadGroup = DispatchGroup()
            
            // send certificates
            var certificateURLs: [String] = []
            for image in self.documentImages {
                guard let data = image.pngData() else { return }
                downloadGroup.enter()
                DataStorageManager.shared.upload(data: data) { (error, name) in
                    if (error != nil) {
                        resultError = error
                    } else {
                        certificateURLs.append(name!)
                    }
                    
                    downloadGroup.leave()
                }
//                downloadGroup.enter()
            }
            downloadGroup.wait()
            // send info to the server
            downloadGroup.enter()
            self.account.certificates = certificateURLs
            self.account.teachingLanguages = self.teachingLanguages
            self.account.chamiType = self.chamiMaxEnabled ? .chamiMax : .chamiPlus
            self.profileService.setProfileLanguages(account: self.account, success: {
                downloadGroup.leave()
            }, failure: { (error) in
                resultError = error
                downloadGroup.leave()
            })

            downloadGroup.wait()
            
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
    }
}
