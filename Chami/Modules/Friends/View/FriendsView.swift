//
//  FriendsView.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol FriendsViewDelegate: NSObjectProtocol {
    
}

protocol FriendsViewProtocol: NSObjectProtocol {
    
    weak var delegate: FriendsViewDelegate? { get set }
    var logoImageView: UIImageView! { get }
    var titleLabel: UILabel! { get }
}

class FriendsView: UIView, FriendsViewProtocol{

    class func create() -> FriendsView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? FriendsView else {
            fatalError("Nib \(viewNibName) does not contain Friends View as first object")
        }
        
        return view
    }
    
    // MARK: - FriendsView interface methods
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!

    weak var delegate: FriendsViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        
        self.textView.allowsEditingTextAttributes = true;
        
        var multipleAttributes = [NSAttributedString.Key : Any]()
        multipleAttributes[NSAttributedString.Key.foregroundColor] = UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1)
        multipleAttributes[NSAttributedString.Key.font] = UIFont(name: "SF UI Display", size: 16.0)
        multipleAttributes[NSAttributedString.Key.paragraphStyle] = paragraphStyle
        
        let attrString = NSMutableAttributedString(string: self.textView.text, attributes: multipleAttributes )
        self.textView.attributedText = attrString    }
    
    // MARK: - IBActions
    
}
