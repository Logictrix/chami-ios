//
//  FriendsBuilder.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class FriendsBuilder: NSObject {

    class func viewController() -> FriendsViewController {

        let view: FriendsViewProtocol = FriendsView.create()
        let model: FriendsModelProtocol = FriendsModel()
        let router = FriendsRouter()
        
        let viewController = FriendsViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()
        
        return viewController
    }
}
