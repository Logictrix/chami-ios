//
//  FriendsViewController.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias FriendsViewControllerType = BaseViewController<FriendsModelProtocol, FriendsViewProtocol, FriendsRouter>

class FriendsViewController: FriendsViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: FriendsViewProtocol!, model: FriendsModelProtocol!, router: FriendsRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView.delegate = self
        model.delegate = self
        
        self.navigationItem.title = NSLocalizedString("Friends", comment: "Friends")
        self.addSideMenuLeftButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
    }
    
    private func updateView() {
        
        if self.model.accountRole == .student {
            self.customView.logoImageView.image = UIImage(named: "mes_lock_scr_btn_lock")
            self.customView.titleLabel.textColor = ColorHelper.regularColor()
            
        } else if self.model.accountRole == .tutor {
            self.customView.logoImageView.image = UIImage(named: "main_scr__icon_lock")
            self.customView.titleLabel.textColor = ColorHelper.regularColor()
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
    }
}

// MARK: - FriendsViewDelegate

extension FriendsViewController: FriendsViewDelegate {

}

// MARK: - FriendsModelDelegate

extension FriendsViewController: FriendsModelDelegate {
}
