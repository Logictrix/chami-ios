//
//  FriendsModel.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol FriendsModelDelegate: NSObjectProtocol {
    
}

protocol FriendsModelProtocol: NSObjectProtocol {
    
    weak var delegate: FriendsModelDelegate? { get set }
    var accountRole: AccountRole { get set }
    
}

class FriendsModel: NSObject, FriendsModelProtocol {
    
    // MARK: - FriendsModel methods

    weak var delegate: FriendsModelDelegate?
    var accountRole: AccountRole = AccountManager.shared.currentRole
    
    /** Implement FriendsModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
