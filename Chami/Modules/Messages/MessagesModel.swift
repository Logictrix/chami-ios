//
//  MessagesModel.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol MessagesModelDelegate: NSObjectProtocol {
    
}

protocol MessagesModelProtocol: NSObjectProtocol {
    
    weak var delegate: MessagesModelDelegate? { get set }
    var accountRole: AccountRole { get set }
    
}

class MessagesModel: NSObject, MessagesModelProtocol {
    
    // MARK: - MessagesModel methods

    weak var delegate: MessagesModelDelegate?
    var accountRole: AccountRole = AccountManager.shared.currentRole
    
    /** Implement MessagesModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
