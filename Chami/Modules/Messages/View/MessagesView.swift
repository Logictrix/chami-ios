//
//  MessagesView.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol MessagesViewDelegate: NSObjectProtocol {
    
}

protocol MessagesViewProtocol: NSObjectProtocol {
    
    weak var delegate: MessagesViewDelegate? { get set }
    var logoImageView: UIImageView! { get }
    var titleLabel: UILabel! { get }
}

class MessagesView: UIView, MessagesViewProtocol{

    class func create() -> MessagesView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? MessagesView else {
            fatalError("Nib \(viewNibName) does not contain Messages View as first object")
        }
        
        return view
    }
    
    // MARK: - MessagesView interface methods
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var textView: UITextView!
    
    weak var delegate: MessagesViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        
        self.textView.allowsEditingTextAttributes = true;
        
        var multipleAttributes = [NSAttributedString.Key : Any]()
        multipleAttributes[NSAttributedString.Key.foregroundColor] = UIColor(red: 140/255, green: 140/255, blue: 140/255, alpha: 1)
        multipleAttributes[NSAttributedString.Key.font] = UIFont(name: "SF UI Display", size: 16.0)
        multipleAttributes[NSAttributedString.Key.paragraphStyle] = paragraphStyle
        
        let attrString = NSMutableAttributedString(string: self.textView.text, attributes: multipleAttributes )
        self.textView.attributedText = attrString
        

    }
    
    // MARK: - IBActions
    
}
