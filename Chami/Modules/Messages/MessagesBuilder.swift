//
//  MessagesBuilder.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class MessagesBuilder: NSObject {

    class func viewController() -> MessagesViewController {

        let view: MessagesViewProtocol = MessagesView.create()
        let model: MessagesModelProtocol = MessagesModel()
        let router = MessagesRouter()
        
        let viewController = MessagesViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()
        
        return viewController
    }
}
