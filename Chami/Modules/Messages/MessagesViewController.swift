//
//  MessagesViewController.swift
//  Chami
//
//  Created by Dima on 2/21/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias MessagesViewControllerType = BaseViewController<MessagesModelProtocol, MessagesViewProtocol, MessagesRouter>

class MessagesViewController: MessagesViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: MessagesViewProtocol!, model: MessagesModelProtocol!, router: MessagesRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView.delegate = self
        model.delegate = self
        
        self.navigationItem.title = NSLocalizedString("Messages", comment: "Messages")
        self.addSideMenuLeftButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
    }
    
    private func updateView() {
        
        if self.model.accountRole == .student {
            self.customView.titleLabel.textColor = ColorHelper.regularColor()
            let img = UIImage(named: "mes_lock_scr_btn_lock")

            self.customView.logoImageView.image = img
        } else if self.model.accountRole == .tutor {
            self.customView.titleLabel.textColor = ColorHelper.regularColor()
            let img = UIImage(named: "main_scr__icon_lock")
            self.customView.logoImageView.image = img

            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
    }
}

// MARK: - MessagesViewDelegate

extension MessagesViewController: MessagesViewDelegate {

}

// MARK: - MessagesModelDelegate

extension MessagesViewController: MessagesModelDelegate {
}
