//
//  ActiveSessionViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias ActiveSessionViewControllerType = BaseViewController<ActiveSessionModelProtocol, ActiveSessionViewProtocol, ActiveSessionRouter>

class ActiveSessionViewController: ActiveSessionViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: ActiveSessionViewProtocol!, model: ActiveSessionModelProtocol!, router: ActiveSessionRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Active Session", comment: "Active Session - Screen title")
        self.navigationItem.hidesBackButton = true
        
        customView.delegate = self
        model.delegate = self
        SessionManager.shared.multicastDelegate.addDelegate(self)

        self.canHandleMeetupRequests = false
    }
    deinit {
        SessionManager.shared.multicastDelegate.removeDelegate(self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
    }

    // MARK: - Public methods
    func setSession(session: Session) {
        session.startDateTime = Date()
        self.model.session = session
    }
    func setMeetUpSession(meetUpSession: MeetUpSession) {
        self.model.currentMeetUpSession = meetUpSession
    }

    
    // MARK: - Private methods
    private func updateView() {
        guard let session = self.model.session else {
            fatalError("\(String(describing: type(of: self))) require session to be defined")
            
        }
        
        Theme.current.apply(filledButton: self.customView.endSessionButton)
        self.customView.bottomColoredView.backgroundColor = Theme.current.regularColor().withAlphaComponent(0.1)
        self.customView.bottomColored2View.backgroundColor = Theme.current.regularColor().withAlphaComponent(0.3)
        self.customView.titleLabel.textColor = Theme.current.regularColor()
        self.customView.sessionTimeStaticLabel.textColor = Theme.current.regularColor()
        self.customView.sessionTimeLabel.textColor = Theme.current.oppositeColor()
        
        self.customView.titleLabel.text = self.model.currentMeetUpSession.partner_full_name
        self.customView.timeStartedLabel.text = FormatterHelper.exampleDateFormatter.string(from: session.startDateTime)
        
        let url = URL(string: self.model.currentMeetUpSession.partner_image_url!)
        self.customView.oppositeLogoImageView.kf.setImage(with: url)
        
        
        if self.model.accountRole == .student {
            
            self.customView.sessionTimerImageView.image = UIImage(named: "active_session_scr_icon_clock")
            self.customView.oppositeNameStaticLabel.text = NSLocalizedString("Tutor name:", comment: "Tutor name:")
            self.customView.sessionCostStaticLabel.text = NSLocalizedString("Current session cost:", comment: "Current session cost:")
            
        } else if self.model.accountRole == .tutor {
            
            self.customView.sessionTimerImageView.image = UIImage(named: "active_session_scr_icon_clock_blu")
            self.customView.oppositeNameStaticLabel.text = NSLocalizedString("Student name:", comment: "Student name:")
            self.customView.sessionCostStaticLabel.text = NSLocalizedString("Current session earnings:", comment: "Current session earnings:")

        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
        if self.model.currentMeetUpSession.isChamiXFlow == true {
            self.customView.bottomColored2View.isHidden = true
        } else {
            self.customView.bottomColored2View.isHidden = false
        }
    }

    private func updateFromTimer(withTimeSeconds: Int) {
        DispatchQueue.main.async {
            self.customView.sessionTimeLabel.text = DateConverter.stringHhMmSsFromSeconds(seconds: withTimeSeconds)
            self.customView.sessionTimeDuplicateLabel.text = DateConverter.stringHhMmFromSeconds(seconds: withTimeSeconds)
        }
    }
    
    // MARK: - SessionManagerDelegate
    func didReceivedMeetUpSessionTick(session: MeetUpSession) {
        guard self.model.currentMeetUpSession.status != .finished else {
            return
        }
        guard self.model.currentMeetUpSession.session_id == session.session_id else {
            return
        }

        
        self.model.currentMeetUpSession.duration = session.duration
        self.customView.sessionCostLabel.text = String.init(format: "%0.2f", self.model.currentMeetUpSession.session_cost.floatValue)
        self.updateFromTimer(withTimeSeconds: session.duration)
    }
    
    func didReceivedMeetUpEndSession(session: MeetUpSession) {
        if self.isVisible == true {
            self.model.currentMeetUpSession.status = .finished
            self.router?.navigateToSesionCompleteScreen(from: self, withSession: self.model.session, withMeetUpSession: self.model.currentMeetUpSession)
        }

    }

}

// MARK: - ActiveSessionViewDelegate

extension ActiveSessionViewController: ActiveSessionViewDelegate {

    func viewSendActionEndSession(view: ActiveSessionViewProtocol) {
        self.setProgressVisible(visible: true)
        self.model.finishSession { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let error = error {
                if (error as NSError).code == 403 {
                    AlertManager.showError(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf)
                } else {
                    AlertManager.showError(withMessage: "This session already finished", onController: strongSelf, closeHandler: {[weak self] in
                        guard let strongSelf = self else { return }
                        strongSelf.setProgressVisible(visible: false)

                        strongSelf.didReceivedMeetUpEndSession(session: strongSelf.model.currentMeetUpSession)
                    })
                    
                }

            } else {
//                strongSelf.router?.navigateToSesionCompleteScreen(from: strongSelf, withSession: strongSelf.model.session, withMeetUpSession: strongSelf.model.currentMeetUpSession)

            }

        }
    }
}

// MARK: - ActiveSessionModelDelegate

extension ActiveSessionViewController: ActiveSessionModelDelegate {
}
