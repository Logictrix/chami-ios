//
//  ActiveSessionView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ActiveSessionViewDelegate: NSObjectProtocol {
    
    func viewSendActionEndSession(view: ActiveSessionViewProtocol)

}

protocol ActiveSessionViewProtocol: NSObjectProtocol {
    
    weak var delegate: ActiveSessionViewDelegate? { get set }
    
    weak var oppositeLogoImageView: UIImageView! { get }
    weak var titleLabel: UILabel! { get }
    
    weak var sessionTimeStaticLabel: UILabel! { get }
    weak var sessionTimeLabel: UILabel! { get }
    weak var sessionTimerImageView: UIImageView! { get }
    weak var sessionTimeDuplicateLabel: UILabel! { get }
    
    weak var oppositeNameStaticLabel: UILabel! { get }
    weak var oppositeNameLabel: UILabel! { get }
    weak var timeStartedLabel: UILabel! { get }
    weak var sessionCostLabel: UILabel! { get }
    weak var sessionCostStaticLabel: UILabel! { get }
    
    weak var endSessionButton: UIButton! { get }
    weak var bottomColoredView: UIView! { get }
    weak var bottomColored2View: UIView! { get }
}

class ActiveSessionView: UIView, ActiveSessionViewProtocol{

    @IBOutlet weak var oppositeLogoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var sessionTimeStaticLabel: UILabel!
    @IBOutlet weak var sessionTimeLabel: UILabel!
    @IBOutlet weak var sessionTimerImageView: UIImageView!
    @IBOutlet weak var sessionTimeDuplicateLabel: UILabel!
    
    @IBOutlet weak var oppositeNameStaticLabel: UILabel!
    @IBOutlet weak var oppositeNameLabel: UILabel!
    @IBOutlet weak var timeStartedLabel: UILabel!
    @IBOutlet weak var sessionCostLabel: UILabel!
    @IBOutlet weak var sessionCostStaticLabel: UILabel!
    
    @IBOutlet weak var endSessionButton: UIButton!
    @IBOutlet weak var bottomColoredView: UIView!
    @IBOutlet weak var bottomColored2View: UIView!

    
    class func create() -> ActiveSessionView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? ActiveSessionView else {
            fatalError("Nib \(viewNibName) does not contain ActiveSession View as first object")
        }
        
        return view
    }
    
    // MARK: - ActiveSessionView interface methods

    weak var delegate: ActiveSessionViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.oppositeLogoImageView.layer.cornerRadius = self.oppositeLogoImageView.bounds.size.height / 2.0
        self.oppositeLogoImageView.layer.masksToBounds = true
        self.oppositeLogoImageView.contentMode = .scaleAspectFill

    }
    
    // MARK: - IBActions
    @IBAction func actionEndSession(_ sender: Any) {
        self.delegate?.viewSendActionEndSession(view: self)
        
    }
    
}
