//
//  ActiveSessionModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ActiveSessionModelDelegate: NSObjectProtocol {
    
}

protocol ActiveSessionModelProtocol: NSObjectProtocol {
    
    weak var delegate: ActiveSessionModelDelegate? { get set }
    var session: Session! { get set }
    var accountRole: AccountRole { get set }
    var currentMeetUpSession: MeetUpSession! { get set }
    func finishSession(completion: @escaping (_ error:Error?) -> Void)

}

class ActiveSessionModel: NSObject, ActiveSessionModelProtocol {
    
    // MARK: - ActiveSessionModel methods
    var session: Session!
    var accountRole: AccountRole = AccountManager.shared.currentRole
    var currentMeetUpSession: MeetUpSession!

    weak var delegate: ActiveSessionModelDelegate?
    let service: SessionServiceProtocol = SessionService()

    /** Implement ActiveSessionModel methods here */
    func finishSession(completion: @escaping (_ error:Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: Error?
            let downloadGroup = DispatchGroup()
            
            // check for correct CODE
            downloadGroup.enter()
            // check for valid code and leave in callback
            // resultError
            self.service.finishSession(session: self.session, duration: Double(self.currentMeetUpSession.duration), success: { (session) in
                downloadGroup.leave()
            }, failure: { (error) in
                resultError = error
                downloadGroup.leave()
            })
            
            downloadGroup.wait()
            
            // send start command
            if resultError == nil {
                downloadGroup.enter()
                SessionManager.shared.executeSessionEnd(session: self.currentMeetUpSession, completion: { (error) in
                    resultError = error
                    downloadGroup.leave()
                })
            }
            downloadGroup.wait()
            
            
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
    }

    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
