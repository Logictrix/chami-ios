//
//  ActiveSessionBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ActiveSessionBuilder: NSObject {

    class func viewController() -> ActiveSessionViewController {

        let view: ActiveSessionViewProtocol = ActiveSessionView.create()
        let model: ActiveSessionModelProtocol = ActiveSessionModel()
        let router = ActiveSessionRouter()
        
        let viewController = ActiveSessionViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
