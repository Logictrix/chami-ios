//
//  ActiveSessionRouter.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ActiveSessionRouter: NSObject {
    
    func navigateToSesionCompleteScreen(from vc: UIViewController, withSession session: Session, withMeetUpSession meetupSession: MeetUpSession) {
        
        let nextVC = RateSessionBuilder.viewController()
        nextVC.setMeetUpSession(meetUpSession: meetupSession)
        nextVC.setSession(session: session)
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
}
