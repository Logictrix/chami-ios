//
//  RequestChamiViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit
import PKHUD
import GoogleMaps
import Stripe

protocol RequestChamiViewControllerDelegate: NSObjectProtocol {
    
    func viewControllerDidFinishWithSelectedLocation(viewController: UIViewController , location: CLLocation?)
}

typealias RequestChamiViewControllerType = BaseViewController<RequestChamiModelProtocol, RequestChamiViewProtocol, RequestChamiRouter>


class RequestChamiViewController: RequestChamiViewControllerType, MKMapViewDelegate {
    
    // MARK: Initializers
    weak var delegate: RequestChamiViewControllerDelegate?

    required init(withView view: RequestChamiViewProtocol!, model: RequestChamiModelProtocol!, router: RequestChamiRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NSLocalizedString("Request a chami", comment: "Request a chami")

        self.canHandleMeetupRequests = false
        
        customView.delegate = self
        model.delegate = self
        self.customView.mapView.delegate = self

        setupLocationManager()
        self.model.selectedCoordinates = LocationManager.shared.coordinate
        centerMapOnLocation(coordinate: LocationManager.shared.coordinate)
        
//        let annotation = MKPointAnnotation()
//        annotation.coordinate = LocationManager.shared.coordinate
//        self.customView.mapView.addAnnotation(annotation)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customView.showCommentInput = (self.model.isChamiX == false)
    }
    
    // MARK: - Public methods
    func setTutor(tutor: User) {
        self.model.currentTutor = tutor
    }
    func setIsChamiX(isChamiX: Bool) {
        self.model.isChamiX = isChamiX
    }

    func updatePlaceCoordinate(to coordinates: CLLocationCoordinate2D) {
        self.model.addressString = ""
        self.model.selectedCoordinates = coordinates
    }
    
    // MARK: - Private methods
    fileprivate func meetUpCurrentTutor() {
        
//        self.router?.navigateToCurrentRequestScreen(from: self, withRequestId: "0", withTutor: self.model.currentTutor)
//        return;

        //self.model.locationImage = self.view.makeScreenshotFromRect(rect: self.customView.mapView.frame)
        self.model.comment = self.customView.commentTextView.text
        self.model.exactLocation = self.customView.exactLocationTextView.text
        self.setProgressVisible(visible: true)
        self.model.meetUpCurrentTutor(completion: { [weak self] (error) in
            guard let strongSelf = self else { return }
            if error == nil {
                strongSelf.setProgressVisible(visible: false)
                
                strongSelf.router?.navigateToCurrentRequestScreen(from: strongSelf, session:strongSelf.model.meetUpSession!, withTutor: strongSelf.model.currentTutor, isChamiX: strongSelf.model.isChamiX)
            } else {
                strongSelf.setProgressVisible(visible: false)
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error!) as String, onController: strongSelf, closeHandler: nil)
            }
        })
    }

    
    // MARK: - MKMapViewDelegate
    var firstLocation = true
    
    func setupLocationManager() {
        self.customView.mapView.showsUserLocation = true
        self.customView.mapView.mapType = MKMapType.standard
        self.customView.mapView.isZoomEnabled = true
        self.customView.mapView.isScrollEnabled = true
    }
    
    func centerMapOnLocation(coordinate: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegion(center: coordinate,
                                                  latitudinalMeters: LocationManager.shared.regionRadius * 2.0, longitudinalMeters: LocationManager.shared.regionRadius * 2.0)
        self.customView.mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
//        let centerCoordinate = mapView.centerCoordinate
//        self.model.selectedCoordinates = CLLocation(latitude: centerCoordinate.latitude, longitude: centerCoordinate.longitude)
        self.model.visibleMapRect = mapView.visibleMapRect
        self.updatePlaceCoordinate(to: (mapView.centerCoordinate))

    }
    
    func mapView(_ mapView: MKMapView, didUpdate userLocation: MKUserLocation) {
//        if firstLocation && userLocation.location != nil {
//            firstLocation = false
//            self.model.selectedCoordinates = LocationManager.shared.coordinate
//        }
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        for view1 in views {
            if view1.annotation is MKUserLocation {
                //
                view1.isEnabled = false
            }
        }
    }
    
//    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
//        if annotation is MKUserLocation {
//            return nil
//        }
//        var pin : MKAnnotationView! = nil
//        let ident = "meetingLocationViewId"
//        pin = mapView.dequeueReusableAnnotationView(withIdentifier: ident)
//        if pin == nil {
//            pin = MKAnnotationView(annotation:annotation, reuseIdentifier:ident)
//            pin!.canShowCallout = false
//            var img = UIImage(named: "find_tut_scr_icon_pointer_map")
//            //TODO: change image depending on chami type
//            let type = self.model.selectedChamiType!
//            switch type {
//            case .chamiX:
//                img = UIImage(named: "find_tut_scr_icon_pointer_map")
//                break
//            case .chamiPlus:
//                img = UIImage(named: "find_tut_scr_icon_pointer_map")
//                break
//            case .chamiMax:
//                img = UIImage(named: "find_tut_scr_icon_pointer_map")
//                break
//            }
//            pin!.image = img
//
//        }
//        pin.annotation = annotation
//        pin.isDraggable = false
//        return pin
//    }
    
//    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
//        switch newState {
//        case .starting:
//            view.dragState = .dragging
//        case .ending, .canceling:
//            view.dragState = .none
//            self.updatePlaceCoordinate(to: (view.annotation?.coordinate)!)
//        default: break
//        }
//    }

}

// MARK: - RequestChamiViewDelegate

extension RequestChamiViewController: RequestChamiViewDelegate {
    
    func view(view: RequestChamiViewProtocol, getAutocompletionsForPattern pattern: String, withCompletion completion: @escaping ((_ results: [GMSAutocompletePrediction]) -> ())) {
        self.model.searchPlaceListWithEnterText(withText: pattern, success: { (results) in
            
            completion(results)

        }) { (error) in
            completion([])
        }

    }
    
    func view(view: RequestChamiViewProtocol, didSearchForObject searchObject: AutoCompleteCustomObject) {
        self.model.updateSearchString(withObject: searchObject)
    
    }
    
    func viewDidSearch(view: RequestChamiViewProtocol) {
        self.model.searchPlaceIfNeeded { [weak self] (strAddress) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            strongSelf.centerMapOnLocation(coordinate: strongSelf.model.selectedCoordinates!)
//            let annotation = MKPointAnnotation()
//            strongSelf.customView.mapView.removeAnnotations(strongSelf.customView.mapView.annotations)
//            annotation.coordinate = (strongSelf.model.selectedCoordinates)!
//            strongSelf.customView.mapView.addAnnotation(annotation)

        }

    }

    func viewSendActionConfirmAndContinue(view: RequestChamiViewProtocol) {
        guard let user = AccountManager.shared.currentUser else {
            print("current user not found")
            return
        }
        
        if self.model.isChamiX == true || user.stripeKeyStudent != nil {
            self.meetUpCurrentTutor()
        } else {
            StripeApiManager.shared.showPaymentViewControllerIfNeeded(fromViewController: self, completion: {[weak self] (saved, error) in
                guard let strongSelf = self else { return }
                if let error = error {
                    print("================\(error.localizedDescription)")
                } else {
                    strongSelf.viewSendActionConfirmAndContinue(view: strongSelf.customView)
                }
                
            })
//            if self.shouldShowPaymentVC() {
//                self.showPaymentVC()
//            }
        }
    }

}

// MARK: - RequestChamiModelDelegate

extension RequestChamiViewController: RequestChamiModelDelegate {
    
    func modelDidUpdateAddress(model: RequestChamiModelProtocol) {
        self.customView.locationLabel.text = self.model.addressString
    }

}
