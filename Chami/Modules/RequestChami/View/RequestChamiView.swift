//
//  RequestChamiView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit
import MLPAutoCompleteTextField
import GoogleMaps

protocol RequestChamiViewDelegate: NSObjectProtocol {
    
    func view(view: RequestChamiViewProtocol, getAutocompletionsForPattern pattern: String, withCompletion completion: @escaping ((_ results: [GMSAutocompletePrediction]) -> ()))
    
    func view(view: RequestChamiViewProtocol, didSearchForObject searchObject: AutoCompleteCustomObject)
    func viewDidSearch(view: RequestChamiViewProtocol)

    func viewSendActionConfirmAndContinue(view: RequestChamiViewProtocol)

}

protocol RequestChamiViewProtocol: NSObjectProtocol {
    
    weak var delegate: RequestChamiViewDelegate? { get set }
    weak var mapView: MKMapView! { get }
    weak var locationLabel: UILabel! { get }
    weak var commentTextView: UITextView! { get }
    weak var exactLocationTextView: UITextView! { get }
    
    var showCommentInput: Bool { set get }
}

class RequestChamiView: UIView, RequestChamiViewProtocol, MLPAutoCompleteTextFieldDataSource, MLPAutoCompleteTextFieldDelegate, UITextFieldDelegate, UITextViewDelegate{
    
    // MARK: - Search for Location view
    @IBOutlet weak var searchForLocationView: UIView!
    
    // MARK: - Search for Location view
    @IBOutlet weak var confirmLocationView: UIView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var confirmLocationButton: UIButton!
    
    
    @IBOutlet weak var commentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentPromptLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!

    @IBOutlet weak var exactLocationPromptLabel: UILabel!
    @IBOutlet weak var exactLocationTextView: UITextView!

    @IBOutlet weak var searchTextField: MLPAutoCompleteTextField!
    
    @IBOutlet weak var mapView: MKMapView!

    var commentPlaceholderLabel : UILabel!
    var exactLocationPlaceholderLabel : UILabel!
    
    // MARK: - RequestChamiView interface methods

    weak var delegate: RequestChamiViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        Theme.current.apply(borderedButton: self.confirmLocationButton)
        self.confirmLocationButton.setAttributedTitle(self.compileSaveButtonTitle(text: "Confirm location and continue "), for: UIControl.State.normal)
        
        self.searchTextField.delegate = self
        self.searchTextField.autoCompleteDelegate = self
        self.searchTextField.autoCompleteDataSource = self
        self.searchTextField.autoCompleteTableOriginOffset = CGSize(width: -20.0, height: 5.0)
        self.searchTextField.autoCompleteRowHeight = 30.0
        self.searchTextField.maximumNumberOfAutoCompleteRows = 6
//        MLPAutoCompleteTextField.tintcolor
        self.searchTextField.tintColor = UIColor.black
        
        // comment
        self.commentPlaceholderLabel = UILabel()
        self.setup(promptLabel: self.commentPromptLabel, textView: self.commentTextView, placeholderLabel: self.commentPlaceholderLabel, placeholderText: "Please write here the type of lesson you want")

        // exact location
        self.exactLocationPlaceholderLabel = UILabel()
        self.setup(promptLabel: self.exactLocationPromptLabel, textView: self.exactLocationTextView, placeholderLabel: self.exactLocationPlaceholderLabel, placeholderText: "Additional directions (e.g. 2nd floor window seat), what you are wearing, your mobile number etc")
        
        
#if (arch(i386) || arch(x86_64)) && os(iOS)
        self.commentTextView.text = "simulator comment"
        self.exactLocationTextView.text = "simulator location"
#endif
    }
    
    var showCommentInput: Bool = true {
        didSet {
            self.commentViewHeightConstraint.constant = self.showCommentInput ? 94 : 0
        }
    }
    
    func setup(promptLabel: UILabel, textView: UITextView, placeholderLabel: UILabel, placeholderText: String) {
        promptLabel.textColor = ColorHelper.regularColor()
        promptLabel.font = FontHelper.defaultMediumFont(withSize: 16)
        
        textView.layer.borderColor = UIColor.darkText.cgColor
        textView.layer.borderWidth = 0.5
        textView.delegate = self
        
        placeholderLabel.text = placeholderText
        placeholderLabel.numberOfLines = 0
        placeholderLabel.font = UIFont.italicSystemFont(ofSize: (textView.font?.pointSize)!)
        placeholderLabel.textColor = UIColor.lightGray
        placeholderLabel.isHidden = !textView.text.isEmpty
        textView.addSubview(placeholderLabel)
        
        let cp_origin = CGPoint(x: 5, y: (textView.font?.pointSize)! / 2)
        placeholderLabel.frame = CGRect(x: cp_origin.x,
                                        y: cp_origin.y,
                                        width: textView.frame.size.width - (cp_origin.x * 2),
                                        height: textView.frame.size.height - (cp_origin.y * 2))
        placeholderLabel.sizeToFit()
        
        
    }
    
    func compileSaveButtonTitle(text: String) -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes:[NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): labelFont,
                                                                NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString(text, comment: "Profile info Edit"),
                                          attributes: textPartAttributes)
        
        titleLabelText.append(textPart)
        
        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        textPartAttributes[NSAttributedString.Key.foregroundColor] = ColorHelper.yellowAppColor()
        titleLabelText.append(NSAttributedString(string: FontImage.chevronRight, attributes: textPartAttributes))
        return titleLabelText
    }
    
    class func create() -> RequestChamiView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? RequestChamiView else {
            fatalError("Nib \(viewNibName) does not contain RequestChamiView as first object")
        }
        
        return view
    }

    // MARK: UITextViewDelegate
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == commentTextView {
            self.commentPlaceholderLabel.isHidden = !textView.text.isEmpty
        } else if textView == exactLocationTextView {
            self.exactLocationPlaceholderLabel.isHidden = !textView.text.isEmpty
        }
    }
    
    // MARK: - UITextFieldDelegate
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var txtAfterUpdate:NSString = textField.text! as NSString
        txtAfterUpdate = txtAfterUpdate.replacingCharacters(in: range, with: string) as NSString
        let autoCompleteObj = AutoCompleteCustomObject(name: txtAfterUpdate as String)

        self.delegate?.view(view: self, didSearchForObject: autoCompleteObj)
        
        return true
    }
    
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        let autoCompleteObj = AutoCompleteCustomObject(name: "")

        self.delegate?.view(view: self, didSearchForObject: autoCompleteObj)
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if let text = textField.text, text.characters.count > 0 {
            self.delegate?.viewDidSearch(view: self)
        }
        return true
    }

    // MARK: - MLPAutoCompleteTextFieldDataSource
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, possibleCompletionsFor string: String!, completionHandler handler: (([Any]?) -> Swift.Void)!) {
        if let string = string, string.characters.count == 0 {
            if handler != nil {
                handler([])
            }
        }
        
        self.delegate?.view(view: self, getAutocompletionsForPattern: string, withCompletion: { (results) in
            var complitions:[AutoCompleteCustomObject] = []
            for obj in results {
                var name = obj.attributedPrimaryText.string
                if let namePart = obj.attributedSecondaryText?.string {
                    name.append(" (\(namePart))")
                }
                let autoCompleteObj = AutoCompleteCustomObject(name: name)
                autoCompleteObj.placeId = obj.placeID
                
                complitions.append(autoCompleteObj)
                if handler != nil {
                    handler(complitions)
                }

            }
        })
    }
    
    
    // MARK: - MLPAutoCompleteTextFieldDelegate
    func autoCompleteTextField(_ textField: MLPAutoCompleteTextField!, didSelectAutoComplete selectedString: String!, withAutoComplete selectedObject: MLPAutoCompletionObject!, forRowAt indexPath: IndexPath!) {
        self.delegate?.view(view: self, didSearchForObject: (selectedObject as! AutoCompleteCustomObject))
        self.delegate?.viewDidSearch(view: self)
    }


    // MARK: - IBActions
    @IBAction func actionConfirmAndContinue(_ sender: Any) {
        self.delegate?.viewSendActionConfirmAndContinue(view: self)
    }
    
}
