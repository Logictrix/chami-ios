//
//  RequestChamiModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import GoogleMaps
import MapKit

protocol RequestChamiModelDelegate: NSObjectProtocol {
    func modelDidUpdateAddress(model: RequestChamiModelProtocol)

}

protocol RequestChamiModelProtocol: NSObjectProtocol {
    
    var accountRole: AccountRole { get set }

    weak var delegate: RequestChamiModelDelegate? { get set }
    var selectedCoordinates: CLLocationCoordinate2D? { get set }
    var visibleMapRect: MKMapRect? { get set }
    var selectedChamiType: ChamiLevelType! { get set }

    func updateSearchString(withObject object: AutoCompleteCustomObject)
    
    func searchPlaceListWithEnterText(withText text: String, success: @escaping (([GMSAutocompletePrediction]) -> ()), failure: @escaping ((_ error: NSError) -> ()))
    
    func searchPlaceIfNeeded(success: @escaping ((String?) -> ()))

    var addressString: String! { get set }
    var searchStringObject: AutoCompleteCustomObject! { get set }

    func meetUpCurrentTutor(completion: @escaping ((_ error: NSError?) -> ()))
    
    var currentTutor: User! { get set }
    var meetUp : MeetUp? { get set }
    
    var meetUpSession: MeetUpSession? { get set }
    var isChamiX: Bool { get set }
    var comment: String? { get set }
    var exactLocation: String? { get set }
    //var locationImage: UIImage? { get set }
}

class RequestChamiModel: NSObject, RequestChamiModelProtocol {
    var comment: String?
    var exactLocation: String?

    var meetUpSession: MeetUpSession?
    //var locationImage: UIImage?

    // MARK: - RequestChamiModel methods
    var isChamiX: Bool = false
    var accountRole: AccountRole = AccountManager.shared.currentRole

    weak var delegate: RequestChamiModelDelegate?
    var currentTutor: User!
    fileprivate let serviceMeetUp: MeetUpServiceProtocol

    override init() {
        self.serviceMeetUp = MeetUpService()
        
    }

    var selectedChamiType: ChamiLevelType! = .chamiPlus
    var meetUp : MeetUp?
    
    var selectedCoordinates: CLLocationCoordinate2D? {
        didSet {
//            if let oldCoordinates = oldValue, let newCoordinates = selectedCoordinates {
//                if LocationManager.compareCoordinates(coord1: oldCoordinates, coord2: newCoordinates) == true {
//                    return
//                }
//            }
            updateSearchIfNeeded()
        }
    }
    var visibleMapRect: MKMapRect? {
        didSet {
            updateSearchIfNeeded()
        }
    }
    var addressString: String! = "" {
        didSet {
            if oldValue != addressString {
                self.delegate?.modelDidUpdateAddress(model: self)
            }
        }
    }

    var searchStringObject: AutoCompleteCustomObject! = AutoCompleteCustomObject(name: "")

    /** Implement RequestChamiModel methods here */
    func updateSearchString(withObject object: AutoCompleteCustomObject) {
        self.searchStringObject = object
    }

    func searchPlaceListWithEnterText(withText text: String, success: @escaping (([GMSAutocompletePrediction]) -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        
        var visibleRegion: GMSCoordinateBounds? = nil

        if let visibleRect = self.visibleMapRect {
            
//            let neMapPoint = MKMapPointMake(MKMapRectGetMaxX(visibleRect), visibleRect.origin.y)
//            let swMapPoint = MKMapPointMake(visibleRect.origin.x, MKMapRectGetMaxY(visibleRect))
//            let neCoord = MKCoordinateForMapPoint(neMapPoint)
//            let swCoord = MKCoordinateForMapPoint(swMapPoint)
            let neMapPoint = MKMapPoint(x: visibleRect.maxX, y: visibleRect.origin.y)
            let swMapPoint = MKMapPoint(x: visibleRect.origin.x, y: visibleRect.maxY)
            visibleRegion = GMSCoordinateBounds(coordinate: neMapPoint.coordinate, coordinate: swMapPoint.coordinate)
        }
        //        let visibleRegion = mapView.projection.visibleRegion()
        //        let bounds = GMSCoordinateBounds(coordinate: visibleRegion.farLeft, coordinate: visibleRegion.nearRight)
       // [MKMapPoint(x: MKMapRect.world.MaxX, y: MKMapRect.world.maxY)]
        //        let filter = GMSAutocompleteFilter()
        //        filter.type = .establishment
        // MKMapPoint(x: MKMapRectGetMaxX(visibleRect), y: visibleRect.origin.y)
        let placesClient = GMSPlacesClient()
        placesClient.autocompleteQuery(text, bounds: visibleRegion, filter: nil, callback: {
            (results, error) -> Void in
            guard error == nil else {
                print("Autocomplete error \(String(describing: error))")
                return
            }
///*
     // 'MKMapPointMake' has been replaced by 'MKMapPoint.init(x:y:)'
            //Replace 'MKMapPointMake(MKMapRectGetMaxX(visibleRect), ' with 'MKMapPoint(x: MKMapRectGetMaxX(visibleRect), y: '
            
            //'MKMapRectGetMaxX' has been replaced by property 'MKMapRect.maxX'

            //
            if let results = results {
                var predictions: [String] = []
                for result in results {
                    predictions.append(result.attributedPrimaryText.string)
//                    print("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
                success(results)

            } else {
//                success(["addsf", "afs ", "asdfa sadf"])

                success([])
            }
        })
    }

    func searchPlaceIfNeeded(success: @escaping ((String?) -> ())) {
        
        guard let placeId = self.searchStringObject.placeId else {
            success("")
            return
        }
        let placesClient = GMSPlacesClient.shared()
        
        placesClient.lookUpPlaceID(placeId, callback: { (place, error) -> Void in
            if let error = error {
                print("lookup place id query error: \(error.localizedDescription)")
                success("")
                return
            }
            
            guard let place = place else {
                print("No place details for \(placeId)")
                success("")
                return
            }
            self.addressString = place.formattedAddress
            self.selectedCoordinates = CLLocationCoordinate2DMake(place.coordinate.latitude, place.coordinate.longitude)
            success(self.addressString)

//            print("Place name \(place.name)")
//            print("Place address \(place.formattedAddress)")
//            print("Place placeID \(place.placeID)")
//            print("Place attributions \(place.attributions)")
        })
    }

    //public var meetUpSession: MeetUpSession
    //func meetUpCurrentTutor(completion: @escaping ((_ error: NSError) -> ()))
    func meetUpCurrentTutor(completion: @escaping ((_ completion: NSError?) -> ())) {
        
        guard let tutor = self.currentTutor else {
            print("tutor not found")
            return
        }
        
        guard let user = AccountManager.shared.currentUser, let lang = user.selectedLanguage  else {
            print("current user not found")
            return
        }
        
        if self.isChamiX == false {
            if self.comment == nil || self.comment?.characters.count == 0 {
                let error = ErrorHelper.compileError(withTextMessage: NSLocalizedString("Please write your comment at first", comment: ""))
                completion(error as NSError)
                return
            }
        }
        
        if self.exactLocation == nil || self.exactLocation?.characters.count == 0 {
            let error = ErrorHelper.compileError(withTextMessage: NSLocalizedString("Please write your exact location at first", comment: ""))
            completion(error as NSError)
            return
        }
        
        var stripeStudentToken = ""
        if let stripeStudentTokenUnwrap = user.stripeKeyStudent {
            stripeStudentToken = stripeStudentTokenUnwrap
        }
        
        //
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: NSError?
            let downloadGroup = DispatchGroup()
            // upload image
//            var imageUrl: String = ""
//            if let locationImage = self.locationImage, let data = UIImagePNGRepresentation(locationImage) {
//                downloadGroup.enter()
//                DataStorageManager.shared.upload(data: data) { (error, name) in
//                    if (error != nil) {
//                        resultError = error as NSError?
//                    } else {
//                        imageUrl = name!
//                    }
//                    
//                    downloadGroup.leave()
//                }
//            }
//            downloadGroup.wait()
            
            if resultError != nil {
                DispatchQueue.main.async {
                    completion(resultError)
                }
                return
            }
            downloadGroup.enter()
            self.serviceMeetUp.createMeetUp(withTutor: tutor.id, latitude: tutor.locationLatitude, longitude: tutor.locationLongitude, languageId:lang.languageId , stripeChargeTokenString: stripeStudentToken, success: { [weak self] (meetUp) in
                guard let strongSelf = self else { return }
                strongSelf.meetUpSession = SessionManager.shared.getNewMeetUpSession()
                guard let session = strongSelf.meetUpSession else {
                    return
                }
                
                session.myId = user.id
                session.partnerId = tutor.id
                session.my_full_name = user.fullName
                session.partner_full_name = tutor.fullName
                session.my_image_url = user.imageUrl
                session.partner_image_url = tutor.imageUrl
                
                session.meetup_id = meetUp.meetUpId
                session.language = lang.languageId
                session.language_name = lang.languageName
                if let nativeLanguage = AccountManager.shared.currentUser?.nativeLanguage {
                    session.request_native_language = nativeLanguage.languageId
                    session.request_native_language_name = nativeLanguage.languageName
                }
                //session.image_url = imageUrl
                session.comment = strongSelf.comment
                session.exactLocation = strongSelf.exactLocation
                session.latitude = NSNumber(value:(strongSelf.selectedCoordinates?.latitude)!)
                session.longitude = NSNumber(value:(strongSelf.selectedCoordinates?.longitude)!)

                SessionManager.shared.executeRequest(session: session, completion: { (error) in
                    resultError = error as NSError?
                    downloadGroup.leave()
                })
                
            }) { (error) in
                resultError = error as NSError?
                downloadGroup.leave()
            }
            
            downloadGroup.wait()
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
        
    }

    // MARK: - Private methods

    /** Implement private methods here */
    private func updateSearchIfNeeded() {
        if self.addressString != "" {
            return
        }
        
        LocationHelper.stringLocationFromCoordinates(latitude: selectedCoordinates!.latitude, longitude: selectedCoordinates!.longitude, completion: {[weak self] (locationString) in
            guard let strongSelf = self else { return }
            strongSelf.addressString = locationString
        })
    }
}
