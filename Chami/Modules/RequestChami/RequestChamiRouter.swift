//
//  RequestChamiRouter.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RequestChamiRouter: NSObject {
    
    func navigateToCurrentRequestScreen(from vc: UIViewController, session: MeetUpSession, withTutor tutor: User, isChamiX: Bool) {
        
        let nextVC = CurrentRequestBuilder.viewController()
        nextVC.setTutor(tutor: tutor)
        nextVC.setMeetUpSession(meetUpSession: session)
        nextVC.setIsChamiX(isChamiX: isChamiX)
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
}
