//
//  RequestChamiBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/22/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RequestChamiBuilder: NSObject {

    class func viewController() -> RequestChamiViewController {

        let view = RequestChamiView.create()
        let model: RequestChamiModelProtocol = RequestChamiModel()
        let router = RequestChamiRouter()
        
        let viewController = RequestChamiViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
