//
//  StripeConnectBuilder.swift
//  
//
//  Created by Serg Smyk on 25/07/2017.
//
//

import UIKit

class StripeConnectBuilder: NSObject {

    class func viewController() -> StripeConnectViewController {

        let view: StripeConnectViewProtocol = StripeConnectView.loadFromXib() as! StripeConnectViewProtocol
        let model: StripeConnectModelProtocol = StripeConnectModel()
        let router = StripeConnectRouter()
        
        let viewController = StripeConnectViewController(withView: view, model: model, router: router)
        return viewController
    }
}
