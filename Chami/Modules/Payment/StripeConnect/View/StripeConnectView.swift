//
//  StripeConnectView.swift
//  
//
//  Created by Serg Smyk on 25/07/2017.
//
//

import UIKit

protocol StripeConnectViewDelegate: NSObjectProtocol {
    
    //func viewSomeAction(view: StripeConnectViewProtocol)
}

protocol StripeConnectViewProtocol: NSObjectProtocol {
    
    weak var delegate: StripeConnectViewDelegate? { get set }
    weak var webViewDelegate: UIWebViewDelegate? { get set }
    
    
    weak var webView: UIWebView! { get set }
    
}

class StripeConnectView: UIView, StripeConnectViewProtocol {
    
    // MARK: - StripeConnectView interface methods

    weak public var delegate: StripeConnectViewDelegate?
    weak public var webViewDelegate: UIWebViewDelegate? {
        didSet {
            self.webView.delegate = self.webViewDelegate
        }
    }
    
    @IBOutlet var webView: UIWebView!
    // MARK: - Overrided methods

    override public func awakeFromNib() {
        super.awakeFromNib()
        self.webView.delegate = self.webViewDelegate
    }
    
    // MARK: - IBActions
    
//    func someButtonAction() {
//
//        self.delegate?.viewSomeAction(view: self)
//    }
}
