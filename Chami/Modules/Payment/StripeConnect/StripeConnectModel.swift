//
//  StripeConnectModel.swift
//  
//
//  Created by Serg Smyk on 25/07/2017.
//
//

import UIKit

protocol StripeConnectModelDelegate: NSObjectProtocol {
    
}

protocol StripeConnectModelProtocol: NSObjectProtocol {
    
    weak var delegate: StripeConnectModelDelegate? { get set }
}

class StripeConnectModel: NSObject, StripeConnectModelProtocol {
    
    // MARK: - StripeConnectModel methods

    weak public var delegate: StripeConnectModelDelegate?
    
    /** Implement StripeConnectModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
