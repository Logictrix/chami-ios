//
//  StripeConnectViewController.swift
//  
//
//  Created by Serg Smyk on 25/07/2017.
//
//

import UIKit


typealias StripeConnectViewControllerType = BaseViewController<StripeConnectModelProtocol, StripeConnectViewProtocol, StripeConnectRouter>

protocol StripeConnectViewControllerDelegate : NSObjectProtocol {
    
    func stripeConnectViewControllerDidRegisterCode(_ stripeConnectViewController: StripeConnectViewControllerType, code: String)
    func stripeConnectViewControllerDidCancel(_ stripeConnectViewController: StripeConnectViewControllerType)
}

class StripeConnectViewController: StripeConnectViewControllerType, UIWebViewDelegate {
    
    weak public var delegate: StripeConnectViewControllerDelegate?
    // MARK: Initializers
    
    required public init(withView view: StripeConnectViewProtocol!, model: StripeConnectModelProtocol!, router: StripeConnectRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Payment Details", comment: "")
        
        customView.delegate = self
        customView.webViewDelegate = self
        model.delegate = self
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if StripeApiManager.shared.stripeOAuthUrl == nil {
            self.setProgressVisible(visible: true)
            StripeApiManager.shared.setupConnectAccountAccess(completion: {[weak self] (error) in
                guard let strongSelf = self else { return }
                
                if let error = error {
                    strongSelf.setProgressVisible(visible: false)
                    AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: self!, closeHandler: nil)
                } else {
                    strongSelf.loadCredentialsPage()
                }
            })
        } else {
            self.loadCredentialsPage()
        }
        
    }
    
    func loadCredentialsPage() {
        if let url = StripeApiManager.shared.stripeOAuthUrl {
            self.customView.webView.loadRequest(URLRequest.init(url: URL.init(string: url)!))
        } else {
            AlertManager.showWarning(withMessage: "No Stripe credentials", onController: self, closeHandler: nil)
        }
    }


// MARK: - UIWebViewDelegate
    func webView(_ webView: UIWebView, shouldStartLoadWith request: URLRequest, navigationType: UIWebView.NavigationType) -> Bool {
        guard let url = request.url?.absoluteString else {
            return true
        }
        //print("url = \(url)")

        guard url.range(of: "://localhost/") != nil else {
            return true
        }
        
        if url.range(of: "error") != nil {
            webView.delegate = nil
            self.delegate?.stripeConnectViewControllerDidCancel(self)
        }
        
        if let code = String.getQueryStringParameter(url: url, param: "code") {
            print("code = \(code)")
            webView.delegate = nil
            self.delegate?.stripeConnectViewControllerDidRegisterCode(self, code: code)
        }
        
        return false
    }
    
    func webViewDidStartLoad(_ webView: UIWebView) {
        self.setProgressVisible(visible: true)
    }
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        //if webView.isLoading == false {
            self.setProgressVisible(visible: false)
        //}
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        self.setProgressVisible(visible: false)
        AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: self, closeHandler: nil)
    }
}

// MARK: - StripeConnectViewDelegate
extension StripeConnectViewController: StripeConnectViewDelegate {
}

// MARK: - StripeConnectModelDelegate

extension StripeConnectViewController: StripeConnectModelDelegate {
}
