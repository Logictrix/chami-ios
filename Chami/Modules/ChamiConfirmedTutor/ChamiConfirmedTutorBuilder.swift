//
//  ChamiConfirmedTutorBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ChamiConfirmedTutorBuilder: NSObject {

    class func viewController() -> ChamiConfirmedTutorViewController {

        let view: ChamiConfirmedTutorViewProtocol = ChamiConfirmedTutorView.create()
        let model: ChamiConfirmedTutorModelProtocol = ChamiConfirmedTutorModel()
        let router = ChamiConfirmedTutorRouter()
        
        let viewController = ChamiConfirmedTutorViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
