//
//  ChamiConfirmedTutorModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ChamiConfirmedTutorModelDelegate: NSObjectProtocol {
    
}

protocol ChamiConfirmedTutorModelProtocol: NSObjectProtocol {
    
    weak var delegate: ChamiConfirmedTutorModelDelegate? { get set }
    var currentMeetUpSession: MeetUpSession! { get set }
    var currentSession: Session? { get set }
    
    func getSession(completion: @escaping (_ error:Error?) -> Void)

    func cancelMeetUpRequest(completion: @escaping (_ error:Error?) -> Void)
}

class ChamiConfirmedTutorModel: NSObject, ChamiConfirmedTutorModelProtocol {
    
    // MARK: - ChamiConfirmedTutorModel methods

    weak var delegate: ChamiConfirmedTutorModelDelegate?
    let service: SessionServiceProtocol = SessionService()

    var currentMeetUpSession: MeetUpSession!
    var currentSession: Session?

    /** Implement ChamiConfirmedTutorModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    func getSession(completion: @escaping (_ error:Error?) -> Void) {
        let sessionId = currentMeetUpSession.session_id
        self.service.getSession(withId: sessionId, success: { (session) in
            self.currentSession = session
            completion(nil)
        }) { (error) in
            completion(error)
        }
    }
    
    func cancelMeetUpRequest(completion: @escaping (_ error:Error?) -> Void) {
        SessionManager.shared.executeRequestCancel(session: self.currentMeetUpSession) { (error) in
            completion(error)
        }
    }
    

}
