//
//  ChamiConfirmedTutorView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit

protocol ChamiConfirmedTutorViewDelegate: NSObjectProtocol {
    
    func viewSendActionLetUsKnow(view: ChamiConfirmedTutorViewProtocol)
}

protocol ChamiConfirmedTutorViewProtocol: NSObjectProtocol {
    
    weak var delegate: ChamiConfirmedTutorViewDelegate? { get set }
    //func updateViewWithSession(session: Session)
    func updateViewWithSession(session: Session, meetupSession: MeetUpSession)
    weak var mapView: MKMapView! { get }
    var bottomPromptView: UIView! { get }
    //weak var mapScreenImageView: UIImageView! { get }
}

class ChamiConfirmedTutorView: UIView, ChamiConfirmedTutorViewProtocol, MKMapViewDelegate{

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var mapScreenImageView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    @IBOutlet weak var codeLabel: UILabel!

    @IBOutlet var customInfoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var codeView: UIView!
    @IBOutlet weak var messageTextViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var messageTextView: UITextView!
    @IBOutlet weak var exactLocationTextView: UITextView!
    
    @IBOutlet var bottomPromptView: UIView!
    @IBAction func actionLetUsKnow(_ sender: Any) {
        self.delegate?.viewSendActionLetUsKnow(view: self)
    }
    
    func updateViewWithSession(session: Session, meetupSession: MeetUpSession) {
        
        let url = URL(string: session.tutorsImageUrl)
        self.logoImageView.kf.setImage(with: url)
        
//        if let image_url = meetupSession.image_url {
//            let urlMap = URL(string: image_url)
//            self.mapScreenImageView.kf.setImage(with: urlMap)
//        }
        
        self.nameLabel.text = session.tutorsFullName
        if let code = session.code {
            self.codeLabel.text = String(describing: code)
        }
        
        let dateString = DateConverter.stringTimeFormat(fromDate: (session.etaDate))
        self.timeLabel.text = dateString
        
        LocationHelper.stringLocationFromCoordinates(latitude: (Double(meetupSession.latitude)), longitude: (Double(meetupSession.longitude)), completion: {[weak self] (locationString) in
            guard let strongSelf = self else { return }
            strongSelf.locationLabel.text = locationString
        })
        
        self.mapView.delegate = self;
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = CLLocationCoordinate2DMake((Double(meetupSession.latitude)), (Double(meetupSession.longitude)))
        //let annotationView = MKAnnotationView()
        //annotationView.annotation = annotation
        //annotationView.image = UIImage(named: "find_tut_scr_icon_pointer_map")
        self.mapView.addAnnotation(annotation)
        
        self.mapView.camera.altitude *= 0.1;
        self.mapView .showAnnotations(self.mapView.annotations, animated: true)
        
        self.messageTextView.text = meetupSession.comment
        self.exactLocationTextView.text = meetupSession.exactLocation
        
        let msgHeight: CGFloat = 72
        let customInfoViewHeight: CGFloat = 216
        self.messageTextViewHeightConstraint.constant = (self.messageTextView.text.characters.count > 0) ? msgHeight : 0
        self.customInfoViewHeightConstraint.constant = (self.messageTextView.text.characters.count > 0) ? customInfoViewHeight : (customInfoViewHeight - msgHeight)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        // Don't want to show a custom image if the annotation is the user's location.
        guard !(annotation is MKUserLocation) else {
            return nil
        }
        
        // Better to make this class property
        let annotationIdentifier = "AnnotationIdentifier"
        
        var annotationView: MKAnnotationView?
        if let dequeuedAnnotationView = mapView.dequeueReusableAnnotationView(withIdentifier: annotationIdentifier) {
            annotationView = dequeuedAnnotationView
            annotationView?.annotation = annotation
        }
        else {
            annotationView = MKAnnotationView(annotation: annotation, reuseIdentifier: annotationIdentifier)
            annotationView?.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        if let annotationView = annotationView {
            // Configure your annotation view here
            annotationView.canShowCallout = true
            annotationView.image = UIImage(named: "find_tut_scr_icon_pointer_map")
        }
        
        return annotationView
    }
    
    class func create() -> ChamiConfirmedTutorView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? ChamiConfirmedTutorView else {
            fatalError("Nib \(viewNibName) does not contain ChamiConfirmedTutor View as first object")
        }
        
        return view
    }
    
    // MARK: - ChamiConfirmedTutorView interface methods

    weak var delegate: ChamiConfirmedTutorViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        self.logoImageView.layer.cornerRadius = self.logoImageView.bounds.size.height / 2.0
        self.logoImageView.layer.masksToBounds = true
        self.codeView.layer.borderColor = RGB(226, 226, 226).cgColor
        self.codeView.layer.borderWidth = 2.0
        self.logoImageView.contentMode = .scaleAspectFill

        self.messageTextView.layer.borderColor = self.codeView.layer.borderColor
        self.messageTextView.layer.borderWidth = self.codeView.layer.borderWidth

        self.exactLocationTextView.layer.borderColor = self.codeView.layer.borderColor
        self.exactLocationTextView.layer.borderWidth = self.codeView.layer.borderWidth

        
    }
    
    // MARK: - IBActions
    
}
