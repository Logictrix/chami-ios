//
//  ChamiConfirmedTutorViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias ChamiConfirmedTutorViewControllerType = BaseViewController<ChamiConfirmedTutorModelProtocol, ChamiConfirmedTutorViewProtocol, ChamiConfirmedTutorRouter>

class ChamiConfirmedTutorViewController: ChamiConfirmedTutorViewControllerType {
    
    // MARK: Initializers
    deinit {
        SessionManager.shared.multicastDelegate.removeDelegate(self)
    }

    required init(withView view: ChamiConfirmedTutorViewProtocol!, model: ChamiConfirmedTutorModelProtocol!, router: ChamiConfirmedTutorRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Chami Confirmed", comment: "Chami Confirmed - Screen title")
        self.navigationItem.setHidesBackButton(true, animated:false)

        customView.delegate = self
        model.delegate = self
        SessionManager.shared.multicastDelegate.addDelegate(self)
    
        self.canHandleMeetupRequests = false
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelMeetup))
        
    }
    @objc func cancelMeetup() {
        self.model.cancelMeetUpRequest {[weak self] (error) in
            guard let strongSelf = self else { return }
            
            if let msg = error?.localizedDescription  {
                strongSelf.showAlert(error: msg)
                return
            }
            strongSelf.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setProgressVisible(visible: true)
        self.model.getSession { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)

            if let error = error {
                AlertManager.showError(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf)

            } else {
                strongSelf.customView.updateViewWithSession(session: strongSelf.model.currentSession!, meetupSession: strongSelf.model.currentMeetUpSession)
                
            }
        }
        
        self.customView.bottomPromptView.isHidden = self.model.currentMeetUpSession.isChamiXFlow
    }
    
    // MARK: - Public methods
    func setMeetUpSession(meetUpSession: MeetUpSession) {
        self.model.currentMeetUpSession = meetUpSession
    }
    
    // MARK: - SessionManagerDelegate
    func didReceivedMeetUpStartSession(session: MeetUpSession) {
        self.router?.navigateToActiveSesionScreen(from: self, withSession: self.model.currentSession!, withMeetUpSession: self.model.currentMeetUpSession)
        
    }
    func didReceivedMeetUpRequestCancel(session: MeetUpSession) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

// MARK: - ChamiConfirmedTutorViewDelegate

extension ChamiConfirmedTutorViewController: ChamiConfirmedTutorViewDelegate {

    func viewSendActionLetUsKnow(view: ChamiConfirmedTutorViewProtocol) {

    }
}

// MARK: - ChamiConfirmedTutorModelDelegate

extension ChamiConfirmedTutorViewController: ChamiConfirmedTutorModelDelegate {
}
