//
//  RoleSwitcherView.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RoleSwitcherViewDelegate: NSObjectProtocol {

    func tapSwitchControl()
}

protocol RoleSwitcherViewProtocol: NSObjectProtocol {

    weak var delegate: RoleSwitcherViewDelegate? { get set }
    func configureAsStudent()
    func configureAsTutor()
}

class RoleSwitcherView: UIView, RoleSwitcherViewProtocol{

    @IBOutlet weak var roleImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchButton: UIButton!

    class func create() -> RoleSwitcherView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? RoleSwitcherView else {
            fatalError("Nib \(viewNibName) does not contain RoleSwitcher View as first object")
        }

        return view
    }

    // MARK: - RoleSwitcherView interface methods

    weak var delegate: RoleSwitcherViewDelegate?

    func configureAsStudent() {
        self.configureCommon()
        self.roleImageView.image = UIImage(named: "who_are_you_scr_btn_tutor")

        let roleString = NSLocalizedString("Tutor", comment: "Switch Role")
        self.titleLabel.attributedText = self.compileTitleString(withRoleString: NSLocalizedString("Tutor", comment: "Switch Role"))
        self.titleLabel.textColor = Theme.tutor.regularColor()
        self.descriptionLabel.text = NSLocalizedString("Please note you may be required to submit evidence of your teaching qualifications and pass a few checks before you can become a chami tutor. Please click below to proceed to registration.", comment: "Switch Role")

        let switchButtonTitle = self.compileButtonTitleString(withRoleString: roleString.lowercased())
        self.switchButton.setTitle(switchButtonTitle, for: .normal)
    }

    func configureAsTutor() {
        self.configureCommon()
        self.roleImageView.image = UIImage(named: "who_are_you_scr_btn_student")

        let roleString = NSLocalizedString("Learner", comment: "Switch Role")
        self.titleLabel.attributedText = self.compileTitleString(withRoleString: roleString)
        self.titleLabel.textColor = Theme.student.regularColor()
        self.descriptionLabel.text = NSLocalizedString("Are you looking to learn a new language from scratch, or just need a refresher course to help you improve your teaching skills? Find a language learning partner for free or receive structured tuition whenever and wherever you want. Click below to proceed to registration.", comment: "Switch Role")

        let switchButtonTitle = self.compileButtonTitleString(withRoleString: roleString.lowercased())
        self.switchButton.setTitle(switchButtonTitle, for: .normal)
    }

    // MARK: - Private methods

    func configureCommon() {
        Theme.current.apply(filledButton: self.switchButton)
    }

    func compileTitleString(withRoleString roleString: String) -> NSAttributedString {
        let resultTitleString = NSMutableAttributedString()

        var attribute = [NSAttributedString.Key.font: FontHelper.defaultFont(withSize: 25)]
        let startString = NSLocalizedString("Become a ", comment: "Switch Role")
        resultTitleString.append(NSAttributedString(string: startString, attributes: attribute))

        attribute = [NSAttributedString.Key.font: FontHelper.defaultMediumFont(withSize: 25)]
        resultTitleString.append(NSAttributedString(string: roleString, attributes: attribute))

        return resultTitleString
    }

    func compileButtonTitleString(withRoleString roleString: String) -> String {
        let templateString = NSLocalizedString("Switch to %@", comment: "Switch Role")
        return String(format: templateString, roleString)
    }

    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - IBActions

    @IBAction func tapSwitchAction() {
        self.delegate?.tapSwitchControl()
    }
}
