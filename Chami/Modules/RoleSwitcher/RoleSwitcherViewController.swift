//
//  RoleSwitcherViewController.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias RoleSwitcherViewControllerType = BaseViewController<RoleSwitcherModelProtocol, RoleSwitcherViewProtocol, RoleSwitcherRouter>

class RoleSwitcherViewController: RoleSwitcherViewControllerType {

    // MARK: Initializers

    required init(withView view: RoleSwitcherViewProtocol!, model: RoleSwitcherModelProtocol!, router: RoleSwitcherRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        customView.delegate = self
        model.delegate = self

        self.addSideMenuLeftButton()
        
        self.canHandleMeetupRequests = false
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        self.updateView()
    }

    // MARK: Private methods

    func updateView() {
        switch self.model.accountRole {
        case .student:
            self.navigationItem.title = NSLocalizedString("Switch to Tutor", comment: "Role Switcher")
            self.customView.configureAsStudent()
        case .tutor:
            self.navigationItem.title = NSLocalizedString("Switch to Student", comment: "Role Switcher")
            self.customView.configureAsTutor()
        default:
            break
        }
    }

    func switchAccountRole() {
        guard let currentUser = AccountManager.shared.currentUser else {
            return
        }

        let newRole = self.model.accountRole.opponent()
        AccountManager.shared.settings?.currentRole = newRole
        AccountManager.shared.updateTheme()
        
        self.setProgressVisible(visible: true)
        
        currentUser.changeProfileTypeWithFinishBlock(role: newRole, completion: { (error:Error?) in
            AccountManager.shared.currentUser?.updateProfileWithFinishBlock(completion: { (error:Error?) in
                
                self.setProgressVisible(visible: false)
                
                if currentUser.hasCompletedProfile(withRole: newRole) {
                    if newRole == .tutor && AccountManager.shared.currentUser?.teachingLanguages.count == 0 {
                        NavigationManager.shared.showEditLanguagesScreen(animated: true)
                    } else {
                        self.router?.navigateToMainScreen(from: self, accountRole: newRole)
                    }
                    
                } else {
                    self.router?.navigateToSetupProfile(from: self, accountRole: newRole)
                }

                
            })
                
                
        })
        
        
    }
}

// MARK: - RoleSwitcherViewDelegate

extension RoleSwitcherViewController: RoleSwitcherViewDelegate {

    func tapSwitchControl() {
        self.switchAccountRole()
    }
}

// MARK: - RoleSwitcherModelDelegate

extension RoleSwitcherViewController: RoleSwitcherModelDelegate {
    func modelDidUpdate(_ model: RoleSwitcherModelProtocol) {
        self.updateView()
    }
}
