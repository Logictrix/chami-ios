//
//  RoleSwitcherModel.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RoleSwitcherModelDelegate: NSObjectProtocol {
    func modelDidUpdate(_ model: RoleSwitcherModelProtocol)
}

protocol RoleSwitcherModelProtocol: NSObjectProtocol {
    
    weak var delegate: RoleSwitcherModelDelegate? { get set }
    var accountRole: AccountRole { get }
}

class RoleSwitcherModel: NSObject, RoleSwitcherModelProtocol {
    
    // MARK: - RoleSwitcherModel methods

    weak var delegate: RoleSwitcherModelDelegate?
    var accountRole: AccountRole {
        didSet {
            self.delegate?.modelDidUpdate(self)
        }
    }
    
    init(withAccountRole role: AccountRole) {
        self.accountRole = role
    }
}
