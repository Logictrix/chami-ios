//
//  RoleSwitcherBuilder.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RoleSwitcherBuilder: NSObject {

    class func viewController() -> RoleSwitcherViewController {

        let accountRole = AccountManager.shared.currentRole
        let model: RoleSwitcherModelProtocol = RoleSwitcherModel(withAccountRole: accountRole)
        let view: RoleSwitcherViewProtocol = RoleSwitcherView.create()
        let router = RoleSwitcherRouter()
        
        let viewController = RoleSwitcherViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
