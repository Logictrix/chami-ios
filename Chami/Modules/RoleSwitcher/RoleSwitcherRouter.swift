//
//  RoleSwitcherRouter.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RoleSwitcherRouter: NSObject {
    
    func navigateToSetupProfile(from vc: UIViewController, accountRole: AccountRole) {
        let setupProfileVC = SetupProfileBuilder.viewController()
        vc.navigationController?.pushViewController(setupProfileVC, animated: true)
    }
    
    func navigateToMainScreen(from vc: UIViewController,  accountRole: AccountRole) {
        AccountManager.shared.currentRole = accountRole
        NavigationManager.shared.showMainScreen(animated: true)
    }
}
