//
//  HomeBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HomeBuilder: NSObject {

    class func viewController() -> HomeViewController {

        let view: HomeViewProtocol = HomeView.create()
        let service = ChamiService()

        let model: HomeModelProtocol = HomeModel(withService: service)
        let router = HomeRouter()
        
        let viewController = HomeViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
