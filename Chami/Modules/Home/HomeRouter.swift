//
//  HomeRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HomeRouter: NSObject {
    
//    func navigateToChamiConfirmedTutorScreen(from vc: UIViewController, meetupSession: MeetUpSession) {
//        
//        let nextVC = ChamiConfirmedTutorBuilder.viewController()
//        
//        nextVC.setMeetUpSession(meetUpSession: meetupSession)
//        
//        vc.navigationController?.pushViewController(nextVC, animated: true)
//    }
    
    func navigateSelectLanguagesScreen(from vc: HomeViewController, withSelectedLanguageId selectedLanguageId: IdType?) {
        
//        let nextVC = ChamiConfirmedTutorBuilder.viewController()
        let nextVC = SelectLanguageBuilder.viewController()

        nextVC.delegate = vc
        nextVC.setSelectedLanguageId(languageId: selectedLanguageId)
        
        // Set passed parameters
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func navigateTutorProfileScreen(from vc: HomeViewController, withTutorProfileId tutorProfileId: IdType, withProfileIdsArray: [User], isChamiX: Bool) {
        
        let nextVC = FindTutorsProfilesBuilder.viewController()
        nextVC.setTutorId(tutorId: tutorProfileId)
        nextVC.setTutorsArray(tutors: withProfileIdsArray)
        nextVC.setIsChamiX(isChamiX: isChamiX)

        vc.navigationController?.pushViewController(nextVC, animated: true)
    }

    func navigateToUserSettingsScreen(from vc: HomeViewController) {
        
        let nextVC = UserSettingsBuilder.viewController()
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
}
