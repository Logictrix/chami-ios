//
//  HomeViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit
import SideMenu
import CoreLocation
import PKHUD

import Stripe

import Alamofire

typealias HomeViewControllerType = BaseViewController<HomeModelProtocol, HomeViewProtocol, HomeRouter>

class HomeViewController: HomeViewControllerType, SelectLanguageViewControllerDelegate, MKMapViewDelegate {
    
    // MARK: Initializers
    
    required init(withView view: HomeViewProtocol!, model: HomeModelProtocol!, router: HomeRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        customView.delegate = self
        model.delegate = self
        
        self.setupNavigationBar()
        
        // setupLocationManager
        self.customView.mapView.showsUserLocation = true
        self.customView.mapView.mapType = MKMapType.standard
        self.customView.mapView.isZoomEnabled = true
        self.customView.mapView.isScrollEnabled = true

        if LocationManager.shared.coordinate.latitude != 0 || LocationManager.shared.coordinate.longitude != 0 {
            centerMapOnLocation(coordinate: LocationManager.shared.coordinate)
        }
        
        self.model.selectedCoordinates = LocationManager.shared.coordinate
        if self.model.accountRole == .tutor {
            self.model.selectedChamiType = .chamiX
        } else {
            self.model.selectedChamiType = .chamiPlus
        }
        
        
        self.customView.isStatusOnline = self.model.onlineStatus

        
        LocationManager.shared.multicastDelegate.addDelegate(self)
        LocationManager.shared.startUpdating()
        
    }
    
    deinit {
        LocationManager.shared.multicastDelegate.removeDelegate(self)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.model.updateSearchIfNeeded()
        
        self.customView.isChangeLanguageNameFlashing = (self.model.selectedLanguage == nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if self.model.accountRole == .student {
            self.changeOnlineStatusTo(isOnline: true)
            
        } else if self.model.accountRole == .tutor {
            if let currentUser = AccountManager.shared.currentUser {
                self.customView.changeViewOnlineStateTo(isOnline: (currentUser.isOnline))
            }
        }
        
        self.updateView()

    }

    override func viewDidLayoutSubviews() {
//        super.viewDidLayoutSubviews()
        self.updateView()
    }
    
//    func handleNewMeetUpRequest(session: MeetUpSession) {
//
//        let userLocation = CLLocation(latitude: CLLocationDegrees(session.latitude), longitude: CLLocationDegrees(session.longitude))
//        
//        let currentUserLocation = CLLocation(latitude: AccountManager.shared.currentUser!.locationLatitude, longitude: AccountManager.shared.currentUser!.locationLongitude)
//        
//        let strLocation = LocationHelper.stringDistanceInMiles(fromLocation: currentUserLocation, toLocation: userLocation)
//
//        if self.model.accountRole == .student {
//            self.customView.showChamiMeetUp(withName: session.partner_full_name!, photoUrl: session.partner_image_url, languageString: session.language_name, milesAway: strLocation, isChamiX: true)
//            
//        } else if self.model.accountRole == .tutor {
//            self.customView.showChamiMeetUp(withName: session.partner_full_name!, photoUrl: session.partner_image_url, languageString: session.language_name, milesAway: strLocation, isChamiX: false)
//
//        } else {
//            fatalError("\(String(describing: type(of: self))) require user role to be defined")
//        }
//
//    }

    // MARK: - SessionManagerDelegate
//    override func didReceivedMeetUpRequest(session: MeetUpSession) {
//        self.model.allMeetUpSessions.append(session)
//        self.model.currentMeetUpSession = session
//        self.handleNewMeetUpRequest(session: session)
//    }
//    override func didReceivedMeetUpRequestCancel(session: MeetUpSession) {
//        self.customView.hideChamiMeetUp()
//    }
//    

    // MARK: - Instance Methods
    fileprivate func updateView() {

        if self.model.accountRole == .student {
            self.customView.changeViewStateTo(viewState: .selectLanguage)

        } else if self.model.accountRole == .tutor {
            self.customView.changeViewOnlineStateTo(isOnline: self.model.onlineStatus)
            self.customView.changeViewStateTo(viewState: .chamiStatus)
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
    }
    

    func changeOnlineStatusTo(isOnline: Bool) {
        
        self.setProgressVisible(visible: true)
        
        self.model.changeOnlineStatus(isOnline: isOnline) { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            if let error = error {
                strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)

//                strongSelf.customView.statusSwitch.isOn = !strongSelf.customView.statusSwitch.isOn
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: nil)
            } else {
//                strongSelf.model.onlineStatus = strongSelf.customView.statusSwitch.isOn
                strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)

            }
            
        }
        
    }
    
    // MARK:
    // MARK: Private Methods
    fileprivate func setupNavigationBar() {
        let settingsButton = UIBarButtonItem.init(image: UIImage.init(named: "top_bar_btn_settings"), style: .plain, target: self, action: #selector(gotoProfileSettings))
        self.navigationItem.rightBarButtonItem = settingsButton
        self.addSideMenuLeftButton()
        
        if let titleImage = UIImage(named: "top_bar_logo_chami") {
            let titleImageView = UIImageView(image: titleImage)
            self.navigationItem.titleView = titleImageView
        }
    }
    
    @objc func gotoProfileSettings() {
        self.router?.navigateToUserSettingsScreen(from: self)
        
    }
    

    // MARK: - MKMapViewDelegate
    var firstLocation = true
    
    
    func centerMapOnLocation(coordinate: CLLocationCoordinate2D) {
        let coordinateRegion = MKCoordinateRegion(center: coordinate,
                                                  latitudinalMeters: LocationManager.shared.regionRadius * 2.0, longitudinalMeters: LocationManager.shared.regionRadius * 2.0)
        self.customView.mapView.setRegion(coordinateRegion, animated: true)
    }

    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        self.model.selectedCoordinates = mapView.centerCoordinate

    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard self.model.accountRole == .student else {
            return
        }
        guard let annotation = view.annotation as? CustomPointAnnotation else {
            return
        }
        mapView.deselectAnnotation(view.annotation!, animated: false)
        let userId = annotation.idValue
        
        var isChamiX = false
        if self.model.selectedChamiType == .chamiX {
            isChamiX = true
        }
        self.router?.navigateTutorProfileScreen(from: self, withTutorProfileId: userId, withProfileIdsArray: self.model.allTutorsFound, isChamiX: isChamiX)
        
        return;
            
    }
    
    func mapView(_ mapView: MKMapView, didAdd views: [MKAnnotationView]) {
        for view1 in views {
            if view1.annotation is MKUserLocation {
                //
                view1.isEnabled = false
            }
        }
    }

    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
//            let reusableUserPin = "userLocationViewId"
//            var pin = mapView.dequeueReusableAnnotationView(withIdentifier: reusableUserPin)
//            let newPin = MKPointAnnotation()
//            if pin == nil {
//                pin = MKAnnotationView(annotation: annotation, reuseIdentifier: reusableUserPin)
//                pin!.canShowCallout = false
//                var img = UIImage(named: "find_tut_scr_icon_pointer_map")
//                if self.model.accountRole == .student {
//                    if self.model.selectedChamiType == .chamiX {
//                        img = UIImage(named: "find_tut_scr_icon_pointer_map_x")
//                    } else if self.model.selectedChamiType == .chamiPlus {
//                        img = UIImage(named: "find_tut_scr_icon_pointer_map_+")
//                    } else {
//                        img = UIImage(named: "find_tut_scr_icon_pointer_map_m")
//                    }
//                    
//                } else if self.model.accountRole == .tutor {
//                    img = UIImage(named: "main_scr_icon_poiner_map")
//                    
//                } else {
//                    fatalError("\(String(describing: type(of: self))) require user role to be defined")
//                }
//                
//                pin!.image = img
//            }
//            return pin
            return nil
        } else {
            var pinInfo = (reusableId: "", pinImageName: "")

            switch self.model.accountRole {
            case .tutor:
                pinInfo = ("tutorsLocationViewId", "find_tut_scr_icon_pointer_map")
            case .student:
                pinInfo = ("studentsLocationViewId", "find_tut_scr_icon_pointer")
            case .notDefined:
                return nil
            }
            
            if let userPin = mapView.dequeueReusableAnnotationView(withIdentifier: pinInfo.reusableId) {
                return userPin
            }
            
            let userPin = MKAnnotationView(annotation: annotation, reuseIdentifier: pinInfo.reusableId)
            userPin.canShowCallout = false
            userPin.image = UIImage(named: pinInfo.pinImageName)
            
            return userPin
        }
    }

    // MARK: - SelectLanguageViewControllerDelegate
    func viewControllerDidFinishWithSelectedLanguage(viewController: UIViewController , language: Language?) {
        _ = self.navigationController?.popViewController(animated: true)
        
        if language != nil {
            self.model.selectedLanguage = language
            self.customView.changeViewLanguageName(languageName: language?.languageName)
        }
    }

    let sessionService: SessionServiceProtocol = SessionService()
    
    func didReceivedMeetUpSessionTick(session: MeetUpSession) {
        if self.isVisible == true {
            print("visible")
            if SessionManager.shared.currentSession != nil {
                self.sessionService.getSession(withId: session.session_id, success: {[weak self] (sessionApi) in
                    guard let strongSelf = self else { return }
                    if strongSelf.isVisible == true {
                        DispatchQueue.main.async {
//                            if session.partnerId.characters.count > 0 {
//                                sessionApi.tutorsId = session.partnerId
//                            }
                            
                            let nextVC = ActiveSessionBuilder.viewController()
                            nextVC.setSession(session: sessionApi)
                            nextVC.setMeetUpSession(meetUpSession: session)
                        
                            strongSelf.navigationController?.pushViewController(nextVC, animated: false)
                        }
                    }
                }, failure: {[weak self] (error) in
                    guard let strongSelf = self else { return }
                    AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: nil)
                })
            }
            
        }
    }
    //func didReceivedMeetUpEndSession(session: MeetUpSession) {}
}

// MARK: - HomeViewDelegate
extension HomeViewController: LocationManagerDelegate {
    func locationManagerDidUpdated(manager:LocationManager) {
        let userLocation = CLLocation.init(latitude: manager.latitude, longitude: manager.longitude)
        if firstLocation {
            firstLocation = false
            self.centerMapOnLocation(coordinate: userLocation.coordinate)
            self.model.selectedCoordinates = LocationManager.shared.coordinate
            
        }
        AccountManager.shared.currentUser!.locationLatitude = userLocation.coordinate.latitude
        AccountManager.shared.currentUser!.locationLongitude = userLocation.coordinate.longitude

    }
}


extension HomeViewController: HomeViewDelegate {

//    func viewSendActionNonInterested(view: HomeViewProtocol) {
//        self.customView.hideChamiMeetUp()
//        let session = self.model.currentMeetUpSession!
//        session.isAccepted = false
//        SessionManager.shared.executeResponse(session: session) { (error) in
//            if let error = error {
//                print(error.localizedDescription)
//            }
//
//        }
//    }
    
//    func viewSendActionChamiXShow(view: HomeViewProtocol) {
//        self.customView.showBotView(withData: "")
//    }

//    func viewSendActionMeetUp(view: HomeViewProtocol) {
//        
//        self.setProgressVisible(visible: true)
//        self.model.startCurrentSession(success: { [weak self] in
//            guard let strongSelf = self else { return }
//            strongSelf.setProgressVisible(visible: false)
//            strongSelf.router?.navigateToChamiConfirmedTutorScreen(from: strongSelf, meetupSession: strongSelf.model.currentMeetUpSession!)
//
//
//        }) { [weak self] (error) in
//            guard let strongSelf = self else { return }
//            strongSelf.setProgressVisible(visible: false)
//            AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: nil)
//
//        }
//
//    }
    
    func viewSendActionSelectLanguage(view: HomeViewProtocol) {

        var languageId: IdType? = nil
        if self.model.selectedLanguage != nil {
            languageId = self.model.selectedLanguage?.languageId
        }
        self.router?.navigateSelectLanguagesScreen(from: self, withSelectedLanguageId: languageId)

    }

    func viewSendActionChangeChamiType(view: HomeViewProtocol, toChamiType: Int) {
        self.model.selectedChamiType = ChamiLevelType.fromInt(value:toChamiType)
        self.showInfo(chamiType: toChamiType)
//        if let pin = self.customView.mapView.view(for: self.customView.mapView.userLocation) {
//            var img = UIImage(named: "find_tut_scr_icon_pointer_map")
//            if self.model.accountRole == .student {
//                if self.model.selectedChamiType == .chamiX {
//                    img = UIImage(named: "find_tut_scr_icon_pointer_map_x")
//                } else if self.model.selectedChamiType == .chamiPlus {
//                    img = UIImage(named: "find_tut_scr_icon_pointer_map_+")
//                } else {
//                    img = UIImage(named: "find_tut_scr_icon_pointer_map_m")
//                }
//                
//            } else if self.model.accountRole == .tutor {
//                img = UIImage(named: "main_scr_icon_poiner_map")
//                
//            } else {
//                fatalError("\(String(describing: type(of: self))) require user role to be defined")
//            }
//            
//            pin.image = img
//
//        }

    }
    
    func showInfo(chamiType: Int) {
        //let infoVC = RatePopupBuilder.viewController(rating: rating)
        let infoVC = HomeInfoPopupViewController()
        infoVC.setChamiType(Type: ChamiLevelType.fromInt(value:chamiType))
        self.addChild(infoVC)
        infoVC.view.frame = self.view.bounds
        self.view.addSubview(infoVC.view)
        infoVC.didMove(toParent: self)
    }

    func viewSendActionChangeOnlineStatus(view: HomeViewProtocol) {
        if self.model.onlineStatus == false {
            if AccountManager.shared.currentUser?.stripeKeyTutor == nil {
                StripeApiManager.shared.showPaymentViewControllerIfNeeded(fromViewController: self, completion: {[weak self] (saved, error) in
                    guard let strongSelf = self else { return }
                    if let error = error {
                        print("================\(error.localizedDescription)")
//                        strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)
                    }
                    if saved == true {
                        strongSelf.changeOnlineStatusTo(isOnline: true)
                        AccountManager.shared.saveCurrentUser()
                    }
                })

            } else {
                self.changeOnlineStatusTo(isOnline: !self.model.onlineStatus)
                AccountManager.shared.saveCurrentUser()
            }
        } else {
            self.changeOnlineStatusTo(isOnline: !self.model.onlineStatus)
            AccountManager.shared.saveCurrentUser()
        }
        self.customView.isStatusOnline = self.model.onlineStatus
    }

}

// MARK: - HomeModelDelegate

extension HomeViewController: HomeModelDelegate {
    func modelDidBeginUpdateTutors(model: HomeModelProtocol) {
        //self.setProgressVisible(visible: true)
    }
    
    func modelDidEndUpdateTutors(model: HomeModelProtocol, withTutors tutors: [User]?, orError error: Error?) {
        //self.setProgressVisible(visible: false)
        if error != nil {
            print("error: \(String(describing: error))")
        }
        guard let tutors = tutors else {
            return
        }
        let allAnnotations = self.customView.mapView.annotations
        self.customView.mapView.removeAnnotations(allAnnotations)
        
        for user1 in tutors {
            let annotation = CustomPointAnnotation()
            annotation.idValue = user1.id
            
            let centerCoordinate = CLLocationCoordinate2D(latitude: user1.locationLatitude, longitude: user1.locationLongitude)
            
            annotation.coordinate = centerCoordinate
            //        annotation.title = "Title"
            self.customView.mapView.addAnnotation(annotation)

        }
        
    }

}


