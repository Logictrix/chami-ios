//
//  HomeInfoPopupViewController.swift
//  Chami
//
//  Created by Dima on 3/10/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HomeInfoPopupViewController: UIViewController, UIGestureRecognizerDelegate {

    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var centerConstraint: NSLayoutConstraint!
    
    var chamiLevel = ChamiLevelType.chamiX;
    
    override func viewDidLoad() {
        super.viewDidLoad()

        /*let architectView = UIView(frame: self.view.bounds)
        self.view.addSubview(architectView)*/
//        let gestureRecognizer = UITapGestureRecognizer(target: self, action: Selector(("handleTap:")))
//        gestureRecognizer.delegate = self
//        self.view.addGestureRecognizer(gestureRecognizer)
    }
    
    func handleTap(gestureRecognizer: UIGestureRecognizer) {
        self.removeAnimate()
    }
    
    func setChamiType(Type: ChamiLevelType)
    {
        chamiLevel = Type
    }
    
    func compileTitleLabelText(level: String) -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultBoldFont(withSize: 22)
        let textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: labelFont,
                                                                 NSAttributedString.Key.foregroundColor: ColorHelper.yellowAppColor()]
        
        let logoImage = UIImage(named: "share_scr_logo_blu")
        let logoAttachment = NSTextAttachment()
        logoAttachment.image = logoImage
        
        logoAttachment.bounds = CGRect(origin: CGPoint(x: 0, y: -labelFont.lineHeight / 2), size: CGSize(width: 67, height: 35))
        let logoAttachmentString = NSAttributedString(attachment: logoAttachment)
        titleLabelText.append(logoAttachmentString)

        let textWhitespacePart = NSAttributedString(string: " ", attributes: textPartAttributes)
        titleLabelText.append(textWhitespacePart)
        let textPart = NSAttributedString(string: level, attributes: textPartAttributes)
        titleLabelText.append(textPart)

        
        return titleLabelText
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        switch chamiLevel {
        case ChamiLevelType.chamiX:
            self.titleLabel.attributedText = self.compileTitleLabelText(level: "x")
            self.infoLabel.text = "Find your next language exchange partner.\nThis option is absolutely free to use."

        case ChamiLevelType.chamiPlus:
            self.titleLabel.attributedText = self.compileTitleLabelText(level: "plus")
            self.infoLabel.text = "Select a tutor and choose your location.\nchami + and chami max sessions are\ncharged per minute."
            
        case ChamiLevelType.chamiMax:
            self.titleLabel.attributedText = self.compileTitleLabelText(level: "max")
            self.infoLabel.text = "Get the ultimate one to one experience.\nchami max tutors are highly qualified and experienced."
        }
        
        self.showAnimate()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.removeAnimate()
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func viewTapAction(_ sender: Any) {
        self.removeAnimate()
    }

    func showAnimate()
    {
        //self.view.transform = CGAffineTransform(scaleX: 1.0, y: 2.0)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            //self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            //self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.1)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
                self.removeFromParent()
            }
        });
    }
}
