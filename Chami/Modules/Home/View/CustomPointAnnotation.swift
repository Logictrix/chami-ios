//
//  CustomPointAnnotation.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/17/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit
class CustomPointAnnotation: MKPointAnnotation {
    var idValue: IdType = ""
}
