//
//  HomeView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit

enum HomeViewState {
//    case searchLocation
    case selectLanguage
    case chamiStatus
    case onlyMap

}

protocol HomeViewDelegate: MKMapViewDelegate{
    
//    func viewSendActionNonInterested(view: HomeViewProtocol)
//    func viewSendActionMeetUp(view: HomeViewProtocol)
    func viewSendActionSelectLanguage(view: HomeViewProtocol)
    func viewSendActionChangeChamiType(view: HomeViewProtocol, toChamiType: Int)
//    func viewSendActionChamiXShow(view: HomeViewProtocol)

    func viewSendActionChangeOnlineStatus(view: HomeViewProtocol)

}

protocol HomeViewProtocol: NSObjectProtocol {
    
    weak var delegate: HomeViewDelegate? { get set }
    func changeViewStateTo(viewState: HomeViewState)
    func changeViewLanguageName(languageName: String?)
    var isChangeLanguageNameFlashing: Bool { set get }
    
    func changeViewOnlineStateTo(isOnline: Bool)

    weak var mapView: MKMapView! { get }
    
    //func showBotView(withData data: String?)
//    func showChamiMeetUp(withName: String, photoUrl: String?, languageString: String, milesAway: String, isChamiX: Bool)
//    func hideChamiMeetUp()
    var myLocationButton: UIButton! { get }

    //weak var statusSwitch: UISwitch! { get }
    var isStatusOnline: Bool { get set }
}

class HomeView: UIView, HomeViewProtocol{
    
    var viewState: HomeViewState = .onlyMap
    
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var myLocationButton: UIButton!
    @IBAction func actionMyLocationShow(_ sender: Any) {
        if let coor = mapView.userLocation.location?.coordinate{
            
            var mapRegion = MKCoordinateRegion()
            mapRegion.center = coor
            mapRegion.span.latitudeDelta = 0.02
            mapRegion.span.longitudeDelta = 0.02
            
            mapView.setRegion(mapRegion, animated: true)
        }

    }
    
    // MARK: - Request View
//    @IBOutlet weak var chamiRequestView: UIView!
//    @IBOutlet weak var studentLogoImageView: UIImageView!
//    @IBOutlet weak var studentNameLabel: UILabel!
//    @IBOutlet weak var studentDestinationLanguageLabel: UILabel!
//    @IBOutlet weak var studentHowFarAwayLabel: UILabel!
//    @IBAction func actionNotInterested(_ sender: Any) {
//        self.delegate?.viewSendActionNonInterested(view: self)
//
//    }
    
//    @IBAction func actionMeetUp(_ sender: Any) {
//        self.makeBotView(activeView: self.chamiRequestView, hidden: true, animated: true)
//
//        self.delegate?.viewSendActionMeetUp(view: self)
//
//    }

//    @IBOutlet weak var studentChamiXRequestView: UIView!
    
//    @IBAction func actionShowChamiX(_ sender: Any) {
//        self.delegate?.viewSendActionChamiXShow(view: self)
//    }
    
    // MARK: - Select Language View
    @IBOutlet weak var selectLanguageView: UIView!
    @IBOutlet weak var selectLanfuageLabel: UILabel!
    @IBAction func actionSelectLanguage(_ sender: Any) {
        self.delegate?.viewSendActionSelectLanguage(view: self)

    }
    func changeViewLanguageName(languageName: String?) {
        if languageName == nil {
           self.selectLanfuageLabel.text = NSLocalizedString("Select language", comment: "Select language to learn")
        } else {
            self.selectLanfuageLabel.text = languageName
        }
    }
    var isChangeLanguageNameFlashing: Bool = false {
        didSet {
            if self.isChangeLanguageNameFlashing != oldValue {
                self.animateChangeLanguageName()
            }
        }
    }
    func animateChangeLanguageName() {
        if isChangeLanguageNameFlashing == false {
            return
        }
        
        let interval = 0.5
        
        DispatchQueue.main.asyncAfter(deadline: .now() + (interval * 2)) {
            UIView.animate(withDuration: interval, animations: {
                self.selectLanfuageLabel.alpha = 0.1
            }, completion: { (completion) in
                
                UIView.animate(withDuration: interval, animations: {
                    self.selectLanfuageLabel.alpha = 1.0
                }, completion: { (completion) in
                    self.animateChangeLanguageName()
                })
            })
        }
    }

    // MARK: - Tutor Status View
    @IBOutlet weak var tutorStatusView: UIView!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var statusSwitch: UISwitch!
    @IBAction func actionStatusSwitch(_ sender: UISwitch) {
//        self.makeBotView(activeView: self.chamiRequestView, hidden: !sender.isOn)
        self.delegate?.viewSendActionChangeOnlineStatus(view: self)
    }
    var isStatusOnline: Bool {
        set {
            self.statusSwitch.isOn = isStatusOnline
            self.statusLabel.text = isStatusOnline ? NSLocalizedString("Online", comment: "") : NSLocalizedString("Offline", comment: "")
        }
        get {
            return self.statusSwitch.isOn
        }
    }
    
    // MARK: - No Chami requests view
    @IBOutlet weak var noChamiRequestsView: UIView!
    
    // MARK: - Student Filter Level view
    @IBOutlet weak var studentFilterLevelView: UIView!
    @IBOutlet weak var studentFilterSlider: UISlider!
    @IBAction func studentFilterSliderChanged(_ sender: UISlider) {
        sender.value = round(sender.value)
//        print(sender.value)
        self.delegate?.viewSendActionChangeChamiType(view: self, toChamiType: Int(sender.value))
    }
    @IBAction func actionButtonFilterTapped(_ sender: UIButton) {
        self.studentFilterSlider.value = Float(sender.tag)
        self.studentFilterSliderChanged(self.studentFilterSlider)
    }
    
    
    // MARK: - Life cycle
    class func create() -> HomeView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? HomeView else {
            fatalError("Nib \(viewNibName) does not contain Home View as first object")
        }
        
        return view
    }
    
    // MARK: - HomeView interface methods

    weak var delegate: HomeViewDelegate? {
        didSet {
            self.mapView.delegate = delegate
        }
    }
    
    func changeViewOnlineStateTo(isOnline: Bool) {
        self.statusSwitch.isOn = isOnline
        if isOnline {
            self.statusLabel.text = "Online"
        } else {
            self.statusLabel.text = "Offline"
        }
    }
    
//    func showChamiMeetUp(withName: String, photoUrl: String?, languageString: String, milesAway: String, isChamiX: Bool) {
//        
//        if let imgUrl = photoUrl {
//            let url = URL(string: imgUrl)
//            self.studentLogoImageView.kf.setImage(with: url)
//        }
//        
//        self.studentNameLabel.text = withName
//        self.studentDestinationLanguageLabel.text = "Wants to learn\n\(languageString)"
//        self.studentHowFarAwayLabel.text = "\(milesAway) miles away"
//        
//        self.makeBotView(activeView: self.chamiRequestView, hidden: false)
//        self.makeBotView(activeView: self.noChamiRequestsView, hidden: true)
//
//    }
//    func hideChamiMeetUp() {
//        self.makeBotView(activeView: self.chamiRequestView, hidden: true)
//        self.makeBotView(activeView: self.noChamiRequestsView, hidden: false)
//    }
//
//    func showBotView(withData data: String?) {
//        if data != nil {
//            self.makeBotView(activeView: self.chamiRequestView, hidden: false)
//            self.makeBotView(activeView: self.noChamiRequestsView, hidden: true)
//
//        } else {
//            self.makeBotView(activeView: self.chamiRequestView, hidden: true)
//            self.makeBotView(activeView: self.noChamiRequestsView, hidden: false)
//        }
//
//    }
//    
    let animationTime = 0.7
    var previousViewState: HomeViewState = .onlyMap
    func changeViewStateTo(viewState: HomeViewState) {
        let animated = true
        if self.previousViewState == viewState {
            return
        }
        self.previousViewState = viewState
        
        selectLanguageView.isHidden = true
        studentFilterLevelView.isHidden = true
//        chamiRequestView.isHidden = true
        tutorStatusView.isHidden = true
        noChamiRequestsView.isHidden = true
//        studentChamiXRequestView.isHidden = true
        
        switch viewState {
        case .selectLanguage:
            self.makeTopView(activeView: self.selectLanguageView, hidden: false, animated: animated)
            self.makeBotView(activeView: self.studentFilterLevelView, hidden: false, animated: animated)
//            self.makeBotView(activeView: self.studentChamiXRequestView, hidden: false)

        case .chamiStatus:
            self.makeTopView(activeView: self.tutorStatusView, hidden: false, animated: animated)
            self.makeBotView(activeView: self.noChamiRequestsView, hidden: false, animated: animated)
//            if self.statusSwitch.isOn {
//                self.makeBotView(activeView: self.chamiRequestView, hidden: false)
//
//            } else {
//                self.makeBotView(activeView: self.noChamiRequestsView, hidden: false)
//
//            }

        case .onlyMap: break

        }
    }

    func makeBotView(activeView: UIView, hidden: Bool) {
        self.makeBotView(activeView: activeView, hidden: hidden, animated: true)
    }
    
    func makeBotView(activeView: UIView, hidden: Bool, animated: Bool) {
        self.layoutIfNeeded()
        DispatchQueue.main.async {
            
            if animated {
                var nFrame = CGRect(x: 0, y: self.bounds.size.height - activeView.bounds.size.height, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                
                if hidden {
                    activeView.frame = nFrame
                    nFrame = CGRect(x: 0, y: self.bounds.size.height, width: self.bounds.size.width, height: activeView.bounds.size.height)
                } else {
                    activeView.frame = CGRect(x: 0, y: self.bounds.size.height, width: self.bounds.size.width, height: activeView.bounds.size.height)
                }
                activeView.isHidden = false
                
                UIView.animate(withDuration: self.animationTime, animations: {
                    activeView.frame = nFrame
                }, completion: { (completed) in
                    activeView.isHidden = hidden
                    
                })
            } else {
                var nFrame = CGRect(x: 0, y: self.bounds.size.height - activeView.bounds.size.height, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                if hidden {
                    nFrame = CGRect(x: 0, y: self.bounds.size.height, width: self.bounds.size.width, height: activeView.bounds.size.height)
                }
                activeView.frame = nFrame
                
                activeView.isHidden = hidden
                
            }
        }

    }
    
    func makeTopView(activeView: UIView, hidden: Bool) {
        self.makeTopView(activeView: activeView, hidden: hidden, animated: true)
    }

    func makeTopView(activeView: UIView, hidden: Bool, animated: Bool) {
        self.layoutIfNeeded()
//        DispatchQueue.main.async {
        
            if animated {
                var nFrame = CGRect(x: 0, y: 0, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                
                if hidden {
                    activeView.frame = nFrame
                    nFrame = CGRect(x: 0, y: -activeView.bounds.size.height, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                } else {
                    activeView.frame = CGRect(x: 0, y: -activeView.bounds.size.height, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                }
                
                activeView.isHidden = false
                
                UIView.animate(withDuration: self.animationTime, animations: {
                    activeView.frame = nFrame
                    
                }, completion: { (completed) in
                    activeView.isHidden = hidden
                    
                })
            } else {
                var nFrame = CGRect(x: 0, y: 0, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                if hidden {
                    nFrame = CGRect(x: 0, y: -activeView.bounds.size.height, width: activeView.bounds.size.width, height: activeView.bounds.size.height)
                }
                activeView.frame = nFrame
                
                activeView.isHidden = hidden
                
            }
//        }

    }

    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.studentFilterSlider.isContinuous = false
        self.studentFilterSlider.setThumbImage(UIImage(named: "find_tut_scr_icon_switch"), for: .normal)
        self.studentFilterSlider.setThumbImage(UIImage(named: "find_tut_scr_icon_switch"), for: .highlighted)
        //self.studentFilterSlider.frame = CGRect(x: 8, y: 34, width: self.frame.size.width - 16, height: 30)
        
        self.backgroundColor = UIColor.red.withAlphaComponent(0.5)
        
//        self.studentLogoImageView.layer.cornerRadius = self.studentLogoImageView.bounds.size.height / 2.0
//        self.studentLogoImageView.contentMode = .scaleAspectFill
//
//        self.studentLogoImageView.layer.masksToBounds = true
        self.mapView.tintColor = Theme.current.regularColor()
        self.myLocationButton.setTitleColor(Theme.current.regularColor(), for: .normal)

        self.mapView.userTrackingMode = .followWithHeading
//        self.mapView.showsCompass = true
        
    }
    
    // MARK: - IBActions
}
