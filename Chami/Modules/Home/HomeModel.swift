//
//  HomeModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit


protocol HomeModelDelegate: NSObjectProtocol {
    func modelDidBeginUpdateTutors(model: HomeModelProtocol)
    func modelDidEndUpdateTutors(model: HomeModelProtocol, withTutors tutors: [User]?, orError error: Error?)

}

protocol HomeModelProtocol: NSObjectProtocol {
    
    weak var delegate: HomeModelDelegate? { get set }
    var accountRole: AccountRole { get set }

    var selectedLanguage: Language? { get set }
    var selectedCoordinates: CLLocationCoordinate2D? { get set }
    var selectedChamiType: ChamiLevelType! { get set }
    var allTutorsFound: [User] { get set }

//    var allMeetUpSessions: [MeetUpSession] { get set }
//    var currentMeetUpSession: MeetUpSession? { get set }
//    func startCurrentSession(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ()))
    
    var onlineStatus: Bool { get set }
    func changeOnlineStatus(isOnline: Bool, completion: @escaping (_ error:Error?) -> Void)

    
    func updateSearchIfNeeded()
    //func updateSearch()
}

class HomeModel: NSObject, HomeModelProtocol {
    
    // MARK: - HomeModel methods
    init(withService service: ChamiServiceProtocol) {
        self.service = service
    }
    
    fileprivate let service: ChamiServiceProtocol
    //fileprivate let serviceSession: SessionServiceProtocol = SessionService()
    fileprivate let serviceAccount: AccountServiceProtocol = AccountService()

    weak var delegate: HomeModelDelegate?
    var allTutorsFound: [User] = []
    
//    var currentMeetUpSession: MeetUpSession?
//    var allMeetUpSessions: [MeetUpSession] = []

    
    var accountRole: AccountRole = (AccountManager.shared.currentUser?.role)!
    var selectedLanguage: Language? {
        didSet {
            if oldValue != selectedLanguage {
                AccountManager.shared.currentUser?.selectedLanguage = selectedLanguage
                self.updateSearchIfNeeded()
            }
        }
    }
    
    var selectedCoordinates: CLLocationCoordinate2D? {
        didSet {
            if let oldCoordinates = oldValue, let newCoordinates = selectedCoordinates {
                if LocationManager.compareCoordinates(coord1: oldCoordinates, coord2: newCoordinates) == true {
                    return
                }
            }
            self.updateSearchIfNeeded()
        }
    }
    var selectedChamiType: ChamiLevelType! = .chamiPlus {
        didSet {
            if oldValue != selectedChamiType {
                self.updateSearchIfNeeded()
            }
        }
    }

    var onlineStatus: Bool = (AccountManager.shared.currentUser?.isOnline)!

    /** Implement HomeModel methods here */
    func changeOnlineStatus(isOnline: Bool, completion: @escaping (_ error:Error?) -> Void) {
        self.serviceAccount.changeUserStatus(isOnline: isOnline) { [weak self] (error) in
            if let error = error {
                completion(error)
            } else {
                guard let strongSelf = self else { return }
                strongSelf.onlineStatus = !strongSelf.onlineStatus
                AccountManager.shared.currentUser?.isOnline = isOnline
                completion(nil)
            }
        }
    }
    
    // MARK: - Private methods
    
    /** Implement private methods here */
//    func startCurrentSession(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ())) {
//        let meetUpId = self.currentMeetUpSession?.meetup_id
//        let meetUpLatitude = self.currentMeetUpSession?.latitude
//        let meetUpLongitude = self.currentMeetUpSession?.longitude
//        let date = Date()
//        
//        self.serviceSession.createSession(withMeetUpId: meetUpId!, latitude: meetUpLatitude as! Double, longitude: meetUpLongitude as! Double, startSessionDate: date, success: { (session) in
//            //
//            let meetupSession = self.currentMeetUpSession
//            meetupSession?.isAccepted = true
//            meetupSession?.costPerHour = session.costPerHour
//            meetupSession?.session_id = session.sessionId
//            SessionManager.shared.executeResponse(session: meetupSession!, completion: { (error) in
//                if let error = error {
//                    failure(error as NSError)
//                } else {
//                    success()
//                }
//            })
//
//        }) { (error) in
//            print(error.localizedDescription)
//            failure(error as NSError)
//        }
//    }

    func updateSearchIfNeeded() {
        if self.accountRole == .tutor {
            self.updateSearch()
        } else if self.selectedCoordinates != nil && selectedLanguage != nil {
            self.updateSearch()
        }
    }
    
    func updateSearch() {
        
        self.delegate?.modelDidBeginUpdateTutors(model: self)
        
        self.service.getTutorsNear(latitude: self.selectedCoordinates!.latitude, longitude: self.selectedCoordinates!.longitude, chamiTypeString: selectedChamiType.rawValue, languageId: selectedLanguage?.languageId, success: { [weak self] (tutors) in
            guard let strongSelf = self else { return }
            strongSelf.allTutorsFound = tutors
            strongSelf.delegate?.modelDidEndUpdateTutors(model: strongSelf, withTutors: tutors, orError: nil)
            
            }, failure: { [weak self] (error) in
                guard let strongSelf = self else { return }
                strongSelf.delegate?.modelDidEndUpdateTutors(model: strongSelf, withTutors: nil, orError: error)
        })
    }
    
}
