//
//  RequestFailedView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RequestFailedViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate {
    func viewSendActionFindAnotherTutor(view: RequestFailedViewProtocol)

}

protocol RequestFailedViewProtocol: NSObjectProtocol {
    
    weak var delegate: RequestFailedViewDelegate? { get set }
    func createGradientLayer()
    weak var collectionView: UICollectionView! { get set }



}

class RequestFailedView: UIView, RequestFailedViewProtocol{

    class func create() -> RequestFailedView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? RequestFailedView else {
            fatalError("Nib \(viewNibName) does not contain RequestFailed View as first object")
        }
        
        return view
    }
    
    
    @IBOutlet weak var findTutorButton: UIButton!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var topView: UIView!
    @IBAction func actionFindTutor(_ sender: Any) {
        self.delegate?.viewSendActionFindAnotherTutor(view: self)
        
    }
    func updateView(isChamiX: Bool) {
        if isChamiX {
            self.descriptionLabel.text = NSLocalizedString("We were unable to get a response from the chami X partner, or they rejected the request.", comment: "We were unable to get a response from the chami X partner, or they rejected the request.")
        } else {
            self.descriptionLabel.text = NSLocalizedString("We were unable to get a response from the tutor, or they rejected the request.", comment: "We were unable to get a response from the tutor, or they rejected the request.")
        }
    }
    @IBOutlet weak var collectionView: UICollectionView!
    // MARK: - RequestFailedView interface methods

    weak var delegate: RequestFailedViewDelegate? {
        didSet {
            self.collectionView.delegate = delegate
            self.collectionView.dataSource = delegate
            
        }
    }

    var gradientLayer: CAGradientLayer!
    
    func createGradientLayer() {
        if gradientLayer != nil {
            gradientLayer.removeFromSuperlayer()
        }
        
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.topView.bounds
        let color1 = RGB(27, 229, 243)
        let color2 = RGB(13, 173, 247)

        gradientLayer.colors = [color1.cgColor, color2.cgColor]
        self.topView.layer.insertSublayer(gradientLayer, at: 0)
        
        
    }

    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        Theme.current.apply(borderedButton: self.findTutorButton)

    }
    
    // MARK: - IBActions
}
