//
//  RequestFailedCollectionViewCell.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RequestFailedCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    static let cellReuseId = "RequestFailedCollectionViewCell"

    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.logoImageView.layer.cornerRadius = self.logoImageView.bounds.size.height / 2.0
        self.logoImageView.layer.masksToBounds = true
        self.logoImageView.contentMode = .scaleAspectFill

    }

}
