//
//  RequestFailedBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RequestFailedBuilder: NSObject {

    class func viewController() -> RequestFailedViewController {

        let view: RequestFailedViewProtocol = RequestFailedView.create()
        let model: RequestFailedModelProtocol = RequestFailedModel()
        let router = RequestFailedRouter()
        
        let viewController = RequestFailedViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
