//
//  RequestFailedViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias RequestFailedViewControllerType = BaseViewController<RequestFailedModelProtocol, RequestFailedViewProtocol, RequestFailedRouter>

class RequestFailedViewController: RequestFailedViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: RequestFailedViewProtocol!, model: RequestFailedModelProtocol!, router: RequestFailedRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Request Failed", comment: "Request Failed - Screen title")
        self.navigationItem.setHidesBackButton(true, animated:false)

        registeNibsForCollectionView(customView.collectionView)
        customView.delegate = self
        model.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.customView.createGradientLayer()
        
    }

    func registeNibsForCollectionView(_ collectionView: UICollectionView) {
        
        let cellNib = UINib(nibName: String(describing: RequestFailedCollectionViewCell.self), bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: RequestFailedCollectionViewCell.cellReuseId)
    }
    
    // MARK: - Public methods
    func setIsChamiX(isChamiX: Bool) {
        self.model.isChamiX = isChamiX
    }

    // MARK: - UICollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: RequestFailedCollectionViewCell.cellReuseId, for: indexPath) as? RequestFailedCollectionViewCell else {
            fatalError()

        }
//        print(flagUrlString)
        //        cell.media = media
        
        return cell
        
        
    }
    
   
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }

}

// MARK: - RequestFailedViewDelegate

extension RequestFailedViewController: RequestFailedViewDelegate {
    func viewSendActionFindAnotherTutor(view: RequestFailedViewProtocol) {
       _ = self.navigationController?.popToRootViewController(animated: true)
    }

}

// MARK: - RequestFailedModelDelegate

extension RequestFailedViewController: RequestFailedModelDelegate {
}
