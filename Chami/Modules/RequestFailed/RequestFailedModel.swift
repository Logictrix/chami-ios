//
//  RequestFailedModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RequestFailedModelDelegate: NSObjectProtocol {
    
}

protocol RequestFailedModelProtocol: NSObjectProtocol {
    
    weak var delegate: RequestFailedModelDelegate? { get set }
    var isChamiX: Bool { get set }

}

class RequestFailedModel: NSObject, RequestFailedModelProtocol {
    
    // MARK: - RequestFailedModel methods
    var isChamiX = false
    weak var delegate: RequestFailedModelDelegate?
    
    /** Implement RequestFailedModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
