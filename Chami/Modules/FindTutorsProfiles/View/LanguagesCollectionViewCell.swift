//
//  LanguagesCollectionViewCell.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class LanguagesCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var flagImageView: UIImageView!
    static let cellReuseId = "LanguagesCollectionViewCell"

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.layoutIfNeeded()
        self.flagImageView.layer.cornerRadius = self.flagImageView.bounds.size.height / 2.0
        self.flagImageView.layer.masksToBounds = true
//        self.backgroundColor = UIColor.brown
        self.flagImageView.backgroundColor = UIColor.orange

    }
    
    override var isSelected: Bool {
        get {
            return super.isSelected
        }
        set {
            if newValue {
                super.isSelected = true
                self.flagImageView.alpha = 0.5
//                print("selected")
            } else if newValue == false {
                super.isSelected = false
                self.flagImageView.alpha = 1.0
//                print("deselected")
            }
        }
    }

}
