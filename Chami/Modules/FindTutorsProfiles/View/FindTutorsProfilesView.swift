//
//  FindTutorsProfilesView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Kingfisher
import Cosmos

protocol FindTutorsProfilesViewDelegate: UICollectionViewDataSource, UICollectionViewDelegate {
    
    func viewSendActionViewVideoIntro(view: FindTutorsProfilesViewProtocol)
    func viewSendActionMessageTutor(view: FindTutorsProfilesViewProtocol)
    func viewSendActionMeetUp(view: FindTutorsProfilesViewProtocol)
    func viewSendActionNotInterested(view: FindTutorsProfilesViewProtocol)


}

protocol FindTutorsProfilesViewProtocol: NSObjectProtocol {
    
    weak var delegate: FindTutorsProfilesViewDelegate? { get set }
    weak var languagesCollectionView: UICollectionView! { get set }
    func updateView(withUser user: User)

}

class FindTutorsProfilesView: UIView, FindTutorsProfilesViewProtocol{

    @IBOutlet weak var videoTutorialButton: UIButton!
    
    @IBOutlet weak var messageTutorButton: UIButton!
    
    @IBOutlet weak var userLogoImageView: UIImageView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var howFarAwayLabel: UILabel!
    @IBOutlet weak var userSexLabel: UILabel!
    @IBOutlet weak var userAgeLabel: UILabel!
    
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var aboutUserTitleLabel: UILabel!
    
    @IBOutlet weak var aboutUserDescriptionTextView: UITextView!
    
    @IBOutlet weak var languagesCollectionView: UICollectionView!
    // MARK - life cycle
    class func create() -> FindTutorsProfilesView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? FindTutorsProfilesView else {
            fatalError("Nib \(viewNibName) does not contain FindTutorsProfiles View as first object")
        }
        
        return view
    }
    
    func updateView(withUser user: User) {

        if (user.imageUrl != nil) {
            let url = URL(string: user.imageUrl!)
            self.userLogoImageView.kf.setImage(with: url)
        }
        self.userNameLabel.text = user.fullName
        self.messageTutorButton.isEnabled = false
        self.messageTutorButton.alpha = 0.5
        
        if (user.introVideoUrl != nil) {
            self.videoTutorialButton.isEnabled = true
        } else {
            self.videoTutorialButton.isEnabled = false
        }
        
        if (user.gender != nil) {
            self.userSexLabel.text = user.gender
        } else {
            self.userSexLabel.text = "gender"
        }
        
        if (user.birthDate != nil) {
            let str = DateConverter.stringFullYears(fromDate: user.birthDate)
            self.userAgeLabel.text = "\(str) years old"
        } else {
            self.userAgeLabel.text = "not set"
        }
        
        if (user.about != nil) {
            self.aboutUserDescriptionTextView.text = user.about
        } else {
            self.aboutUserDescriptionTextView.text = ""
        }
        //print("user.rating = \(user.rating)")
        
        self.ratingView.rating = user.rating
//        self.ratingView.layoutIfNeeded()
        self.ratingView.setNeedsDisplay()
        
        let milesStr = user.distanceToCurrentUser
        self.howFarAwayLabel.text = "\(milesStr) miles away"
        
        self.languagesCollectionView.reloadData()
            

    }

    // MARK: - FindTutorsProfilesView interface methods

    weak var delegate: FindTutorsProfilesViewDelegate? {
        didSet {
            self.languagesCollectionView.delegate = delegate
            self.languagesCollectionView.dataSource = delegate

        }
    }
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        Theme.current.apply(borderedButton2: self.messageTutorButton)
        Theme.current.apply(borderedButton2: self.videoTutorialButton)
        self.layoutIfNeeded()
        self.userLogoImageView.layer.cornerRadius = self.userLogoImageView.bounds.size.height / 2.0
        self.userLogoImageView.layer.masksToBounds = true
        self.userLogoImageView.contentMode = .scaleAspectFill

        self.ratingView.setupPresentation()
        self.ratingView.settings.fillMode = .precise
    }
    
    // MARK: - IBActions
    @IBAction func actionViewVideoIntro(_ sender: Any) {
        self.delegate?.viewSendActionViewVideoIntro(view: self)

    }
    @IBAction func actionMessageTutor(_ sender: Any) {
        self.delegate?.viewSendActionMessageTutor(view: self)

    }
    @IBAction func actionMeetUp(_ sender: Any) {
        self.delegate?.viewSendActionMeetUp(view: self)

    }
    
    @IBAction func actionNotInterested(_ sender: Any) {
        self.delegate?.viewSendActionNotInterested(view: self)

    }
}
