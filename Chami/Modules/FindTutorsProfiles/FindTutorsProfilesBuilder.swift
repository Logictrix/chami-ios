//
//  FindTutorsProfilesBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class FindTutorsProfilesBuilder: NSObject {

    class func viewController() -> FindTutorsProfilesViewController {

        let view: FindTutorsProfilesViewProtocol = FindTutorsProfilesView.create()
        let model: FindTutorsProfilesModelProtocol = FindTutorsProfilesModel()
        let router = FindTutorsProfilesRouter()
        
        let viewController = FindTutorsProfilesViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
