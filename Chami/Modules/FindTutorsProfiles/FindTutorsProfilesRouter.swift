//
//  FindTutorsProfilesRouter.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class FindTutorsProfilesRouter: NSObject {
    
    func navigateRequestChamiScreen(from vc: FindTutorsProfilesViewController, withTutor tutor: User, isChamiX: Bool) {
        
        let nextVC = RequestChamiBuilder.viewController()
        nextVC.delegate = vc
        nextVC.setTutor(tutor: tutor)
        nextVC.setIsChamiX(isChamiX: isChamiX)

        vc.navigationController?.pushViewController(nextVC, animated: true)
    }

}
