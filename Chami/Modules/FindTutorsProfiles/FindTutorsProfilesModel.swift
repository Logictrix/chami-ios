//
//  FindTutorsProfilesModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol FindTutorsProfilesModelDelegate: NSObjectProtocol {

}

protocol FindTutorsProfilesModelProtocol: NSObjectProtocol {
    
    weak var delegate: FindTutorsProfilesModelDelegate? { get set }
//    func getLanguageFlagWithIndex(_ index: Int) -> String?
    var tutorId: IdType? { get set }
    var currentTutor: User! { get set }
    var isChamiX: Bool { get set }

    func updateTutorProfile(withId tutorId: IdType, success: @escaping ((_ userProfile: User) -> ()), failure: @escaping ((_ error: NSError) -> ()))
    
    var allTutorsFound: [User] { get set }
    func setNextTutorId(previousTutorId: IdType) -> IdType?

}

class FindTutorsProfilesModel: NSObject, FindTutorsProfilesModelProtocol {
    
    // MARK: - FindTutorsProfilesModel methods

    weak var delegate: FindTutorsProfilesModelDelegate?
    var tutorId: IdType?
    
    var currentTutor: User!
    var allTutorsFound: [User] = []
    var isChamiX: Bool = false

    override init() {
        self.chamiService = ChamiService()
        self.serviceMeetUp = MeetUpService()
        super.init()
    }
    
    fileprivate let chamiService: ChamiServiceProtocol
    fileprivate let serviceMeetUp: MeetUpServiceProtocol

    /** Implement FindTutorsProfilesModel methods here */
//    func getLanguageFlagWithIndex(_ index: Int) -> String? {
//        return "flag url not implemented"
//    }
    func setNextTutorId(previousTutorId: IdType) -> IdType? {
        var prevIndex = 0
        
        for (indx, user1) in self.allTutorsFound.enumerated() {
            if user1.id == previousTutorId {
                prevIndex = indx
                break
            }
        }
        
        self.allTutorsFound.remove(at: prevIndex)
        if self.allTutorsFound.count > 0 {
            let user1 = self.allTutorsFound[0]
            self.tutorId = user1.id
            return user1.id
            
        }
        
        return nil
    }

    func updateTutorProfile(withId tutorId: IdType, success: @escaping ((_ userProfile: User) -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        
        self.chamiService.getTutorDetail(withId: tutorId, success: { [weak self] (user) in
            guard let strongSelf = self else { return }
            strongSelf.currentTutor = user
            success(user)

        }) { (error) in
//            guard let strongSelf = self else { return }
            //print(error.localizedDescription)
//            AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: self.delegate as! UIViewController, closeHandler: nil)
            failure(error as NSError)

        }

    }
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
