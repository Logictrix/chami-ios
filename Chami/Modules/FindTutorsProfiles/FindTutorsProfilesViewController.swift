//
//  FindTutorsProfilesViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/6/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import PKHUD
import MapKit
import AVFoundation
import AVKit

typealias FindTutorsProfilesViewControllerType = BaseViewController <FindTutorsProfilesModelProtocol, FindTutorsProfilesViewProtocol, FindTutorsProfilesRouter>

class FindTutorsProfilesViewController: FindTutorsProfilesViewControllerType, RequestChamiViewControllerDelegate {
    
    private var isFirstTutorOnScreen = true
    // MARK: Initializers
    
    required init(withView view: FindTutorsProfilesViewProtocol!, model: FindTutorsProfilesModelProtocol!, router: FindTutorsProfilesRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
//        self.edgesForExtendedLayout = .top

        registeNibsForCollectionView(customView.languagesCollectionView)
        customView.delegate = self
        model.delegate = self
        
        self.canHandleMeetupRequests = false
        
        if let titleImage = UIImage(named: "top_bar_logo_chami") {
            let titleImageView = UIImageView(image: titleImage)
            self.navigationItem.titleView = titleImageView
        }
        self.navigationItem.setHidesBackButton(true, animated:false)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeButtonTapped))

    }
    
    func setTutorId(tutorId: IdType) {
        self.model.tutorId = tutorId
        self.updateTutorProfile(withId: tutorId)
    }
    
    func setTutorsArray(tutors: [User]) {
        self.model.allTutorsFound = tutors
    }
    func setIsChamiX(isChamiX: Bool) {
        self.model.isChamiX = isChamiX
    }

    func setNextTutorId(previousTutorId: IdType) {
        let nId = self.model.setNextTutorId(previousTutorId: previousTutorId)
        if nId != nil {
            self.updateTutorProfile(withId: nId!)

        } else {
           _ = self.navigationController?.popViewController(animated: true)
        }
    }

    @objc private func closeButtonTapped() {
        _ = self.navigationController?.popViewController(animated: true)
    }

    private func updateTutorProfile(withId tutorId: IdType) {
        self.setProgressVisible(visible: true)
        self.model.updateTutorProfile(withId: tutorId, success: { [weak self] (user) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            if strongSelf.isFirstTutorOnScreen {
                strongSelf.isFirstTutorOnScreen = false
            } else {
                // to run something in 0.1 seconds
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.02) {
                    (strongSelf.customView as! UIView).pushTransition(duration: 0.3)
                }
                
            }
            
            strongSelf.updateView(withUser: strongSelf.model.currentTutor)


        }) { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            //print(error.localizedDescription)
            AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: nil)

        }
    }
    
    func updateView(withUser user: User) {
        self.customView.updateView(withUser: user)
    }
    
    func registeNibsForCollectionView(_ collectionView: UICollectionView) {
        
        let cellNib = UINib(nibName: String(describing: LanguagesCollectionViewCell.self), bundle: nil)
        collectionView.register(cellNib, forCellWithReuseIdentifier: LanguagesCollectionViewCell.cellReuseId)
    }
    
    override func viewDidLayoutSubviews() {
        //        super.viewDidLayoutSubviews()
        guard let user = self.model.currentTutor else {
            return
        }
        self.updateView(withUser: user)
    }


    // MARK: - UICollectionView Delegate
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: LanguagesCollectionViewCell.cellReuseId, for: indexPath) as? LanguagesCollectionViewCell else {
                fatalError()
        }
        
        let language = self.model.currentTutor.teachingLanguages[indexPath.row]
        if (language.languageImageUrl != nil) {
            let url = URL(string: language.languageImageUrl!)
            cell.flagImageView.kf.setImage(with: url)
        }
        cell.priceLabel.text = "\(language.languageCost)"
//        cell.media = media
        
        return cell

        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let user = self.model.currentTutor, let languages = user.teachingLanguages else {
            return 0
        }
        return languages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        self.showVideoPicker { (url:NSURL) in
//            print(url)
//        }        
    }

    // MARK: - RequestChamiViewControllerDelegate
    func viewControllerDidFinishWithSelectedLocation(viewController: UIViewController , location: CLLocation?) {
        _ = self.navigationController?.popViewController(animated: true)
        
        if location != nil {
//            self.model.selectedLanguage = language
//            self.customView.changeViewLanguageName(languageName: language?.languageName)
        }
    }

}

// MARK: - FindTutorsProfilesViewDelegate

extension FindTutorsProfilesViewController: FindTutorsProfilesViewDelegate {

    func viewSendActionViewVideoIntro(view: FindTutorsProfilesViewProtocol) {
        guard let videoUrl = self.model.currentTutor.introVideoUrl else {
            return
        }
        let videoURL = URL(string: videoUrl)
        
        let player = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = player
        self.present(playerViewController, animated: true) {
            playerViewController.player!.play()
        }
    }
    
    func viewSendActionMessageTutor(view: FindTutorsProfilesViewProtocol) {
        
    }
    
    func viewSendActionMeetUp(view: FindTutorsProfilesViewProtocol) {
        self.router?.navigateRequestChamiScreen(from: self, withTutor: self.model.currentTutor, isChamiX: self.model.isChamiX)
    }
    
    func viewSendActionNotInterested(view: FindTutorsProfilesViewProtocol) {
        self.setNextTutorId(previousTutorId: self.model.tutorId!)
        
    }
}

// MARK: - FindTutorsProfilesModelDelegate

extension FindTutorsProfilesViewController: FindTutorsProfilesModelDelegate {
}
