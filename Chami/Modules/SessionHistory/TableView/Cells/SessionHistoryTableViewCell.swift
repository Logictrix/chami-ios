//
//  SessionHistoryTableViewCell.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Cosmos

class SessionHistoryTableViewCell: UITableViewCell {
    
    weak var delegate: SessionHistoryCellDelegate?

    @IBOutlet weak var userLogoImageView: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var sessionDateLabel: UILabel!
    @IBOutlet weak var startTimeLabel: UILabel!
    @IBOutlet weak var sessionLengthLabel: UILabel!
    @IBOutlet weak var sessionCostLabel: UILabel!
    @IBOutlet weak var transactionRefLabel: UILabel!
    
    @IBOutlet weak var staticTransactionRefLabel: UILabel!
    @IBOutlet weak var staticSessionCostLabel: UILabel!
    
    @IBOutlet weak var rateView: CosmosView!
    @IBOutlet weak var arrowImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layoutIfNeeded()
        self.userLogoImageView.layer.cornerRadius = self.userLogoImageView.bounds.size.height / 2.0
        self.userLogoImageView.layer.masksToBounds = true
        self.userLogoImageView.contentMode = .scaleAspectFill

        self.rateView.setupPresentation()

    }

    // MARK: - IBAction

}


protocol SessionHistoryCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    func cellDidTapSomeButton(cell: SessionHistoryTableViewCell)
}
