//
//  SessionHistoryDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class SessionHistoryDataSource: NSObject, UITableViewDataSource {
    
    weak var cellDelegate: SessionHistoryCellDelegate?
    private let model: SessionHistoryModelProtocol
    private let cellReuseId = "SessionHistoryTableViewCell"
    
    init(withModel model: SessionHistoryModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: SessionHistoryTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: SessionHistoryTableViewCell, forItem item: Session) {
        cell.sessionCostLabel.text = "£\(item.cost/100).\(item.cost%100)"
        
        cell.startTimeLabel.text = DateConverter.stringTimeFormat(fromDate: item.startDateTime)
        cell.sessionDateLabel.text = "\(DateConverter.stringDateWithoutTimeFormat(fromDate: item.startDateTime))"
        cell.userNameLabel.text = item.tutorsFullName
        cell.userNameLabel.textColor = Theme.current.regularColor()
        cell.transactionRefLabel.text = item.transactionRef

        cell.sessionLengthLabel.text = "\(item.durationMinutes) minutes"
        let url = URL(string: item.tutorsImageUrl)
        cell.userLogoImageView.kf.setImage(with: url)

        if item.rating > 0 || self.model.accountRole == .tutor {
            cell.rateView.isHidden = false
            cell.rateView.rating = Double(item.rating)
            cell.arrowImageView.isHidden = true

        } else {
            cell.rateView.isHidden = true
//            cell.rateView.rating = Double(item.rating)
            cell.arrowImageView.isHidden = false

        }
    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? SessionHistoryTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let item = self.model.items[indexPath.row];
        self.configure(cell: cell, forItem: item)
        if self.model.accountRole == .student {
            cell.arrowImageView.image = UIImage(named: "help_faqs_scr_arrow_right_yellow")
            cell.staticSessionCostLabel.text = NSLocalizedString("Session Cost:", comment: "Session Cost")
            cell.staticTransactionRefLabel.text = NSLocalizedString("Transaction ref:", comment: "Transaction ref")/*NSLocalizedString("Session ID:", comment: "Session ID")*/

        } else if self.model.accountRole == .tutor {
            cell.arrowImageView.image = UIImage(named: "help_faqs_scr_arrow_right_blu")
            cell.staticSessionCostLabel.text = NSLocalizedString("Session Earnings:", comment: "Session Cost")
            cell.staticTransactionRefLabel.text = NSLocalizedString("Transaction ref:", comment: "Transaction ref")

        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }

        return cell
    }
}
