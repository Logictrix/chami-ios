//
//  SessionHistoryBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SessionHistoryBuilder: NSObject {

    class func viewController() -> SessionHistoryViewController {
        let view: SessionHistoryViewProtocol = SessionHistoryView.create()
        let model: SessionHistoryModelProtocol = SessionHistoryModel()
        let dataSource = SessionHistoryDataSource(withModel: model)
        let router = SessionHistoryRouter()
        
        let viewController = SessionHistoryViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
