//
//  SessionHistoryModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SessionHistoryModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: SessionHistoryModelProtocol)
}

protocol SessionHistoryModelProtocol: NSObjectProtocol {
    
    weak var delegate: SessionHistoryModelDelegate? { get set }
    var items: [Session] { get }
    var accountRole: AccountRole { get set }
    func updateHistory(completion: @escaping (_ error:Error?) -> Void)
    func updateHistoryForSession(session: Session, completion: @escaping (_ session: Session?, _ error:Error?) -> Void)

}

class SessionHistoryModel: NSObject, SessionHistoryModelProtocol {
    
    override init() {
        self.items = []
        super.init()

    }
    
    var accountRole: AccountRole = AccountManager.shared.currentRole
    fileprivate let serviceSession: SessionServiceProtocol = SessionService()

    // MARK: - SessionHistoryModel methods

    weak var delegate: SessionHistoryModelDelegate?
    public private(set) var items: [Session]
    
    func updateHistory(completion: @escaping (_ error:Error?) -> Void) {

        self.serviceSession.getSessionHistory(success: { (sessions) in
            //
            self.items = sessions
            completion(nil)
            
        }) { (error) in
            completion(error)
            
        }
        
    }

    func updateHistoryForSession(session: Session, completion: @escaping (_ session: Session?, _ error:Error?) -> Void) {
        self.serviceSession.getSessionHistoryDetail(session: session, success: { (session) in
            //
            completion(session, nil)

        }) { (error) in
            completion(nil, error)

        }
    }

    /** Implement SessionHistoryModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
