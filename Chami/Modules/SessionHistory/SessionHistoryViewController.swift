//
//  SessionHistoryViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias SessionHistoryViewControllerType = BaseViewController<SessionHistoryModelProtocol, SessionHistoryViewProtocol, SessionHistoryRouter>

class SessionHistoryViewController: SessionHistoryViewControllerType, UITableViewDelegate {
    
    private var dataSource: SessionHistoryDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: SessionHistoryViewProtocol, model: SessionHistoryModelProtocol, router: SessionHistoryRouter, dataSource: SessionHistoryDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        self.navigationItem.title = NSLocalizedString("Session History", comment: "Session History")
        self.addSideMenuLeftButton()
    }
    
    internal required init(withView view: SessionHistoryViewProtocol!, model: SessionHistoryModelProtocol!, router: SessionHistoryRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.setProgressVisible(visible: true)

        self.model.updateHistory { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            strongSelf.customView.noHistoryLabel.isHidden = strongSelf.model.items.count > 0

            if let error = error {
                strongSelf.customView.tableView.reloadData()
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: {
                })
                
            } else {
                strongSelf.customView.tableView.reloadData()
            }
            
        }
    }

    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        let session = self.model.items[indexPath.row]
//        if session.rating > 0 {
//            //
//        } else {
            self.setProgressVisible(visible: true)
            
            self.model.updateHistoryForSession(session: session) { [weak self] (session, error) in
                guard let strongSelf = self else { return }
                strongSelf.setProgressVisible(visible: false)
                
                if let error = error {
                    strongSelf.customView.tableView.reloadData()
                    AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: {
                    })
                    
                } else {
                    strongSelf.router?.navigateToViewRateSession(from:strongSelf, withSession:session!)
                }
            }
//        }
    }
    
}

// MARK: - SessionHistoryViewDelegate

extension SessionHistoryViewController: SessionHistoryViewDelegate {

}

// MARK: - SessionHistoryModelDelegate

extension SessionHistoryViewController: SessionHistoryModelDelegate {
    
    func modelDidChanged(model: SessionHistoryModelProtocol) {
    }
}

// MARK: - SessionHistoryCellDelegate

extension SessionHistoryViewController: SessionHistoryCellDelegate {
    
    func cellDidTapSomeButton(cell: SessionHistoryTableViewCell) {
    }
}
