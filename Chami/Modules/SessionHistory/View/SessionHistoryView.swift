//
//  SessionHistoryView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SessionHistoryViewDelegate: NSObjectProtocol {
    
}

protocol SessionHistoryViewProtocol: NSObjectProtocol {
    
    weak var delegate: SessionHistoryViewDelegate? { get set }
    var tableView: UITableView! { get }
    weak var noHistoryLabel: UILabel! { get }
}

class SessionHistoryView: UIView, SessionHistoryViewProtocol{

    class func create() -> SessionHistoryView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? SessionHistoryView else {
            fatalError("Nib \(viewNibName) does not contain SessionHistory View as first object")
        }
        
        return view
    }
    
    // MARK: - SessionHistoryView interface methods

    weak var delegate: SessionHistoryViewDelegate?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var noHistoryLabel: UILabel!

    // add view private properties/outlets/methods here
    
    // MARK: - IBActions
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        // setup view and table view programmatically here
    }
}
