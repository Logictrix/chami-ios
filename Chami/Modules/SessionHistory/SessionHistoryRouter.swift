//
//  SessionHistoryRouter.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SessionHistoryRouter: NSObject {
    
    func navigateToViewRateSession(from vc: UIViewController, withSession: Session) {
        
        let nextVC = ViewRateSessionBuilder.viewController()
        nextVC.setSession(session: withSession)
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }

}
