//
//  MeetUpRequestModel.swift
//  Chami
//
//  Created by Serg Smyk on 20/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

public protocol MeetUpRequestModelDelegate: NSObjectProtocol {
    
}

protocol MeetUpRequestModelProtocol: NSObjectProtocol {
    
    weak var delegate: MeetUpRequestModelDelegate? { get set }
    
    var currentMeetUpSession: MeetUpSession? { get set }
    
    func cancelCurrentSession(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ()))
    func startCurrentSession(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ()))
}

class MeetUpRequestModel: NSObject, MeetUpRequestModelProtocol {
    
    var currentMeetUpSession: MeetUpSession?
    // MARK: - MeetUpRequestModel methods

    weak public var delegate: MeetUpRequestModelDelegate?
    
    /** Implement MeetUpRequestModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
    fileprivate let serviceSession: SessionServiceProtocol = SessionService()
    func cancelCurrentSession(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        let session = self.currentMeetUpSession!
        
        session.isAccepted = false
        SessionManager.shared.executeResponse(session: session) { (error) in
            if let error = error {
                print(error.localizedDescription)
                failure(error as NSError)
            } else {
                success()
            }
        }
    }
    func startCurrentSession(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        let meetUpId = self.currentMeetUpSession?.meetup_id
        let meetUpLatitude = self.currentMeetUpSession?.latitude
        let meetUpLongitude = self.currentMeetUpSession?.longitude
        let date = Date()
        
        self.serviceSession.createSession(withMeetUpId: meetUpId!, latitude: meetUpLatitude as! Double, longitude: meetUpLongitude as! Double, startSessionDate: date, success: { (session) in
            //
            let meetupSession = self.currentMeetUpSession
            meetupSession?.isAccepted = true
            meetupSession?.session_id = session.sessionId
            meetupSession?.costPerHour = session.costPerHour
            if AccountManager.shared.currentRole == .student {
                meetupSession?.costPerHour = 0
            }
            
            SessionManager.shared.executeResponse(session: meetupSession!, completion: { (error) in
                if let error = error {
                    failure(error as NSError)
                } else {
                    success()
                }
            })
            
        }) { (error) in
            print(error.localizedDescription)
            failure(error as NSError)
        }
    }
}
