//
//  MeetUpRequestBuilder.swift
//  Chami
//
//  Created by Serg Smyk on 20/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class MeetUpRequestBuilder: NSObject {

    class func viewControllerForSessionRequest(sessionRequest: MeetUpSession) -> MeetUpRequestViewController {

        let view: MeetUpRequestViewProtocol = MeetUpRequestView.loadFromXib() as! MeetUpRequestViewProtocol
        
        let model: MeetUpRequestModelProtocol = MeetUpRequestModel()
        model.currentMeetUpSession = sessionRequest
        
        let router = MeetUpRequestRouter()
        
        let viewController = MeetUpRequestViewController(withView: view, model: model, router: router)
        return viewController
    }
}
