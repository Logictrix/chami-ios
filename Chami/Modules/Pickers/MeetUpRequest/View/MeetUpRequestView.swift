//
//  MeetUpRequestView.swift
//  Chami
//
//  Created by Serg Smyk on 20/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol MeetUpRequestViewDelegate: NSObjectProtocol {
    
    //func viewSomeAction(view: MeetUpRequestViewProtocol)
    func meetUpRequestAccepted(accepted: Bool)
}

protocol MeetUpRequestViewProtocol: NSObjectProtocol {
    
    weak var titleRequestLabel: UILabel! { get }
    
    weak var studentLogoImageView: UIImageView! { get }
    weak var studentNameLabel: UILabel! { get }
    weak var studentDestinationLanguageLabel: UILabel! { get }
    weak var studentHowFarAwayLabel: UILabel! { get }
    
    var showCommentOutput: Bool { set get }
    weak var commentContainerView: UIView! { get }
    weak var commentTextView: UITextView! { get }
    
    weak var exactLocationContainerView: UIView! { get }
    weak var exactLocationTextView: UITextView! { get }
    
    weak var delegate: MeetUpRequestViewDelegate? { get set }
    
    var commentString: String? { get set }
    var exactLocationString: String? { get set }
}

class MeetUpRequestView: UIView, MeetUpRequestViewProtocol{
    
    
    @IBOutlet weak var mainConteinerView: UIView!
    @IBOutlet weak var mainConteinerViewHeight: NSLayoutConstraint!
    @IBOutlet weak var titleRequestLabel: UILabel!
    
    @IBOutlet weak var studentLogoImageView: UIImageView!
    @IBOutlet weak var studentNameLabel: UILabel!
    @IBOutlet weak var studentDestinationLanguageLabel: UILabel!
    @IBOutlet weak var studentHowFarAwayLabel: UILabel!

    
    @IBOutlet weak var commentViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var commentContainerView: UIView!
    @IBOutlet weak var commentTitleLabel: UILabel!
    @IBOutlet weak var commentTextView: UITextView!

    @IBOutlet weak var exactLocationContainerView: UIView!
    @IBOutlet weak var exactLocationTitleLabel: UILabel!
    @IBOutlet weak var exactLocationTextView: UITextView!
    
    // MARK: - MeetUpRequestView interface methods

    weak public var delegate: MeetUpRequestViewDelegate?

    var commentString: String? {
        didSet {
            self.commentTextView.text = self.commentString
            self.commentTextView.contentOffset = CGPoint(x: 0, y: 0)
        }
    }

    var exactLocationString: String? {
        didSet {
            self.exactLocationTextView.text = self.exactLocationString
            self.exactLocationTextView.contentOffset = CGPoint(x: 0, y: 0)
        }
    }
    var showCommentOutput: Bool = true {
        didSet {
            let mainConteinerHeight: CGFloat = 418
            let commentHeight: CGFloat = 94
            self.commentViewHeightConstraint.constant = self.showCommentOutput ? commentHeight : 0
            self.mainConteinerViewHeight.constant = self.showCommentOutput ? mainConteinerHeight : (mainConteinerHeight - commentHeight)
        }
    }
    // MARK: - Overrided methods

    override public func awakeFromNib() {
        super.awakeFromNib()
        
        self.studentLogoImageView.layer.cornerRadius = self.studentLogoImageView.bounds.size.height / 2.0
        self.studentLogoImageView.contentMode = .scaleAspectFill
        
        self.studentLogoImageView.layer.masksToBounds = true

        self.commentTitleLabel.textColor = ColorHelper.regularColor()
        self.commentTitleLabel.font = FontHelper.defaultMediumFont(withSize: 16)

        self.exactLocationTitleLabel.textColor = ColorHelper.regularColor()
        self.exactLocationTitleLabel.font = FontHelper.defaultMediumFont(withSize: 16)
    }

    @IBAction func actionCancel(_ sender: UIButton) {
        self.delegate?.meetUpRequestAccepted(accepted: false)
    }
    
    @IBAction func actionAccept(_ sender: UIButton) {
        self.delegate?.meetUpRequestAccepted(accepted: true)
    }
    // MARK: - IBActions

}
