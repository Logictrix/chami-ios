//
//  MeetUpRequestViewController.swift
//  Chami
//
//  Created by Serg Smyk on 20/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import CoreLocation

typealias MeetUpRequestViewControllerType = MVCViewController<MeetUpRequestModelProtocol, MeetUpRequestViewProtocol, MeetUpRequestRouter>

typealias AnswerSelectBlock = ((_ controller: MeetUpRequestViewController, _ answer: Bool) -> ())

class MeetUpRequestViewController: MeetUpRequestViewControllerType {
    
    deinit {
        self.soundPlayer?.stopPlayback()
    }
    // MARK: Initializers
    var answerSelectBlock: AnswerSelectBlock?
    
    required public init(withView view: MeetUpRequestViewProtocol!, model: MeetUpRequestModelProtocol!, router: MeetUpRequestRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    fileprivate var soundPlayer: SoundPlayback?
    // MARK: - View life cycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        
        customView.delegate = self
        model.delegate = self
        
        self.updateView()
        
        self.soundPlayer = SoundPlayback.playerWithDefaultSound()
        self.soundPlayer?.play()

    }
//    override func viewDidAppear(_ animated: Bool) {
//        super.viewDidAppear(animated)
//    }
//    override func viewDidDisappear(_ animated: Bool) {
//        super.viewDidDisappear(animated)
//    }

  
    func compileLanguageTitleLabelText(lang1: String, lang2: String) -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        let defaultFont = FontHelper.defaultFont(withSize: UI.labelFontSize)
        let defaultMediumFont = FontHelper.defaultMediumFont(withSize: UI.labelFontSize)
        
        let lang1Attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): defaultMediumFont,
                                                              NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.black]
        let arrowAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): defaultMediumFont,
                                                              NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): ColorHelper.yellowAppColor()]
        let lang2Attributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): defaultFont,
                                                              NSAttributedString.Key(rawValue: NSAttributedString.Key.foregroundColor.rawValue): UIColor.darkText]

        let lang1Part = NSAttributedString(string:lang1, attributes: lang1Attributes)
        titleLabelText.append(lang1Part)
        let arrowPart = NSAttributedString(string:" ➙ ", attributes: arrowAttributes)
        titleLabelText.append(arrowPart)
        let lang2Part = NSAttributedString(string:lang2, attributes: lang2Attributes)
        titleLabelText.append(lang2Part)
        
        return titleLabelText
    }
    
    func updateView() {
        guard let session = self.model.currentMeetUpSession else {
            return
        }
        let userLocation = CLLocation(latitude: CLLocationDegrees(session.latitude), longitude: CLLocationDegrees(session.longitude))
        
        let currentUserLocation = CLLocation(latitude: LocationManager.shared.latitude, longitude: LocationManager.shared.longitude)
        
        let milesAway = LocationHelper.stringDistanceInMiles(fromLocation: currentUserLocation, toLocation: userLocation)

        
        if let imgUrl = session.partner_image_url {
            let url = URL(string: imgUrl)
            self.customView.studentLogoImageView.kf.setImage(with: url)
        }
        self.customView.studentNameLabel.text = session.partner_full_name!
        
        self.customView.studentDestinationLanguageLabel.text = "Wants to learn\n\(session.language_name)"
        if session.request_native_language_name.characters.count > 0 {
            if session.isChamiXFlow == true {
                self.customView.studentDestinationLanguageLabel.text = nil
                let attributedText = self.compileLanguageTitleLabelText(lang1: session.request_native_language_name, lang2: session.language_name)
                self.customView.studentDestinationLanguageLabel.attributedText = attributedText
            }
        }
        
        
        self.customView.studentHowFarAwayLabel.text = "\(milesAway) miles away"

        self.customView.commentString = session.comment
        self.customView.exactLocationString = session.exactLocation
        
        if AccountManager.shared.currentRole == .student {
            self.customView.titleRequestLabel.text = "You have a chami X request"
        } else {
            self.customView.titleRequestLabel.text = "You have a chami request"
        }
        
        if session.comment == nil || session.isChamiXFlow == true {
            self.customView.showCommentOutput = false
        } else {
            self.customView.showCommentOutput = true
        }
    }
}

// MARK: - MeetUpRequestViewDelegate

extension MeetUpRequestViewController: MeetUpRequestViewDelegate {

    func meetUpRequestAccepted(accepted: Bool) {

        if self.answerSelectBlock != nil {
            self.answerSelectBlock!(self, accepted)
        }
    }
}

// MARK: - MeetUpRequestModelDelegate

extension MeetUpRequestViewController: MeetUpRequestModelDelegate {
}
