//
//  SelectLanguageBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SelectLanguageBuilder: NSObject {

    class func viewController() -> SelectLanguageViewController {
        let view: SelectLanguageViewProtocol = SelectLanguageView.create()
        let model: SelectLanguageModelProtocol = SelectLanguageModel()
        let dataSource = SelectLanguageDataSource(withModel: model)
        let router = SelectLanguageRouter()
        
        let viewController = SelectLanguageViewController(withView: view, model: model, router: router, dataSource: dataSource)
        
        return viewController
    }
}
