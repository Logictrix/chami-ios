//
//  SelectLanguageRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SelectLanguageRouter: NSObject {
    
    class func push(from vc: UIViewController, delegate:SelectLanguageViewControllerDelegate) {
        let selectLangVC = SelectLanguageBuilder.viewController()
        selectLangVC.delegate = delegate
        vc.navigationController?.pushViewController(selectLangVC, animated: true)
    }
}
