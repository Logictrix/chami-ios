//
//  SelectLanguageViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import PKHUD

typealias SelectLanguageViewControllerType = BaseViewController<SelectLanguageModelProtocol, SelectLanguageViewProtocol, SelectLanguageRouter>

protocol SelectLanguageViewControllerDelegate: NSObjectProtocol {
    
    func viewControllerDidFinishWithSelectedLanguage(viewController: UIViewController , language: Language?)
}

class SelectLanguageViewController: SelectLanguageViewControllerType, UITableViewDelegate {
    
    private var dataSource: SelectLanguageDataSource!
    weak var delegate: SelectLanguageViewControllerDelegate?

    // MARK: - Initializers

    convenience init(withView view: SelectLanguageViewProtocol, model: SelectLanguageModelProtocol, router: SelectLanguageRouter, dataSource: SelectLanguageDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        // your custom code
    }
    
    internal required init(withView view: SelectLanguageViewProtocol!, model: SelectLanguageModelProtocol!, router: SelectLanguageRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.navigationItem.title = NSLocalizedString("Select a language", comment: "Select a language")
        let img = UIImage(named: "top_bar_btn_close")
        
        let button: UIButton = UIButton(type: .custom)
        button.setImage(img, for: UIControl.State.normal)
        button.addTarget(self, action: #selector(closeButtonTapped), for: UIControl.Event.touchUpInside)
        button.frame = CGRect(x: 0, y: 0, width: 30, height: 40)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: button)// UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeButtonTapped))
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
        

    }
    
    func setSelectedLanguageId(languageId: IdType?) {
        self.model.selectedId = languageId
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setProgressVisible(visible: true)
        self.model.loadLanguages(success: { [weak self] in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            strongSelf.customView.tableView.reloadData()
            
        }) {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            print("error: \(error)")
            
        }
    }

    @objc private func closeButtonTapped() {
        self.delegate?.viewControllerDidFinishWithSelectedLanguage(viewController: self, language: nil)
        
    }

    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        self.model.changeSelectedLanguage(withRowIndex: indexPath.row)

//        self.router?.navigateToSomeScreen(from: self, withBackgroundColor: UIColor.gray)
    }
}

// MARK: - SelectLanguageViewDelegate

extension SelectLanguageViewController: SelectLanguageViewDelegate {

}

// MARK: - SelectLanguageModelDelegate

extension SelectLanguageViewController: SelectLanguageModelDelegate {
    
    func modelDidChanged(model: SelectLanguageModelProtocol) {
        self.customView.tableView.reloadData()
    }
    
    func modelDidSelectLanguage(model: SelectLanguageModelProtocol, language: Language?) {
        self.delegate?.viewControllerDidFinishWithSelectedLanguage(viewController: self, language: language)
        
    }

}

// MARK: - SelectLanguageCellDelegate

extension SelectLanguageViewController: SelectLanguageCellDelegate {
    
    func cellDidTapSomeButton(cell: SelectLanguageTableViewCell) {
        
    }
}
