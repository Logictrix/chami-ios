//
//  SelectLanguageView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SelectLanguageViewDelegate: NSObjectProtocol {
    
}

protocol SelectLanguageViewProtocol: NSObjectProtocol {
    
    weak var delegate: SelectLanguageViewDelegate? { get set }
    var tableView: UITableView! { get }
}

class SelectLanguageView: UIView, SelectLanguageViewProtocol{

    class func create() -> SelectLanguageView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? SelectLanguageView else {
            fatalError("Nib \(viewNibName) does not contain SelectLanguage View as first object")
        }
        
        return view
    }
    
    // MARK: - SelectLanguageView interface methods

    weak var delegate: SelectLanguageViewDelegate?
    @IBOutlet weak var tableView: UITableView!

    // add view private properties/outlets/methods here
    
    // MARK: - IBActions
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        // setup view and table view programmatically here
    }
}
