//
//  SelectLanguageDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Kingfisher

class SelectLanguageDataSource: NSObject, UITableViewDataSource {
    
    weak var cellDelegate: SelectLanguageCellDelegate?
    private let model: SelectLanguageModelProtocol
    private let cellReuseId = "SelectLanguageTableViewCell"
    
    init(withModel model: SelectLanguageModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: SelectLanguageTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: SelectLanguageTableViewCell, forItem item: Language) {
        cell.titleLabel.text = item.languageName
        if (item.languageImageUrl != nil) {
            let url = URL(string: item.languageImageUrl!)
            cell.flagImageView.kf.setImage(with: url)
        }
    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? SelectLanguageTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let item = self.model.items[indexPath.row];
        self.configure(cell: cell, forItem: item)
        if item.languageId == self.model.selectedId {
            cell.selectedMarkImageView.isHidden = false
        } else {
            cell.selectedMarkImageView.isHidden = true
        }
        return cell
    }
}
