//
//  SelectLanguageTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SelectLanguageTableViewCell: UITableViewCell {
    
    weak var delegate: SelectLanguageCellDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var flagImageView: UIImageView!
    
    @IBOutlet weak var selectedMarkImageView: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    // MARK: - IBAction
    
    @IBAction func someButtonAction() {
        self.delegate?.cellDidTapSomeButton(cell: self)
    }
}


protocol SelectLanguageCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    func cellDidTapSomeButton(cell: SelectLanguageTableViewCell)
}
