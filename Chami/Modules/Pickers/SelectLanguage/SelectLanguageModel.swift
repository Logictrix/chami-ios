//
//  SelectLanguageModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SelectLanguageModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: SelectLanguageModelProtocol)
    func modelDidSelectLanguage(model: SelectLanguageModelProtocol, language: Language?)

}

protocol SelectLanguageModelProtocol: NSObjectProtocol {
    
    weak var delegate: SelectLanguageModelDelegate? { get set }
    var items: [Language] { get }
    var selectedId: IdType? { get set }
    func changeSelectedLanguage(withRowIndex: Int)

    func loadLanguages(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ()))

}

class SelectLanguageModel: NSObject, SelectLanguageModelProtocol {
    
    override init() {
        self.items = []
        super.init()

    }
    
    
    // MARK: - SelectLanguageModel methods

    weak var delegate: SelectLanguageModelDelegate?
    public private(set) var items: [Language]
    var selectedId: IdType?

    func changeSelectedLanguage(withRowIndex: Int) {
        if self.items.count > withRowIndex {
            let item = self.items[withRowIndex]
            self.selectedId = item.languageId
            self.delegate?.modelDidChanged(model: self)
            self.delegate?.modelDidSelectLanguage(model: self, language: item)
        } else {
            print("\(#function), error: no object at index")

        }

    }
    /** Implement SelectLanguageModel methods here */
    func loadLanguages(success: @escaping (() -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        SettingsService().getAvailableLanguages(success: { [weak self] (languages) in
            guard let strongSelf = self else { return }
            strongSelf.items = languages
            
            success()
//            strongSelf.delegate?.modelDidChanged(model: strongSelf)
            
        }) { (error) in
//            print(error.localizedDescription)
            failure(error as NSError)
        }
    }

    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
