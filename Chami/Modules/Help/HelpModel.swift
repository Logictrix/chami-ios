//
//  HelpModel.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol HelpModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: HelpModelProtocol)
}

protocol HelpModelProtocol: NSObjectProtocol {
    
    weak var delegate: HelpModelDelegate? { get set }
    var items: [HelpAndFaqDataType] { get }
    var accountRole: AccountRole { get set }

}

class HelpModel: NSObject, HelpModelProtocol {
    
    override init() {
        self.items = []
        super.init()

        self.items = self.createItemsData()
    }
    
    var accountRole: AccountRole = AccountManager.shared.currentRole

    func createItemsData() -> [(title: String, description: String, attributedDescription: NSAttributedString?)] {
        //let item1: HelpAndFaqDataType = (title: "How much does a session cost?", description: "There are three types of sessions you can request, each with different pricing.\n\nchami X: Short for chami language exchange, is completely free!\n\n chami +: The maximum hourly rate for chami+ is £20, the rate can vary between £0 - £20 per hour.\n\nChami max: The maximum hourly rate for chami max is £50, the rate can vary between £0 - £50 per hour.\n\nYou will be charged for the first 20 minutes at the beginning of the session, thereafter charged per minute.\nExample:\nTutor rate ￡18 p/h\n\nMinimum charge: ￡6 (for 20 minutes)\n\nAfter first 20 minutes: 30 pence per minute\nTuition cost if the session lasts 21 minutes : ￡6.30\n\nYou can end the session whenever you like but if you end the session within the first 20 minutes you will still be charged for the first 20 minutes. If you don’t show up / cancel after 5 minutes after accepting a session you will be charged a fixed cancellation fee of ￡5.")
        let item1AttributedText = NSAttributedString.loadRTFD(from: "PaymentInfo")
        let item1: HelpAndFaqDataType = ("How much does a session cost?", "", item1AttributedText)
        //let item1: HelpAndFaqDataType = (title: "How much does a session cost?", description: "chami X (Language exchange) : Free\nchami + (Standard tuition) : £0 -£0.42 pence per minute\nchami max (Premium tuition) : £0-£0.83 pence per minute.\n\nYou will be charged for the first 20\nminutes at the beginning of the session,\nthereafter charged per minute.\n\nExample: chami + session for 21 minutes\nTutor rate: £18 (£0.30 per minute)\nMinimum charge (first 20 minutes): £6.00\nTuition cost: £6.30\n\nYou can end the session whenever you\nlike but if you end the session within the\nfirst 20 minutes, you will still be charged\nfor the first 20 minutes. If you don’t\nshow up/cancel after 5 minutes of accepting\na session you will be charged a fixed\ncancellation fee of £ 5.", attributedDescription: nil)
        
        let item2: HelpAndFaqDataType = (title: "Where does the session take place?", description: "This is the best part of learning with chami. ANYWHERE YOU WANT!\n\nTake a stroll along the Thames, explore the British museum or simply choose to meet at your favourite local café, just remember to keep an eye on the metre and end the session when you are done.\n", attributedDescription: nil)
        let item3: HelpAndFaqDataType = (title: "What’s the difference between tutor and learner?", description: "With chami, users have the ability to switch between learning and teaching with a simple touch of a button. Anyone who knows a language and is confident enough to teach can login as a chami + tutor. chami max tutors however will need to go through some checks before they can become a chami max tutor.\n", attributedDescription: nil)

        let item4AttributedText = NSAttributedString.loadRTF(from: "WhoCanBeATutor")
        let item4: HelpAndFaqDataType = ("Who can be a tutor?", "", item4AttributedText)
        //let item4: HelpAndFaqDataType = (title: "Who can be a tutor?", description: "chami +: Anyone who is fluent in a particular language and confident enough to teach.\n\nChami max: Qualified tutors with at least 2 years of teaching experience.\n", attributedDescription: nil)
        
        let item5: HelpAndFaqDataType = (title: "Are tutors chami staff?", description: "No, chami does not employ any tutors as we believe tutors perform at their best when they have the freedom to teach in their own ways. But to ensure chami tutors provide quality teaching to chami learners, we conduct qualification & experience checks for every chami max tutor. Also many of chami max tutors are qualified tutors provided by a reputable teacher training agency with whom chami has a formal partnership.\n", attributedDescription: nil)

        let item6AttributedText = NSAttributedString.loadRTF(from: "Tutors_HowDoIGetPaid")
        let item6: HelpAndFaqDataType = ("[Tutors] How do I get paid?", "", item6AttributedText)

        
        return [item1, item2, item3, item4, item5, item6]
    }
    
    // MARK: - HelpModel methods

    weak var delegate: HelpModelDelegate?
    public private(set) var items: [HelpAndFaqDataType]
    
    /** Implement HelpModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
