//
//  HelpTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HelpTableViewCell: UITableViewCell {
    
    weak var delegate: HelpCellDelegate?
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var logoImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        let screenSize = UIScreen.main.bounds
        let separatorHeight = CGFloat(1) / UIScreen.main.scale
        let additionalSeparator = UIView.init(frame: CGRect(x: 0, y: self.frame.size.height-separatorHeight, width: screenSize.width, height: separatorHeight))
        additionalSeparator.backgroundColor = RGB(228, 228, 228)
        self.addSubview(additionalSeparator)
    }

    // MARK: - IBAction
    
    @IBAction func someButtonAction() {
        self.delegate?.cellDidTapSomeButton(cell: self)
    }
}


protocol HelpCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    func cellDidTapSomeButton(cell: HelpTableViewCell)
}
