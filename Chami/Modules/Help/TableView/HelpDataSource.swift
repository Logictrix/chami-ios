//
//  HelpDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class HelpDataSource: NSObject, UITableViewDataSource {
    
    weak var cellDelegate: HelpCellDelegate?
    private let model: HelpModelProtocol
    private let cellReuseId = "HelpTableViewCell"
    
    init(withModel model: HelpModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: HelpTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: HelpTableViewCell, forItem item: HelpAndFaqDataType) {
        cell.titleLabel.text = item.title
    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? HelpTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let item = self.model.items[indexPath.row];
        self.configure(cell: cell, forItem: item)
        
        if self.model.accountRole == .student {
            cell.logoImage.image = UIImage(named: "help_faqs_scr_arrow_right_yellow")
        } else if self.model.accountRole == .tutor {
            cell.logoImage.image = UIImage(named: "help_faqs_scr_arrow_right_blu")
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        return cell
    }
}
