//
//  HelpView.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol HelpViewDelegate: NSObjectProtocol {
    
}

protocol HelpViewProtocol: NSObjectProtocol {
    
    weak var delegate: HelpViewDelegate? { get set }
    var tableView: UITableView! { get }
}

class HelpView: UIView, HelpViewProtocol{

    class func create() -> HelpView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? HelpView else {
            fatalError("Nib \(viewNibName) does not contain Help View as first object")
        }
        
        return view
    }
    
    // MARK: - HelpView interface methods

    weak var delegate: HelpViewDelegate?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerLabel: UILabel!

    // add view private properties/outlets/methods here
    
    // MARK: - IBActions
        
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        self.headerLabel.text = NSLocalizedString("Do you need help with chami? This is the right place for getting your answers.", comment: "FAQ&Help")
        
        
//        let px = 1 / UIScreen.main.scale
//        let frame = CGRectMake(0, 0, self.tableView.frame.size.width, px)
//        let line = UIView(frame: frame)
//        self.tableView.tableHeaderView = line
//        line.backgroundColor = self.tableView.separatorColor
        
        
        let px = CGFloat(1) / UIScreen.main.scale
        let frame = CGRect(x: 0, y: 0, width: self.tableView.frame.size.width, height: px)
        let line: UIView = UIView(frame: frame)
        self.tableView.tableHeaderView = line
        line.backgroundColor = RGB(228, 228, 228)
    }
}
