//
//  HelpBuilder.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HelpBuilder: NSObject {

    class func viewController() -> HelpViewController {
        let view: HelpViewProtocol = HelpView.create()
        let model: HelpModelProtocol = HelpModel()
        let dataSource = HelpDataSource(withModel: model)
        let router = HelpRouter()
        
        let viewController = HelpViewController(withView: view, model: model, router: router, dataSource: dataSource)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
