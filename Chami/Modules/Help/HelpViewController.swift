//
//  HelpViewController.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias HelpViewControllerType = BaseViewController<HelpModelProtocol, HelpViewProtocol, HelpRouter>

class HelpViewController: HelpViewControllerType, UITableViewDelegate {
    
    private var dataSource: HelpDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: HelpViewProtocol, model: HelpModelProtocol, router: HelpRouter, dataSource: HelpDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        self.navigationItem.title = NSLocalizedString("Help & FAQs", comment: "Help")
        self.addSideMenuLeftButton()
    }
    
    internal required init(withView view: HelpViewProtocol!, model: HelpModelProtocol!, router: HelpRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
    }
    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        let item = self.model.items[indexPath.row]
        
        self.router?.navigateToHelpDetailScreen(from: self, withHelpItem: item)
    }
}

// MARK: - HelpViewDelegate

extension HelpViewController: HelpViewDelegate {

}

// MARK: - HelpModelDelegate

extension HelpViewController: HelpModelDelegate {
    
    func modelDidChanged(model: HelpModelProtocol) {
    }
}

// MARK: - HelpCellDelegate

extension HelpViewController: HelpCellDelegate {
    
    func cellDidTapSomeButton(cell: HelpTableViewCell) {
    }
}
