//
//  HelpRouter.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class HelpRouter: NSObject {
    
    func navigateToHelpDetailScreen(from vc: UIViewController, withHelpItem helpItem: HelpAndFaqDataType) {
        let nextVC = HelpDetailBuilder.viewController()
        nextVC.setHelpItem(helpItem: helpItem)
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}
