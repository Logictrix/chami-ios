//
//  AboutYouModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol AboutYouModelDelegate: NSObjectProtocol {
    
}

protocol AboutYouModelProtocol: NSObjectProtocol {
    
    weak var delegate: AboutYouModelDelegate? { get set }
    
    var aboutString:String? {get set}
    
    func validateFields() -> String?
    func setup(completion: @escaping (_ error:Error?) -> Void)
}

class AboutYouModel: NSObject, AboutYouModelProtocol {
    internal var aboutString: String?

    fileprivate var account = AccountManager.shared.currentUser!

    // MARK: - AboutYouModel methods

    weak var delegate: AboutYouModelDelegate?
    
    
    func validateFields() -> String? {
        let errorMSG = NSLocalizedString("Please enter some information about you", comment: "")
        if self.aboutString?.characters.count == 0 {
            return errorMSG
        }
        if let string = self.aboutString {
            let trimmedString = string.trimmingCharacters(in: .whitespacesAndNewlines)
            if trimmedString.characters.count == 0 {
                return errorMSG
            }
        }
        return nil
    }
    
    
    fileprivate let profileService = ProfileService()
    internal func setup(completion: @escaping (Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: NSError?
            let downloadGroup = DispatchGroup()
            
            // send data to server
            self.account.about = self.aboutString
            
            downloadGroup.enter()
            self.profileService.setProfileInfo(account: self.account, success: {
                downloadGroup.leave()
            }, failure: { (error) in
                resultError = error as NSError?
                downloadGroup.leave()
            })
//            self.profileService.setProfileInfo(account: self.account, success: {(model) in
//                downloadGroup.leave()
//
//                }, failure: { (error) in
//                    resultError = error as NSError?
//                    downloadGroup.leave()
//            })
            
            downloadGroup.wait()
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
    }    
    

}
