//
//  AboutYouView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol AboutYouViewDelegate: NSObjectProtocol {
    
    func viewDidClickContinueButton(view: AboutYouViewProtocol, text:String)
}

protocol AboutYouViewProtocol: NSObjectProtocol {
    
    weak var delegate: AboutYouViewDelegate? { get set }
    weak var aboutTextView: UITextView! { get set }
}

class AboutYouView: UIView, AboutYouViewProtocol{
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var aboutTextView: UITextView!
    @IBOutlet weak var continueButton: UIButton!

    class func create() -> AboutYouView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? AboutYouView else {
            fatalError("Nib \(viewNibName) does not contain AboutYou View as first object")
        }
        
        return view
    }
    
    // MARK: - AboutYouView interface methods

    weak var delegate: AboutYouViewDelegate?
    
    // MARK: - Overrided methods
    func compileSaveButtonTitle(text: String) -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: labelFont,
                                                                 NSAttributedString.Key.foregroundColor: ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString(text, comment: "AboutYouView info Edit"),
                                          attributes: textPartAttributes)
        
        titleLabelText.append(textPart)
        
        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        textPartAttributes[NSAttributedString.Key.foregroundColor] = ColorHelper.yellowAppColor()
        titleLabelText.append(NSAttributedString(string: FontImage.checkbox, attributes: textPartAttributes))
        return titleLabelText
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 20)
        self.titleLabel.textColor = Theme.current.regularColor()
        
        self.descriptionLabel.font = FontHelper.defaultFont(withSize: 18)
        self.aboutTextView.font = FontHelper.defaultFont(withSize: 16)
        
        Theme.current.apply(borderedButton: self.continueButton)
        
        if AccountManager.shared.currentRole == .student {
            self.continueButton.setAttributedTitle(self.compileSaveButtonTitle(text: "You're done! Let's get started "), for: UIControl.State.normal)
        }
    }
    
    // MARK: - IBActions
    
    @IBAction func continueButtonClick(_ sender: Any) {
        self.delegate?.viewDidClickContinueButton(view: self, text:self.aboutTextView.text)
    }
}
