//
//  AboutYouRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AboutYouRouter: NSObject {
    
    func navigateToNextScreen(from vc: UIViewController) {
        if AccountManager.shared.currentRole == .tutor {
            let nextVC = EditLanguagesBuilder.viewController()
            nextVC.isLoginMode = true
            vc.navigationController?.pushViewController(nextVC, animated: true)
        } else {
            NavigationManager.shared.showMainScreen(animated: true)
        }
    }
}
