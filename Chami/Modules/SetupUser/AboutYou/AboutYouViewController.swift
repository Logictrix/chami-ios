//
//  AboutYouViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias AboutYouViewControllerType = BaseViewController<AboutYouModelProtocol, AboutYouViewProtocol, AboutYouRouter>

class AboutYouViewController: AboutYouViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: AboutYouViewProtocol!, model: AboutYouModelProtocol!, router: AboutYouRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let currentUser = AccountManager.shared.currentUser {
            customView.aboutTextView.text = currentUser.about
        }
        
        customView.delegate = self
        model.delegate = self
        self.title = NSLocalizedString("About you", comment: "About you")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.customView.aboutTextView.becomeFirstResponder()
    }
}

// MARK: - AboutYouViewDelegate

extension AboutYouViewController: AboutYouViewDelegate {

    func viewDidClickContinueButton(view: AboutYouViewProtocol, text:String) {
        self.model.aboutString = customView.aboutTextView.text
        if let warning = self.model.validateFields() {
            self.showAlert(text: warning)
            return
        }
        self.setProgressVisible(visible: true)
        self.model.setup {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let msg = error?.localizedDescription  {
                strongSelf.showAlert(error: msg)
                return
            }
            strongSelf.router?.navigateToNextScreen(from: strongSelf)
        }
    }
}

// MARK: - AboutYouModelDelegate

extension AboutYouViewController: AboutYouModelDelegate {
}
