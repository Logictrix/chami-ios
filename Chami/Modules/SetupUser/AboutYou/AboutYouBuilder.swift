//
//  AboutYouBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class AboutYouBuilder: NSObject {

    class func viewController() -> AboutYouViewController {

        let view: AboutYouViewProtocol = AboutYouView.create()
        let model: AboutYouModelProtocol = AboutYouModel()
        let router = AboutYouRouter()
        
        let viewController = AboutYouViewController(withView: view, model: model, router: router)
        return viewController
    }
}
