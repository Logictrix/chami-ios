//
//  SetupCompleteModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SetupCompleteModelDelegate: NSObjectProtocol {
    
}

protocol SetupCompleteModelProtocol: NSObjectProtocol {
    
    weak var delegate: SetupCompleteModelDelegate? { get set }
    func changeOnlineStatus(isOnline: Bool, completion: @escaping (_ isOnline: Bool, _ error:Error?) -> Void)

}

class SetupCompleteModel: NSObject, SetupCompleteModelProtocol {
    
    // MARK: - SetupCompleteModel methods

    weak var delegate: SetupCompleteModelDelegate?
    fileprivate let serviceAccount: AccountServiceProtocol = AccountService()

    /** Implement SetupCompleteModel methods here */
    func changeOnlineStatus(isOnline: Bool, completion: @escaping (_ isOnline: Bool, _ error:Error?) -> Void) {
        self.serviceAccount.changeUserStatus(isOnline: isOnline) { (error) in
            if let error = error {
                completion(false, error)
            } else {
//                guard let strongSelf = self else { return }
//                strongSelf.onlineStatus = !strongSelf.onlineStatus
                completion(isOnline, nil)
            }
        }
    }

    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
