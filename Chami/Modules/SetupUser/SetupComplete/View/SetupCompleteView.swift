//
//  SetupCompleteView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SetupCompleteViewDelegate: NSObjectProtocol {
    
    func viewContinueActionAction(view: SetupCompleteViewProtocol, online: Bool)
}

protocol SetupCompleteViewProtocol: NSObjectProtocol {
    
    weak var delegate: SetupCompleteViewDelegate? { get set }
}

class SetupCompleteView: UIView, SetupCompleteViewProtocol{

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var onlineButton: UIButton!
    @IBOutlet weak var offlineButton: UIButton!
    
    class func create() -> SetupCompleteView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? SetupCompleteView else {
            fatalError("Nib \(viewNibName) does not contain SetupComplete View as first object")
        }
        
        return view
    }
    
    // MARK: - SetupCompleteView interface methods

    weak var delegate: SetupCompleteViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 23)
        self.contentTextView.font = FontHelper.defaultFont(withSize: 17)
        Theme.current.apply(filledButton: self.onlineButton)
        Theme.current.apply(borderedButton: self.offlineButton)
    }
    
    // MARK: - IBActions
    
    @IBAction func onlineButtonAction(_ sender: UIButton) {
        self.delegate?.viewContinueActionAction(view: self, online: true)
    }
    @IBAction func offlineButtonAction(_ sender: UIButton) {
        self.delegate?.viewContinueActionAction(view: self, online: false)
    }
}
