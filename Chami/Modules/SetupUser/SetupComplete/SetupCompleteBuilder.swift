//
//  SetupCompleteBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SetupCompleteBuilder: NSObject {

    class func viewController() -> SetupCompleteViewController {

        let view: SetupCompleteViewProtocol = SetupCompleteView.create()
        let model: SetupCompleteModelProtocol = SetupCompleteModel()
        let router = SetupCompleteRouter()
        
        let viewController = SetupCompleteViewController(withView: view, model: model, router: router)
        return viewController
    }
}
