//
//  SetupCompleteViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias SetupCompleteViewControllerType = BaseViewController<SetupCompleteModelProtocol, SetupCompleteViewProtocol, SetupCompleteRouter>

class SetupCompleteViewController: SetupCompleteViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: SetupCompleteViewProtocol!, model: SetupCompleteModelProtocol!, router: SetupCompleteRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = NSLocalizedString("Complete", comment: "")
        
        customView.delegate = self
        model.delegate = self
    }
    
    func changeOnlineStatusTo(isOnline: Bool) {
        
        self.setProgressVisible(visible: true)
        self.model.changeOnlineStatus(isOnline: isOnline) { [weak self] (isOnlineNow, error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            if let error = error {
//                strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)
                
                //                strongSelf.customView.statusSwitch.isOn = !strongSelf.customView.statusSwitch.isOn
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: nil)
            } else {
                //                strongSelf.model.onlineStatus = strongSelf.customView.statusSwitch.isOn
//                strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)
                strongSelf.gotoHomeScreen(isOnline: isOnlineNow)
            }
        }
        
    }
    
    func gotoHomeScreen(isOnline: Bool) {
        AccountManager.shared.currentUser?.isOnline = isOnline
        
        NavigationManager.shared.showMainScreen(animated: false)
        
//        if isOnline == true {
//            let controller = NavigationManager.shared.currentVisibleController()
//            if controller?.isKind(of: HomeViewController.self) == true {
//                let homeVC = controller as! HomeViewController
//                homeVC.changeOnlineStatusTo(isOnline: true)
//            }
//        }

    }

}

// MARK: - SetupCompleteViewDelegate

extension SetupCompleteViewController: SetupCompleteViewDelegate {

    func viewContinueActionAction(view: SetupCompleteViewProtocol, online: Bool) {
        if online == true {
            if AccountManager.shared.currentUser?.stripeKeyTutor == nil {
                StripeApiManager.shared.showPaymentViewControllerIfNeeded(fromViewController: self, completion: {[weak self] (saved, error) in
                    guard let strongSelf = self else { return }
                    if let error = error {
                        print("================\(error.localizedDescription)")
                        //                        strongSelf.customView.changeViewOnlineStateTo(isOnline: strongSelf.model.onlineStatus)
                    }
                    if saved == true {
                        strongSelf.changeOnlineStatusTo(isOnline: online)
                    }
                })
                
            } else {
                self.changeOnlineStatusTo(isOnline: online)
            }

        } else {
            self.changeOnlineStatusTo(isOnline: false)

        }

        
    }
}

// MARK: - SetupCompleteModelDelegate

extension SetupCompleteViewController: SetupCompleteModelDelegate {
}
