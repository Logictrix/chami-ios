//
//  SetupProfileBuilder.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SetupProfileBuilder: NSObject {

    class func viewController() -> SetupProfileViewController {
        let view: SetupProfileViewProtocol = SetupProfileView.create()
        let model: SetupProfileModelProtocol = SetupProfileModel()
        let dataSource = SetupProfileDataSource(withModel: model)
        let router = SetupProfileRouter()
        
        let viewController = SetupProfileViewController(withView: view, model: model, router: router, dataSource: dataSource)
        viewController.edgesForExtendedLayout = UIRectEdge()
        return viewController
    }
}
