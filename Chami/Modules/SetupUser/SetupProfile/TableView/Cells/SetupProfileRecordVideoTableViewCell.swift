//
//  SetupProfileRecordVideoTableViewCell.swift
//  Chami
//
//  Created by Serg Smyk on 13/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit



protocol SetupProfileRecordVideoTableViewCellDelegate: NSObjectProtocol {
    
    /** Delegate method example */
    func cellDidTapRecordButtonAction(cell: SetupProfileRecordVideoTableViewCell)
    
}

class SetupProfileRecordVideoTableViewCell: UITableViewCell {
    weak var delegate: SetupProfileRecordVideoTableViewCellDelegate?

    //@IBOutlet weak var recorVideoView: UIView!
    @IBOutlet weak var recordVideoTitle: UILabel!
    @IBOutlet weak var recordVideoDescription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.recordVideoTitle.textColor = Theme.current.regularColor()
        self.recordVideoTitle.font = FontHelper.defaultMediumFont(withSize: 15)
        
        self.recordVideoDescription.font = FontHelper.defaultFont(withSize: 15)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func actionRecord(_ sender: Any) {
        self.delegate?.cellDidTapRecordButtonAction(cell: self)
    }
//    @IBAction func recondVideoClick(_ sender: UIButton) {
//        self.delegate?.footerViewDidClickRecordButton(view:self)
//    }
}
