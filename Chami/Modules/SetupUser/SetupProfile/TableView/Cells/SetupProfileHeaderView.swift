//
//  SetupProfileHeaderView.swift
//  Chami
//
//  Created by Serg Smyk on 09/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SetupProfileHeaderDelegate: NSObjectProtocol {
    
    func headerViewDidClickSelectImageButton(view: SetupProfileHeaderView)

}

class SetupProfileHeaderView: GradientView {

    weak var delegate: SetupProfileHeaderDelegate?
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    
    
    override func awakeFromNib() {
        self.backgroundColor = Theme.current.regularColor()
        self.imageView.layer.cornerRadius = min(self.imageView.frame.size.width, self.imageView.frame.size.height) / 2
        
        let titleString = NSLocalizedString("Change profile picture", comment: "Register")
        let titleAttributedString = titleString.underline(font: FontHelper.defaultMediumFont(withSize: UI.formCellFontSize), color: Theme.current.regularColor())
        self.selectButton.titleLabel!.attributedText = titleAttributedString
        self.selectButton.setAttributedTitle(titleAttributedString, for: UIControl.State.normal)
        
        if (AccountManager.shared.currentUser?.role == .student) {
            
            let color0 = UIColor(red: 40.0/255.0, green: 168.0/255.0, blue: 246.0/255.0, alpha: 1.0)
            let color1 = UIColor(red: 25.0/255.0, green: 224.0/255.0, blue: 240.0/255.0, alpha: 1.0)
            let color2 = UIColor(red: 25.0/255.0, green: 224.0/255.0, blue: 240.0/255.0, alpha: 1.0)
            //let color2 = UIColor(red: 12.0/255.0, green: 164.0/255.0, blue: 246.0/255.0, alpha: 1.0)
            self.colors = [color0, color1, color2]
        } else {
            self.colors = [Theme.current.regularColor().lighterColor(), Theme.current.regularColor().darkerColor()]
        }
        self.imageView.image = Theme.current.defaultUserImage()
    }
    
    @IBAction func selectButtonAction(_ sender: UIButton) {
        self.delegate?.headerViewDidClickSelectImageButton(view:self)
    }
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

}
