//
//  SetupProfileFooterView.swift
//  Chami
//
//  Created by Serg Smyk on 09/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

//protocol FormCellProtocol: NSObjectProtocol {
//
//    weak var delegate: FormCellDelegate? { get set }
//
//    var field: FormField? { get set }
//
//    func setValidationState(_ state: ValidationState)
//    func forceEndEditing()
//    func activate()
//
//}
//
protocol SetupProfileFooterDelegate: NSObjectProtocol {
    func footerViewDidClickContinueButton(view: SetupProfileFooterView)
}


class SetupProfileFooterView: UIView {

    weak var delegate: SetupProfileFooterDelegate?
    
    
    @IBOutlet weak var continueButton: UIButton!
    
    
    func compileSaveButtonTitle(text: String) -> NSAttributedString {
        let titleLabelText = NSMutableAttributedString()
        
        let labelFont = FontHelper.defaultMediumFont(withSize: UI.buttonFontSize)
        var textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: labelFont,
                                                                 NSAttributedString.Key.foregroundColor: ColorHelper.regularColor()]
        let textPart = NSAttributedString(string: NSLocalizedString(text, comment: "Profile info Edit"),
                                          attributes: textPartAttributes)
        
        titleLabelText.append(textPart)
        
        textPartAttributes[NSAttributedString.Key.font] = FontHelper.awesomeFont(withSize: UI.buttonFontSize)
        textPartAttributes[NSAttributedString.Key.foregroundColor] = ColorHelper.yellowAppColor()
        titleLabelText.append(NSAttributedString(string: FontImage.checkbox, attributes: textPartAttributes))
        return titleLabelText
    }
    
    override func awakeFromNib() {
        
        Theme.current.apply(borderedButton: self.continueButton)
        
        let account = AccountManager.shared.currentUser
        if (account?.hasCompletedProfile(withRole: AccountManager.shared.currentRole))! {
            self.continueButton.setAttributedTitle(self.compileSaveButtonTitle(text: "Save changes "), for: UIControl.State.normal)
            
        }
//        else if AccountManager.shared.currentRole == .student {
//            self.continueButton.setAttributedTitle(self.compileSaveButtonTitle(text: "You're done! Let's get started "), for: UIControlState.normal)
//        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        var imageInsets = self.continueButton.imageEdgeInsets
        imageInsets.left = self.bounds.size.width - 50
        self.continueButton.imageEdgeInsets = imageInsets

    }


    @IBAction func continueClick(_ sender: UIButton) {
        self.delegate?.footerViewDidClickContinueButton(view:self)
    }
}
