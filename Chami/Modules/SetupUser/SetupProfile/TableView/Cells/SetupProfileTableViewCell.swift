//
//  SetupProfileTableViewCell.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit


//protocol FormCellProtocol: NSObjectProtocol {
//    
//    weak var delegate: FormCellDelegate? { get set }
//    
//    var field: FormField? { get set }
//    
//    func setValidationState(_ state: ValidationState)
//    func forceEndEditing()
//    func activate()
//    
//}
//
//protocol FormCellDelegate: NSObjectProtocol {
//    
//    func cell<CellType: FormCellProtocol>(_ cell: CellType, didEnterString string: String?) where CellType: UITableViewCell
//}


class SetupProfileTableViewCell: UITableViewCell {
    
    weak var delegate: SetupProfileCellDelegate?
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var editButton: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.titleLabel.textColor = Theme.current.regularColor()
        self.titleLabel.font = FontHelper.defaultMediumFont(withSize: 15)
    }

    // MARK: - IBAction
    
    @IBAction func someButtonAction() {
        self.delegate?.cellDidTapSomeButton(cell: self)
    }
}


protocol SetupProfileCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    func cellDidTapSomeButton(cell: SetupProfileTableViewCell)
}
