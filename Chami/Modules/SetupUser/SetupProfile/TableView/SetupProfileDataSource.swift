//
//  SetupProfileDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
//import SetupProfileHeaderView
import Kingfisher

class SetupProfileDataSource: NSObject, UITableViewDataSource {
    
    
    weak var cellDelegate: SetupProfileCellDelegate?
    weak var cellDelegate2: SetupProfileRecordVideoTableViewCellDelegate?

    private let model: SetupProfileModelProtocol
    private let cellReuseId = "SetupProfileTableViewCell"
    private let recordCellReuseId = "SetupProfileRecordVideoTableViewCell"
    
    init(withModel model: SetupProfileModelProtocol) {
        self.model = model
    }

    func setupTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: SetupProfileTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
        
        let recordCellNib = UINib(nibName: String(describing: SetupProfileRecordVideoTableViewCell.self), bundle: nil)
        tableView.register(recordCellNib, forCellReuseIdentifier: recordCellReuseId)
    }
    
    // MARK: - Private methods
    
    // MARK: - UITableViewDataSource
    internal func numberOfSections(in tableView: UITableView) -> Int {
        return 2
//        if AccountManager.shared.currentRole == .tutor {
//            return 2
//        }
//        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 1 {
            return 1
        }
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 1 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: recordCellReuseId) as? SetupProfileRecordVideoTableViewCell else {
                fatalError()
            }
            cell.delegate = cellDelegate2
            

            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? SetupProfileTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let item:SetupProfileItem = self.model.items[indexPath.row];
        cell.titleLabel.text = item.title
        cell.descriptionLabel.text = item.value
        cell.iconImageView.image = item.icon
        
        return cell
    }
}
