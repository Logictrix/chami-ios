//
//  SetupProfileRouter.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class SetupProfileRouter: NSObject {
    
    func navigateToNextScreen(from vc: UIViewController) {
//        let nextVC = AboutYouBuilder.viewController()
//        vc.navigationController?.pushViewController(nextVC, animated: true)

        let nextVC = AboutYouBuilder.viewController()
        vc.navigationController?.pushViewController(nextVC, animated: true)

//        if AccountManager.shared.currentRole == .tutor {
//            let nextVC = AboutYouBuilder.viewController()
//            vc.navigationController?.pushViewController(nextVC, animated: true)
//        } else {
//            NavigationManager.shared.showMainScreen(animated: true)
//        }
    }
//    func navigateToHelpDetailScreen(from vc: UIViewController, withBackgroundColor backgroundColor: UIColor) {
//        let nextVC = HelpDetailBuilder.viewController()
//        
//        vc.navigationController?.pushViewController(nextVC, animated: true)
//    }
    
    // Navigation example method
//    func navigateToSomeScreen(from vc: UIViewController, withBackgroundColor backgroundColor: UIColor) {
//        // Create new screen. Here you should use another Builder to create it.
//        let someScreenVC = UIViewController()
//        // Set passed parameters
//        someScreenVC.view.backgroundColor = backgroundColor
//        
//        if UI_USER_INTERFACE_IDIOM() == .pad {
//            someScreenVC.modalPresentationStyle = .pageSheet
//            someScreenVC.modalTransitionStyle = .crossDissolve
//            
//            vc.navigationController?.present(someScreenVC, animated: true, completion: nil)
//        } else {
//            vc.navigationController?.pushViewController(someScreenVC, animated: true)
//        }
//    }
}
