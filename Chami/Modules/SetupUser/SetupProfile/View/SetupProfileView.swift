//
//  SetupProfileView.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol SetupProfileViewDelegate: NSObjectProtocol {
    func viewDidClickSelectImageButton(view: SetupProfileViewProtocol)
    func viewDidClickContinueButton(view: SetupProfileViewProtocol)
}

protocol SetupProfileViewProtocol: NSObjectProtocol {
    
    weak var delegate: SetupProfileViewDelegate? { get set }
    var tableView: UITableView! { get }
    func setupTableView()
}

class SetupProfileView: UIView, SetupProfileViewProtocol{

    class func create() -> SetupProfileView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? SetupProfileView else {
            fatalError("Nib \(viewNibName) does not contain SetupProfile View as first object")
        }
        
        return view
    }
    
    // MARK: - SetupProfileView interface methods

    weak var delegate: SetupProfileViewDelegate?
    @IBOutlet weak var tableView: UITableView!

    // add view private properties/outlets/methods here
    
    // MARK: - IBActions

    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    func setupTableView() {
        
        if tableView.tableHeaderView == nil {
            let headerView = SetupProfileHeaderView.loadFromXib(owner: nil) as! SetupProfileHeaderView
            tableView.tableHeaderView = headerView
            headerView.delegate = self
        
            let footerView = SetupProfileFooterView.loadFromXib(owner: nil) as! SetupProfileFooterView
            tableView.tableFooterView = footerView
            footerView.delegate = self
        }
    }
}


extension SetupProfileView: SetupProfileHeaderDelegate, SetupProfileFooterDelegate {
    internal func headerViewDidClickSelectImageButton(view: SetupProfileHeaderView) {
        self.delegate?.viewDidClickSelectImageButton(view: self)
    }
    internal func footerViewDidClickContinueButton(view: SetupProfileFooterView) {
        self.delegate?.viewDidClickContinueButton(view: self)
    }
}

extension SetupProfileView {
    func selectDateWithFinishBlock(initialDate:Date, completion: @escaping (_ result:Date) -> Void) {
        let datePicker = ApplicationPickerView.createWithType(type: ApplicationPickerType.dateType)
        datePicker.titleLabel.text = "Date of birth"
        datePicker.datePickerView.maximumDate = Calendar.current.date(byAdding: .year, value: -18, to: Date())
        datePicker.datePickerView.minimumDate = Calendar.current.date(byAdding: .year, value: -100, to: Date())
        if initialDate.timeIntervalSince(datePicker.datePickerView.maximumDate!) < 0 {
            datePicker.datePickerView.date = initialDate
        } else {
            datePicker.datePickerView.date = datePicker.datePickerView.maximumDate!
        }
        datePicker.showInView(view: self, animated: true)
        datePicker.doneActionBlock = ({ [weak datePicker] in
            if let date = datePicker?.datePickerView.date {
                completion(date)
            }
        })
    }
    func selectNationalityWithFinishBlock(nationalities:[String], initialIndex:Int, completion: @escaping (_ index:Int) -> Void) {
        let nationalityPicker = ApplicationPickerView.createWithType(type: ApplicationPickerType.defaultType)
        nationalityPicker.titleLabel.text = "Nationality"
        nationalityPicker.pickerTitles = nationalities
        nationalityPicker.selectedIndex = initialIndex
        nationalityPicker.showInView(view: self, animated: true)
        nationalityPicker.doneActionBlock = ({ [weak nationalityPicker] in
            if let index = nationalityPicker?.selectedIndex {
                completion(index)
            }
        })
    }
    
    func selectNativeLanguageWithFinishBlock(languages:[String], initialIndex:Int, completion: @escaping (_ index:Int) -> Void) {
        let nationalityPicker = ApplicationPickerView.createWithType(type: ApplicationPickerType.defaultType)
        nationalityPicker.titleLabel.text = NSLocalizedString("Native language", comment: "")
        nationalityPicker.pickerTitles = languages
        nationalityPicker.selectedIndex = initialIndex
        nationalityPicker.showInView(view: self, animated: true)
        nationalityPicker.doneActionBlock = ({ [weak nationalityPicker] in
            if let index = nationalityPicker?.selectedIndex {
                completion(index)
            }
        })
    }
    
    func enterPhoneNumberWithFinishBlock(controller:UIViewController, initialPhoneNumber:String, completion: @escaping (_ resultPhoneNumber:String) -> Void) {
        let alert = UIAlertController(title: "Phone number", message: "", preferredStyle: UIAlertController.Style.alert)
        alert.addTextField { (textField : UITextField!) in
            textField.placeholder = "Enter phone number"
            textField.keyboardType = UIKeyboardType.phonePad
        }
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: { action in }))
        //weak let weakAlert = alert
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { [weak alert] action in
            if let textField = alert?.textFields?.first, let text = textField.text {
                completion(text)
            }
        }))
        
        controller.present(alert, animated: true, completion: nil)
    }
    func selectGenderWithFinishBlock(genders:[String], initialGender:Int, completion: @escaping (_ index:Int) -> Void) {
        let genderPicker = ApplicationPickerView.createWithType(type: ApplicationPickerType.defaultType)
        genderPicker.titleLabel.text = "Gender"
        genderPicker.pickerTitles = genders
        genderPicker.selectedIndex = initialGender
        genderPicker.showInView(view: self, animated: true)
        genderPicker.doneActionBlock = ({ [weak genderPicker] in
            if let index = genderPicker?.selectedIndex {
                completion(index)
            }
        })
    }
}
