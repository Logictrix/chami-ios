//
//  SetupProfileModel.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Kingfisher
import PKHUD

enum SetupProfileModelType : Int {
    case birthDay
    case nationality
    case phone
    case gender
    case language
}

enum SetupProfileGenderType : String {
    case notDefined = ""
    case male = "Male"
    case female = "Female"
}

typealias SetupProfileItem = (type: SetupProfileModelType, icon:UIImage?, title:String, value:String?)
typealias SetupProfileItemArray = [SetupProfileItem]



protocol SetupProfileModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: SetupProfileModelProtocol)
}

protocol SetupProfileModelProtocol: NSObjectProtocol {
    weak var delegate: SetupProfileModelDelegate? { get set }
    var items: SetupProfileItemArray { get }
    
    var profileImage:UIImage? {get set}
    //var newProfileImage:UIImage? {get set}
    var dateOfBirth:Date? {get set}
    var nationalityInfo:String? {get set}
    var phoneNumber:String? {get set}
    var gender:SetupProfileGenderType! {get set}
    var localRecordedProfilePreviewVideoURL:NSURL? {get set}
    var profilePreviewVideoURL:NSURL? {get set}
    var nativeLanguage:Language? {get set}
    var isLoginMode: Bool! {get set}

    func genders() -> [SetupProfileGenderType]
    func countries() -> [String]
    func validateFields() -> String?
    
    func setup(completion: @escaping (_ error:Error?) -> Void)
    func sendDataToServer(completion: @escaping (_ error:Error?) -> Void)
    
    //var isLoginMode: Bool! {get set}
    func getAvailableLanguages(success: @escaping (([Language]) -> ()), failure: @escaping ((_ error: NSError) -> ()))
}




class SetupProfileModel: NSObject, SetupProfileModelProtocol {
    internal var isLoginMode: Bool! = true {
        didSet {
            self.onChanged()
        }
    }
    
    fileprivate var availableLanguages: [Language] = []
    func getAvailableLanguages(success: @escaping (([Language]) -> ()), failure: @escaping ((_ error: NSError) -> ())) {
        SettingsService().getAvailableLanguages(success: { [weak self] (languages) in
            guard let strongSelf = self else { return }
            strongSelf.availableLanguages = languages
            
            success(strongSelf.availableLanguages)
        }) { (error) in
            failure(error as NSError)
        }
    }
    

    fileprivate let profileService = ProfileService()
    fileprivate var account = AccountManager.shared.currentUser!
    
    fileprivate var hasChanges:Bool = false
    
    override init() {
        super.init()
        
        self.account = AccountManager.shared.currentUser!
        
        if (self.account.imageUrl != nil && self.account.imageUrl.characters.count > 0) {
            if let url = URL.init(string: self.account.imageUrl){
                HUD.show(.progress)
                ImageDownloader.default.downloadImage(with: url, options: [], progressBlock: nil) { [weak self]
                    (image, error, url, data) in
                    HUD.hide(animated: true)
                    guard let strongSelf = self else { return }
                    if (image != nil) {
                        let prevState = strongSelf.hasChanges
                        let prevStateImage = strongSelf.profileImageChanged
                        
                        strongSelf.profileImage = image
                        
                        strongSelf.hasChanges = prevState
                        strongSelf.profileImageChanged = prevStateImage
                    }
                }
            }
        }
        
        self.dateOfBirth = self.account.birthDate
        self.nationalityInfo = self.account.nationality
        self.phoneNumber = self.account.phoneNumber
        if (self.account.gender != nil) {
            self.gender = SetupProfileGenderType.init(rawValue: self.account.gender)
        } else {
            self.gender = .notDefined
        }
        
        self.nativeLanguage = self.account.nativeLanguage
        
        
        
        
        if (self.account.introVideoUrl != nil) {
            self.profilePreviewVideoURL = NSURL.init(string: self.account.introVideoUrl)
        }
        
        self.hasChanges = false
        
    }
    internal func setup(completion: @escaping (Error?) -> Void) {
        if self.hasChanges == false  {
            completion(nil)
            return
        }
        self.sendDataToServer(completion: completion)
    }
    
    internal func sendDataToServer(completion: @escaping (Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: NSError?
            let downloadGroup = DispatchGroup()
            
            // upload image
            if self.profileImageChanged == true {
                if let image = self.profileImage, let data = image.pngData() {
                    downloadGroup.enter()
                    DataStorageManager.shared.upload(data: data) { (error, name) in
                        if (error != nil) {
                            resultError = error as NSError?
                        } else {
                            self.account.imageUrl = name
                        }
                        
                        downloadGroup.leave()
                    }
                }
            }
            downloadGroup.wait()
            // upload video
            if let urlPath = self.localRecordedProfilePreviewVideoURL?.path {
                downloadGroup.enter()
                DataStorageManager.shared.upload(filepath: urlPath, completion: { (error, name) in
                    if (error != nil) {
                        resultError = error as NSError?
                    } else {
                        self.account.introVideoUrl = name
                    }

                    downloadGroup.leave()
                })
            }
            downloadGroup.wait()
            
            // send data to server
            self.account.birthDate = self.dateOfBirth
            self.account.nationality = self.nationalityInfo
            self.account.phoneNumber = self.phoneNumber
            self.account.gender = self.gender.rawValue
            self.account.nativeLanguage = self.nativeLanguage
            
            downloadGroup.enter()
            self.profileService.setProfileInfo(account: self.account, success: {
              //  guard let strongSelf = self else { return }
                self.hasChanges = false
                AccountManager.shared.currentUser = self.account
                downloadGroup.leave()
                
            }, failure: { (error) in
                resultError = error as NSError?
                downloadGroup.leave()
            })
            
            downloadGroup.wait()
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
    }

    weak var delegate: SetupProfileModelDelegate?
    var items: SetupProfileItemArray {
        get {
            return self.createItems()
        }
    }
    fileprivate var profileImageChanged:Bool = false
    var profileImage:UIImage?{
        didSet {
            self.profileImageChanged = true
            self.onChanged()
        }
    }
    var dateOfBirth:Date? {
        didSet {
            self.onChanged()
        }
    }
    var nationalityInfo:String? {
        didSet {
            self.onChanged()
        }
    }
    var phoneNumber:String? {
        didSet {
            self.onChanged()
        }
    }
    var gender:SetupProfileGenderType! = .notDefined {
        didSet {
            self.onChanged()
        }
    }
    var profilePreviewVideoURL:NSURL?
//    {
//        didSet {
//            self.onChanged()
//        }
//    }
    var localRecordedProfilePreviewVideoURL:NSURL? {
        didSet {
            self.onChanged()
        }
    }

    
    var nativeLanguage:Language? {
        didSet {
            self.onChanged()
        }
    }
    
    func onChanged() {
        self.hasChanges = true
        self.delegate?.modelDidChanged(model: self)

    }

    func validateFields() -> String? {
        if profileImage == nil {
            return NSLocalizedString("Please select profile image", comment: "")
        }
//        if nationalityInfo == nil {
//            return NSLocalizedString("Please select nationality", comment: "")
//        }
//        if dateOfBirth == nil {
//            return NSLocalizedString("Please select date of birth", comment: "")
//        }
//        if let phone = self.phoneNumber {
//            if phone.isPhoneNumber() == false {
//                return NSLocalizedString("Please enter a valid phone number", comment: "")
//            }
//        } else {
//            return NSLocalizedString("Please enter your phone number", comment: "")
//        }
        if self.account.role == .student
        {
            if self.nativeLanguage == nil {
                return NSLocalizedString("Please select native language", comment: "")
            }
        }
        if ((self.account.introVideoUrl == nil || self.account.introVideoUrl.characters.count == 0)
            && self.localRecordedProfilePreviewVideoURL == nil) {
            return NSLocalizedString("Please record a preview video", comment: "")
        }
        
        return nil
    }
    
    private func createItems() -> SetupProfileItemArray {
        //let dateFormatter = DateFormatter()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd, yyyy"

        var dateString: String? = nil
        if let dateOfBirth = self.dateOfBirth {
            dateString = dateFormatter.string(from: dateOfBirth)
            
        }
        var arrayOfItems:SetupProfileItemArray = []
        arrayOfItems.append((type: .birthDay, icon:UIImage(named: "reg_scr_icon_date"), title:NSLocalizedString("Date of birth", comment: ""), value: dateString))
        arrayOfItems.append((type: .nationality, icon:UIImage(named: "reg_scr_icon_nat"), title:NSLocalizedString("Nationality", comment: ""), value: self.nationalityInfo))
        arrayOfItems.append((type: .phone, icon:UIImage(named: "reg_scr_icon_phone"), title:NSLocalizedString("Phone number", comment: ""), value: self.phoneNumber))
        arrayOfItems.append((type: .gender, icon:UIImage(named: "profile_inf_scr_icon_gender"), title:NSLocalizedString("Gender", comment: ""), value: self.gender?.rawValue))
        if self.account.role == .student
        {
            arrayOfItems.append((type: .language, icon:UIImage(named: "reg_scr_icon_nat"), title:NSLocalizedString("Native language", comment: ""), value: self.nativeLanguage?.languageName))
        }
        return arrayOfItems
    }
    func genders() -> [SetupProfileGenderType] {
        return [.notDefined, .male, .female]
    }
    func countries() -> [String] {
        let locale = Locale.current
        let countries = Locale.isoRegionCodes.map {
            locale.localizedString(forRegionCode: $0)!
            }.sorted()
        var result = countries
        result.insert("", at: 0)
        return result
    }
}
