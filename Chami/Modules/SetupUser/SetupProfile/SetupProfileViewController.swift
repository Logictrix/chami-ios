//
//  SetupProfileViewController.swift
//  Chami
//
//  Created by Igor Markov on 1/30/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias SetupProfileViewControllerType = BaseViewController<SetupProfileModelProtocol, SetupProfileViewProtocol, SetupProfileRouter>

class SetupProfileViewController: SetupProfileViewControllerType, UITableViewDelegate {
    
//    var hasSaveOption: Bool! = true {
//        didSet {
//            self.dataSource.setupTableView(tableView: self.customView.tableView)
//        }
//    }
    
    private var dataSource: SetupProfileDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: SetupProfileViewProtocol, model: SetupProfileModelProtocol, router: SetupProfileRouter, dataSource: SetupProfileDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate2 = self
    }
    
    internal required init(withView view: SetupProfileViewProtocol!, model: SetupProfileModelProtocol!, router: SetupProfileRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Profile information", comment: "Registration")
        
//        if self.model.isLoginMode == true {
//            //self.navigationItem.leftBarButtonItem = UIBarButtonItem.init(customView: UIView.init())
//        } else {
//            self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Save", style: .plain, target: self, action: #selector(saveAndBack))
//        }
        
        self.customView.delegate = self
        
        self.connectTableViewDependencies()
        
    }
//    func saveAndBack() {
//        self.saveData {[weak self] (saved) in
//            if saved == true {
//                guard let strongSelf = self else { return }
////                _ = strongSelf.navigationController?.popViewController(animated: true)
//                _ = strongSelf.navigationController?.popToRootViewController(animated: true)
//            }
//        }
//    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.customView.setupTableView()
    }
    
    private func connectTableViewDependencies() {
        self.customView.tableView.delegate = self
        self.dataSource.setupTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        if indexPath.section == 1 {
            self.showChooseVideoActionSheet(completion: { [weak self] (url) in
                guard let strongSelf = self else { return }
                strongSelf.model.localRecordedProfilePreviewVideoURL = url

            })
            return
        }
        let view:SetupProfileView = self.view as! SetupProfileView
        
        //let type = SetupProfileModelType(rawValue: indexPath.row)
        guard let modelType = SetupProfileModelType(rawValue: indexPath.row) else {
            return
        }
        switch modelType {
        case .birthDay:
            view.selectDateWithFinishBlock(initialDate: self.model.dateOfBirth ?? Date(), completion: {[weak self] (date:Date) in
                self?.model.dateOfBirth = date
            })
            break
        case .nationality:
            view.selectNationalityWithFinishBlock(nationalities: self.model.countries(), initialIndex: 0, completion: {[weak self] (index:Int) in
                self?.model.nationalityInfo = self?.model.countries()[index]
            })
            

            break
        case .phone:
            view.enterPhoneNumberWithFinishBlock(controller: self, initialPhoneNumber: "", completion: {[weak self] (result:String) in
                self?.model.phoneNumber = result
            })
            break
        case .gender:
            let genders = self.model.genders()
            let gendersStrings = genders.map({ gender -> String in
                return gender.rawValue
            })
            let index = self.model.genders().index(of: self.model.gender)
            view.selectGenderWithFinishBlock(genders: gendersStrings, initialGender:index!, completion: {[weak self] (index:Int) in
                self?.model.gender = genders[index]
            })
            break
        case .language:
            self.setProgressVisible(visible: true)
            self.model.getAvailableLanguages(success: {[weak self] (langs) in
                guard let strongSelf = self else { return }
                strongSelf.setProgressVisible(visible: false)
            
                let langsTitles = langs.map({ $0.languageName })
                view.selectNativeLanguageWithFinishBlock(languages: langsTitles, initialIndex: 0, completion: {[weak self] (index:Int) in
                    guard let strongSelf = self else { return }
                    strongSelf.model.nativeLanguage = langs[index]
                })
                }, failure: {[weak self] (error) in
                    guard let strongSelf = self else { return }
                    strongSelf.setProgressVisible(visible: false)
                    AlertManager.showError(withMessage: error.localizedDescription, onController: strongSelf)
            })
            



            break
        }
    }
    
    func saveData(completion: @escaping (_ saved: Bool) -> Void) {
        if let warning = self.model.validateFields() {
            self.showAlert(text: warning)
            completion(false)
            return
        }
        self.setProgressVisible(visible: true)
        self.model.setup {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let msg = error?.localizedDescription  {
                strongSelf.showAlert(error: msg)
                completion(false)
                return
            }
            completion(true)
        }
    }
}

//extension SetupProfileViewController: SelectLanguageViewControllerDelegate {
//    func viewControllerDidFinishWithSelectedLanguage(viewController: UIViewController , language: Language?) {
//        _ = self.navigationController?.popToViewController(self, animated: true)
//        self.model.nativeLanguage = language
//    }
//}

extension SetupProfileViewController: SetupProfileRecordVideoTableViewCellDelegate {
    
    func cellDidTapRecordButtonAction(cell: SetupProfileRecordVideoTableViewCell) {
        self.showChooseVideoActionSheet(completion: { [weak self] (url) in
            guard let strongSelf = self else { return }
            strongSelf.model.localRecordedProfilePreviewVideoURL = url
            
        })

    }
}

extension SetupProfileViewController: SetupProfileViewDelegate {

    func viewDidClickSelectImageButton(view: SetupProfileViewProtocol) {
        self.showChoosePhotoActionSheet { [weak self] (image) in
            guard let strongSelf = self else { return }
            strongSelf.model.profileImage = self?.resizeImage(image: image, newWidth: 200)
        }
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage? {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage
    }
    
    func viewDidClickContinueButton(view: SetupProfileViewProtocol) {
        self.saveData {[weak self] (saved) in
            if saved == true {
                guard let strongSelf = self else { return }
                if strongSelf.model.isLoginMode == false {
                    _ = strongSelf.navigationController?.popViewController(animated: true)
                } else {
                    strongSelf.router?.navigateToNextScreen(from: strongSelf)
                }
            }
        }
    }
}

// MARK: - SetupProfileModelDelegate

extension SetupProfileViewController: SetupProfileModelDelegate {
    
    func modelDidChanged(model: SetupProfileModelProtocol) {
        
        if let image = self.model.profileImage {
            let headerView = self.customView.tableView.tableHeaderView as! SetupProfileHeaderView
            headerView.imageView.image = image
        }
        
//        if self.model.isLoginMode == false {
//            self.customView.tableView.tableFooterView = nil
//        }
        
        self.customView.tableView.reloadData()
    }
}

