//
//  FreeCreditsView.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol FreeCreditsViewDelegate: NSObjectProtocol {
    
}

protocol FreeCreditsViewProtocol: NSObjectProtocol {
    
    weak var delegate: FreeCreditsViewDelegate? { get set }
    var logoImageView: UIImageView! { get }
    var titleLabel: UILabel! { get }

}

class FreeCreditsView: UIView, FreeCreditsViewProtocol{
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!

    class func create() -> FreeCreditsView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? FreeCreditsView else {
            fatalError("Nib \(viewNibName) does not contain FreeCredits View as first object")
        }
        
        return view
    }
    
    // MARK: - FreeCreditsView interface methods

    weak var delegate: FreeCreditsViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - IBActions
    
}
