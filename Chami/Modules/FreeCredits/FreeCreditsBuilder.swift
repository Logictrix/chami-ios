//
//  FreeCreditsBuilder.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class FreeCreditsBuilder: NSObject {

    class func viewController() -> FreeCreditsViewController {

        let view: FreeCreditsViewProtocol = FreeCreditsView.create()
        let model: FreeCreditsModelProtocol = FreeCreditsModel()
        let router = FreeCreditsRouter()
        
        let viewController = FreeCreditsViewController(withView: view, model: model, router: router)
        return viewController
    }
}
