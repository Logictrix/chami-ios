//
//  FreeCreditsModel.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit


protocol FreeCreditsModelDelegate: NSObjectProtocol {
    
}

protocol FreeCreditsModelProtocol: NSObjectProtocol {
    
    weak var delegate: FreeCreditsModelDelegate? { get set }
    var accountRole: AccountRole { get set }

}

class FreeCreditsModel: NSObject, FreeCreditsModelProtocol {
    
    // MARK: - FreeCreditsModel methods

    weak var delegate: FreeCreditsModelDelegate?
    var accountRole: AccountRole = AccountManager.shared.currentRole

    /** Implement FreeCreditsModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
