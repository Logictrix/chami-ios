//
//  RateSessionModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RateSessionModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: RateSessionModelProtocol)
}

protocol RateSessionModelProtocol: NSObjectProtocol {
    
    weak var delegate: RateSessionModelDelegate? { get set }
    var items: [SessionSkillDataType] { get set }
    var session: Session! { get set }
    var accountRole: AccountRole { get set }
    var currentMeetUpSession: MeetUpSession! { get set }
    
    func sendFeedback(completion: @escaping (_ error:Error?) -> Void)
    func updateSession(completion: @escaping (_ error:Error?) -> Void)

}

class RateSessionModel: NSObject, RateSessionModelProtocol {
    
    var session: Session!
    var accountRole: AccountRole = AccountManager.shared.currentRole
    var currentMeetUpSession: MeetUpSession!
    
    
    fileprivate let serviceSession: SessionServiceProtocol = SessionService()

    override init() {
        self.items = []
        super.init()
        if self.accountRole == .tutor {
            self.items = self.createItems()
        }
    }
    
    func sendFeedback(completion: @escaping (_ error:Error?) -> Void) {
        self.session.studentsRates = self.items
        
        if self.accountRole == .student {
            self.serviceSession.sendSessionStudentFeedback(session: self.session, success: { (session) in
                completion(nil)
            }, failure: { (error) in
                completion(error)

            })

        } else {
            self.serviceSession.sendSessionTutorFeedback(session: self.session, success: { (session) in
                completion(nil)
            }, failure: { (error) in
                completion(error)
            })

        }
        
    }

    func updateSession(completion: @escaping (_ error:Error?) -> Void) {
        self.session.studentsRates = self.items
        self.serviceSession.getSessionResult(session: self.session, success: { (session) in
            //let tempPartnerId = self.session.tutorsId
            self.session = session
            //self.session.tutorsId = tempPartnerId
            completion(nil)
        }) { (error) in
            completion(error)

        }
    }

    
    func createItems() -> [SessionSkillDataType] {
        let sessionSkillSpeaking = (skillName: NSLocalizedString("Speaking", comment:"Speaking - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_speaking", isEditable: false)
        let sessionSkillListening = (skillName: NSLocalizedString("Listening", comment:"Listening - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_listening", isEditable: false)
        let sessionSkillReading = (skillName: NSLocalizedString("Reading", comment:"Reading - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_reading", isEditable: false )
        let sessionSkillWriting = (skillName: NSLocalizedString("Writing", comment:"Writing - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_writing" , isEditable: false)
        let sessionSkillPronunciation = (skillName: NSLocalizedString("Pronunciation", comment:"Pronunciation - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_pron", isEditable: false )
        let sessionSkillAttitudeToLearning = (skillName: NSLocalizedString("Attitude to Learning", comment:"Attitude to Learning - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_attit", isEditable: false )

        return [sessionSkillSpeaking, sessionSkillListening, sessionSkillReading, sessionSkillWriting, sessionSkillPronunciation, sessionSkillAttitudeToLearning]
    }
    

    // MARK: - RateSessionModel methods

    weak var delegate: RateSessionModelDelegate?
    var items: [SessionSkillDataType]
    
    /** Implement RateSessionModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
