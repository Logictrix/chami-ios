//
//  RateSessionView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Cosmos

protocol RateSessionViewDelegate: NSObjectProtocol {
    
    func viewFinishAction(view: RateSessionViewProtocol)
    func viewDidSetRating(view: RateSessionViewProtocol, rating: Double)

}

protocol RateSessionViewProtocol: NSObjectProtocol {
    
    weak var delegate: RateSessionViewDelegate? { get set }
    var tableView: UITableView! { get }
    
    var simpleHeaderForTableView: UIView! { get }
    var headerForTableView: UIView! { get }
    var footerForTableView: UIView! { get }

    var tutorNameLabel: UILabel! { get }
    var sessionStartTimeLabel: UILabel! { get }
    var sessionEndTimeLabel: UILabel! { get }
    var sessionCostLabel: UILabel! { get }

    var provideFeddbackStaticLabel: UILabel! { get }
    var oppositeNameStaticLabel: UILabel! { get }
    func updateFooter(isChamiMax: Bool, isStudent: Bool)
    var coloredView: UIView! { get }
    var feedbackTextView: UITextView! { get }
    var cefrView: UIView! { get }
    var cefrTextField: UITextField! { get }
}

class RateSessionView: UIView, RateSessionViewProtocol{
    
    @IBOutlet var cefrView: UIView!
    @IBOutlet var tableView: UITableView!
    
    @IBOutlet var simpleHeaderForTableView: UIView!
    @IBOutlet var headerForTableView: UIView!
    @IBOutlet var footerForTableView: UIView!
    
    @IBOutlet var tutorNameLabel: UILabel!
    @IBOutlet var sessionStartTimeLabel: UILabel!
    @IBOutlet var sessionEndTimeLabel: UILabel!
    @IBOutlet var sessionCostLabel: UILabel!
    
    @IBOutlet var simpleRatingView: CosmosView!
    @IBOutlet var ratingView: CosmosView!
    @IBOutlet var footerRateButton: UIButton!
    
    @IBOutlet var coloredView: UIView!
    
    @IBOutlet var feedbackTextView: UITextView!

    @IBOutlet var provideFeddbackStaticLabel: UILabel!
    
    @IBOutlet var oppositeNameStaticLabel: UILabel!
    
    @IBOutlet var cefrTextField: UITextField!
    class func create() -> RateSessionView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? RateSessionView else {
            fatalError("Nib \(viewNibName) does not contain RateSession View as first object")
        }
        
        return view
    }
    
    // MARK: - RateSessionView interface methods
    weak var delegate: RateSessionViewDelegate?
    
    // add view private properties/outlets/methods here
    
    // MARK: - IBActions
    
    @IBAction func actionFinish(_ sender: Any) {
        self.delegate?.viewFinishAction(view: self)

    }
    
    
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        self.ratingView.didFinishTouchingCosmos = didFinishTouchingCosmos
        self.ratingView.setupPresentation()
        
        self.simpleRatingView.didFinishTouchingCosmos = didFinishTouchingCosmos
        self.simpleRatingView.setupPresentation()

        
        Theme.current.apply(filledButton: self.footerRateButton)
        
//        self.updateFooter(isChamiMax: false, isStudent: false)
//        setRatingVisible(visible: true, isChamiMax: false, isChamiX: false)
    }
    
    func updateFooter(isChamiMax: Bool, isStudent: Bool) {
    
//        let margin: CGFloat = 5.0
//        var topOffset: CGFloat = margin
        
        if isStudent {
            self.cefrView.isHidden = true
//            var newStarsFrame = ratingView.frame
//            newStarsFrame.origin.y = topOffset
//            self.ratingView.frame = newStarsFrame
//            self.ratingView.isHidden = false
//            topOffset = ratingView.frame.maxY
//            topOffset += margin
            
            //self.provideFeddbackStaticLabel.isHidden = true
            
//            self.provideFeddbackStaticLabel.isHidden = false
//            var newFooterTitleFrame = self.provideFeddbackStaticLabel.frame
//            newFooterTitleFrame.origin.y = topOffset
//            self.provideFeddbackStaticLabel.frame = newFooterTitleFrame
//            topOffset = self.provideFeddbackStaticLabel.frame.maxY
//            topOffset += margin
//
//
//            var newFooterTextFrame = self.feedbackTextView.frame
//            newFooterTextFrame.origin.y = topOffset
//            self.feedbackTextView.frame = newFooterTextFrame
//            topOffset = self.feedbackTextView.frame.maxY
//            topOffset += margin


        } else {
            self.ratingView.isHidden = true
            if isChamiMax {
                self.provideFeddbackStaticLabel.isHidden = true
                self.cefrView.isHidden = false
//                topOffset = self.cefrView.frame.maxY
//                topOffset += margin
            } else {
                self.cefrView.isHidden = true
            }
//            self.provideFeddbackStaticLabel.isHidden = false
//            var newFooterTitleFrame = self.provideFeddbackStaticLabel.frame
//            newFooterTitleFrame.origin.y = topOffset
//            self.provideFeddbackStaticLabel.frame = newFooterTitleFrame
//            topOffset = self.provideFeddbackStaticLabel.frame.maxY
//            topOffset += margin
//
//
//            var newFooterTextFrame = self.feedbackTextView.frame
//            newFooterTextFrame.origin.y = topOffset
//            self.feedbackTextView.frame = newFooterTextFrame
//            topOffset = self.feedbackTextView.frame.maxY
//            topOffset += margin

        }

//        var newFooterButtonFrame = self.footerRateButton.frame
//        newFooterButtonFrame.origin.y = topOffset
//        self.footerRateButton.frame = newFooterButtonFrame
//        topOffset = self.footerRateButton.frame.maxY
//        topOffset += margin
//
//        var newfooterFrame2 = self.footerForTableView.frame
//        newfooterFrame2.size.height = topOffset
//        self.footerForTableView.frame = newfooterFrame2
        

//        self.tableView.tableFooterView = nil
//        self.tableView.tableFooterView = self.footerForTableView;
    }
    
    private func didFinishTouchingCosmos(_ rating: Double) {
        delegate?.viewDidSetRating(view: self, rating: rating)
//        ratingView.value = Float(rating)
//        self.ratingLabel.text = ViewController.formatValue(rating)
//        ratingLabel.textColor = UIColor(red: 183/255, green: 186/255, blue: 204/255, alpha: 1)
    }
}
