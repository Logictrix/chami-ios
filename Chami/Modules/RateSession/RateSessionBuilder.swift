//
//  RateSessionBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RateSessionBuilder: NSObject {

    class func viewController() -> RateSessionViewController {
        let view: RateSessionViewProtocol = RateSessionView.create()
        let model: RateSessionModelProtocol = RateSessionModel()
        let dataSource = RateSessionDataSource(withModel: model)
        let router = RateSessionRouter()
        
        let viewController = RateSessionViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
