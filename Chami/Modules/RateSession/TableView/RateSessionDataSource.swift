//
//  RateSessionDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class RateSessionDataSource: NSObject, UITableViewDataSource {
    
    weak var cellDelegate: RateSessionCellDelegate?
    private let model: RateSessionModelProtocol
    private let cellReuseId = "RateSessionTableViewCell"
    
    init(withModel model: RateSessionModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: RateSessionTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: RateSessionTableViewCell, forItem item: SessionSkillDataType) {
        cell.titleLabel.text = item.skillName
        cell.slider.value = Float(item.skillValue)
        cell.logoImageView.image = UIImage(named:item.skillLogoUrl)
        cell.switchActivateSkill.isOn = item.isEditable
        
        cell.slider.isEnabled = item.isEditable
//        cell.layoutIfNeeded()
        
        if self.model.accountRole == .student {
            let imageSliderThumb = UIImage(named: "session_scr_swith")
            
            cell.slider.setThumbImage(imageSliderThumb, for: .normal)
            
            cell.slider.setMinimumTrackImage(UIImage(named: "session_slider_student_sel"), for: .normal)
        } else if self.model.accountRole == .tutor {
            let imageSliderThumb = UIImage(named: "session_scr_swith_blu")
            
            cell.slider.setThumbImage(imageSliderThumb, for: .normal)
            
            cell.slider.setMinimumTrackImage(UIImage(named: "session_slider_tutor_sel"), for: .normal)
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }

    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? RateSessionTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let item = self.model.items[indexPath.row];
        self.configure(cell: cell, forItem: item)
//        cell.labelsView.ticksDistance = (cell.labelsView.bounds.size.width) / CGFloat(cell.labelsView.labelsArray.count)

        return cell
    }
}
