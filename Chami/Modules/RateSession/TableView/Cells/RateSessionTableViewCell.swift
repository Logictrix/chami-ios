//
//  RateSessionTableViewCell.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RateSessionTableViewCell: UITableViewCell {
    
    weak var delegate: RateSessionCellDelegate?
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var labelsView: DiscreteLabelsScaleView!
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var slider: UISlider!
    @IBOutlet weak var switchActivateSkill: UISwitch!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.slider.isContinuous = false
//        self.labelsView.ticksDistance = (self.labelsView.bounds.size.width) / CGFloat(self.labelsView.labelsArray.count)
//        self.labelsView.backgroundColor = UIColor.red
    }
    
    
    // MARK: - IBAction
    
    @IBAction func actionSliderChanged(_ sender: UISlider) {
        sender.value = round(sender.value)
        print(sender.value)
        self.delegate?.cellDidChangeSliderValue(cell: self, toSliderValue: Int(sender.value))

    }
    
    @IBAction func actionSwitchChanged(_ sender: Any) {
        self.delegate?.cellDidSendSwitchAction(cell: self)
    }
}


protocol RateSessionCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    func cellDidSendSwitchAction(cell: RateSessionTableViewCell)
    func cellDidChangeSliderValue(cell: RateSessionTableViewCell, toSliderValue: Int)

}
