//
//  RateSessionViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias RateSessionViewControllerType = BaseViewController<RateSessionModelProtocol, RateSessionViewProtocol, RateSessionRouter>



class RateSessionViewController: RateSessionViewControllerType, UITableViewDelegate {
    
    private var dataSource: RateSessionDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: RateSessionViewProtocol, model: RateSessionModelProtocol, router: RateSessionRouter, dataSource: RateSessionDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        // your custom code
    }
    
    internal required init(withView view: RateSessionViewProtocol!, model: RateSessionModelProtocol!, router: RateSessionRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:false)
        self.title = NSLocalizedString("Session Complete", comment: "Session Complete - Screen title")

        self.customView.delegate = self
        self.connectTableViewDependencies()
        if self.model.currentMeetUpSession.isChamiXFlow == true {
            self.customView.tableView.tableHeaderView = self.customView.simpleHeaderForTableView
        } else {
            self.customView.tableView.tableHeaderView = self.customView.headerForTableView
        }
//        self.customView.tableView.tableHeaderView = self.customView.headerForTableView
        
        self.customView.tableView.tableFooterView = self.customView.footerForTableView

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
        
        var isStudent = false
        var isChamiMax = false

        if self.model.accountRole == .student {
            isStudent = true
        } else {
            if self.model.session.isChamiMax {
                isChamiMax = true
            }
        }
        self.customView.updateFooter(isChamiMax: isChamiMax, isStudent: isStudent)
        
        
        self.setProgressVisible(visible: true)
        
        self.model.updateSession { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            if let error = error {
                strongSelf.updateView()
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: {
                })
                
            } else {
                strongSelf.updateView()
            }
        }
            

    }
    
    
    // MARK: - Public methods
    func setSession(session: Session) {
        self.model.session = session
    }
    func setMeetUpSession(meetUpSession: MeetUpSession) {
        self.model.currentMeetUpSession = meetUpSession
    }

    
    // MARK: - Private methods
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    private func updateView() {
        guard let session = self.model.session else {
            fatalError("\(String(describing: type(of: self))) require session to be defined")
            
        }
        
        
        self.customView.sessionStartTimeLabel.text = DateConverter.stringTimeFormat(fromDate: session.startDateTime)
        self.customView.sessionEndTimeLabel.text = DateConverter.stringTimeFormat(fromDate: session.endDateTime)
        self.customView.sessionCostLabel.text = "£\(session.cost/100).\(session.cost%100)"

        self.customView.coloredView.backgroundColor = Theme.current.regularColor().withAlphaComponent(0.3)

        if self.model.accountRole == .student {
            self.customView.tutorNameLabel.text = session.tutorsFullName
            if self.model.currentMeetUpSession.isChamiXFlow == true {
                self.customView.provideFeddbackStaticLabel.text = NSLocalizedString("Please provide feedback for your chami X partner:", comment: "Please provide feedback for your chami X partner:")
                self.customView.oppositeNameStaticLabel.text = NSLocalizedString("chami X partner name:", comment: "chami X partner:")
                
//                self.customView.provideFeddbackStaticLabel.isHidden = true
//                self.customView.feedbackTextView.isHidden = true
                
            } else {
                self.customView.provideFeddbackStaticLabel.text = NSLocalizedString("Please provide feedback for your tutor:", comment: "Please provide feedback for your tutor:")
                self.customView.oppositeNameStaticLabel.text = NSLocalizedString("Tutor name:", comment: "Tutor name:")

            }


        } else if self.model.accountRole == .tutor {
            self.customView.oppositeNameStaticLabel.text = NSLocalizedString("Student name:", comment: "Tutor name:")
            self.customView.provideFeddbackStaticLabel.text = NSLocalizedString("Please provide feedback for your student:", comment: "Please provide feedback for your student:")

//            self.customView.tutorNameLabel.text = model.currentMeetUpSession.partner_full_name
            self.customView.tutorNameLabel.text = session.tutorsFullName

        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
        self.customView.tableView.reloadData()
    }
    
    func viewDidSetRating(view: RateSessionViewProtocol, rating: Double) {
        self.model.session.rating = Int(rating)
        
        if self.model.currentMeetUpSession.isChamiXFlow == false {
            let popOverVC = RatePopupBuilder.viewController(rating: rating)
            self.addChild(popOverVC)
            popOverVC.view.frame = self.view.bounds
            self.view.addSubview(popOverVC.view)
            popOverVC.didMove(toParent: self)
        }
    }

    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

// MARK: - RateSessionViewDelegate

extension RateSessionViewController: RateSessionViewDelegate {

    func viewFinishAction(view: RateSessionViewProtocol) {
        self.model.session.feedback = self.customView.feedbackTextView.text
        //self.model.session.tutorsId = self.model.currentMeetUpSession.partnerId
        
        var doubleVal = 0.0
        if let txt = self.customView.cefrTextField.text, let ff = Double(txt) {
            doubleVal = ff
            if doubleVal < 0.0 {
                doubleVal = 0.0
            }
            if doubleVal > 10.0 {
                doubleVal = 10.0
            }
            self.model.session.ieltsScore = NSNumber(value: doubleVal)
        }
        
        self.setProgressVisible(visible: true)
        self.model.sendFeedback {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if let error = error {
                AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf, closeHandler: {
                    _ = strongSelf.navigationController?.popToRootViewController(animated: true)
                })

            } else {
                _ = strongSelf.navigationController?.popToRootViewController(animated: true)

            }
        }
    }
}

// MARK: - RateSessionModelDelegate

extension RateSessionViewController: RateSessionModelDelegate {
    
    func modelDidChanged(model: RateSessionModelProtocol) {
    }
}

// MARK: - RateSessionCellDelegate

extension RateSessionViewController: RateSessionCellDelegate {
    
    func cellDidSendSwitchAction(cell: RateSessionTableViewCell) {
        if let indexPath = self.customView.tableView.indexPath(for: cell) {
            var item = self.model.items[indexPath.row]
            item.isEditable = !item.isEditable
            self.model.items[indexPath.row] = item

            self.customView.tableView.reloadRows(at: [indexPath], with: UITableView.RowAnimation.none)
        }
    }
    
    func cellDidChangeSliderValue(cell: RateSessionTableViewCell, toSliderValue: Int) {
        if let indexPath = self.customView.tableView.indexPath(for: cell) {
            var item = self.model.items[indexPath.row]
            item.skillValue = toSliderValue
            self.model.items[indexPath.row] = item
            
        }

    }
}
