//
//  ShareHowToModel.swift
//  Chami
//
//  Created by Serg Smyk on 12/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

public protocol ShareHowToModelDelegate: NSObjectProtocol {
    
}

public protocol ShareHowToModelProtocol: NSObjectProtocol {
    
    weak var delegate: ShareHowToModelDelegate? { get set }
    //var accountRole: AccountRole { get }
}

public class ShareHowToModel: NSObject, ShareHowToModelProtocol {
    
    // MARK: - ShareHowToModel methods

    weak public var delegate: ShareHowToModelDelegate?
//    var accountRole: AccountRole {
//        get {
//            return AccountManager.shared.currentRole
//        }
//    }
    
    /** Implement ShareHowToModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
