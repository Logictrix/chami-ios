//
//  ShareHowToBuilder.swift
//  Chami
//
//  Created by Serg Smyk on 12/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ShareHowToBuilder: NSObject {

    class func viewController() -> ShareHowToViewController {

        let view: ShareHowToViewProtocol = ShareHowToView.loadFromXib() as! ShareHowToViewProtocol
        let model: ShareHowToModelProtocol = ShareHowToModel()
        let router = ShareHowToRouter()
        
        let viewController = ShareHowToViewController(withView: view, model: model, router: router)
        return viewController
    }
}
