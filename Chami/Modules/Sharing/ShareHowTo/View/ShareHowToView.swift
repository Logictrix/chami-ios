//
//  ShareHowToView.swift
//  Chami
//
//  Created by Serg Smyk on 12/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

public protocol ShareHowToViewDelegate: NSObjectProtocol {
    
    func viewSomeAction(view: ShareHowToViewProtocol)
}

public protocol ShareHowToViewProtocol: NSObjectProtocol {
    
    weak var delegate: ShareHowToViewDelegate? { get set }
    var logoImageView: UIImageView! { get }
}

public class ShareHowToView: UIView, ShareHowToViewProtocol{
    
    // MARK: - ShareHowToView interface methods

    @IBOutlet weak public var logoImageView: UIImageView!
    weak public var delegate: ShareHowToViewDelegate?
    
    // MARK: - Overrided methods

    override public func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - IBActions
    
    func someButtonAction() {

        self.delegate?.viewSomeAction(view: self)
    }
}
