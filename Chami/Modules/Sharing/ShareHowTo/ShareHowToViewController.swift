//
//  ShareHowToViewController.swift
//  Chami
//
//  Created by Serg Smyk on 12/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit


typealias ShareHowToViewControllerType = BaseViewController<ShareHowToModelProtocol, ShareHowToViewProtocol, ShareHowToRouter>

class ShareHowToViewController: ShareHowToViewControllerType {
    
    // MARK: Initializers
    
    required public init(withView view: ShareHowToViewProtocol!, model: ShareHowToModelProtocol!, router: ShareHowToRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override public func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("How sharing works", comment: "")
        
        customView.delegate = self
        model.delegate = self
        
        
        if AccountManager.shared.currentRole == .student {
            self.customView.logoImageView.image = UIImage(named: "share_scr_logo_blu")
            
        } else if AccountManager.shared.currentRole == .tutor {
            self.customView.logoImageView.image = UIImage(named: "share_scr_logo")
            
        }
    }
}

// MARK: - ShareHowToViewDelegate

extension ShareHowToViewController: ShareHowToViewDelegate {

    public func viewSomeAction(view: ShareHowToViewProtocol) {
    }
}

// MARK: - ShareHowToModelDelegate

extension ShareHowToViewController: ShareHowToModelDelegate {
}
