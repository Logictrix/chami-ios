//
//  ShareView.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ShareViewDelegate: NSObjectProtocol {
    
    func viewDidTapShare(view: ShareViewProtocol)
    func viewDidTapHowTo(view: ShareViewProtocol)
}

protocol ShareViewProtocol: NSObjectProtocol {
    
    weak var delegate: ShareViewDelegate? { get set }
    var logoImageView: UIImageView! { get }
    var shareButton: UIButton! { get }
    var titleLabel: UILabel! { get }
}

class ShareView: UIView, ShareViewProtocol{

    class func create() -> ShareView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? ShareView else {
            fatalError("Nib \(viewNibName) does not contain Share View as first object")
        }
        
        return view
    }
    
    // MARK: - ShareView interface methods

    @IBOutlet weak var howToButton: UIButton!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var logoImageView: UIImageView!
    
    @IBOutlet weak var shareButton: UIButton!
    weak var delegate: ShareViewDelegate?
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
    // MARK: - IBActions
    
    @IBAction func actionHowTo(_ sender: UIButton) {
        self.delegate?.viewDidTapHowTo(view: self)
    }

    @IBAction func actionShare(_ sender: Any)
    {
        self.delegate?.viewDidTapShare(view: self)
    }
}
