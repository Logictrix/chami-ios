//
//  ShareModel.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ShareModelDelegate: NSObjectProtocol {
    
}

protocol ShareModelProtocol: NSObjectProtocol {
    
    weak var delegate: ShareModelDelegate? { get set }
    var accountRole: AccountRole { get set }

}

class ShareModel: NSObject, ShareModelProtocol {
    
    // MARK: - ShareModel methods

    weak var delegate: ShareModelDelegate?
    var accountRole: AccountRole = AccountManager.shared.currentRole

    /** Implement ShareModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
