//
//  ShareBuilder.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ShareBuilder: NSObject {

    class func viewController() -> ShareViewController {

        let view: ShareViewProtocol = ShareView.create()
        let model: ShareModelProtocol = ShareModel()
        let router = ShareRouter()
        
        let viewController = ShareViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
