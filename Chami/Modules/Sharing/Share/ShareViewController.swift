//
//  ShareViewController.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias ShareViewControllerType = BaseViewController<ShareModelProtocol, ShareViewProtocol, ShareRouter>

class ShareViewController: ShareViewControllerType {
    
    // MARK: Initializers
    
    required init(withView view: ShareViewProtocol!, model: ShareModelProtocol!, router: ShareRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        customView.delegate = self
        model.delegate = self
        
        self.navigationItem.title = NSLocalizedString("Share Chami", comment: "Share")
        self.addSideMenuLeftButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
    }

    private func updateView() {
        Theme.current.apply(filledButton: self.customView.shareButton)

        if self.model.accountRole == .student {
            self.customView.logoImageView.image = UIImage(named: "share_scr_logo_blu")
            self.customView.titleLabel.textColor = ColorHelper.regularColor()
            
        } else if self.model.accountRole == .tutor {
            self.customView.logoImageView.image = UIImage(named: "share_scr_logo")
            self.customView.titleLabel.textColor = ColorHelper.regularColor()

        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
    }

}

// MARK: - ShareViewDelegate

extension ShareViewController: ShareViewDelegate {
    
    func viewDidTapShare(view: ShareViewProtocol)
    {
//        let textToShare = "Swift is awesome!  Check out this website about it!"
//            
//        if let myWebsite = NSURL(string: "http://www.codingexplorer.com/") {
//            let objectsToShare = [textToShare, myWebsite] as [Any]
//            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//                
//            activityVC.popoverPresentationController?.sourceView = sender
//            self.presentedViewController(activityVC, animated: true, completion: nil)
//        }
        
        let shareText = "Check out Chami in AppStore!"
        let shareLink = "https://itunes.apple.com/us/app/chami/id1233714703?ls=1&mt=8"
        if let image = UIImage(named: "logo_text") {
            let vc = UIActivityViewController(activityItems: [shareText, shareLink, image], applicationActivities: [])
            present(vc, animated: true)
        }
        
    }
    
    func viewDidTapHowTo(view: ShareViewProtocol) {
        self.router?.navigateToHowTo(from: self)
    }
}

// MARK: - ShareModelDelegate

extension ShareViewController: ShareModelDelegate {
}
