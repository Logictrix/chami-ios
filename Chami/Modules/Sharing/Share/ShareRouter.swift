//
//  ShareRouter.swift
//  Chami
//
//  Created by Igor Markov on 2/8/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ShareRouter: NSObject {
    func navigateToHowTo(from vc: UIViewController) {
        let controller = ShareHowToBuilder.viewController()
        
        vc.navigationController?.pushViewController(controller, animated: true)
    }
}
