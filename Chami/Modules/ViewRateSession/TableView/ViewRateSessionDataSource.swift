//
//  ViewRateSessionDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class ViewRateSessionDataSource: NSObject, UITableViewDataSource {
    
    weak var cellDelegate: ViewRateSessionCellDelegate?
    private let model: ViewRateSessionModelProtocol
    private let cellReuseId = "ViewRateSessionTableViewCell"
    
    init(withModel model: ViewRateSessionModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: ViewRateSessionTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: ViewRateSessionTableViewCell, forItem item: SessionSkillDataType) {
        cell.titleLabel.text = item.skillName
        cell.valueLabel.text = String(item.skillValue)
        cell.slider.value = Float(item.skillValue)
        cell.logoImageView.image = UIImage(named:item.skillLogoUrl)
        if self.model.accountRole == .student {
            let imageSld = UIImage(named: "session_slider2_student_sel") //?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 15, bottom: 0, right: 15), resizingMode: .stretch)
            
            cell.slider.setMinimumTrackImage(imageSld, for: .normal)
        } else if self.model.accountRole == .tutor {
            cell.slider.setMinimumTrackImage(UIImage(named: "session_slider2_tutor_sel"), for: .normal)
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }


    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? ViewRateSessionTableViewCell else {
            fatalError()
        }
        
        cell.delegate = cellDelegate
        
        let item = self.model.items[indexPath.row];
        self.configure(cell: cell, forItem: item)
        
        return cell
    }
}
