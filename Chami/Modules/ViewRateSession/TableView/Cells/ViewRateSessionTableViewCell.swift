//
//  ViewRateSessionTableViewCell.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ViewRateSessionTableViewCell: UITableViewCell {
    
    weak var delegate: ViewRateSessionCellDelegate?
    @IBOutlet weak var titleLabel: UILabel!

    @IBOutlet weak var slider: UISlider!
    
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var logoImageView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()

        self.slider.isContinuous = false
        let imageSliderThumb = UIImage(named: "clearImage")
        
        self.slider.setThumbImage(imageSliderThumb, for: .normal)
        
        self.slider.setMaximumTrackImage(imageSliderThumb, for: .normal)

        
    }

    // MARK: - IBAction
    
    @IBAction func someButtonAction() {
        self.delegate?.cellDidTapSomeButton(cell: self)
    }
}


protocol ViewRateSessionCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    func cellDidTapSomeButton(cell: ViewRateSessionTableViewCell)
}
