//
//  ViewRateSessionViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Kingfisher

typealias ViewRateSessionViewControllerType = BaseViewController<ViewRateSessionModelProtocol, ViewRateSessionViewProtocol, ViewRateSessionRouter>

class ViewRateSessionViewController: ViewRateSessionViewControllerType, UITableViewDelegate {
    
    private var dataSource: ViewRateSessionDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: ViewRateSessionViewProtocol, model: ViewRateSessionModelProtocol, router: ViewRateSessionRouter, dataSource: ViewRateSessionDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        // your custom code
    }
    
    internal required init(withView view: ViewRateSessionViewProtocol!, model: ViewRateSessionModelProtocol!, router: ViewRateSessionRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Session Rating", comment: "Session Rating - Screen title")

        self.customView.delegate = self
        self.connectTableViewDependencies()
        self.customView.tableView.tableHeaderView = self.customView.headerForTableView;
        self.customView.tableView.tableFooterView = self.customView.footerForTableView;

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
        
    }
    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    private func updateView() {
        guard let session = self.model.session else {
            fatalError("\(String(describing: type(of: self))) require session to be defined")

        }
        self.customView.userNameLabel.text = session.tutorsFullName
        self.customView.sessionDateLabel.text = "\(DateConverter.stringDateWithoutTimeFormat(fromDate: session.startDateTime))"

        let url = URL(string: session.tutorsImageUrl)
        self.customView.userLogoImageView.kf.setImage(with: url)

        
        if self.model.accountRole == .student {
            self.customView.commentsTextView.text = session.feedback
            
            if (session.rating == 0) {
                self.customView.ratingView.isHidden = true
            } else {
                self.customView.ratingView.isHidden = false
                self.customView.ratingView.rating = Double(session.rating)
            }
            
        } else if self.model.accountRole == .tutor {
            self.customView.commentsTextView.text = session.feedback
            
            self.customView.ratingView.isHidden = false
            self.customView.ratingView.rating = Double(session.rating)
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
        self.customView.tableView.reloadData()

        
    }
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
//        self.router?.navigateToSomeScreen(from: self, withBackgroundColor: UIColor.gray)
        
        
    }
    
    // MARK: - Public methods
    func setSession(session: Session) {
        self.model.session = session
    }

}

// MARK: - ViewRateSessionViewDelegate

extension ViewRateSessionViewController: ViewRateSessionViewDelegate {

}

// MARK: - ViewRateSessionModelDelegate

extension ViewRateSessionViewController: ViewRateSessionModelDelegate {
    
    func modelDidChanged(model: ViewRateSessionModelProtocol) {
    }
}

// MARK: - ViewRateSessionCellDelegate

extension ViewRateSessionViewController: ViewRateSessionCellDelegate {
    
    func cellDidTapSomeButton(cell: ViewRateSessionTableViewCell) {
    }
}
