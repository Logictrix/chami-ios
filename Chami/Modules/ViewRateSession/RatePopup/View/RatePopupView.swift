//
//  RatePopupView.swift
//  Chami
//
//  Created by Dima on 2/23/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RatePopupViewDelegate: NSObjectProtocol {
    
    func viewDidTapDone(view: RatePopupViewProtocol)
}

protocol RatePopupViewProtocol: NSObjectProtocol {
    
    weak var delegate: RatePopupViewDelegate? { get set }
    var tableView: UITableView! { get }
    func setRating(rating: Double)
}

class RatePopupView: UIView, RatePopupViewProtocol{

    class func create() -> RatePopupView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? RatePopupView else {
            fatalError("Nib \(viewNibName) does not contain RatePopup View as first object")
        }
        
        return view
    }
    
    // MARK: - RatePopupView interface methods

    weak var delegate: RatePopupViewDelegate?
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!

    // add view private properties/outlets/methods here
    
    
    
    // MARK: - IBActions
    
    @IBAction func doneButtonAction(_ sender: Any) {
        delegate?.viewDidTapDone(view: self)
    }
    
    func setRating(rating: Double)
    {
        var strTitle = NSLocalizedString("What went wrong?", comment: "What went wrong?")
        
        if rating > 3.0 {
            strTitle = NSLocalizedString("What were you most satisfied with?", comment: "What were you most satisfied with?")
        }
        titleLabel.text = strTitle //String(format: "I put %.0f stars because:", rating)
    }
    
    
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()

        // setup view and table view programmatically here
    }
}
