//
//  RatePopupModel.swift
//  Chami
//
//  Created by Dima on 2/23/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol RatePopupModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: RatePopupModelProtocol)
}

protocol RatePopupModelProtocol: NSObjectProtocol {
    
    weak var delegate: RatePopupModelDelegate? { get set }
    var items: [String] { get }
}

class RatePopupModel: NSObject, RatePopupModelProtocol {
    
    override init() {
        self.items = []
        super.init()

        self.items = self.getTestItems()
    }
    
    func getTestItems() -> [String] {
        return ["Time management", "Professionalism", "Teaching style", "Enthusiasm"]
    }
    
    // MARK: - RatePopupModel methods

    weak var delegate: RatePopupModelDelegate?
    public private(set) var items: [String]
    
    /** Implement RatePopupModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
