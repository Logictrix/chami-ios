//
//  RatePopupTableViewCell.swift
//  Chami
//
//  Created by Dima on 2/23/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit


class RatePopupTableViewCell: UITableViewCell {
    
    weak var delegate: RatePopupCellDelegate?
    //override var selected: Bool
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var leftImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.leftImage.image = UIImage.init(named: "popup_btn")
//        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapped(_:)))
    }
    
//    func tapped(_ sender: UIButton) {
//        self.
//    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        //super.setSelected(selected, animated: animated)
        if (selected)
        {
            self.leftImage.image = UIImage.init(named: "popup_btn_selected")
        }
        else
        {
            self.leftImage.image = UIImage.init(named: "popup_btn")
        }
    }

    // MARK: - IBAction
    
    
}


protocol RatePopupCellDelegate: NSObjectProtocol {

    /** Delegate method example */
    //var selected: Bool { get set }
    func cellDidTapSomeButton(cell: RatePopupTableViewCell)
}
