//
//  RatePopupDataSource.swift
//  SwiftMVCTemplate
//
//  Created by Igor Markov on 11/14/16.
//  Copyright © 2016 DB Best Technologies LLC. All rights reserved.
//

import UIKit

class RatePopupDataSource: NSObject, UITableViewDataSource {
    
    weak var cellDelegate: RatePopupCellDelegate?
    private let model: RatePopupModelProtocol
    private let cellReuseId = "RatePopupTableViewCell"
    
    init(withModel model: RatePopupModelProtocol) {
        self.model = model
    }

    func registerNibsForTableView(tableView: UITableView) {
        let cellNib = UINib(nibName: String(describing: RatePopupTableViewCell.self), bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: cellReuseId)
    }
    
    // MARK: - Private methods
    
    func configure(cell: RatePopupTableViewCell, forItem item: String) {
        cell.titleLabel.text = item
    }
    
    // MARK: - UITableViewDataSource

    private func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.model.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: cellReuseId) as? RatePopupTableViewCell else {
            fatalError()
        }
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        
        cell.delegate = cellDelegate
        
        let testItem = self.model.items[indexPath.row];
        self.configure(cell: cell, forItem: testItem)
        
        return cell
    }
}
