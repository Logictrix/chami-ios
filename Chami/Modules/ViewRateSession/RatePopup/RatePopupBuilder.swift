//
//  RatePopupBuilder.swift
//  Chami
//
//  Created by Dima on 2/23/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class RatePopupBuilder: NSObject {

    class func viewController(rating: Double) -> RatePopupViewController {
        let view: RatePopupViewProtocol = RatePopupView.create()
        view.setRating(rating: rating)
        let model: RatePopupModelProtocol = RatePopupModel()
        let dataSource = RatePopupDataSource(withModel: model)
        let router = RatePopupRouter()
        
        let viewController = RatePopupViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
