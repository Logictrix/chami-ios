//
//  RatePopupViewController.swift
//  Chami
//
//  Created by Dima on 2/23/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias RatePopupViewControllerType = MVCViewController<RatePopupModelProtocol, RatePopupViewProtocol, RatePopupRouter>

class RatePopupViewController: RatePopupViewControllerType, UITableViewDelegate {
    
    private var dataSource: RatePopupDataSource!
    
    // MARK: - Initializers

    convenience init(withView view: RatePopupViewProtocol, model: RatePopupModelProtocol, router: RatePopupRouter, dataSource: RatePopupDataSource) {
        
        self.init(withView: view, model: model, router: router)
        
        self.model.delegate = self

        self.dataSource = dataSource
        self.dataSource.cellDelegate = self
        
        // your custom code
    }
    
    internal required init(withView view: RatePopupViewProtocol!, model: RatePopupModelProtocol!, router: RatePopupRouter?) {
        super.init(withView: view, model: model, router: router)
    }

    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.customView.delegate = self
        self.connectTableViewDependencies()
        
        self.showAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }
    
    private func connectTableViewDependencies() {
        
        self.customView.tableView.delegate = self
        self.dataSource.registerNibsForTableView(tableView: self.customView.tableView)
        self.customView.tableView.dataSource = self.dataSource
    }
    
    // MARK: - Table view delegate
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //tableView.deselectRow(at: indexPath, animated: NO)
        
        
    }
}

// MARK: - RatePopupViewDelegate

extension RatePopupViewController: RatePopupViewDelegate {

    func viewDidTapDone(view: RatePopupViewProtocol) {
        self.removeAnimate()
    }
}

// MARK: - RatePopupModelDelegate

extension RatePopupViewController: RatePopupModelDelegate {
    
    func modelDidChanged(model: RatePopupModelProtocol) {
    }
}

// MARK: - RatePopupCellDelegate

extension RatePopupViewController: RatePopupCellDelegate {
    
    func cellDidTapSomeButton(cell: RatePopupTableViewCell) {
    }
}
