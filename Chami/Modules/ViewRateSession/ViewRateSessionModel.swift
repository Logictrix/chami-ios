//
//  ViewRateSessionModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ViewRateSessionModelDelegate: NSObjectProtocol {
    
    func modelDidChanged(model: ViewRateSessionModelProtocol)
}

protocol ViewRateSessionModelProtocol: NSObjectProtocol {
    
    weak var delegate: ViewRateSessionModelDelegate? { get set }
    var items: [SessionSkillDataType] { get }
    var session: Session? { get set }
    var accountRole: AccountRole { get set }

}

class ViewRateSessionModel: NSObject, ViewRateSessionModelProtocol {
    
    override init() {
//        self.items = []
        super.init()

//        self.items = self.createItems()
    }
    
    var session: Session?
    var accountRole: AccountRole = AccountManager.shared.currentRole
    

    func createItems() -> [SessionSkillDataType] {
        let sessionSkillSpeaking = (skillName: NSLocalizedString("Speaking", comment:"Speaking - Session Skills to rate"), skillValue: 5, skillLogoUrl:"session_scr_icon_speaking", isEditable: false )
        let sessionSkillListening = (skillName: NSLocalizedString("Listening", comment:"Listening - Session Skills to rate"), skillValue: 3, skillLogoUrl:"session_scr_icon_listening", isEditable: false )
        let sessionSkillReading = (skillName: NSLocalizedString("Reading", comment:"Reading - Session Skills to rate"), skillValue: 4, skillLogoUrl:"session_scr_icon_reading", isEditable: false )
        let sessionSkillWriting = (skillName: NSLocalizedString("Writing", comment:"Writing - Session Skills to rate"), skillValue: 6, skillLogoUrl:"session_scr_icon_writing", isEditable: false )
        let sessionSkillPronunciation = (skillName: NSLocalizedString("Pronunciation", comment:"Pronunciation - Session Skills to rate"), skillValue: 10, skillLogoUrl:"session_scr_icon_pron", isEditable: false )
        let sessionSkillAttitudeToLearning = (skillName: NSLocalizedString("Attitude to Learning", comment:"Attitude to Learning - Session Skills to rate"), skillValue: 7, skillLogoUrl:"session_scr_icon_attit", isEditable: false )
        
        return [sessionSkillSpeaking, sessionSkillListening, sessionSkillReading, sessionSkillWriting, sessionSkillPronunciation, sessionSkillAttitudeToLearning]
    }
    
    // MARK: - ViewRateSessionModel methods

    weak var delegate: ViewRateSessionModelDelegate?
    var items: [SessionSkillDataType] {
        get {
            return self.session!.studentsRates
        }
    }
    
    /** Implement ViewRateSessionModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    
}
