//
//  ViewRateSessionBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ViewRateSessionBuilder: NSObject {

    class func viewController() -> ViewRateSessionViewController {
        let view: ViewRateSessionViewProtocol = ViewRateSessionView.create()
        let model: ViewRateSessionModelProtocol = ViewRateSessionModel()
        let dataSource = ViewRateSessionDataSource(withModel: model)
        let router = ViewRateSessionRouter()
        
        let viewController = ViewRateSessionViewController(withView: view, model: model, router: router, dataSource: dataSource)
        return viewController
    }
}
