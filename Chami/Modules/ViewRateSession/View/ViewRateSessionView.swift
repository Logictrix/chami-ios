//
//  ViewRateSessionView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Cosmos

protocol ViewRateSessionViewDelegate: NSObjectProtocol {
    
}

protocol ViewRateSessionViewProtocol: NSObjectProtocol {
    
    weak var delegate: ViewRateSessionViewDelegate? { get set }
    var tableView: UITableView! { get }
    var headerForTableView: UIView! { get }
    var footerForTableView: UIView! { get }
    var ratingView: CosmosView! { get }
    
    var userNameLabel: UILabel!  { get }
    var sessionDateLabel: UILabel!  { get }
    var userLogoImageView: UIImageView!  { get }
    var commentsTextView: UITextView!  { get }
}


class ViewRateSessionView: UIView, ViewRateSessionViewProtocol{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var headerForTableView: UIView!
    @IBOutlet weak var footerForTableView: UIView!
    @IBOutlet weak var ratingView: CosmosView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var sessionDateLabel: UILabel!
    @IBOutlet weak var userLogoImageView: UIImageView!
    
    @IBOutlet weak var commentsTextView: UITextView!
 
    class func create() -> ViewRateSessionView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? ViewRateSessionView else {
            fatalError("Nib \(viewNibName) does not contain ViewRateSession View as first object")
        }
        
        return view
    }
    
    // MARK: - ViewRateSessionView interface methods

    weak var delegate: ViewRateSessionViewDelegate?

    // add view private properties/outlets/methods here
    
    // MARK: - IBActions
        
    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        self.userLogoImageView.layer.cornerRadius = self.userLogoImageView.bounds.size.height / 2.0
        self.userLogoImageView.layer.masksToBounds = true
        self.userLogoImageView.contentMode = .scaleAspectFill
        self.userNameLabel.textColor = Theme.current.regularColor()

        self.ratingView.setupPresentation()
        // setup view and table view programmatically here
    }
}
