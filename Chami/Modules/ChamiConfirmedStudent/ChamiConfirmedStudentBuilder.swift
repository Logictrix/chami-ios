//
//  ChamiConfirmedStudentBuilder.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ChamiConfirmedStudentBuilder: NSObject {

    class func viewController() -> ChamiConfirmedStudentViewController {

        let view: ChamiConfirmedStudentViewProtocol = ChamiConfirmedStudentView.create()
        let model: ChamiConfirmedStudentModelProtocol = ChamiConfirmedStudentModel()
        let router = ChamiConfirmedStudentRouter()
        
        let viewController = ChamiConfirmedStudentViewController(withView: view, model: model, router: router)
        viewController.edgesForExtendedLayout = UIRectEdge()

        return viewController
    }
}
