//
//  ChamiConfirmedStudentViewController.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit

typealias ChamiConfirmedStudentViewControllerType = BaseViewController<ChamiConfirmedStudentModelProtocol, ChamiConfirmedStudentViewProtocol, ChamiConfirmedStudentRouter>

class ChamiConfirmedStudentViewController: ChamiConfirmedStudentViewControllerType {
    
    // MARK: Initializers
    deinit {
        SessionManager.shared.multicastDelegate.removeDelegate(self)
    }
    
    required init(withView view: ChamiConfirmedStudentViewProtocol!, model: ChamiConfirmedStudentModelProtocol!, router: ChamiConfirmedStudentRouter?) {
        super.init(withView: view, model: model, router: router)
    }
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = NSLocalizedString("Chami Confirmed", comment: "Chami Confirmed - Screen title")
        self.navigationItem.setHidesBackButton(true, animated:false)

        customView.delegate = self
        model.delegate = self
        
        self.canHandleMeetupRequests = false
        
        SessionManager.shared.multicastDelegate.addDelegate(self)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(barButtonSystemItem: .cancel, target: self, action: #selector(cancelMeetup))
    }
    
    @objc func cancelMeetup() {
        self.model.cancelMeetUpRequest {[weak self] (error) in
            guard let strongSelf = self else { return }
            
            if let msg = error?.localizedDescription  {
                strongSelf.showAlert(error: msg)
                return
            }
            strongSelf.navigationController?.popToRootViewController(animated: true)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.updateView()
        self.setProgressVisible(visible: true)
        self.model.getSession { [weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            
            if let error = error {
                AlertManager.showError(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: strongSelf)
                
            } else {
                strongSelf.updateView()
            }
        }
        
    }

    fileprivate func updateView() {
        self.customView.updateView(withTutorName: self.model.currentTutor.fullName!, withTutorImage: self.model.currentTutor.imageUrl)
        self.customView.updateView(isChamiX: (self.model.isChamiX))

        if self.model.currentSession != nil {
            let dateString = DateConverter.stringTimeFormat(fromDate: (self.model.currentSession?.etaDate)!)
            LocationHelper.stringLocationFromCoordinates(latitude: (Double(self.model.currentMeetUpSession.latitude)), longitude: (Double(self.model.currentMeetUpSession.longitude)), completion: {[weak self] (locationString) in
                guard let strongSelf = self else { return }
                strongSelf.customView.updateView(withLocationString: locationString, withEtaTimeString: dateString)
            })
        }
    }
    
    // MARK: - Public methods
    func setTutor(tutor: User) {
        self.model.currentTutor = tutor
    }
    func setMeetUp(meetUp: MeetUp) {
        self.model.currentMeetUp = meetUp
    }
    func setMeetUpSession(meetUpSession: MeetUpSession) {
        self.model.currentMeetUpSession = meetUpSession
    }
    func setIsChamiX(isChamiX: Bool) {
        self.model.isChamiX = isChamiX
    }
    
    // MARK: - SessionManagerDelegate
    func didReceivedMeetUpRequestCancel(session: MeetUpSession) {
        self.navigationController?.popToRootViewController(animated: true)
    }

}

// MARK: - ChamiConfirmedStudentViewDelegate

extension ChamiConfirmedStudentViewController: ChamiConfirmedStudentViewDelegate {

    func viewSendActionStartSession(view: ChamiConfirmedStudentViewProtocol) {
        self.model.confirmationCode = self.customView.pinText
        guard let code = self.model.confirmationCode, let _ = Int(code) else {
            AlertManager.showError(withMessage: "Please enter the code", onController: self)
            return
        }
        self.setProgressVisible(visible: true)

        self.model.startSession {[weak self] (error) in
            guard let strongSelf = self else { return }
            strongSelf.setProgressVisible(visible: false)
            if error == nil {
                strongSelf.router?.navigateToActiveSesionScreen(from: strongSelf, withSession: strongSelf.model.currentSession!, withMeetUpSession: strongSelf.model.currentMeetUpSession)

            } else {
                if (error! as NSError).code == -1011 {
                    AlertManager.showError(withMessage: "Session code is incorrect", onController: strongSelf)
                } else {
                    AlertManager.showError(withMessage: ErrorHelper.errorMessageFor(Error: error!) as String, onController: strongSelf)
                }
            }
            
        }
        
        
    }
}

// MARK: - ChamiConfirmedStudentModelDelegate

extension ChamiConfirmedStudentViewController: ChamiConfirmedStudentModelDelegate {
}
