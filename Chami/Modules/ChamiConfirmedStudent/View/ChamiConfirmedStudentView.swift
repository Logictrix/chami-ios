//
//  ChamiConfirmedStudentView.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ChamiConfirmedStudentViewDelegate: NSObjectProtocol {
    
    func viewSendActionStartSession(view: ChamiConfirmedStudentViewProtocol)
}

protocol ChamiConfirmedStudentViewProtocol: NSObjectProtocol {
    
    weak var delegate: ChamiConfirmedStudentViewDelegate? { get set }
    
    func updateView(withTutorName tutorName: String, withTutorImage imageUrlString: String?)
    func updateView(isChamiX chamiX: Bool)
    func updateView(withLocationString locationString: String, withEtaTimeString etaString: String)
    
    var pinText: String? { get }

}

class ChamiConfirmedStudentView: UIView, ChamiConfirmedStudentViewProtocol{
    
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var nameDuplicateLabel: UILabel!
    
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    @IBOutlet weak var startSessionButton: UIButton!
    @IBOutlet weak var pinTextField: UITextField!
    
    @IBOutlet weak var labelOnTheirWay: UILabel!
    
    @IBOutlet weak var infoLabal: UILabel!
    @IBOutlet weak var pinView: UIView!
    
    @IBOutlet weak var tutorNameStaticLabel: UILabel!
    internal var pinText: String? {
        get {
            return self.pinTextField.text!
        }
    }
    class func create() -> ChamiConfirmedStudentView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: nil, options: nil)
        guard let view = nibContent?.first as? ChamiConfirmedStudentView else {
            fatalError("Nib \(viewNibName) does not contain ChamiConfirmedStudent View as first object")
        }
        
        return view
    }
    
    // MARK: - ChamiConfirmedStudentView interface methods

    weak var delegate: ChamiConfirmedStudentViewDelegate?
    
    func updateView(withTutorName tutorName: String, withTutorImage imageUrlString: String?) {
        self.nameLabel.text = tutorName
        self.nameDuplicateLabel.text = tutorName
        
        
        if (imageUrlString != nil) {
            let url = URL(string: imageUrlString!)
            self.logoImageView.kf.setImage(with: url)
        }
        
    }
    
    func updateView(isChamiX chamiX: Bool) {
        var textOnTheirWay = NSLocalizedString("Your tutor is on their way", comment: "Your tutor is on their way")
        
        var textForTutorName = NSLocalizedString("Tutor name:", comment: "Tutor name:")
        var textForInfo = NSLocalizedString("When your tutor arrives, enter their provided chami session PIN below", comment: "When your tutor arrives, enter their provided chami session PIN below")
        if chamiX {
            textForTutorName = NSLocalizedString("Partner name:", comment: "Partner name:")

            textOnTheirWay = NSLocalizedString("Your chami X partner is on their way", comment: "")
            textForInfo = NSLocalizedString("When your chami X partner arrives, enter their provided chami session PIN below", comment: "")

        }
//        self.pinView.isHidden = chamiX
//        self.infoLabal.isHidden = chamiX
        self.infoLabal.text = textForInfo
        self.tutorNameStaticLabel.text = textForTutorName
        self.labelOnTheirWay.text = textOnTheirWay

    }
    
    func updateView(withLocationString locationString: String, withEtaTimeString etaString: String) {
        self.locationLabel.text = locationString
        self.timeLabel.text = etaString
    }

    // MARK: - Overrided methods

    override func awakeFromNib() {
        super.awakeFromNib()
        
        Theme.current.apply(filledButton: self.startSessionButton)
        self.layoutIfNeeded()
        self.logoImageView.layer.cornerRadius = self.logoImageView.bounds.size.height / 2.0
        self.logoImageView.layer.masksToBounds = true
        self.logoImageView.contentMode = .scaleAspectFill

    }
    
    // MARK: - IBActions
    
    @IBAction func actionStartSession(_ sender: Any) {
        self.delegate?.viewSendActionStartSession(view: self)

    }
    
    
}
