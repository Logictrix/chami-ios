//
//  ChamiConfirmedStudentModel.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

protocol ChamiConfirmedStudentModelDelegate: NSObjectProtocol {
    
}

protocol ChamiConfirmedStudentModelProtocol: NSObjectProtocol {
    
    weak var delegate: ChamiConfirmedStudentModelDelegate? { get set }
    var currentTutor: User! { get set }
    var currentMeetUp: MeetUp! { get set }
    var currentMeetUpSession: MeetUpSession! { get set }

    var currentSession: Session? { get set }
    var isChamiX: Bool { get set }

    var requireConfirmationCode: Bool { get set }
    var confirmationCode: String? { set get }
    
    func getSession(completion: @escaping (_ error:Error?) -> Void)
    func cancelMeetUpRequest(completion: @escaping (_ error:Error?) -> Void)
    func startSession(completion: @escaping (_ error:Error?) -> Void)
}

class ChamiConfirmedStudentModel: NSObject, ChamiConfirmedStudentModelProtocol {
    
    var requireConfirmationCode = true//(AccountManager.shared.currentRole == .tutor)
    var confirmationCode: String?
    

    weak var delegate: ChamiConfirmedStudentModelDelegate?
    var currentTutor: User!
    var currentMeetUp: MeetUp!
    var currentMeetUpSession: MeetUpSession!
    var currentSession: Session? = Session()
    var isChamiX: Bool = false

    let service: SessionServiceProtocol = SessionService()

    /** Implement ChamiConfirmedStudentModel methods here */
    
    
    // MARK: - Private methods
    
    /** Implement private methods here */
    func getSession(completion: @escaping (_ error:Error?) -> Void) {
        let sessionId = currentMeetUpSession.session_id
        self.service.getSession(withId: sessionId, success: { (session) in
            self.currentSession = session
            completion(nil)
        }) { (error) in
            completion(error)
        }
    }
    
    func cancelMeetUpRequest(completion: @escaping (_ error:Error?) -> Void) {
        SessionManager.shared.executeRequestCancel(session: self.currentMeetUpSession) { (error) in
            completion(error)
        }
    }
    
    internal func startSession(completion: @escaping (_ error:Error?) -> Void) {
        DispatchQueue.global(qos: .userInitiated).async {
            var resultError: Error?
            let downloadGroup = DispatchGroup()
            
            // check for correct CODE
            if self.requireConfirmationCode == true {
                downloadGroup.enter()
                // check for valid code and leave in callback
                // resultError
                self.currentSession = Session()
                self.currentSession?.sessionId = self.currentMeetUpSession.session_id
                self.currentSession?.code = NSNumber(value:Int(self.confirmationCode!)!)
                self.currentSession?.costPerHour = self.currentMeetUpSession.costPerHour
                
                self.service.startSession(session: self.currentSession!, success: { (session) in
                    //self.currentSession = session
                    //self.currentSession?.costPerHour = self.currentMeetUpSession.costPerHour
                    //self.currentMeetUpSession.costPerHour = (self.currentSession?.costPerHour)!
                    downloadGroup.leave()

                }, failure: { (error) in
                    resultError = error
                    downloadGroup.leave()

                })
            }
            downloadGroup.wait()
            
            // send start command
            if resultError == nil {
                downloadGroup.enter()
                SessionManager.shared.executeSessionStart(session: self.currentMeetUpSession, completion: { (error) in
                    resultError = error
                    downloadGroup.leave()
                })
                
            }
            downloadGroup.wait()
            
            
            DispatchQueue.main.async {
                completion(resultError)
            }
        }
    }
}

