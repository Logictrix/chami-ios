//
//  ChamiConfirmedStudentRouter.swift
//  Chami
//
//  Created by Pavel Korinenko on 2/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

class ChamiConfirmedStudentRouter: NSObject {
    
    func navigateToActiveSesionScreen(from vc: UIViewController, withSession session: Session, withMeetUpSession meetupSession: MeetUpSession) {
        
        let nextVC = ActiveSessionBuilder.viewController()
        nextVC.setSession(session: session)
        nextVC.setMeetUpSession(meetUpSession: meetupSession)
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
}
