//
//  UIColor+Common.swift
//  Chami
//
//  Created by Serg Smyk on 15/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    
    func lighterColor() -> UIColor {
        return self.lighterColor(offset: 0.02)
    }
    func lighterColor(offset:CGFloat) -> UIColor {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            fRed = min(fRed + offset, 1.0)
            fGreen = min(fGreen + offset, 1.0)
            fBlue = min(fBlue + offset, 1.0)
            let color = UIColor.init(_colorLiteralRed:Float(fRed), green:Float(fGreen), blue:Float(fBlue), alpha: Float(fAlpha))
            return color
        }
        return self
    }
    func darkerColor() -> UIColor {
        return self.darkerColor(offset:0.2)
    }
    func darkerColor(offset:CGFloat) -> UIColor {
        var fRed : CGFloat = 0
        var fGreen : CGFloat = 0
        var fBlue : CGFloat = 0
        var fAlpha: CGFloat = 0
        if self.getRed(&fRed, green: &fGreen, blue: &fBlue, alpha: &fAlpha) {
            fRed = max(fRed - offset, 0.0)
            fGreen = max(fGreen - offset, 0.0)
            fBlue = min(fBlue - offset, 0.0)
            let color = UIColor.init(_colorLiteralRed:Float(fRed), green:Float(fGreen), blue:Float(fBlue), alpha: Float(fAlpha))
            return color
        }
        return self
    }
}
