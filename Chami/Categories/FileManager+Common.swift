//
//  FileManager+Common.swift
//  Chami
//
//  Created by Serg Smyk on 27/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
extension FileManager {
    func fileSize(path:String) -> NSNumber {
        let fileSize = try! FileManager.default.attributesOfItem(atPath: path)[FileAttributeKey.size] as! NSNumber
        return fileSize
    }
}
