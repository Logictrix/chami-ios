//
//  String+Hashes.swift
//  Stadio
//
//  Created by Igor Markov on 8/8/16.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation

extension String {
    
    func MD5() -> String {
        var digest = Array<UInt8>(repeating:0, count:Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5(self, CC_LONG(self.lengthOfBytes(using: .utf8)), &digest)
        let hexString = digest.map{ String(format: "%02hhx", $0) }.joined()
        return hexString
    }
    
    func SHA256() -> String {
        var digest = Array<UInt8>(repeating:0, count:Int(CC_SHA256_DIGEST_LENGTH))
        CC_SHA256(self, CC_LONG(self.lengthOfBytes(using: .utf8)), &digest)
        let hexString = digest.map{ String(format: "%02hhx", $0) }.joined()
        return hexString
    }
}
