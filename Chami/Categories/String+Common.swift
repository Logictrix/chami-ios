//
//  String+Common.swift
//  Chami
//
//  Created by Serg Smyk on 21/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

enum FilePathType: Int {
    case undefined
    case image
    case video
    //case other
}

extension String {
    func filePathType() -> FilePathType {
        let extentionsImage = ["png", "jpg", "jpeg", "gif", "bmp", "exif"]
        let extentionsVideo = ["mov", "avi", "mp4", "mpeg", "3gp", "wmv", "qt", "flv"]
        
        let extention = (self as NSString).pathExtension.lowercased()
        
        if extentionsImage.contains(extention) {
            return .image
        }
        if extentionsVideo.contains(extention) {
            return .video
        }
        
        return .undefined
    }
    func underline(font:UIFont, color:UIColor) -> NSAttributedString {
        let textPartAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue):font,
                                                                 NSAttributedString.Key.foregroundColor: color,
                                                                 NSAttributedString.Key.underlineStyle: NSUnderlineStyle.single.rawValue]
        
        let textPart = NSAttributedString(string: self, attributes: textPartAttributes)
        
        let resultText = NSMutableAttributedString()
        resultText.append(textPart)
        return resultText
    }
    
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedString.Key.font: font], context: nil)
        
        return boundingBox.height
    }
    
    func isPhoneNumber() -> Bool {
        let phoneNumber = self
        let phoneRegex = "^[ +0-9]{8,17}$"
        let predicate = NSPredicate.init(format: "SELF MATCHES %@", phoneRegex)
        let result = predicate.evaluate(with: phoneNumber)
        return result
    }
    func isEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        let result = emailTest.evaluate(with: self)
        
//        let regex = "[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$"
//        let predicate = NSPredicate.init(format: "SELF MATCHES %@", regex)
//        let result = predicate.evaluate(with: self)
        return result
    }
    
    static func getQueryStringParameter(url: String, param: String) -> String? {
        guard let url = URLComponents(string: url) else { return nil }
        return url.queryItems?.first(where: { $0.name == param })?.value
    }
//    -(BOOL)isPhoneNumber {
//    //    if ([self characterAtIndex:0] != '+') {
//    //        return NO;
//    //    }
//    //    return (self.length > 7 && self.length < 16) ? YES : NO;
//    
//    //    NSString *regex = @"^\\+(?:[0-9] ?){6,14}[0-9]$";
//    //    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", regex];
//    //    return [test evaluateWithObject:candidate];
//    
//    NSString *phoneNumber = self;
//    NSString *phoneRegex = @"^[ +0-9]{6,17}$";//@"[235689][0-9]{6,14}([0-9]{3})?";
//    NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
//    BOOL result = [test evaluateWithObject:phoneNumber];
//    return result;
//    }
    

}

extension NSAttributedString {
    func heightWithConstrainedWidth(width: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.height
    }
    
    func widthWithConstrainedHeight(height: CGFloat) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, context: nil)
        
        return boundingBox.width
    }
    
    class func loadRTFD(from resource: String) -> NSAttributedString? {
        guard let url = Bundle.main.url(forResource: resource, withExtension: "rtfd") else { return nil }
        
        //guard let data = try? Data(contentsOf: url) else { return nil }
        return try? NSAttributedString.init(url: url,
                                            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.rtfd],
                                            documentAttributes: nil)
        
        //        let attributedText = NSAttributedString.init(url: url, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: nil)
        //        //let attributedText = NSAttributedString(fileURL: url, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: nil, error: &error)
        //        return attributedText
        
        //        return try? NSAttributedString(data: data,
        //                                       options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType],
        //                                       documentAttributes: nil)
    }
    class func loadRTF(from resource: String) -> NSAttributedString? {
        guard let url = Bundle.main.url(forResource: resource, withExtension: "rtf") else { return nil }
        
        //guard let data = try? Data(contentsOf: url) else { return nil }
        return try? NSAttributedString.init(url: url,
                                            options: [NSAttributedString.DocumentReadingOptionKey.documentType:NSAttributedString.DocumentType.rtf],
                                            documentAttributes: nil)
        
        //        let attributedText = NSAttributedString.init(url: url, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: nil)
        //        //let attributedText = NSAttributedString(fileURL: url, options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType], documentAttributes: nil, error: &error)
        //        return attributedText
        
        //        return try? NSAttributedString(data: data,
        //                                       options: [NSDocumentTypeDocumentAttribute:NSRTFTextDocumentType],
        //                                       documentAttributes: nil)
    }
}


