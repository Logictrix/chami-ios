//
//  UIView+Extensions.swift
//  Chami
//
//  Created by Igor Markov on 2/3/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

extension UIView {
    
    func pushTransition(duration:CFTimeInterval) {
        let animation:CATransition = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.push
        animation.subtype = CATransitionSubtype.fromRight
        animation.duration = duration
        self.layer.add(animation, forKey: CATransitionType.push.rawValue)
    }
    
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    func roundCorners(corners:UIRectCorner, radius: CGFloat, separatorHeight: CGFloat) {
        var maskBounds = self.bounds
        if (separatorHeight > 0) {
            maskBounds.size.height = maskBounds.size.height - separatorHeight
        }

        let path = UIBezierPath(roundedRect: maskBounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
    
    class func loadFromXib(owner: NSObject? = nil) -> UIView {
        let viewNibName = String(describing: self)
        let nibContent = Bundle.main.loadNibNamed(viewNibName, owner: owner, options: nil)
        guard let view = nibContent?.first as? UIView else {
            fatalError("Nib \(viewNibName) does not contain SetupProfile View as first object")
        }
        
        return view
    }
    
    func makeScreenshot() -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, self.isOpaque, UIScreen.main.scale)
        self.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
    func makeScreenshotFromRect(rect:CGRect) -> UIImage? {
        let image = self.makeScreenshot()
        if let img = image {
            var rect = rect
            rect.origin.x *= img.scale
            rect.origin.y *= img.scale
            rect.size.width *= img.scale
            rect.size.height *= img.scale
            
            let drawImage = img.cgImage!.cropping(to: rect)
            let bimage = UIImage(cgImage: drawImage!)
            return bimage
        }
        return image
    }
}
