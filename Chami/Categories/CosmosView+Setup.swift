//
//  CosmosView+Setup.swift
//  Chami
//
//  Created by Serg Smyk on 01/11/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Cosmos

extension CosmosView {

    func setupPresentation() {
        self.rating = 0.0
        
        // Set the color of a filled star
        self.settings.filledColor = ColorHelper.yellowAppColor()
        
        // Set the border color of an empty star
        self.settings.emptyBorderColor = UIColor.lightGray
        
        // Set the border color of a filled star
        self.settings.filledBorderColor = UIColor.lightGray
    }

}
