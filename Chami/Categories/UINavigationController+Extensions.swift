//
//  UINavigationController+Extensions.swift
//  Chami
//
//  Created by Igor Markov on 1/31/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    
    override open var preferredStatusBarStyle: UIStatusBarStyle {
        return self.topViewController?.preferredStatusBarStyle ?? .lightContent
    }
}
