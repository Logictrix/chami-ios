//
//  StripeApiManager.swift
//  Chami
//
//  Created by Pavel Korinenko on 3/9/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import Stripe
import Alamofire
import PKHUD

class StripeApiManager: NSObject, STPPaymentMethodsViewControllerDelegate, STPAddCardViewControllerDelegate, StripeConnectViewControllerDelegate {
    
    func resetManager() {
        StripeApiManager.shared.currentCustomer = nil
        StripeApiManager.shared.currentTutor = nil
        StripeApiManager.shared.shouldSaveTutor = false
        StripeApiManager.shared.shouldSaveStudent = false
    }
    
    var currentCustomer: STPCustomer?
    var currentTutor: STPCustomer?
    var shouldSaveTutor: Bool = false
    var shouldSaveStudent: Bool = false

    static let shared = StripeApiManager()
    private override init() {
//        super.init()
    }

    private var parentVC: UIViewController?
    private var completionHandler: (Bool, Error?) -> Void = {_,_ in 
        
    }
    
    func showPaymentViewControllerIfNeeded(fromViewController: UIViewController, completion: @escaping (_ saved :Bool, _ error: Error?) -> Void) {
        if self.shouldShowPaymentVC() {
            self.showPaymentViewController(fromViewController: fromViewController, completion: completion)
        }
    }
    
    func showPaymentViewController(fromViewController: UIViewController, completion: @escaping (_ saved :Bool, _ error: Error?) -> Void) {
        self.parentVC = fromViewController
        self.completionHandler = completion
        self.showPaymentVC()
    }
    
    class func compileError(withTextMessage message: String ) -> Error {
        let infoDict = [NSLocalizedDescriptionKey: message]
        
        let error = NSError(domain: "", code: 2021, userInfo: infoDict)
        return (error as Error)
    }

    // MARK: - Payments
    fileprivate func shouldShowPaymentVC() -> Bool {
        var rFlag = true
        let curretnRole = (AccountManager.shared.currentUser?.role)!
        if curretnRole == .student {
            if let _ = AccountManager.shared.currentUser?.stripeKeyStudent {
                rFlag = false
            }
            
        } else if curretnRole == .tutor {
            if let _ = AccountManager.shared.currentUser?.stripeKeyTutor {
                rFlag = false
            }
            //            rFlag = true
            
        } else {
            rFlag = false
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        return rFlag
    }

    fileprivate func showPaymentVC() {
        //self.showPaymentVC_old()
        let curretnRole = (AccountManager.shared.currentUser?.role)!
        if curretnRole == .student {
            self.showPaymentVC_old()
        } else {
            let vc = StripeConnectBuilder.viewController()
            vc.delegate = self
            self.parentVC?.navigationController?.pushViewController(vc, animated: true)
        }
    }
    fileprivate func showPaymentVC_old() {
        StripeApiManager.shared.resetManager()
        
        let theme = STPTheme.default()
        theme.secondaryBackgroundColor = Theme.current.regularColor()
        theme.secondaryForegroundColor = UIColor.lightGray
        theme.accentColor = UIColor.white
        
        let config = STPPaymentConfiguration.shared()
        //config.smsAutofillDisabled = true
        
        let curretnRole = (AccountManager.shared.currentUser?.role)!
        if curretnRole == .student {
            
            let vc = STPPaymentMethodsViewController(configuration: STPPaymentConfiguration.shared(), theme: theme, apiAdapter: StripeApiManager.shared, delegate: self)
            self.parentVC?.navigationController?.pushViewController(vc, animated: true)

        } else if curretnRole == .tutor {
            HUD.show(.progress)
                        
            StripeApiManager.shared.retrieveCustomer { (customer, error) in
                //
                HUD.hide(animated: true)

                let vc = STPAddCardViewController(configuration:config , theme: theme)
                vc.delegate = self
                vc.managedAccountCurrency = "usd"
                
                self.parentVC?.navigationController?.pushViewController(vc, animated: true)
            }
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
        
    }
    
    fileprivate func saveStripeKeyIfNeeded(success: @escaping (Bool, Error?) -> Void) {
        
        let curretnRole = (AccountManager.shared.currentUser?.role)!
        if curretnRole == .student {
            if let stripeKey = StripeApiManager.shared.currentCustomer?.stripeID, StripeApiManager.shared.shouldSaveStudent {
                let profileService = ProfileService()
                
                HUD.show(.progress)
                profileService.updateProfileStripeStudentKey(stripeKey: stripeKey, success: {
                    HUD.hide(animated: true)

                    success(true, nil)
                    
                }, failure: { (error) in
                    HUD.hide(animated: true)
                    success(false, error)
                })
            } else {
                success(false, nil)
            }
            
        } else if curretnRole == .tutor {
            if let stripeKey = StripeApiManager.shared.currentTutor?.stripeID, StripeApiManager.shared.shouldSaveTutor {
                let profileService = ProfileService()
                HUD.show(.progress)
                profileService.updateProfileStripeTutorKey(stripeKey: stripeKey, success: {
                    HUD.hide(animated: true)
                    success(true, nil)
                    
                }, failure: { (error) in
                    HUD.hide(animated: true)
                    success(false, error)
                })
                
            } else {
                success(false, nil)
            }
            
        } else {
            fatalError("\(String(describing: type(of: self))) require user role to be defined")
        }
    }
    
    // MARK: StripeConnectViewControllerDelegate
    func stripeConnectViewControllerDidRegisterCode(_ stripeConnectViewController: StripeConnectViewControllerType, code: String) {
        
        let profileService = ProfileService()
        HUD.show(.progress)
        profileService.updateProfileStripeConnectAccountKey(stripeKey: code, success: {
            HUD.hide(animated: true)
            self.completionHandler(true, nil)
            let nav = stripeConnectViewController.navigationController
            stripeConnectViewController.navigationController?.popViewController(animated: true)
            
//            if nav != nil {
//                self.showInformAlert(from: nav!)
//            }

            
            
        }, failure: { (error) in
            HUD.hide(animated: true)
            self.completionHandler(false, error)
            stripeConnectViewController.navigationController?.popViewController(animated: true)
        })
        
        
    }
//    fileprivate func showInformAlert(from controller: UIViewController) {
//        guard let user = AccountManager.shared.currentUser else {
//            return;
//        }
//
//        if user.role == .tutor {
//            let text = "If you wish to check your bank account details please visit stripe.com and login with your details"
//            AlertManager.showAlert(withTitle: "", message: text, onController: controller, closeHandler: {
//            })
//        }
//
//    }
    
    
    func stripeConnectViewControllerDidCancel(_ stripeConnectViewController: StripeConnectViewControllerType) {
        stripeConnectViewController.navigationController?.popViewController(animated: true)
    }

    // MARK: STPPaymentMethodsViewControllerDelegate
    func paymentMethodsViewController(_ paymentMethodsViewController: STPPaymentMethodsViewController, didSelect paymentMethod: STPPaymentMethod) {
        
    }
    
    func paymentMethodsViewController(_ paymentMethodsViewController: STPPaymentMethodsViewController, didFailToLoadWithError error: Error) {
        
        AlertManager.showAlert(withTitle: "Something went wrong", message: error.localizedDescription, onController: self.parentVC!, closeHandler: {
            paymentMethodsViewController.dismiss(completion: {
            })
        })
        
        
    }
    
    func paymentMethodsViewControllerDidFinish(_ paymentMethodsViewController: STPPaymentMethodsViewController) {
        
        paymentMethodsViewController.dismiss {
            self.saveStripeKeyIfNeeded(success: { (saved, error) in
                self.completionHandler(saved, error)
            })
            
        }
    }
    
    // MARK: STPAddCardViewControllerDelegate
    func paymentMethodsViewControllerDidCancel(_ paymentMethodsViewController: STPPaymentMethodsViewController) {
        _ = self.parentVC?.navigationController?.popViewController(animated: true)
    }
    func addCardViewControllerDidCancel(_ addCardViewController: STPAddCardViewController) {
//        _ = self.navigationController?.popViewController(animated: true)
        
//        AccountManager.shared.currentUser?.stripeKeyTutor = nil

        _ = self.parentVC?.navigationController?.popViewController(animated: true)
    }
    
    func addCardViewController(_ addCardViewController: STPAddCardViewController, didCreateToken token: STPToken, completion: @escaping (Error?) -> Swift.Void) {
        
        StripeApiManager.shared.attachSource(toCustomer: token) { (error) in
            if let error = error {
                AlertManager.showAlert(withTitle: "", message: error.localizedDescription, onController: self.parentVC!, closeHandler: {
                    //
                    _ = self.parentVC?.navigationController?.popViewController(animated: true)
                })

            } else {
//                _ = self.navigationController?.popViewController(animated: true)
                _ = self.parentVC?.navigationController?.popViewController(animated: true)
                self.saveStripeKeyIfNeeded(success: { (saved, error) in
                    self.completionHandler(saved, error)

                })
            }
        }
        
    }
    
    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////
    var stripeKey: String?
    var stripeOAuthUrl: String?
    
    fileprivate let utility = UtilityService()
    func setupConnectAccountAccess(completion: @escaping (_ error:Error?) -> Void) {
        
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            completion(NSError.init(domain: "User not logged in! Token is mandatory!", code: 400, userInfo: nil))
            return
        }
        utility.getStripeCredentials(authorization: authorization, success: {[weak self] (stripeKey, stripeOAuthUrl) in
            guard let strongSelf = self else { return }
            strongSelf.stripeKey = stripeKey
            strongSelf.stripeOAuthUrl = stripeOAuthUrl
            
            completion(nil)
        }) { (error) in
            completion(error)
        }
    }
    /////////////////////////////////
    /////////////////////////////////
    /////////////////////////////////

}

extension StripeApiManager: STPBackendAPIAdapter {
    
    func retrieveCustomer(_ completion: STPCustomerCompletionBlock?) {
            guard let user = AccountManager.shared.currentUser else {
                completion!(nil, StripeApiManager.compileError(withTextMessage: "user not found"))

                return;
            }
        
            if user.role == .student {
                if let customer = StripeApiManager.shared.currentCustomer {
                    completion!(customer, nil)
                    return;
                }
                
                var methodType: HTTPMethod = .post
                var requestUrl = "https://api.stripe.com/v1/customers"

                if let stripeKey = AccountManager.shared.currentUser?.stripeKeyStudent {
                    methodType = .get
                    requestUrl = "https://api.stripe.com/v1/customers/\(stripeKey)"
                }

//                var parameters = ["email" : ""]
//                if let email = AccountManager.shared.currentUser?.email {
//                    parameters = ["email" : email]
//                }
                
                let token = "Bearer \(AppApiServices.stripeSecretKey)" //"Bearer sk_live_D3YFTGC12DPzPZk8yERaZPhM
                let headers: HTTPHeaders = [
                    "Authorization": token,
                    "Content-Type": "application/x-www-form-urlencoded",
                ]
                
                Alamofire.request(requestUrl, method: methodType, parameters: nil, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in

                    if let JSON = response.result.value {
                        print("JSON: \(JSON)")
                        let customerDeserializer = STPCustomerDeserializer(data: JSON, urlResponse: response.response, error: nil)
                        let customer : STPCustomer? = customerDeserializer.customer
                        StripeApiManager.shared.currentCustomer = customer
                        completion!(customer, customerDeserializer.error)
                        
                    } else {
                        completion!(nil, StripeApiManager.compileError(withTextMessage: "Stripe: can't parse json"))

                    }
                }
            } else if user.role == .tutor {
                if let customer = StripeApiManager.shared.currentTutor {
                    completion!(customer, nil)
                    return;
                }

                var methodType: HTTPMethod = .post
                var requestUrl = "https://api.stripe.com/v1/accounts"
                var parameters = ["managed" : "true"]

//                AccountManager.shared.currentUser?.stripeKeyTutor = "acct_19xHAcJUqLe1w0P0"
                if let stripeKey = AccountManager.shared.currentUser?.stripeKeyTutor {
                    methodType = .get
                    requestUrl = "https://api.stripe.com/v1/accounts/\(stripeKey)"
                    parameters = [:]
                }

                
                let token = "Bearer \(AppApiServices.stripeSecretKey)" //"Bearer sk_live_D3YFTGC12DPzPZk8yERaZPhM
                let headers: HTTPHeaders = [
                    "Authorization": token,
                    "Content-Type": "application/x-www-form-urlencoded",
                    //            "Accept": "application/json"
                ]
//                let tutorAcc: STPBankAccount = STPBankAccount.decodedObject(fromAPIResponse: JSON)
//                StripeApiManager.shared.currentTutor = tutorAcc

                Alamofire.request(requestUrl, method: methodType, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in
                    
                    if let data = response.result.value {
//                        print("JSON: \(JSON)")
                        let customerDeserializer = STPCustomerDeserializer(data: data, urlResponse: response.response, error: nil)
                        let customer : STPCustomer? = customerDeserializer.customer
                        StripeApiManager.shared.currentTutor = customer
                        //TODOwarning  try to parse to card object
//                        STPCard
                        
                        if let data = response.result.value {
                            do {
                                let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! Dictionary<String, AnyObject>
                                print("JSON: \(parsedData)")
                                if let cardsData: NSDictionary = parsedData["external_accounts"] as? NSDictionary {
                                    if let cardsDataArray: NSArray = cardsData.object(forKey: "data") as? NSArray {
                                        var arrayCards: [STPCard] = []
                                        for dictCard1 in cardsDataArray {
                                            if dictCard1 is NSDictionary {
                                                let cardStripe : STPCard = STPCard.decodedObject(fromAPIResponse: dictCard1 as? [AnyHashable : Any])!
                                                arrayCards.append(cardStripe)
                                            }
                                        }
//                                        customer?.sources = arrayCards
                                    }
                                }
                            } catch {
                                completion!(nil, StripeApiManager.compileError(withTextMessage: "Stripe: can't parse json"))
                                
                            }
                        }
                        
                        completion!(customer, customerDeserializer.error)
                        
                    } else {
                        completion!(nil, StripeApiManager.compileError(withTextMessage: "Stripe: can't parse json"))


                    }
                }

            } else {
                completion!(nil, StripeApiManager.compileError(withTextMessage: "user role not defined"))

            }
    }
    
    func attachSource(toCustomer source: STPSourceProtocol, completion: @escaping STPErrorBlock) {
//        
//    }
//    func attachSource(toCustomer source: STPSource, completion: @escaping STPErrorBlock) {
        let token = "Bearer \(AppApiServices.stripeSecretKey)" //"Bearer sk_live_D3YFTGC12DPzPZk8yERaZPhM
        let headers: HTTPHeaders = [
            "Authorization": token,
            "Content-Type": "application/x-www-form-urlencoded",
            //            "Accept": "application/json"
        ]
        let stripeIdToken = source.stripeID //tok_19voRFHVv50S7cefVE4csZkl
        let parameters = ["source" : stripeIdToken]
        
        guard let user = AccountManager.shared.currentUser else {
            completion(StripeApiManager.compileError(withTextMessage: "user not found"))

            return;
        }
        
        if user.role == .student {
            let customerToken: String! = (StripeApiManager.shared.currentCustomer?.stripeID)!
            let strUrl = "https://api.stripe.com/v1/customers/\(customerToken!)/sources"
            
            Alamofire.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in
                
                completion(response.error)
            }

        } else if user.role == .tutor {
            let customerToken: String! = (StripeApiManager.shared.currentTutor?.stripeID)!
            
            let strUrl = "https://api.stripe.com/v1/accounts/\(customerToken!)/external_accounts"
            let parameters = ["external_account" : stripeIdToken]
            
            Alamofire.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in
                
                if let data = response.result.value {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! Dictionary<String, AnyObject>
                        print("JSON: \(parsedData)")
                        if let errorFromStripe: NSDictionary = parsedData["error"] as? NSDictionary {
                            if let errorMessage = errorFromStripe.value(forKey: "message") {
                                
                                completion(StripeApiManager.compileError(withTextMessage: errorMessage as! String))

                            } else {
                                
                                completion(StripeApiManager.compileError(withTextMessage: "Stripe: can't parse json"))

                            }
                        } else {
                            self.shouldSaveTutor = true
                            completion(nil)
                        }
                    } catch {
                        completion(StripeApiManager.compileError(withTextMessage: "Stripe: can't parse json"))

                    }

                } else {
                    completion(response.error)

                }
                
                
                
            }

        } else {
            completion(StripeApiManager.compileError(withTextMessage: "user role not defined"))
            
        }

    }
    
    func selectDefaultCustomerSource(_ source: STPSourceProtocol, completion: @escaping STPErrorBlock) {
     
//    }
//    func selectDefaultCustomerSource(_ source: STPSource, completion: @escaping STPErrorBlock) {
        guard let user = AccountManager.shared.currentUser else {
            
            return;
        }
        
        if user.role == .student {
            let token = "Bearer \(AppApiServices.stripeSecretKey)" //"Bearer sk_live_D3YFTGC12DPzPZk8yERaZPhM
            let headers: HTTPHeaders = [
                "Authorization": token,
                "Content-Type": "application/x-www-form-urlencoded",
                //            "Accept": "application/json"
            ]
            let stripeIdToken = source.stripeID //card_19vpNLHVv50S7cefJbJIGPTP
            let parameters = ["default_source" : stripeIdToken,
                              "description" : "test description"]
            
            
            let customerToken: String! = (StripeApiManager.shared.currentCustomer?.stripeID)! // cus_ADiJExFg2fZXMb
            let strUrl = "https://api.stripe.com/v1/customers/\(customerToken!)"
            
            Alamofire.request(strUrl, method: .post, parameters: parameters, encoding: URLEncoding.httpBody, headers: headers).responseData { (response) in
                
                if let data = response.result.value {
                    do {
                        let parsedData = try JSONSerialization.jsonObject(with: data as Data, options: .allowFragments) as! Dictionary<String, AnyObject>
                        print("JSON: \(parsedData)")
                        self.shouldSaveStudent = true

                    } catch {
                        completion(StripeApiManager.compileError(withTextMessage: "Stripe: can't parse json"))
                        
                    }
                    
                } else {
                    completion(response.error)
                    
                }

//                completion(response.error)
            }
            
        } else if user.role == .tutor {
            completion(nil)
            
        } else {
            completion(StripeApiManager.compileError(withTextMessage: "user role not defined"))
            
        }
        
    }
}
