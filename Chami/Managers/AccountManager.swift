//
//  AccountManager.swift
//  Chami
//
//  Created by Igor Markov on 2/5/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

enum AuthenticationStatus {
    case unauthenticated
    case authenticated
}

enum AccountRole: String {
    case notDefined
    case tutor
    case student

    func opponent() -> AccountRole {
        switch self {
        case .tutor:
            return .student
        case .student:
            return .tutor
        case .notDefined:
            return .notDefined
        }
    }
}

typealias SettingsType = (currentRole: AccountRole, reserved: String) // there is Reserved tuple element becouse single element tuple can not have single element

class AccountManager: NSObject {

    var deviceId: String {
        get {
            let deviceUDIDKey = "deviceUDIDKey"
            var deviceUDID = UserDefaults.standard.string(forKey: deviceUDIDKey)
            if deviceUDID == nil || deviceUDID?.characters.count == 0 {
                deviceUDID = UUID().uuidString
                UserDefaults.standard.set(deviceUDID, forKey: deviceUDIDKey)
                UserDefaults.standard.synchronize()
            }
            return deviceUDID!
        }
    }

    
    static let shared = AccountManager()
    fileprivate override init() {

        currentUser = AccountManager.loadCurrentUser()
        settings = AccountManager.loadSettings()
        if currentUser != nil, settings == nil {
            self.settings = AccountManager.createDefaultSettins()
        }
        super.init()
    }

    var currentUser: Account? {
        didSet {
            saveCurrentUserInfoToUserDefaults()
            
            if self.currentUser != nil {
                if self.currentUser?.isOnline == true {
                    NotificationsManager.shared.setNotificationEnabled(enabled: true)
                }
            } else {
                NotificationsManager.shared.setNotificationEnabled(enabled: false)
            }
        }
    }

    func saveCurrentUser() {

        saveCurrentUserInfoToUserDefaults()
    }

    func loggedUserIn() {

        //self.settings = AccountManager.createDefaultSettins()
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.userDidChange), object: self)
    }

    fileprivate let accountService = AccountService()
    func loggedUserOut() {
        self.accountService.logoutUser { (error) in
            
        }
        
        self.settings = nil
        self.currentUser = nil
        NotificationCenter.default.post(name: Foundation.Notification.Name(rawValue: Notification.userDidChange), object: self)
    }

    func hasAuthenticationStatus() -> AuthenticationStatus {

        guard let currentUser = currentUser else {
            return .unauthenticated
        }

        if !currentUser.isAuthorized() {
            return .unauthenticated
        }

        return .authenticated
    }
    
    func updateTheme(to role: AccountRole? = nil) {
        
        
        var usingRole = self.currentRole
        if let role = role {
            usingRole = role
        }
        
        switch usingRole {
        case .student:
            Theme.change(to: .student)
        case .tutor:
            Theme.change(to: .tutor)
        default:
            Theme.change(to: .common)
        }
        
        Theme.setupAppearance()
    }

    // MARK: - Serialization of current user

    fileprivate func saveCurrentUserInfoToUserDefaults() {

        if let currentUser = self.currentUser {
            let encodedUser = NSKeyedArchiver.archivedData(withRootObject: currentUser)
            UserDefaults.standard.set(encodedUser, forKey: AccountManager.currentUserKey)
        } else {
            UserDefaults.standard.removeObject(forKey: AccountManager.currentUserKey)
        }

        UserDefaults.standard.synchronize()
    }

    fileprivate static func loadCurrentUser() -> Account? {

        var loadedCurrentUser: Account? = nil

        let encodedUser = UserDefaults.standard.object(forKey: AccountManager.currentUserKey)
        if let encodedUser = encodedUser as? Data {
            let user = NSKeyedUnarchiver.unarchiveObject(with: encodedUser)
            if let user = user as? Account {
                loadedCurrentUser = user
            }
        }

        print("\(#function) loadedCurrentUser: \(String(describing: loadedCurrentUser))")
        return loadedCurrentUser
    }

    fileprivate static let currentUserKey = "currentUserKey"

    // MARK: - Settings

    var settings: SettingsType? {
        didSet {
            saveSettingsToUserDefaults()
        }
    }
    
    var currentRole: AccountRole {
        get {
            if let settings = self.settings {
                return settings.currentRole
            } else {
                return .notDefined
            }
        }
        
        set {
            guard var settings = self.settings else {
                var newSettings = AccountManager.createDefaultSettins()
                newSettings.currentRole = newValue
                self.settings = newSettings
                return
            }
            
            settings.currentRole = newValue
            self.settings = settings
        }
    }

    // MARK: - Serialization of settings

    fileprivate static func createDefaultSettins() -> SettingsType {

        return SettingsType(currentRole: .notDefined, reserved: "")
    }

    fileprivate static func loadSettings() -> SettingsType? {

        var settings: SettingsType? = nil

        let userDefaults = UserDefaults.standard
        guard let currentRoleString = userDefaults.object(forKey: currentRoleKey) as? String,
            let currentRole = AccountRole(rawValue: currentRoleString) else {
                return nil
        }

        settings = SettingsType(currentRole: currentRole, reserved: "")

        print("\(#function) settings: \(String(describing: settings))")
        return settings
    }

    fileprivate func saveSettingsToUserDefaults() {

        let userDefaults = UserDefaults.standard
        if let settings = self.settings {
            userDefaults.set(settings.currentRole.rawValue, forKey: AccountManager.currentRoleKey)
        } else {
            userDefaults.removeObject(forKey: AccountManager.currentRoleKey)
        }

        UserDefaults.standard.synchronize()
    }

    
    fileprivate static let currentRoleKey = "currentRoleKey"
}
