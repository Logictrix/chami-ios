//
//  DataStorageManager.swift
//  Chami
//
//  Created by Serg Smyk on 23/02/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import AWSS3


class DataStorageManager: NSObject {
    
    static let shared = DataStorageManager()

    var transferManager:AWSS3TransferManager!
//    var transferManager:AWSS3TransferManager {
//        get {
//            //static var manager:AWSS3TransferManager?
//            
//            //AWSServiceManager defaultServiceManager].defaultServiceConfiguration
//            return AWSS3TransferManager.default()
//        }
//    }
    
    fileprivate var folderName: String!
    fileprivate var access: String!
    fileprivate var secret: String!
    fileprivate var regionName: String!
    var regionType: AWSRegionType {
        get {
            switch self.regionName {
            case "us-east-1": return .USEast1
            case "us-west-1": return .USWest1
            case "us-west-2": return .USWest2
            case "eu-west-1": return .EUWest1
            case "eu-central-1": return .EUCentral1
            case "ap-northeast-1": return .APNortheast1
            case "ap-northeast-2": return .APNortheast2
            case "ap-southeast-1": return .APSoutheast1
            case "ap-southeast-2": return .APSoutheast2
            case "sa-east-1": return .SAEast1
            case "cn-north-1": return .CNNorth1
            case "us-gov-west-1": return .USGovWest1
            case "eu-west-1": return .EUWest1
            case "eu-west-2": return .EUWest2
            default: return .Unknown
            }
        }
    }
    var isSetup: Bool {
        get {
            guard let folderName = self.folderName else {
                return false
            }
            guard let access = self.access else {
                return false
            }
            guard let secret = self.secret else {
                return false
            }
            guard let region = self.regionName else {
                return false
            }
            return (folderName.characters.count > 0 && access.characters.count > 0 && secret.characters.count > 0 && region.characters.count > 0)
        }
    }
    
    override init() {
        AWSLogger.default().logLevel = .verbose
        super.init()
        //self.setup(folder: "chami-media", access: "AKIAIIMPT5NL64KMNQUA", secret: "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", region:"eu-west-2")
    }
    
    fileprivate let utility = UtilityService()
    func setupAccess(completion: @escaping (_ error:Error?) -> Void) {
        
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            completion(ErrorHelper.compileError(withTextMessage: "User not logged in! Token is mandatory!"))
            return
        }
        utility.getS3credentials(authorization:authorization, success: {[weak self] (name, key, secret, point) in
            guard let strongSelf = self else { return }
            strongSelf.setup(folder: name, access: key, secret: secret, region: point)
            
            completion(nil)
        }) { (error) in
            completion(error)
        }
    }
        
    func setup(folder:String, access:String, secret:String, region:String) {
        self.folderName = folder
        self.access = access
        self.secret = secret
        self.regionName = region
        
        //let credentialsProvider = AWSStaticCredentialsProvider.name
        let credentialsProvider = AWSStaticCredentialsProvider.init(accessKey: self.access, secretKey: self.secret)
        let configuration = AWSServiceConfiguration.init(region: self.regionType, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = configuration
        self.transferManager = AWSS3TransferManager.default()
    }
    
    func download(filename:String, completion: @escaping (_ error:NSError?, _ filepath:String?) -> Void) {
        
        if self.isSetup == false {
            self.setupAccess(completion: {[weak self] (error) in
                if error != nil {
                    completion(error as NSError?, nil)
                    return
                }
                guard let strongSelf = self else { return }
                strongSelf.downloadForce(filename: filename, completion: completion)
            })
        } else {
            self.downloadForce(filename: filename, completion: completion)
        }
    }
    fileprivate func downloadForce(filename:String, completion: @escaping (_ error:NSError?, _ filepath:String?) -> Void) {
        let downloadRequest = AWSS3TransferManagerDownloadRequest()
        guard let request = downloadRequest else {
            completion(ErrorHelper.compileError(withTextMessage: "Can't download data") as NSError, nil)
            return
        }
        request.bucket = self.folderName
        request.key = filename
        request.downloadingFileURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(filename)
        
        //
        
        self.transferManager.download(request).continueWith(executor: AWSExecutor.mainThread(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error downloading: \(String(describing: request.key)) Error: \(error)")
                    }
                } else {
                    print("Error downloading: \(String(describing: request.key)) Error: \(error)")
                }
                completion(error, nil)
                return nil
            }
            print("Download complete for: \(String(describing: request.key))")
            if task.result != nil {
                completion(nil, request.downloadingFileURL.path)
            } else {
                completion(ErrorHelper.compileError(withTextMessage: "Bad server response") as NSError, nil)
            }
            return nil
        })
    }
    
    func upload(data:Data, completion: @escaping (_ error:Error?, _ filename:String?) -> Void) {
        let tempFilename = UUID().uuidString
        let tempPathURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(tempFilename)
        do {
            try data.write(to: tempPathURL, options: .atomic)
        } catch {
            completion(error, nil)
            return
        }
        self.upload(filepath: tempPathURL.path) { (error, filename) in
            do {
                try FileManager.default.removeItem(at: tempPathURL)
            } catch {
                completion(error, filename)
                return
            }
            
            completion(error, filename)
        }
    }
    
    func upload(filepath:String, completion: @escaping (_ error:Error?, _ filename:String?) -> Void) {
        if self.isSetup == false {
            self.setupAccess(completion: {[weak self] (error) in
                if error != nil {
                    completion(error, nil)
                    return
                }
                guard let strongSelf = self else { return }
                strongSelf.uploadForce(filepath: filepath, completion: completion)
            })
        } else {
            self.uploadForce(filepath: filepath, completion: completion)
        }
    }
    
    func uploadForce(filepath:String, completion: @escaping (_ error:Error?, _ filename:String?) -> Void) {
        
        let uploadRequest = AWSS3TransferManagerUploadRequest()
        guard let request = uploadRequest else {
            completion(ErrorHelper.compileError(withTextMessage: "Can't upload data") as NSError, nil)
            return
        }
        request.bucket = self.folderName
        request.key = UUID().uuidString
        request.body = URL(fileURLWithPath: filepath)
        request.acl = .publicRead
        
        if filepath.filePathType() == .image {
            request.contentType = "image/png"
        } else if filepath.filePathType() == .video {
            request.contentType = "video/mp4"
        }
        
        transferManager.upload(request).continueWith(executor: AWSExecutor.default(), block: { (task:AWSTask<AnyObject>) -> Any? in
            
            if let error = task.error as NSError? {
                if error.domain == AWSS3TransferManagerErrorDomain, let code = AWSS3TransferManagerErrorType(rawValue: error.code) {
                    switch code {
                    case .cancelled, .paused:
                        break
                    default:
                        print("Error uploading: \(String(describing: request.key)) Error: \(error)")
                    }
                } else {
                    print("Error uploading: \(String(describing: request.key)) Error: \(error)")
                }
            }
            
            let uploadOutput = task.result
            print("Upload complete for: \(String(describing: request.key))")
            print("Rsult: \(String(describing: uploadOutput))")
            let url = String.init(format: "https://s3.%@.amazonaws.com/%@/%@", self.regionName, request.bucket!, request.key!)
            completion(task.error, url)
            return nil
        })
    }
}
