//
//  WebSocketsManager.swift
//  Chami
//
//  Created by Serg Smyk on 02/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import SocketIO
import CoreLocation

protocol WebSocketsManagerDelegate: class, NSObjectProtocol {
    func didReceivedMessage(command:String, info: NSDictionary)
}
//enum WebSocketsNotification {
//    static let meetupRequestReceived = "meetupRequestReceived"
//    static let userDidUpdate = "userDidUpdateNotification"
//}

class WebSocketsManager: NSObject {
    
    static let shared = WebSocketsManager()
    
    
    fileprivate var socket: SocketIOClient?
    
    let multicastDelegate = MulticastDelegate<WebSocketsManagerDelegate>()
    
    var currentUserId: String?
    var socketHost: String?
    var socketPort: String?
    var socketHostAddress: String {
        get {
            var address = "" //"http://52.56.126.208:9988"//"http://192.168.3.133:3021"
            if let host = self.socketHost {
                address = host
                if let port = self.socketPort {
                    address = address.appending(":")
                    address = address.appending(port)
                }
            }
            //address = address.appending("?EIO=3&transport=websocket")
            return address
        }
    }
    var isSetup: Bool {
        get {
            guard let host = self.socketHost, host.characters.count > 0 else {
                return false
            }
            guard let port = self.socketPort, port.characters.count > 0 else {
                return false
            }
            return true
        }
    }

    
    
    fileprivate let utility = UtilityService()
    func setupAccess(completion: @escaping (_ error:Error?) -> Void) {
        
        guard let authorization = AccountManager.shared.currentUser?.authorization else {
            completion(NSError.init(domain: "User not logged in! Token is mandatory!", code: 400, userInfo: nil))
            return
        }
        utility.getWScredentials(authorization: authorization, success: {[weak self] (socketHost, socketPort) in
            guard let strongSelf = self else { return }
            strongSelf.socketHost = socketHost
            strongSelf.socketPort = socketPort
            
            completion(nil)
        }) { (error) in
            completion(error)
        }
    }
    
    
    
    override init() {
        super.init()
        
        NotificationCenter.default.addObserver(self, selector: #selector(onCurrentUserChanged), name: NSNotification.Name(rawValue: Notification.userDidChange), object: nil)
        self.onCurrentUserChanged(notification: nil)
    }

    @objc fileprivate func onCurrentUserChanged(notification: Foundation.Notification?) {
        if let user = AccountManager.shared.currentUser {
            self.currentUserId = user.id
            self.connect()
        } else {
            self.disconnect()
        }
    }

    
    func disconnect() {
        guard let socket = self.socket else {
            print("Seckets didn't connected")
            return
        }
        socket.disconnect()
    }
    
    func connect() {
//        if TARGET_OS_SIMULATOR == 0 {
//            return
//        }
        
        if self.isSetup == true  {
            self.connectAfterSetup()
        } else {
            self.setupAccess(completion: { (error) in
                if error != nil {
                    print("Error getWScredentials\(String(describing: error))")
                }
                self.connectAfterSetup()
            })
        }
    }
    func connectAfterSetup() {
        print("self.socketHostAddress",self.socketHostAddress)
//        self.socket = SocketIOClient(manager: URL(string: self.socketHostAddress)! as! SocketManagerSpec, nsp: "swift")
        
//        self.socket = SocketIOClient(socketURL: URL(string: self.socketHostAddress)!, config: [.log(true), .forcePolling(false), .forceWebsockets(true)])
        
//        print(URL(string: self.socketHostAddress)! as! SocketManagerSpec)
//
        let url = URL(string: self.socketHostAddress)!
        print("url",url)
        let manager = SocketManager(socketURL: url)
        var defaultNamespaceSocket : SocketIOClient!
       // var socket : SocketIOClient!

        defaultNamespaceSocket = manager.defaultSocket
        socket = manager.socket(forNamespace: "/swift")
        
        guard let socket = self.socket else {
            print("Can't initialize web seckets")
            return
        }
        socket.onAny {
            print("====================: \($0.event), with items: \(String(describing: $0.items))")
            
            if let items = $0.items, let info = items.last as? NSDictionary {
                
                let command: String = $0.event
                self.multicastDelegate.invokeDelegates({ (delegate) in
                    delegate.didReceivedMessage(command: command, info: info)
                })
            }
            
            
        }
        socket.on("error") {data, ack in
            print("Got error")
        }
        socket.on("disconnect") {data, ack in
            print("socket disconnected")
        }
        
        // Connect and register
        //#1
        socket.on("connect") {[weak self] data, ack in
            print("socket connected")
            guard let strongSelf = self else { return }
            guard let currentUserId = strongSelf.currentUserId else {
                print("Please set currentUserId")
                return
            }
            let dic = NSMutableDictionary()
            dic["user_id"] = currentUserId
            strongSelf.socket?.emit("registration", dic)
        }
        
        manager.reconnectWait = 3
     //   socket.manager?.reconnectWait = 3
       // socket.manager?.reconnects = true
        manager.reconnects = true
        
        socket.connect(timeoutAfter: 5) {
          //  socket.reconnect()
            socket.manager?.reconnect()
        }
        
    }
    
    //#2
    func sendCommand(command:String, info: NSDictionary, completion: @escaping (_ error:Error?) -> Void) {
        guard let socket = self.socket else {
            print("Web seckets is not connected")
            return
        }
        socket.emit(command, info)
        completion(nil)
    }

}
