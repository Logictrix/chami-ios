//
//  LocationManager.swift
//  Chami
//
//  Created by Serg Smyk on 15/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import CoreLocation

//#define CLCOORDINATE_EPSILON 0.005f
//#define CLCOORDINATES_EQUAL2( coord1, coord2 )

protocol LocationManagerDelegate {
    func locationManagerDidUpdated(manager:LocationManager)
}

class LocationManager: NSObject, CLLocationManagerDelegate {
    
    static let shared = LocationManager()
    
    let multicastDelegate = MulticastDelegate<LocationManagerDelegate>()
    
    let regionRadius: CLLocationDistance = 1000
//    var latitude:Double! = 66.2367742
//    var longitude:Double! = 60.0065155
    
    var latitude:Double! = 0.0
    var longitude:Double! = 0.0
    
    var shouldSendLocationToServer: Bool! = true
    
    var coordinate: CLLocationCoordinate2D! {
        get {
            return CLLocationCoordinate2DMake(self.latitude, self.longitude)
        }
    }
    
    class func compareCoordinates(coord1: CLLocationCoordinate2D, coord2: CLLocationCoordinate2D) -> Bool {
        let minor = 0.005
        let result = (fabs(coord1.latitude - coord2.latitude) < minor && fabs(coord1.longitude - coord2.longitude) < minor)
        return result
    }
    
    fileprivate let locationManager: CLLocationManager = CLLocationManager()
    func startUpdating() {
        self.stopUpdating()
        self.locationManager.delegate = self
        self.locationManager.distanceFilter = kCLDistanceFilterNone
        self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
    }
    
    func stopUpdating() {
        self.locationManager.stopUpdatingLocation()
    }
    
    
    internal func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
    }
    
    let accountService = AccountService()
    internal func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        guard let location = locations.last else {
            return
        }

        
        if LocationManager.compareCoordinates(coord1: self.coordinate, coord2: location.coordinate) == true {
            return
        }
        
        self.latitude = location.coordinate.latitude
        self.longitude = location.coordinate.longitude
        
        AccountManager.shared.currentUser!.locationLatitude = self.latitude
        AccountManager.shared.currentUser!.locationLongitude = self.longitude

        self.multicastDelegate.invokeDelegates { (delegate) in
            delegate.locationManagerDidUpdated(manager: self)
        }
        
        if self.shouldSendLocationToServer == true {
            self.sendMyLocationToServer()
        }
        
    }
    
    func sendMyLocationToServer() {
        guard let latitude = self.latitude, let longitude = self.longitude else {
            print("location not sent")
            return;
        }
        
        self.accountService.sendDeviceInfo(longitude: String(longitude),
                                           latitude: String(latitude),
                                           apnsToken: NotificationsManager.shared.apnsToken)
        { (error) in
        }
    }
}
