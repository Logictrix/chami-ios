//
//  CurrentSessionManager.swift
//  Chami
//
//  Created by Pavel Korinenko on 3/15/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import MapKit

//typealias FilterStep = (language: Language, location: CLLocationCoordinate2D, chamiLevel: ChamiLevelType)

enum StudentSessionStepType: Int {
    
    case tutorFilter // Map screen (Home)
    case tutorSelect // find tutors screen
    case tutorDefineLocation // request a chami screen
    case tutorWaitConfirmation // current request screen
    case sessionBeforeStart // chami confirmed screen
    case sessionStarted // active session screen
    case sessionFinished // session complete screen
    
    var description: String {
        switch self {
        case .tutorSelect:
            return "Choose specific tutor"
        default:
            return "StudentSessionStepType"
        }
    }
}

enum TutorSessionStepType: Int {
    
    case offlineMode // Map screen (Home)
    case waitRequest // Map screen (Home)
    case answerRequest // find tutors screen
    case waitConfirmation // chami confirmed screen
    case sessionInProgress // active session screen
    case sessionFinished // session complete screen
    
    var description: String {
        switch self {
        case .offlineMode:
            return ""
        default:
            return "TutorSessionStepType"
        }
    }
}

class CurrentSessionManager: NSObject {
    
    static let shared = CurrentSessionManager()
//    var filterStep: FilterStep
    var chamiLevelType: ChamiLevelType = .chamiPlus
    
    
    func resetManager() {
    }
    private override init() {
    }

    var currentStudentStep: StudentSessionStepType = .tutorFilter
    var currentTutorStep: TutorSessionStepType = .offlineMode

    var shouldSaveStudent: Bool = false

}

class Psevdo: NSObject {
    
    var session:Session = Session()
    var user:User = User.init(withId: "56")
    var meetupSession = MeetUpSession()
    var users:[User] {
        get {
            return [self.user]
        }
    }

    
    func createPsevdo() {
                let session = Session()
                session.sessionId = "10"
                session.tutorsFullName = "name1"
                session.startDateTime = Date()
                session.endDateTime = Date().addingTimeInterval(1000)
                session.cost = 10031
                session.isChamiMax = true
        
//                var users: [User] = []
//                users.append(self.user)
        
//                self.navigateToActiveSesionScreen(from: self, withSession: session, withMeetUpSession: meetupSession)
//                self.navigateToChamiConfirmedStudentScreen(from: self, withMeetUp: MeetUp(), withMeetUpSession: meetupSession, withTutor: user, isChamiX: session.isChamiX)
//                self.router?.navigateTutorProfileScreen(from: self, withTutorProfileId: "56", withProfileIdsArray: users, isChamiX: true)
//        
//                self.navigateToSesionCompleteScreen(from: self, withSession: session, withMeetUpSession: meetupSession)
//                self.navigateToChamiConfirmedTutorScreen(from: self, meetupSession: meetupSession)
//                self.navigateToRequestFailedScreen(from: self, isChami: true)

    }
    
    func navigateToRequestFailedScreen(from vc: UIViewController, isChami: Bool) {
        
        let nextVC = RequestFailedBuilder.viewController()
        nextVC.setIsChamiX(isChamiX: isChami)
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func navigateToChamiConfirmedStudentScreen(from vc: UIViewController, withMeetUp meetUp: MeetUp, withMeetUpSession session: MeetUpSession, withTutor tutor: User, isChamiX: Bool) {
        
        let nextVC = ChamiConfirmedStudentBuilder.viewController()
        nextVC.setMeetUp(meetUp: meetUp)
        nextVC.setTutor(tutor: tutor)
        nextVC.setMeetUpSession(meetUpSession: session)
        nextVC.setIsChamiX(isChamiX: isChamiX)
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func navigateToChamiConfirmedTutorScreen(from vc: UIViewController, meetupSession: MeetUpSession) {
        
        let nextVC = ChamiConfirmedTutorBuilder.viewController()
        
        nextVC.setMeetUpSession(meetUpSession: meetupSession)
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func navigateToSesionCompleteScreen(from vc: UIViewController, withSession session: Session, withMeetUpSession meetupSession: MeetUpSession) {
        
        let nextVC = RateSessionBuilder.viewController()
        nextVC.setMeetUpSession(meetUpSession: meetupSession)
        nextVC.setSession(session: session)
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func navigateToActiveSesionScreen(from vc: UIViewController, withSession session: Session, withMeetUpSession meetupSession: MeetUpSession) {
        
        let nextVC = ActiveSessionBuilder.viewController()
        nextVC.setSession(session: session)
        nextVC.setMeetUpSession(meetUpSession: meetupSession)
        
        vc.navigationController?.pushViewController(nextVC, animated: true)
    }
    

}
