//
//  NavigationManager.swift
//  Chami
//
//  Created by Igor Markov on 1/31/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit
import SideMenu

class NavigationManager {

    static let shared = NavigationManager()
    fileprivate init() {}

    var sideMenuNavigationController: UISideMenuNavigationController? = nil

    fileprivate var homeViewController: HomeViewController? = nil
    fileprivate var messagesViewController: MessagesViewController? = nil
    fileprivate var friendsViewController: FriendsViewController? = nil
    fileprivate var sessionHistoryViewController: SessionHistoryViewController? = nil
    fileprivate var languagesViewController: LanguagesViewController? = nil
    fileprivate var shareViewController: ShareViewController? = nil
    fileprivate var freeCreditsViewController: FreeCreditsViewController? = nil
    fileprivate var helpViewController: HelpViewController? = nil
    fileprivate var roleSwitcherViewController: RoleSwitcherViewController? = nil

    func currentVisibleController() -> UIViewController? {
        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return nil
        }
        
        return sideMenuNavigationController.viewControllers.last
    }
    
    func showAuthentication(animated: Bool) {        
        AccountManager.shared.updateTheme(to: .student)
        self.showViewController(viewController: WelcomeBuilder.viewController(), animated: animated)
    }
    
    func showSelectRole(animated: Bool) {
        self.showViewController(viewController: SelectRoleBuilder.viewController(), animated: animated)
    }
    func showSetupProfileScreen(animated: Bool) {
        self.showViewController(viewController: SetupProfileBuilder.viewController(), animated: animated)
    }
    func showEditLanguagesScreen(animated: Bool) {
        self.showViewController(viewController: EditLanguagesBuilder.viewController(), animated: animated)
    }
    
    func showLoginScreen(animated: Bool) {
        self.showViewController(viewController: LoginBuilder.viewController(), animated: animated)
    }
    
    func showViewController(viewController:UIViewController, animated: Bool) {
    
        guard let optWindow = UIApplication.shared.delegate?.window, let window = optWindow else {
            return
        }

        AccountManager.shared.updateTheme(to: AccountManager.shared.currentUser?.role)
        
        let selectRoleNavController = UINavigationController(rootViewController: viewController)
        let oldRootVC = window.rootViewController
        
        let duration = animated ? UI.animationDuration : 0;
        UIView.transition(with: window, duration: duration, options: .curveLinear, animations: {
            oldRootVC?.view.alpha = 0
            window.rootViewController = selectRoleNavController
        }, completion: { (completed) in
            self.destroyMainViewControllers()
        })
    }
    
    
    func showMainScreen(animated: Bool) {
        guard let optWindow = UIApplication.shared.delegate?.window, let window = optWindow else {
            return
        }

        self.destroyMainViewControllers()
        AccountManager.shared.updateTheme()
        
        let homeVC = HomeBuilder.viewController()
        let homeNVC = UINavigationController(rootViewController: homeVC)
        let oldRootVC = window.rootViewController

        let duration = animated ? UI.animationDuration : 0;
        UIView.transition(with: window, duration: duration, options: .curveLinear, animations: {
            oldRootVC?.view.alpha = 0
            window.rootViewController = homeNVC
        }, completion: { (completed) in
            if self.sideMenuNavigationController == nil {
                self.createSideMenuNavigationViewController()
//                SideMenuManager.menuAddPanGestureToPresent(toView: homeNVC.navigationBar)
//                SideMenuManager.menuAddScreenEdgePanGesturesToPresent(toView: homeNVC.view)
            }
        })
    }

    func showHomeScreen(animated: Bool) {
        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }
        
        if self.homeViewController == nil {
            self.homeViewController = HomeBuilder.viewController()
        }
        
        sideMenuNavigationController.pushViewController(self.homeViewController!, animated: animated)
    }
    
    func showMessagesScreen(animated: Bool) {
        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }
        
        if self.messagesViewController == nil {
            self.messagesViewController = MessagesBuilder.viewController()
        }
        
        sideMenuNavigationController.pushViewController(self.messagesViewController!, animated: animated)
    }
    
    func showFriendsScreen(animated: Bool) {
        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }
        
        if self.friendsViewController == nil {
            self.friendsViewController = FriendsBuilder.viewController()
        }
        
        sideMenuNavigationController.pushViewController(self.friendsViewController!, animated: animated)
    }

    func showSessionHistoryScreen(animated: Bool) {

        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }

        if self.sessionHistoryViewController == nil {
            self.sessionHistoryViewController = SessionHistoryBuilder.viewController()
        }

        sideMenuNavigationController.pushViewController(self.sessionHistoryViewController!, animated: animated)
    }

    func showLanguagesScreen(animated: Bool) {

        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }

        if self.languagesViewController == nil {
            self.languagesViewController = LanguagesBuilder.viewController()
        }

        sideMenuNavigationController.pushViewController(self.languagesViewController!, animated: animated)
    }

    func showShareScreen(animated: Bool) {

        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }

        if self.shareViewController == nil {
            self.shareViewController = ShareBuilder.viewController()
        }

        sideMenuNavigationController.pushViewController(self.shareViewController!, animated: animated)
    }

    func showFreeCreditsScreen(animated: Bool) {

        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }

        if self.freeCreditsViewController == nil {
            self.freeCreditsViewController = FreeCreditsBuilder.viewController()
        }

        sideMenuNavigationController.pushViewController(self.freeCreditsViewController!, animated: animated)
    }

    func showHelpScreen(animated: Bool) {

        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }

        if self.helpViewController == nil {
            self.helpViewController = HelpBuilder.viewController()
        }

        sideMenuNavigationController.pushViewController(self.helpViewController!, animated: animated)
    }

    func showSwitchRole(animated: Bool) {
        
        guard let sideMenuNavigationController = self.sideMenuNavigationController else {
            print("\(#function) self.sideMenuNavigationController is nil")
            return
        }
        
        if self.roleSwitcherViewController == nil {
            self.roleSwitcherViewController = RoleSwitcherBuilder.viewController()
        }
        
        sideMenuNavigationController.pushViewController(self.roleSwitcherViewController!, animated: animated)
    }
    
    // MARK: - Private methods

    fileprivate func createSideMenuNavigationViewController() {
        let role = AccountManager.shared.currentRole
        let sideMenuVC = SideMenuBuilder.viewController(withAccountRole: role)
        let sideMenuNavVC = UISideMenuNavigationController(rootViewController: sideMenuVC)
//        sideMenuNavVC.leftSide = true
        SideMenuManager.defaultManager.menuLeftNavigationController = sideMenuNavVC
        SideMenuManager.defaultManager.menuPushStyle = .replace
        SideMenuManager.defaultManager.menuFadeStatusBar = false
        SideMenuManager.defaultManager.menuPresentMode = .viewSlideInOut
        SideMenuManager.defaultManager.menuEnableSwipeGestures = false
        SideMenuManager.defaultManager.menuAnimationBackgroundColor = ColorHelper.regularColor()
        SideMenuManager.defaultManager.menuShadowOpacity = 0.0
        
        
        self.sideMenuNavigationController = sideMenuNavVC
    }

    fileprivate func destroyMainViewControllers() {

        SideMenuManager.defaultManager.menuLeftNavigationController = nil
        self.sideMenuNavigationController = nil
        self.homeViewController = nil
        self.sessionHistoryViewController = nil
        self.languagesViewController = nil
        self.shareViewController = nil
        self.messagesViewController = nil
        self.freeCreditsViewController = nil
        self.helpViewController = nil
        self.roleSwitcherViewController = nil
    }
}
