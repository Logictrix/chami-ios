//
//  NotificationsManager.swift
//  Chami
//
//  Created by Serg Smyk on 04/04/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit
import UserNotifications
import PKHUD

protocol NotificationsManagerProtocol: class, NSObjectProtocol {
    var apnsToken: String? { set get }
    func setNotificationEnabled(enabled: Bool)
    func handleNotificationRegistrationSettings(notificationSettings: UIUserNotificationSettings)
    func handleDeviceTokenRegistration(data: Data?)
    func handleNotification(userInfo: AnyObject?)
    
    func handleAllDeliveredNotifications()
    func setupNewNotificationsCounter()
}

class NotificationsManager: NSObject, NotificationsManagerProtocol, SessionManagerDelegate {

    static let shared = NotificationsManager()
    fileprivate let accountService = AccountService()
    
    override init() {
        super.init()
        SessionManager.shared.multicastDelegate.addDelegate(self)
    }
    
    fileprivate let apnsTokenKey = "NotificationsManager.apnsToken"
    var apnsToken: String? {
        
        set {
            if newValue == nil {
                UserDefaults.standard.removeObject(forKey: apnsTokenKey)
            } else {
                UserDefaults.standard.set(newValue, forKey: apnsTokenKey)
            }
            UserDefaults.standard.synchronize()
        }
        get {
            if let token = UserDefaults.standard.object(forKey: apnsTokenKey) {
                return token as? String
            }
            return nil
        }
    }
    
    
    
    
    func setNotificationEnabled(enabled: Bool) {
        if enabled == true {
            let settings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            
            UIApplication.shared.registerUserNotificationSettings(settings)
        } else {
            UIApplication.shared.unregisterForRemoteNotifications()
        }
    }
    
    func handleNotificationRegistrationSettings(notificationSettings: UIUserNotificationSettings) {
        UIApplication.shared.registerForRemoteNotifications()
    }
    
    func handleDeviceTokenRegistration(data: Data?) {
        
        

        if let deviceToken = data {
            
            let deviceTokenString = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
            //let deviceTokenString = String(data: deviceToken.base64EncodedData(), encoding: .utf8)?.trimmingCharacters(in: CharacterSet.whitespaces).trimmingCharacters(in: CharacterSet(charactersIn: "<>"))
        
//            let token = String(data: tokenData, encoding: String.Encoding.utf8)
//            if let tokenString = token {
//            var deviceTokenString = tokenString
//            deviceTokenString = deviceTokenString.trimmingCharacters(in: CharacterSet.init(charactersIn: "<>"))
//            deviceTokenString = deviceTokenString.replacingOccurrences(of: " ", with: "")
            self.apnsToken = deviceTokenString;
            print("deviceTokenString = \(deviceTokenString)")
            if AccountManager.shared.currentUser != nil {
                self.accountService.sendDeviceInfo(longitude: String(LocationManager.shared.longitude),
                                                   latitude: String(LocationManager.shared.latitude),
                                                   apnsToken: self.apnsToken,
                                                   completion: { (error) in
                                                    if error == nil {
                                                        NSLog("APNS token updated!")
//                                                        guard let optWindow = UIApplication.shared.delegate?.window, let window = optWindow else {
//                                                            return
//                                                        }
//                                                        guard let nav = window.rootViewController as? UINavigationController else {
//                                                            return
//                                                        }
//                                                        AlertManager.showError(withMessage: self.apnsToken!, onController: nav)
                                                    } else {
                                                        NSLog("APNS token NOT updated! \(String(describing: error))")
                                                    }
                })
            }
        } else {
            self.apnsToken = nil;
        }
    }
    func convertJsonToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    func handleNotification(userInfo: AnyObject?) {
        print("handleNotification")
        if UIApplication.shared.applicationState == .active {
            self.parseNotification(userInfo: userInfo)
        } else {
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.parseNotification(userInfo: userInfo)
            }
        }
    }
    fileprivate func parseNotification(userInfo: AnyObject?) {
        if let info = userInfo {
            if (info.isKind(of: NSDictionary.self)) {
                print("handleNotification: \(String(describing: userInfo))")
                
                let messageJSON = info["message"] as! String
                if let messageDic = self.convertJsonToDictionary(text: messageJSON) {
                    if let command = messageDic["actionName"] {
                        //if command == "" {
                            SessionManager.shared.didReceivedMessage(command:command as! String, info: messageDic as NSDictionary)
                            //}
                    }
                }
            }
        }
    }
    
    func handleAllDeliveredNotifications() {
        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().getDeliveredNotifications {[weak self] (notifications) in
//                DispatchQueue.main.async {
//                    guard let strongSelf = self else { return }
//                    for notification in notifications {
//                        let userInfo = notification.request.content.userInfo
//                        
//                        strongSelf.handleNotification(userInfo: userInfo as AnyObject)
//                    }
                    UNUserNotificationCenter.current().removeAllDeliveredNotifications()
//                    
//                }
//            }
        }
        UIApplication.shared.cancelAllLocalNotifications()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    fileprivate var taskIdentifier = UIBackgroundTaskIdentifier.invalid
    func setupNewNotificationsCounter() {
        let application = UIApplication.shared
        application.cancelAllLocalNotifications()
        
        self.taskIdentifier = application.beginBackgroundTask(withName: "apns task") { [weak self] in
            guard let strongSelf = self else { return }
            application.endBackgroundTask(strongSelf.taskIdentifier)
            strongSelf.taskIdentifier = UIBackgroundTaskIdentifier.invalid
        }

    }
    fileprivate var sessionRequestsControllerPopups = NSMutableArray()

    func didReceivedMeetUpRequest(session: MeetUpSession) {
        for popup in self.sessionRequestsControllerPopups {
            guard let controller = popup as? MeetUpRequestViewController else {
                continue
            }
            if controller.model.currentMeetUpSession?.session_id == session.session_id {
                return
            }
        }
        
        guard let optWindow = UIApplication.shared.delegate?.window, let window = optWindow else {
            return
        }
        guard let nav = window.rootViewController as? UINavigationController else {
            return
        }
        guard let topViewController = nav.topViewController else {
            return
        }
        guard topViewController.view.window != nil else {
            return;
        }
        
        //
        guard topViewController.responds(to: #selector(getter: BaseViewController<Any, Any, Any>.canHandleMeetupRequests) as Selector) else {
            return
        }
        let selector = #selector(getter: BaseViewController<Any, Any, Any>.canHandleMeetupRequests) as Selector
        let canHandleMeetupRequests = (topViewController.perform(selector) != nil)
        guard canHandleMeetupRequests == true else {
            return
        }
        //
        
        guard let parentVC = topViewController.view.window?.rootViewController else {
            return
        }
        
        let controller = MeetUpRequestBuilder.viewControllerForSessionRequest(sessionRequest: session)
        parentVC.view.addSubview(controller.view)
        parentVC.addChild(controller)
        self.sessionRequestsControllerPopups.add(controller)
        controller.answerSelectBlock = {[weak self](controller: MeetUpRequestViewController, answer: Bool) in
            guard let strongSelf = self else { return }
            strongSelf.hideMeetUpRequestPopup(controller: controller)
            
            if answer == false {
                controller.model.cancelCurrentSession(success: {
                    
                }, failure: { (error) in
                    
                })
                
            } else {
                strongSelf.hideAllMeetUpRequests()
                HUD.show(.progress)
                //(topViewController as! UIViewControllerProgress).setProgressVisible(visible: true)
                controller.model.startCurrentSession(success: {
                    //guard let strongSelf = self else { return }
                    HUD.hide(animated: true)
                    //(topViewController as! UIViewControllerProgress).setProgressVisible(visible: false)
                    
                    //                    if strongSelf.isKind(of: HomeViewController.self) == false {
                    //                        NavigationManager.shared.showHomeScreen(animated: false)
                    //                    }
                    
                    let nextVC = ChamiConfirmedTutorBuilder.viewController()
                    nextVC.setMeetUpSession(meetUpSession: session)
                    //let topVC = NavigationManager.shared.sideMenuNavigationController?.topViewController
                    topViewController.navigationController?.pushViewController(nextVC, animated: false)
                    
                }) { (error) in
                    //guard let strongSelf = self else { return }
                    HUD.hide(animated: true)
                    //(topViewController as! UIViewControllerProgress).setProgressVisible(visible: false)
                    AlertManager.showWarning(withMessage: ErrorHelper.errorMessageFor(Error: error) as String, onController: topViewController, closeHandler: nil)
                    
                }
            }
        }
        self.layoutSessionRequests()
    }
    
    func layoutSessionRequests() {
        for i in 0...self.sessionRequestsControllerPopups.count - 1
        {
            let controller = self.sessionRequestsControllerPopups[i] as! UIViewController
            guard let superview = controller.view.superview else {
                continue
            }
            //controller.view.frame = CGRect(x: 0, y: superview.frame.size.height - controller.view.frame.size.height, width: superview.frame.size.width, height: controller.view.frame.size.height)
            controller.view.frame = superview.bounds
            
        }
    }
    func hideAllMeetUpRequests() {
        var i = self.sessionRequestsControllerPopups.count - 1
        while i >= 0 {
            let controller = self.sessionRequestsControllerPopups[i] as! MeetUpRequestViewController
            self.hideMeetUpRequestPopup(controller: controller)
            i -= 1
        }
    }
    
    func hideMeetUpRequestPopup(controller: UIViewController) {
        self.sessionRequestsControllerPopups.remove(controller);
        controller.view.removeFromSuperview()
        controller.removeFromParent()
    }
    
    func didReceivedMeetUpRequestCancel(session: MeetUpSession) {
        
        // hide popup
        var i = self.sessionRequestsControllerPopups.count - 1
        while i >= 0 {
            
            let controller = self.sessionRequestsControllerPopups[i] as! MeetUpRequestViewController
            if controller.model.currentMeetUpSession?.session_id == session.session_id {
                self.hideMeetUpRequestPopup(controller: controller)
            }
            i -= 1
        }
        
    }
}

