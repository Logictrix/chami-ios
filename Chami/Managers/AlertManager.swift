//
//  AlertManager.swift
//  Chami
//
//  Created by Igor Markov on 2/13/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

typealias ActionHandlerType = () -> ()

class AlertManager {
    
    class func showAlert(withTitle title: String?, message: String?, onController: UIViewController, closeHandler: ActionHandlerType?) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK alert button"), style: .cancel) { (alertAction) in
            if let closeHandler = closeHandler {
                closeHandler()
            }
        }

        alertController.addAction(okAction)

        onController.present(alertController, animated: true, completion: nil)
    }

    class func showWarning(withMessage message: String, onController: UIViewController, closeHandler: ActionHandlerType? = nil) {

        self .showAlert(withTitle: nil, message: message, onController: onController, closeHandler: closeHandler)
    }

    class func showError(withMessage message: String, onController: UIViewController, closeHandler: ActionHandlerType? = nil) {

        self .showAlert(withTitle: NSLocalizedString("Error", comment: "Error alert title"), message: message, onController: onController, closeHandler: closeHandler)
    }

    class func showQuestion(withTitle title: String?, message: String?, onController: UIViewController, okHandler: ActionHandlerType?, cancelHandler: ActionHandlerType? = nil) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK alert button"), style: .default) { (alertAction) in
            if let okHandler = okHandler {
                okHandler()
            }
        }
        alertController.addAction(okAction)

        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "Cancel alert button"), style: .cancel) { (alertAction) in
            if let cancelHandler = cancelHandler {
                cancelHandler()
            }
        }
        alertController.addAction(cancelAction)

        onController.present(alertController, animated: true, completion: nil)
    }

    class func showDeleteQuestion(withTitle title: String?, message: String?, onController: UIViewController, okHandler: ActionHandlerType? = nil, cancelHandler: ActionHandlerType? = nil) {

        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)

        let okAction = UIAlertAction(title: NSLocalizedString("OK", comment: "OK alert button"), style: .destructive) { (alertAction) in
            if let okHandler = okHandler {
                okHandler()
            }
        }
        alertController.addAction(okAction)

        let noAction = UIAlertAction(title: NSLocalizedString("NO", comment: "NO alert button"), style: .cancel) { (alertAction) in
            if let cancelHandler = cancelHandler {
                cancelHandler()
            }
        }
        alertController.addAction(noAction)

        onController.present(alertController, animated: true, completion: nil)
    }

}
