
//  Theme.swift
//  Chami
//
//  Created by Igor Markov on 2/3/17.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import Foundation
import UIKit

protocol ThemeProtocol {
    func regularColor() -> UIColor
    func oppositeColor() -> UIColor

    func apply(borderedButton button: UIButton)
    func apply(borderedButton button: UIButton, isOpposit: Bool)
    func apply(onlyBorderedButton button: UIButton, isOpposit: Bool)
    
    func defaultUserImage() -> UIImage
}

enum ThemeType {
    case tutor
    case student
    case common
}

class Theme {

    fileprivate static var currentInstance = ThemeImplementation()
    fileprivate init() {}

    static let tutor = TutorThemeImplementation()
    static let student = StudentThemeImplementation()
    static let common = CommonThemeImplementation()
    private(set) static var current = ThemeImplementation()

    class func change(to themeType: ThemeType) {
        switch themeType {
        case .tutor:
            current = tutor
        case .student:
            current = student
        case .common:
            current = common
        }
    }

    class func setupAppearance() {
        let barButtonFont = FontHelper.defaultMediumFont(withSize: UI.labelFontSize)
        let barButtonAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: barButtonFont,
                                                                  NSAttributedString.Key.foregroundColor: UIColor.white]
        UIBarButtonItem.appearance().setTitleTextAttributes(barButtonAttributes, for: .normal)
        
        let navigationBarFont = FontHelper.defaultBoldFont(withSize: 18)
        let navigationBarColor = ColorHelper.lightTextColor()
        let navigationBarAttributes: [NSAttributedString.Key: Any] = [NSAttributedString.Key(rawValue: NSAttributedString.Key.font.rawValue): navigationBarFont,
                                                                      NSAttributedString.Key.foregroundColor: navigationBarColor]
        UINavigationBar.appearance().titleTextAttributes = navigationBarAttributes
        UINavigationBar.appearance().barTintColor = ColorHelper.regularColor()
        UINavigationBar.appearance().tintColor = UIColor.white
        //UIBarButtonItem.appearance().tintColor = UIColor.white
        
        for window in UIApplication.shared.windows {
            for view in window.subviews {
                if view.isKind(of: UITextField.self) {
                    continue
                }
                view.removeFromSuperview()
                window.addSubview(view)
            }
            // update the status bar if you change the appearance of it.
            //window.rootViewController?.setNeedsStatusBarAppearanceUpdate()
        }
        //[NotificationCenter.default post(name: <#T##NSNotification.Name#>, object: <#T##Any?#>)]
    }
}

class ThemeImplementation: ThemeProtocol {

    func regularColor() -> UIColor {
        assert(false, "should be implemented in subclasses")
        return UIColor.red
    }

    func oppositeColor() -> UIColor {
        assert(false, "should be implemented in subclasses")
        return UIColor.red
    }

    func profileBackgroundGradientColor() -> [UIColor] {
        assert(false, "should be implemented in subclasses")
        return []
    }
    
    func apply(borderedButton button: UIButton) {
        self.apply(borderedButton: button, isOpposit: false)
    }
    func apply(onlyBorderedButton button: UIButton, isOpposit: Bool) {
        button.layer.borderWidth = UI.buttonBorderWidth
        button.layer.cornerRadius = UI.cornerRadius
        
        let color = (isOpposit == true) ? self.oppositeColor() : self.regularColor()
        
        button.layer.borderColor = color.cgColor
    }
    func apply(borderedButton button: UIButton, isOpposit: Bool) {
        button.layer.borderWidth = UI.buttonBorderWidth
        button.layer.cornerRadius = UI.cornerRadius
        button.setTitleColor(UIColor.lightGray, for: .highlighted)
        button.titleLabel!.font = FontHelper.defaultMediumFont(withSize: 18)

        let color = (isOpposit == true) ? self.oppositeColor() : self.regularColor()

        button.layer.borderColor = color.cgColor
        button.setTitleColor(color, for: .normal)
    }

    func defaultUserImage() -> UIImage {
        assert(false, "should be implemented in subclasses")
        return UIImage()
    }
    
    func apply(filledButton button: UIButton) {
        button.layer.cornerRadius = UI.cornerRadius
        
        button.backgroundColor = ColorHelper.regularColor()
        button.setTitleColor(ColorHelper.lightTextColor(), for: .normal)
        button.setTitleColor(UIColor.lightGray, for: .highlighted)
        button.titleLabel!.font = FontHelper.defaultMediumFont(withSize: 18)
    }
    
    func apply(borderedButton2 button: UIButton) {
        button.layer.borderColor = ColorHelper.regularColor().cgColor
        button.layer.borderWidth = 1.5
        button.layer.cornerRadius = UI.cornerRadius
        
        button.setTitleColor(ColorHelper.regularColor(), for: .normal)
        button.titleLabel!.font = FontHelper.defaultMediumFont(withSize: 14)
    }
    
    func apply(borderedButton3 button: UIButton) {
        button.layer.borderColor = ColorHelper.lightGrayColor().cgColor
        button.layer.borderWidth = 1.5
        button.layer.cornerRadius = UI.cornerRadius
        
        button.setTitleColor(ColorHelper.regularColor(), for: .normal)
        button.titleLabel!.font = FontHelper.defaultMediumFont(withSize: 14)
    }


}

class TutorThemeImplementation: ThemeImplementation {

    override func regularColor() -> UIColor {
        return RGB(237, 208, 69)
    }

    override func defaultUserImage() -> UIImage {
        return UIImage.init(named: "who_are_you_scr_btn_tutor")!
    }

    override func oppositeColor() -> UIColor {
        return RGB(13, 173, 247)
    }

    override func profileBackgroundGradientColor() -> [UIColor] {
        return [RGB(247, 223, 89), RGB(241, 191, 56)]
    }
}

class StudentThemeImplementation: ThemeImplementation {

    override func regularColor() -> UIColor {
        return RGB(11, 165, 247)
    }

    override func defaultUserImage() -> UIImage {
        return UIImage.init(named: "who_are_you_scr_btn_student")!
    }

    override func oppositeColor() -> UIColor {
        return RGB(237, 208, 69)
    }

    override func profileBackgroundGradientColor() -> [UIColor] {
        return [RGB(27, 229, 243), RGB(13, 173, 247)]
    }
}

class CommonThemeImplementation: ThemeImplementation {
    
    override func regularColor() -> UIColor {
        return RGB(11, 165, 247)
    }
    override func defaultUserImage() -> UIImage {
        return UIImage.init(named: "who_are_you_scr_btn_tutor")!
    }
    override func oppositeColor() -> UIColor {
        return RGB(237, 208, 69)
    }

}
