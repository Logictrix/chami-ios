//
//  SessionManager.swift
//  Chami
//
//  Created by Serg Smyk on 16/03/2017.
//  Copyright © 2017 Chami Apps Limited. All rights reserved.
//

import UIKit

enum SessionManagerCommand: String {
    case cmdMeetupRequest = "request:meetup"
    case cmdMeetupResponse = "response:meetup"
    case cmdMeetupRequestCancel = "cancel:meetup"
    case cmdMeetupStartSession = "start:session"
    case cmdMeetupSessionTick = "tick:session"
    case cmdMeetupEndSession = "end:session"
}


@objc protocol SessionManagerDelegate: class, NSObjectProtocol {
    @objc optional func didReceivedMeetUpRequest(session: MeetUpSession)
    @objc optional func didReceivedMeetUpRequestCancel(session: MeetUpSession)
    @objc optional func didReceivedMeetUpResponse(session: MeetUpSession)
    @objc optional func didReceivedMeetUpStartSession(session: MeetUpSession)
    @objc optional func didReceivedMeetUpSessionTick(session: MeetUpSession)
    @objc optional func didReceivedMeetUpEndSession(session: MeetUpSession)
}

typealias SessionsArray = [MeetUpSession]
class SessionManager: NSObject {
    var appVersion: String {
        get {
            let dictionary = Bundle.main.infoDictionary!
            let version = dictionary["CFBundleShortVersionString"] as! String
            let build = dictionary["CFBundleVersion"] as! String
            return "\(version) build \(build)"
        }
    }
    
    fileprivate var thisVersionKey:String {
        get {
            return "\(self.appVersion) launch counter"
        }
    }
    
    var launchCounter: Int {
        
        set {
            UserDefaults.standard.set(newValue, forKey: self.thisVersionKey)
            UserDefaults.standard.synchronize()
        }
        get {
            let counter = UserDefaults.standard.integer(forKey: self.thisVersionKey)
            return counter
        }
    }
    
    static let shared = SessionManager()
    var sessions: SessionsArray = []
    let multicastDelegate = MulticastDelegate<SessionManagerDelegate>()
    
    //var debugObject: DebugTestObject!
    override init() {
        super.init()
        WebSocketsManager.shared.multicastDelegate.addDelegate(self)
        
        NotificationCenter.default.addObserver(self, selector: #selector(onAppGoToBackground), name: UIApplication.willResignActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(onAppGoFromBackground), name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc fileprivate func onAppGoToBackground() {
        self.saveCurrentSessions()
    }
    @objc fileprivate func onAppGoFromBackground() {
        self.restoreCurrentSession()
    }
    
    func getNewMeetUpSession() -> MeetUpSession {
        let session = MeetUpSession()
        self.sessions.append(session)
        return session;
    }
    func getSessionByInfo(dic:NSDictionary) -> MeetUpSession {
        if let internal_session_id = dic["internal_session_id"] as? IdType {
            for session in self.sessions {
                if session.internal_session_id == internal_session_id {
                    return session
                }
            }
        }
        if let session_id = dic["session_id"] as? IdType {
            for session in self.sessions {
                if session.session_id.characters.count > 0 && session_id.characters.count > 0 && session.session_id == session_id {
                    return session
                }
            }
        }
        return self.getNewMeetUpSession();
    }
    
    func executeRequest(session: MeetUpSession, completion: @escaping (_ error:Error?) -> Void) {
        WebSocketsManager.shared.sendCommand(command: SessionManagerCommand.cmdMeetupRequest.rawValue, info: session.toDictionary) { (error) in
            session.status = .requestSent
            completion(error)
        }
    }
    func executeRequestCancel(session: MeetUpSession, completion: @escaping (_ error:Error?) -> Void) {
        WebSocketsManager.shared.sendCommand(command: SessionManagerCommand.cmdMeetupRequestCancel.rawValue, info: session.toDictionary) { (error) in
            session.status = .requestCanceled
            completion(error)
        }
    }
    func executeResponse(session: MeetUpSession, completion: @escaping (_ error:Error?) -> Void) {
        WebSocketsManager.shared.sendCommand(command: SessionManagerCommand.cmdMeetupResponse.rawValue, info: session.toDictionary) { (error) in
            session.status = .responseSent
            completion(error)
        }
    }
    func executeSessionStart(session: MeetUpSession, completion: @escaping (_ error:Error?) -> Void) {
        WebSocketsManager.shared.sendCommand(command: SessionManagerCommand.cmdMeetupStartSession.rawValue, info: session.toDictionary) { (error) in
            session.status = .started
            completion(error)
        }
    }
    func executeSessionEnd(session: MeetUpSession, completion: @escaping (_ error:Error?) -> Void) {
        WebSocketsManager.shared.sendCommand(command: SessionManagerCommand.cmdMeetupEndSession.rawValue, info: session.toDictionary) { (error) in
            session.status = .finished
            completion(error)
        }
    }
    
    
    fileprivate let CurrentSessionKey = "CurrentSessionKey"
    func saveCurrentSessions() {
        let result = NSMutableSet()
        for session in self.sessions {
            guard session.status == .started else {
                continue
            }
            if let info = session.toDictionary {
                //info["status"] = String(describing: session.status)
                result.add(info)
            }
        }
        if result.count > 0 {
            UserDefaults.standard.set(result.allObjects, forKey: CurrentSessionKey)
        } else {
            UserDefaults.standard.removeObject(forKey: CurrentSessionKey)
        }
        UserDefaults.standard.synchronize()
    }
    
    func restoreCurrentSession() {

        if let infos = UserDefaults.standard.array(forKey: CurrentSessionKey) {
            for infoItem in infos {
                guard let info: NSDictionary = infoItem as? NSDictionary else {
                    continue
                }
                let session = self.getSessionByInfo(dic: info)
                session.updateFromDictionary(dic: info, changePartner: false)
                if session.status != .finished {
                    session.status = .started
                }
//                if let status: String = info["status"] as? String {
//                    session.status = MeetUpSessionStatus(rawValue: uint(status)!)!
//                }
            }
        }
    }
    
    var currentSessions: [MeetUpSession] {
        get {
            let result = NSMutableSet()
            for session in self.sessions {
                if session.status == .started{
                    result.add(session)
                }
            }
            return result.allObjects as! [MeetUpSession]
        }
    }
    
    var currentSession: MeetUpSession? {
        get {
            return self.currentSessions.last
        }
    }

}

extension SessionManager: WebSocketsManagerDelegate {
    func didReceivedMessage(command:String, info: NSDictionary) {
        guard let cmd = SessionManagerCommand.init(rawValue: command) else {
            return
        }
        let session = self.getSessionByInfo(dic: info)

        if let statusRevision = info["statusRevision"] as? NSNumber {
            if statusRevision.intValue < session.statusRevision {
                return
            }
        }
        session.updateFromDictionary(dic: info, changePartner: true)
        

        var selector: Selector?
        switch cmd {
        case .cmdMeetupRequest:
            session.status = .requestReceived
            //1
//            selector = #selector(SessionManagerDelegate.didReceivedMeetUpRequest(session:))
            //2
            var hasActiveSession = false
            for sessionQ in self.sessions {
                if sessionQ.status == .requestSent {
                    hasActiveSession = true
                    break
                }
                if sessionQ.status == .started {
                    hasActiveSession = true
                    break
                }
                if sessionQ.status == .responseSent && sessionQ.isAccepted == true {
                    hasActiveSession = true
                    break
                }
                if sessionQ.status == .responseReceived && sessionQ.isAccepted == true {
                    hasActiveSession = true
                    break
                }
            }
            
            if hasActiveSession == true {
                session.isAccepted = false
                self.executeResponse(session: session) { (error) in
                }
            } else {
                selector = #selector(SessionManagerDelegate.didReceivedMeetUpRequest(session:))
            }
            //
            break
        case .cmdMeetupRequestCancel:
            session.status = .requestCanceled
            selector = #selector(SessionManagerDelegate.didReceivedMeetUpRequestCancel(session:))
            break
        case .cmdMeetupResponse:
            session.status = .responseReceived
            selector = #selector(SessionManagerDelegate.didReceivedMeetUpResponse(session:))
            break
        case .cmdMeetupStartSession:
            session.status = .started
            selector = #selector(SessionManagerDelegate.didReceivedMeetUpStartSession(session:))
            break
        case .cmdMeetupSessionTick:
            session.status = .started
            selector = #selector(SessionManagerDelegate.didReceivedMeetUpSessionTick(session:))
            break
        case .cmdMeetupEndSession:
            session.status = .finished
            selector = #selector(SessionManagerDelegate.didReceivedMeetUpEndSession(session:))
            break
//        default:
//            break
        }
        
        if selector != nil {
            self.multicastDelegate.invokeDelegates { (deledate) in
                
                if deledate.responds(to: selector) {
                    deledate.perform(selector, with: session)
                }
            }
        }
        
    }
}
